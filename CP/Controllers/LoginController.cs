﻿using CP.App_Start;
using DatabaseHelper.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using IdentityManagement.DAL;
using IdentityManagement.Entities;
using IdentityManagement.Utilities;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class LoginController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Login
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            if (Utils.IfUserAuthenticated())
            {
                return RedirectToLocal("");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(LoginInfo objLogin, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userStore = DependencyResolver.Current.GetService<UserStore>();
                ApplicationUser oUser = await SignInManager.UserManager.FindByNameAsync(objLogin.UserName);
                //User oUser = await userStore.FindByNameAsync(objLogin.UserName);
                if (oUser != null && ObjectClassesLibrary.Classes.HashSalt.VerifyPassword(objLogin.Password, oUser.Password, oUser.Salt))
                {
                    switch (oUser.Active)
                    {
                        case "False":
                            ModelState.AddModelError("error", "Error: User account is inactive.");
                            break;
                        case "True":
                            SignInManager.SignIn(oUser, false, false);



                            //IList<string> roleList = UserRoleControl.GetUserRoles(oUser.UserId);
                            //foreach (string role in roleList)
                            //{
                            //    UserManager.AddToRole(oUser.UserId, role);
                            //}
                            Session["userId"] = oUser.UserId;
                            Session["position"] = oUser.Position;
                            Session["name"] = oUser.UserName;
                            Session["profileId"] = oUser.ProfileId;

                            return RedirectToAction("Index", "Home");
                    }

                }
                //if (oUser.Salt == null)
                //{
                //    HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, objLogin.Password);
                //}
                //if (oUser != null && objLogin.Password == oUser.Password)
                //{
                //    Session["userId"] = oUser.UserId;
                //    Session["position"] = oUser.Position;
                //    Session["name"] = oUser.Name;
                //    Session["profileId"] = oUser.ProfileId;

                //    return RedirectToAction("Index", "Home");
                //}
                else
                {
                    ModelState.AddModelError("error", "Error: Invalid login details.");
                }
            }
            return View(objLogin);
        }
        public ActionResult Logout()
        {
            Session["userId"] = null;
            Session["position"] = null;
            Session["name"] = null;
            Session["profileid"] = null;
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Login");
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}