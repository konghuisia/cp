﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class HomeController : Controller
    {
        RequestStore RequestStore;
        LeadStore LeadStore;
        AppointmentStore AppointmentStore;
        UserStore UserStore;
        AppointmentTypeStore AppointmentTypeStore;
        DraftStore DraftStore;
        ProfileStore ProfileStore;
        UserAccessStore UserAccessStore;
        SourceStore SourceStore;
        DebtorStore DebtorStore;
        AuditTrailStore AuditTrailStore;
        SubscriptionStore SubscriptionStore;

        public HomeController()
        {
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            LeadStore = DependencyResolver.Current.GetService<LeadStore>();
            AppointmentStore = DependencyResolver.Current.GetService<AppointmentStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            AppointmentTypeStore = DependencyResolver.Current.GetService<AppointmentTypeStore>();
            DraftStore = DependencyResolver.Current.GetService<DraftStore>();
            ProfileStore = DependencyResolver.Current.GetService<ProfileStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            SourceStore = DependencyResolver.Current.GetService<SourceStore>();
            DebtorStore = DependencyResolver.Current.GetService<DebtorStore>();
            AuditTrailStore = DependencyResolver.Current.GetService<AuditTrailStore>();
            SubscriptionStore = DependencyResolver.Current.GetService<SubscriptionStore>();
        }
        public async Task<ActionResult> Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var vm = new Models.HomePageViewModel();
            vm.Module = "Home";

            return View(vm);
        }
        public async Task<ActionResult> _Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.LeadIndexViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            
            return PartialView(vm);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public async Task<ActionResult> _Choice()
        {
            var vm = new Models.HomePageViewModel();
            vm.Position = Utils.GetUserGroup();
            vm.UserId = Utils.GetUserId();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(vm.Position);
            
            return PartialView(vm);
        }
        public async Task<ActionResult> _Dashboard()
        {
            var vm = new Models.HomePageViewModel();
            vm.Position = Utils.GetUserGroup();
            vm.UserId = Utils.GetUserId();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(vm.Position);

            var pid = ""; var p = vm.Position; var id = vm.UserId;

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            vm.Users = await UserStore.GetAllUser();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Leads = await LeadStore.GetAllLead();
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.RMO = vm.Users.FindAll(x => x.Position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487");
            vm.Debtors = await DebtorStore.GetAllDebtor();
            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);

            vm.Requests.RemoveAll(x => x.Status == null);
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.RMO.RemoveAll(x => x.UserId != id);
                vm.Leads.RemoveAll(x => x.AssignTo != id);
            }
            else if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                vm.Requests.RemoveAll(x => x.Approval1User == null && x.Approval2User == null);
            }
            else if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                vm.Requests.RemoveAll(x => x.LawFirm != pid);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")
            {
                vm.Requests.RemoveAll(x => x.Stage < 27);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Leads.RemoveAll(x => x.AssignTo == null);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }
            vm.Profiles = vm.Profiles.Where(x => vm.Sources.Any(z => z.SourceId == x.Source)).ToList();
            vm.Requests = vm.Requests.Where(x => vm.Profiles.Any(z => z.ProfileId == x.ProfileId)).ToList();
            vm.Requests = vm.Requests.Where(x => vm.Leads.Any(z => z.RequestId == x.RequestId)).ToList();

            return PartialView(vm);
        }
        public async Task<ActionResult> MyTask()
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.MyTaskViewModel();
            List<SubscriptionStatus> SubStatus = await SubscriptionStore.GetSubscriptionStatus();
            List<SubscriptionRequest> SubscriptionRequest = await SubscriptionStore.GetAllSubscriptionRequests();
            vm.Drafts = await DraftStore.GetDraftByCreatedById(id);
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Leads = await LeadStore.GetAllLead();
            //vm.SubscriptionRequest = await SubscriptionStore.GetSubscriptionRequests();
            vm.Users = await UserStore.GetAllUser();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.AuditTrails = await AuditTrailStore.GetAllAuditTrail();
            vm.UserId = id;
            
            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);

            vm.Requests.RemoveAll(x => x.Status == null);

            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Leads.RemoveAll(x => x.AssignTo != id);
                vm.Requests.RemoveAll(x => x.Stage < 1);
                SubscriptionRequest.RemoveAll(x => x.CreatedBy != id);
            }
            else if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                vm.Requests.RemoveAll(x => x.Approval1User == null && x.Approval2User == null);
            }
            else if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                vm.Requests.RemoveAll(x => x.LawFirm != pid);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")
            {
                vm.Requests.RemoveAll(x => x.Stage < 27);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Requests.RemoveAll(x => x.Stage < 1);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }

            //add subscription request into request
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487" || p == "A4C2EBBE-9391-44AF-BD49-572BBC9EB22E" || p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                foreach (var sr in SubscriptionRequest)
                {
                    var r = new Request();
                    r.Stage = sr.Stage;
                    //r.RequestId = vm.Leads[0].RequestId;
                    r.ProfileId = sr.RequesterId;
                    r.CreatedAt = sr.RequestDate;
                    r.UpdatedAt = sr.UpdatedAt;
                    r.Status = SubStatus.Find(x => x.Stage == sr.Stage).Name;
                    r.id = sr.id;
                    r.Type = "Subscription";
                    r.SubscriptionRequestId = sr.RequestId;
                    r.Approval1UserAction = sr.Approval1UserAction;
                    r.Approval2UserAction = sr.Approval2UserAction;
                    vm.Requests.Add(r);
                }
            }

            foreach (var v in vm.Requests)
            {
                v.AccessStatus = new ObjectClassesLibrary.Classes.LeadUserHasAccess().checkAccess(p, v.Stage, id);
                v.BoxStatus = new ObjectClassesLibrary.Classes.BoxStatus().CheckBoxStatus(p, v.Stage, id, v.Approval1UserAction);
            }

            return View(vm);
        }
        public async Task<ActionResult> Calendar()
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.CalendarViewModel();
            vm.AppointmentTypes = await AppointmentTypeStore.GetAllAppointmentType();
            if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5" || p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.AppointmentTypes.RemoveAll(x => x.Name.Substring(0, 3) == "Pre");
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1" || p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.AppointmentTypes.RemoveAll(x => x.Name.Substring(0, 3) != "Pre");
            }
            else
            {
                vm.AppointmentTypes = new List<AppointmentType>();
            }

            return View(vm);
        }
        public async Task<JsonResult> GetCalendar(string type, string date)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();

            var vm = new Models.CalendarViewModel();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Leads = await LeadStore.GetAllLead();
            vm.Appointments = await AppointmentStore.GetAllAppointment();

            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Leads.RemoveAll(x => x.AssignTo != id);
            }
            vm.Requests.RemoveAll(x => !vm.Leads.Exists(b => x.RequestId == b.RequestId));
            vm.Appointments.RemoveAll(x => !vm.Requests.Exists(b => x.RequestId == b.RequestId) || x.RequestId == null && x.Date == null);

            foreach(var v in vm.Appointments)
            {
                if(v.AppointmentType == "3E653BCE-AE1B-46C4-840D-A75A5B4EDF15" || v.AppointmentType == "Pre-Engagement")
                {
                    v.Color = "DeepSkyBlue";
                }
                else if(v.AppointmentType == "95CBF154-443F-4E49-B33F-F647E9BE5B1B" || v.AppointmentType == "Appointment 2")
                {
                    v.Color = "Orange";
                }
                else if (v.AppointmentType == "8307A31F-33C7-44F9-A498-93387E4E918A" || v.AppointmentType == "Appointment 3")
                {
                    v.Color = "Green";
                }
                else if (v.AppointmentType == "7F240754-470A-4EDE-AD18-C2B63CA56271" || v.AppointmentType == "Appointment 4")
                {
                    v.Color = "MediumSlateBlue";
                }
                if (!string.IsNullOrEmpty(date))
                {

                    string date2 = date;
                    if (date.Length > 13)
                    {
                        date = date.Substring(0, 6) + " " + date.Substring(date.Length - 4);
                        v.calendarView = "agendaWeek";
                    }

                    else if (date.Length <= 9)
                    {
                        v.calendarView = "month";
                    }

                    else if (date.Length <= 12)
                    {
                        v.calendarView = "agendaDay";
                    }

                    v.calendarDate = DateTime.Parse(date);
                    date = date2;
                }
                else
                {
                    v.calendarView = "month";
                }
            }

            if (type == "Orange")
            {
                vm.Appointments.RemoveAll(x => x.AppointmentType != "95CBF154-443F-4E49-B33F-F647E9BE5B1B");
            }
            else if (type == "DeepSkyBlue")
            {
                vm.Appointments.RemoveAll(x => x.AppointmentType != "3E653BCE-AE1B-46C4-840D-A75A5B4EDF15");
            }
            else if (type == "Green")
            {
                vm.Appointments.RemoveAll(x => x.AppointmentType != "8307A31F-33C7-44F9-A498-93387E4E918A");
            }
            else if (type == "MediumSlateBlue")
            {
                vm.Appointments.RemoveAll(x => x.AppointmentType != "7F240754-470A-4EDE-AD18-C2B63CA56271");
            }


            if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5" || p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Appointments.RemoveAll(x => x.AppointmentType != "8307A31F-33C7-44F9-A498-93387E4E918A" && x.AppointmentType != "7F240754-470A-4EDE-AD18-C2B63CA56271");
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1" || p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Appointments.RemoveAll(x => x.AppointmentType != "3E653BCE-AE1B-46C4-840D-A75A5B4EDF15" && x.AppointmentType != "95CBF154-443F-4E49-B33F-F647E9BE5B1B");
            }
            else
            {
                vm.Appointments = new List<Appointment>();
            }

            var events = vm.Appointments;
            return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public JsonResult SendEmailInvite(Email emailModel, List<string> recipients, string DateStart)
        {
            var r = new Response();
            try
            {
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                MailMessage mailMessage = new MailMessage();

                smtpClient.Credentials = new NetworkCredential
                {
                    UserName = "corporatepartnermail@gmail.com",
                    Password = "CP##2019"
                };

                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                mailMessage.From = new MailAddress("corporatepartnermail@gmail.com");
                foreach (var recipient in recipients)
                {
                    mailMessage.To.Add(recipient);
                }
                mailMessage.Subject = emailModel.Subject;
                mailMessage.Body = emailModel.Description;

                StringBuilder sb = new StringBuilder();
                string DateFormat = "yyyyMMddTHHmmssZ";
                string now = DateTime.Now.ToUniversalTime().ToString(DateFormat);
                sb.AppendLine("BEGIN:VCALENDAR");
                sb.AppendLine("PRODID:-//Compnay Inc//Product Application//EN");
                sb.AppendLine("VERSION:2.0");
                sb.AppendLine("METHOD:REQUEST");

                DateTime dtStart = Convert.ToDateTime(emailModel.DateStart);
                DateTime dtEnd = Convert.ToDateTime(emailModel.DateEnd);
                sb.AppendLine("BEGIN:VEVENT");
                sb.AppendLine("DTSTART:" + dtStart.ToUniversalTime().ToString(DateFormat));
                sb.AppendLine("DTEND:" + dtEnd.ToUniversalTime().ToString(DateFormat));
                sb.AppendLine("DTSTAMP:" + now);
                sb.AppendLine("UID:" + Guid.NewGuid());
                sb.AppendLine("ORGANIZER;CN=" + mailMessage.From + ":" + mailMessage.From + "");
                foreach (var recipient in recipients)
                {
                    sb.AppendLine("ATTENDEE;CN=" + recipient + ";CUTYPE=INDIVIDUAL;PARTSTAT=ACCEPTED;ROLE=REQ-PARTICIPANT;RSVP=TRUE:" + recipient + "");
                }
                sb.AppendLine("CREATED:" + now);
                sb.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + emailModel.Description);
                //sb.AppendLine("DESCRIPTION:" + res.Details);
                sb.AppendLine("LAST-MODIFIED:" + now);
                sb.AppendLine("LOCATION:" + emailModel.Location);
                sb.AppendLine("SEQUENCE:0");
                sb.AppendLine("SUMMARY:" + emailModel.Subject);
                sb.AppendLine("STATUS:CONFIRMED");
                //sb.AppendLine("SUMMARY:" + res.Summary);
                sb.AppendLine("TRANSP:OPAQUE");
                sb.AppendLine("END:VEVENT");

                sb.AppendLine("END:VCALENDAR");
                var calendarBytes = Encoding.UTF8.GetBytes(sb.ToString());
                MemoryStream stream = new MemoryStream(calendarBytes);
                Attachment icsAttachment = new Attachment(stream, "invitation.ics", "text/calendar");

                mailMessage.Attachments.Add(icsAttachment);

                smtpClient.Send(mailMessage);

                r.Message = "Email invitation succesfully sent out.";
                r.RedirectUrl = "/Home/Calendar";
            }
            catch (Exception e)
            {
                ViewBag.EmailError = ("Error: {0}", e);
                r.Error = "Error.";
            }

            return Json(r);
        }

        public async Task<JsonResult> ChangePassword(string curPass, string pass)
        {
            var id = Utils.GetUserId();
            var r = new Response();

            try
            {
                if (await UserStore.ChangePassword(new ObjectClassesLibrary.Classes.User() { UserId = id, Password = curPass, CreatedBy = id }, pass))
                {
                    r.Message = "Password changed.";
                }
                else
                {
                    r.Error = "Unable to update password, please try again later.";
                }

                return Json(r);
            }
            catch (Exception ex)
            {
                r.Error = ex.Message;
                return Json(r);
            }
        }
    }
}