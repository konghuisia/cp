﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class CaseController : Controller
    {
        ProfileStore ProfileStore;
        RequestStore RequestStore;
        SourceStore SourceStore;
        LeadStore LeadStore;
        UserStore UserStore;
        DebtorStore DebtorStore;
        StatusStore StatusStore;
        UserAccessStore UserAccessStore;
        DraftStore DraftStore;

        public CaseController()
        {
            ProfileStore = DependencyResolver.Current.GetService<ProfileStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            LeadStore = DependencyResolver.Current.GetService<LeadStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            SourceStore = DependencyResolver.Current.GetService<SourceStore>();
            DebtorStore = DependencyResolver.Current.GetService<DebtorStore>();
            StatusStore = DependencyResolver.Current.GetService<StatusStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            DraftStore = DependencyResolver.Current.GetService<DraftStore>();
        }
        // GET: Case
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> Choice(string module)
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.LeadIndexViewModel();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Leads = await LeadStore.GetAllLead();
            vm.Users = await UserStore.GetAllUser();
            vm.RMO = vm.Users.FindAll(x => x.Position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487");
            vm.Debtors = await DebtorStore.GetAllDebtor();

            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);
            vm.Requests.RemoveAll(x => x.Status == null);
            vm.Requests.RemoveAll(x => x.Stage == 21 || x.Stage == 35);
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.RMO.RemoveAll(x => x.UserId != id);
                vm.Leads.RemoveAll(x => x.AssignTo != id);
            }
            else if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                vm.Requests.RemoveAll(x => x.Approval1User == null && x.Approval2User == null);

                if (id == "31316B60-95E1-4727-9242-CD7B51B47469")
                {
                    vm.Requests.RemoveAll(x => x.Stage == 7 || x.Stage == 18 || x.Stage == 26);
                }
            }
            else if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                vm.Requests.RemoveAll(x => x.LawFirm != pid);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")
            {
                vm.Requests.RemoveAll(x => x.Stage < 33);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Leads.RemoveAll(x => x.AssignTo == null);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }

            vm.Module = module;
            if (string.IsNullOrEmpty(module))
            {
                vm.Module = "Lead";
            }

            return View(vm);
        }
        public async Task<ActionResult> _Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.LeadIndexViewModel();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Leads = await LeadStore.GetAllLead();
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.RMO = vm.Users.FindAll(x => x.Position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487");
            vm.Debtors = await DebtorStore.GetAllDebtor();
            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);

            vm.Requests.RemoveAll(x => x.Status == null);
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.RMO.RemoveAll(x => x.UserId != id);
                vm.Leads.RemoveAll(x => x.AssignTo != id);
            }
            else if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                vm.Requests.RemoveAll(x => x.Approval1User == null && x.Approval2User == null);
            }
            else if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                vm.Requests.RemoveAll(x => x.LawFirm != pid);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")
            {
                vm.Requests.RemoveAll(x => x.Stage < 27);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Leads.RemoveAll(x => x.AssignTo == null);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }
            vm.Profiles = vm.Profiles.Where(x => vm.Sources.Any(z => z.SourceId == x.Source)).ToList();
            vm.Requests = vm.Requests.Where(x => vm.Profiles.Any(z => z.ProfileId == x.ProfileId)).ToList();
            vm.Requests = vm.Requests.Where(x => vm.Leads.Any(z => z.RequestId == x.RequestId)).ToList();

            return PartialView(vm);
        }
        public async Task<ActionResult> _CaseIndex()
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.LeadIndexViewModel();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Leads = await LeadStore.GetAllLead();
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.Statuses = await StatusStore.StatusList();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);
            vm.Drafts = await DraftStore.GetDraftByCreatedById(id);
            vm.UserId = id;
            vm.Position = p;

            vm.Requests.RemoveAll(x => x.Status == null || x.Status == "New");
            vm.Requests.RemoveAll(x => x.Status.Length >= 4 && x.Status.Substring(0, 4) != "Case");
            vm.Statuses.RemoveAll(x => x.Name.Length >= 4 && x.Name.Substring(0, 4) == "Lead");
            vm.Requests.RemoveAll(x => x.Stage == 21 || x.Stage == 35);
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Leads.RemoveAll(x => x.AssignTo != id);
            }
            else if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                vm.Requests.RemoveAll(x => x.Approval1User == null && x.Approval2User == null);
            }
            else if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                vm.Requests.RemoveAll(x => x.LawFirm != pid);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")
            {
                vm.Requests.RemoveAll(x => x.Stage < 33);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Leads.RemoveAll(x => x.AssignTo == null);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }

            foreach (var v in vm.Requests)
            {
                v.AccessStatus = new ObjectClassesLibrary.Classes.LeadUserHasAccess().checkAccess(p, v.Stage, id);
            }

            return PartialView(vm);
        }
    }
}