﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class SubscriptionsController : Controller
    {
        SubscriptionStore SubscriptionStore;
        UserAccessStore UserAccessStore;
        UserStore UserStore;
        RequestStore RequestStore;
        UserPositionStore UserPositionStore;
        StatusOfCaseStore StatusOfCaseStore;
        StateCityStore StateCityStore;
        PersonStore PersonStore;
        AddressStore AddressStore;
        ProfileStore ProfileStore;
        AuditTrailStore AuditTrailStore;
        PackageStore PackageStore;
        CommentRemarkStore CommentRemarkStore;
        DocumentStore DocumentStore;
        DraftStore DraftStore;
        SourceStore SourceStore;

        // GET: Case
        public ActionResult Index()
        {
            return View();
        }

        public SubscriptionsController()
        {
            SubscriptionStore = DependencyResolver.Current.GetService<SubscriptionStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            UserPositionStore = DependencyResolver.Current.GetService<UserPositionStore>();
            StatusOfCaseStore = DependencyResolver.Current.GetService<StatusOfCaseStore>();
            StateCityStore = DependencyResolver.Current.GetService<StateCityStore>();
            PersonStore = DependencyResolver.Current.GetService<PersonStore>();
            AddressStore = DependencyResolver.Current.GetService<AddressStore>();
            ProfileStore = DependencyResolver.Current.GetService<ProfileStore>();
            AuditTrailStore = DependencyResolver.Current.GetService<AuditTrailStore>();
            PackageStore = DependencyResolver.Current.GetService<PackageStore>();
            CommentRemarkStore = DependencyResolver.Current.GetService<CommentRemarkStore>();
            DocumentStore = DependencyResolver.Current.GetService<DocumentStore>();
            DraftStore = DependencyResolver.Current.GetService<DraftStore>();
            SourceStore = DependencyResolver.Current.GetService<SourceStore>();
        }

        public async Task<ActionResult> Choice()
        {
            if (Utils.IfUserAuthenticated())
            {
                //var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.SubscriptionIndexViewModal();
            //vm.Subscriptions = await SubscriptionStore.GetSubscriptions();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.User = await UserStore.GetUserByUserid(id);
            return View(vm);
        }

        public async Task<ActionResult> Subscription(string ProfileId, string page)
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.SubscriptionIndexViewModal();
            vm.Subscription = await SubscriptionStore.GetSubscriptionByProfileId(ProfileId);
            vm.Profile = await ProfileStore.GetProfileByProfileId(ProfileId);
            vm.ProfileId = ProfileId;
            vm.Page = page;
            //vm.Address = await AddressStore.GetAddressByProfileId(ProfileId);
            //vm.Person = await PersonStore.GetPersonByProfileId(ProfileId);
            //vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByProfileId(ProfileId);
            //vm.ProfileAuditTrails = await AuditTrailStore.GetProfileAuditTrailByProfileId(ProfileId);
            //vm.Positions = await UserPositionStore.GetAllUserPosition();
            //vm.Users = await UserStore.GetAllUser();
            //vm.Sources = await SourceStore.GetAllSource();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            //vm.CommentRemark.RemoveAll(x => x.Type.TrimEnd() != "Profile");
            //vm.State = await StateCityStore.StateList();
            //vm.City = await StateCityStore.CityList();
            //vm.Page = page;
            return View(vm);
        }

        public async Task<ActionResult> _SubscriptionAccount(string profileId, string page)
        {
            if (Utils.IfUserAuthenticated())
            {
                //var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            //var pid = "";

            var vm = new Models.SubscriptionIndexViewModal();
            vm.Subscription = await SubscriptionStore.GetSubscriptionByProfileId(profileId);
            vm.Profile = await ProfileStore.GetProfileByProfileId(profileId);
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.SubscriptionAuditTrails = await AuditTrailStore.GetSubscriptionAuditTrailsByProfileId(profileId);
            vm.Page = page;

            return PartialView(vm);
        }

        public async Task<ActionResult> _RedemptionHistory(string profileId, string page)
        {
            if (Utils.IfUserAuthenticated())
            {
                //var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            //var pid = "";

            var vm = new Models.SubscriptionIndexViewModal();
            //vm.Subscription = await SubscriptionStore.GetSubscriptionByProfileId(profileId);
            //vm.Profile = await ProfileStore.GetProfileByProfileId(profileId);
            vm.RedemptionHistory = await SubscriptionStore.GetRedemptionHistories(profileId);
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.Page = page;

            return PartialView(vm);
        }

        public async Task<ActionResult> _Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                //var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";
            var status = 1;

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                status = 2;
            }

            var vm = new Models.SubscriptionIndexViewModal();
            vm.SubscriptionRequest = await SubscriptionStore.GetSubscriptionRequests(status, id);
            vm.Packages = await PackageStore.GetPackages();
            vm.SubStatus = await SubscriptionStore.GetSubscriptionStatus();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.User = await UserStore.GetUserByUserid(id);

            return PartialView(vm);
        }

        public async Task<ActionResult> _PendingIndex()
        {
            if (Utils.IfUserAuthenticated())
            {
                //var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.SubscriptionIndexViewModal();
            
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.User = await UserStore.GetUserByUserid(id);

            return PartialView(vm);
        }

        public async Task<ActionResult> NewSubscription()
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var userName = Utils.GetDisplayName();

            var vm = new Models.NewSubscriptionViewModel();
            vm.Request.CreatedBy = id;
            vm.Request.RequestId = await RequestStore.CreateRequest(vm.Request);
            vm.User = await UserStore.GetAllUser();
            vm.StatusOfCases = await StatusOfCaseStore.GetAllStatusOfCase();
            vm.UserPosition = await UserPositionStore.GetUserPositionByUserPositionId(p);
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.Packages = await PackageStore.GetPackages();
            vm.UserId = id;
            vm.State = await StateCityStore.StateList();
            vm.City = await StateCityStore.CityList();
            vm.Name = userName;
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();




            if (TempData.ContainsKey("cr"))
            {
                vm.CommentRemark = TempData["cr"] as CommentRemark;
            }
            else
            {
                vm.CommentRemark = new CommentRemark();
            }

            if (TempData.ContainsKey("address"))
            {
                vm.Address = TempData["address"] as Address;
            }
            else
            {
                vm.Address = new Address();
            }

            if (TempData.ContainsKey("profile"))
            {
                vm.Profile = TempData["profile"] as Profile;
            } else
            {
                vm.Profile = new Profile();
            }

            if (TempData.ContainsKey("person"))
            {
                vm.Person = TempData["person"] as Person;
            }
            else
            {
                vm.Person = new Person();
            }

            //vm.RedemptionHistory = await SubscriptionStore.GetRedemptionHistories("3337AB45-3C27-4A09-918F-EC3899A17625");

            vm.User = vm.User.FindAll(x => x.Position == "22C63EE9-290E-4160-A293-6ECB49B4677F");
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.User.RemoveAll(x => x.UserId != id);
            }

            return View(vm);
        }

        public async Task<JsonResult> checkCreditorProfile(string businessRegNo)
        {
            var vm = new Models.SubscriptionIndexViewModal();

            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Profiles.RemoveAll(x => x.ProfileType == 2);

            vm.ProfileExists = vm.Profiles.Exists(p => p.BusinessRegNo == businessRegNo);
            //vm.isSubscriber = vm.Profiles.Exists(p => p.DMS == true);
            var subs = vm.Profiles.Where(x => (x.BusinessRegNo == businessRegNo) && (x.DMS == 1));

            if(subs.Count() <= 0)
            {
                vm.isSubscriber = false;
            } else
            {
                vm.isSubscriber = true;
            }

            if (vm.ProfileExists)
            {
                vm.Profile = vm.Profiles.First(x => x.BusinessRegNo == businessRegNo);
                vm.Person = await PersonStore.GetPersonByProfileId(vm.Profile.ProfileId);
            } 
            vm.Profiles.RemoveAll(x => x.ProfileId != null);
            return Json(vm);
        }

        public async Task<JsonResult> NewSubscriptionDraft(Profile profile, Address address, Person person, CommentRemark cr, Request request, IList<ObjectClassesLibrary.Classes.Document> documents, AuditTrail auditTrail, Draft draft)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            cr.CreatedBy = id;

            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                //lead.AssignTo = id;
                //request.Stage = 1;
            }
            else
            {
                request.Stage = 0;
            }

            request.Status = "Subscription (New)";


            var r = new Response();
            request.CreatedBy = id;
            request.RequestId = await RequestStore.CreateRequest(request);
            cr.Stage = request.Stage;
            cr.RequestId = request.RequestId;

            if (!string.IsNullOrEmpty(request.RequestId))
            {
                if (!string.IsNullOrEmpty(profile.ProfileId) && profile.ProfileId != "null")
                {
                    address.ProfileId = profile.ProfileId;
                    person.ProfileId = profile.ProfileId;
                    cr.ProfileId = profile.ProfileId;
                    request.ProfileId = profile.ProfileId;
                    //lead.RequestId = request.RequestId;
                    //afi.ProfileId = request.ProfileId;

                    address.AddressId = await AddressStore.UpdateAddressByAddressId(address);
                    person.PersonId = await PersonStore.UpdatePersonByPersonId(person);
                    cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                    //lead.LeadId = await LeadStore.CreateLead(lead);
                    //afi.AssetForInvestigationId = await AssetForInvestigationStore.CreateAssetForInvestigation(afi, id, profile.ProfileId);
                    //bool c = await CompanyFinancialInformationStore.CreateCompanyFinancialInformation(cfi, id, profile.ProfileId);
                    //bool l = await LitigationRecordStore.CreateLitigationRecord(lr, id, profile.ProfileId);
                    //bool b = await BlackListingRecordStore.CreateBlackListingRecord(br, id, profile.ProfileId);
                    bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                    request.PersonId = person.PersonId;
                    request.AddressId = address.AddressId;
                    //request.LeadId = lead.LeadId;

                    var rr = await RequestStore.UpdateRequest(request);

                    if (!string.IsNullOrEmpty(rr))
                    {
                        draft.CreatedBy = id;
                        draft.RequestId = rr;
                        draft.Active = true;
                        draft.DraftId = await DraftStore.CreateDraft(draft);

                        if (!string.IsNullOrEmpty(draft.DraftId))
                        {
                            r.Message = "Draft saved successfully.";
                            r.RedirectUrl = "/Home/MyTask";
                        }
                        else
                        {
                            r.Error = "Unable to save draft, please try again later.";
                        }
                    }
                    else
                    {
                        r.Error = "Unable to save draft, please try again later.";
                    }
                }
                //else
                //{
                //    profile.ProfileId = await ProfileStore.CreateProfile(profile, id);

                //    if (!string.IsNullOrEmpty(profile.ProfileId))
                //    {
                //        address.ProfileId = profile.ProfileId;
                //        person.ProfileId = profile.ProfileId;
                //        cr.ProfileId = profile.ProfileId;
                //        //lead.RequestId = request.RequestId;
                //        //afi.ProfileId = request.ProfileId;

                //        address.AddressId = await AddressStore.CreateAddress(address);
                //        person.PersonId = await PersonStore.CreatePerson(person);
                //        cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                //        //lead.LeadId = await LeadStore.CreateLead(lead);
                //        //afi.AssetForInvestigationId = await AssetForInvestigationStore.CreateAssetForInvestigation(afi, id, profile.ProfileId);
                //        //bool c = await CompanyFinancialInformationStore.CreateCompanyFinancialInformation(cfi, id, profile.ProfileId);
                //        //bool l = await LitigationRecordStore.CreateLitigationRecord(lr, id, profile.ProfileId);
                //        //bool b = await BlackListingRecordStore.CreateBlackListingRecord(br, id, profile.ProfileId);
                //        bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                //        if (/*string.IsNullOrEmpty(address.AddressId)*/ || string.IsNullOrEmpty(person.PersonId) /*|| string.IsNullOrEmpty(cr.CommentRemarkId)*/ || /*string.IsNullOrEmpty(lead.LeadId)*/)
                //        {
                //            r.Error = "Unable to save draft, please try again later.";
                //        }
                //        else
                //        {
                //            request.ProfileId = profile.ProfileId;
                //            request.PersonId = person.PersonId;
                //            request.AddressId = address.AddressId;
                //            //request.LeadId = lead.LeadId;

                //            var rr = await RequestStore.UpdateRequest(request);

                //            if (!string.IsNullOrEmpty(rr))
                //            {
                //                draft.CreatedBy = id;
                //                draft.RequestId = rr;
                //                draft.Active = true;
                //                draft.DraftId = await DraftStore.CreateDraft(draft);

                //                if (!string.IsNullOrEmpty(draft.DraftId))
                //                {
                //                    r.Message = "Draft saved successfully.";
                //                    r.RedirectUrl = "/Home/MyTask";
                //                }
                //                else
                //                {
                //                    r.Error = "Unable to save draft, please try again later.";
                //                }

                //            }
                //            else
                //            {
                //                r.Error = "Unable to save draft, please try again later.";
                //            }
                //        }
                //    }
                //}
            }

            return Json(r);
        }


        public async Task<JsonResult> CreateNewSubscription(Profile profile, Address address, Person person, CommentRemark cr, Request request, IList<ObjectClassesLibrary.Classes.Document> documents, Subscriptions subscription, AuditTrail auditTrail)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            cr.CreatedBy = id;

            var r = new Response();
            request.CreatedBy = id;
            request.RequestId = await RequestStore.CreateRequest(request);
            cr.Stage = request.Stage;
            cr.RequestId = request.RequestId;

            

            if (!string.IsNullOrEmpty(request.RequestId))
            {
                if (!string.IsNullOrEmpty(profile.ProfileId) && profile.ProfileId != "null")
                {
                    address.ProfileId = profile.ProfileId;
                    person.ProfileId = profile.ProfileId;
                    cr.ProfileId = profile.ProfileId;
                    request.ProfileId = profile.ProfileId;

                    address.AddressId = await AddressStore.UpdateAddressByAddressId(address);
                    person.PersonId = await PersonStore.UpdatePersonByPersonId(person);
                    cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);                   
                    bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                    request.PersonId = person.PersonId;
                    request.AddressId = address.AddressId;
                    request.ProfileId = profile.ProfileId;
                    request.PersonId = person.PersonId;
                    request.AddressId = address.AddressId;

                    var rr = await RequestStore.UpdateRequest(request);

                    if (string.IsNullOrEmpty(rr))
                    {
                        r.Error = "Unable to save record, please try again later.";
                    }
                    else
                    {
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Profile", "Subscription Request Created.", cr.Comment, request.Stage);
                        r.Message = "Record saved successfully.";
                        r.RedirectUrl = "/Profile/Choice";
                    }
                }
                else
                {
                    profile.ProfileId = await ProfileStore.CreateProfile(profile, id);

                    if (!string.IsNullOrEmpty(profile.ProfileId))
                    {
                        address.ProfileId = profile.ProfileId;
                        person.ProfileId = profile.ProfileId;
                        cr.ProfileId = profile.ProfileId;

                        subscription.RequestId = request.RequestId;
                        subscription.ProfileId = profile.ProfileId;

                        subscription.SubsId = await SubscriptionStore.CreateSubscription(subscription, id);

                        address.AddressId = await AddressStore.CreateAddress(address);
                        person.PersonId = await PersonStore.CreatePerson(person);
                        cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                        bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                        if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(person.PersonId) /*|| string.IsNullOrEmpty(cr.CommentRemarkId)*/)
                        {
                            r.Error = "Unable to save record, please try again later.";
                        }
                        else
                        {
                            request.Approval1User = subscription.Approver1_Id;
                            request.ProfileId = profile.ProfileId;
                            request.PersonId = person.PersonId;
                            request.AddressId = address.AddressId;
                            request.Approval1User = subscription.Approver1_Id;

                            var rr = await RequestStore.UpdateRequest(request);

                            if (!string.IsNullOrEmpty(rr))
                            {
                                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Profile", "Subscription Request Created.", cr.Comment, request.Stage);
                                r.Message = "Record saved successfully.";
                                r.RedirectUrl = "/Profile/Choice";
                            }
                            else
                            {
                                r.Error = "Unable to save record, please try again later.";
                            }
                        }
                    }
                }
            }

            return Json(r);
        }


        public async Task<ActionResult> SubscriptionApproval(string requestId, string page)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();

            int i = 0;
            var vm = new Models.SubscriptionIndexViewModal();
            //vm.DocumentTypes = await DocumentTypeStore.GetAllDocumentType();
            vm.Request = await RequestStore.GetRequestByRequestId(requestId);
            vm.Users = await UserStore.GetAllUser();
            vm.Users2 = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.StatusOfCases = await StatusOfCaseStore.GetAllStatusOfCase();
            vm.States = await StateCityStore.StateList();
            vm.Cities = await StateCityStore.CityList();
            //vm.Users = vm.Users.FindAll(x => x.Position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487");
            vm.UserPositions = await UserPositionStore.GetAllUserPosition();
            //vm.Debtors = await DebtorStore.GetDebtorByRequestId(requestId);
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.SubRequest = await SubscriptionStore.GetSubscriptionRequestByRequestId(requestId);
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            //vm.Negotiator = vm.Users2.FindAll(x => x.Position == "E207490E-1A8C-405D-B415-B100ADB0D04F");
            vm.Position = p;
            vm.UserId = id;
            vm.Page = page;
            //vm.Sop = await SopStore.SopList();
            //vm.SopRequests = await SopRequestsStore.GetSopRequestsByRequestsId(requestId);
            //vm.PageSegmentAccess.ProfileAddressPersonAccess = true;
            //if (vm.Request.Stage < 7 || vm.Request.Stage == 10)
            //{
            //    vm.PageSegmentAccess.ProfileAddressPersonAccess = true;
            //    vm.PageSegmentAccess.AppointmentDocAccess = true;
            //    vm.PageSegmentAccess.CreditorDocAccess = true;
            //    vm.PageSegmentAccess.CtosDocAccess = true;
            //    vm.PageSegmentAccess.DebtorAccess = true;
            //    vm.PageSegmentAccess.CFIAccess = true;
            //    vm.PageSegmentAccess.LRAccess = true;
            //    vm.PageSegmentAccess.BRAccess = true;
            //}
            //else if (vm.Request.Stage == 12)
            //{
            //    vm.PageSegmentAccess.AppointmentDocAccess = true;
            //}
            //else if (vm.Request.Stage == 13)
            //{
            //    vm.PageSegmentAccess.CreditorDocAccess = true;
            //}

            //if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            //{
            //    vm.Users.RemoveAll(x => x.UserId != id);
            //}

            if (vm != null)
            {
                vm.Profile = await ProfileStore.GetProfileByProfileId(vm.Request.ProfileId);
                vm.Address = await AddressStore.GetAddressByAddressId(vm.Request.AddressId);
                vm.Person = await PersonStore.GetPersonByPersonId(vm.Request.PersonId);
                vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByRequestId(vm.Request.RequestId);

                vm.Documents = await DocumentStore.GetDocumentByRequestId(requestId);
                vm.AuditTrails = await AuditTrailStore.GetAuditTrailByRequestId(requestId);
                vm.AuditTrails = vm.AuditTrails.FindAll(x => x.Type.TrimEnd() == "Lead" || x.Type.TrimEnd() == "Appt");

                var docs = await DocumentStore.GetDocumentByRequestId(requestId);

                //var requiredDocs = vm.ShowInSelects.FindAll(x => x.Required == true);
                //requiredDocs.RemoveAll(x => x.Stage > vm.Request.Stage);

                //if (!string.IsNullOrEmpty(vm.Request.Dispute1User) && !string.IsNullOrEmpty(vm.Request.Dispute2User))
                //{
                //    requiredDocs.RemoveAll(x => x.DocumentTypeId == "54992022-FABB-4C43-859B-459AD29B30FA");
                //}

                //if (requiredDocs.Count == 0)
                //{
                //    vm.DocRequired = true;
                //}
                //else
                //{
                //    foreach (var rd in requiredDocs)
                //    {
                //        var c = docs.Find(x => x.Type == rd.DocumentTypeId);

                //        if (c != null)
                //        {
                //            i++;
                //        }
                //    }
                //    if (i >= requiredDocs.Count)
                //    {
                //        vm.DocRequired = true;
                //    }
                //}
            }

            return View(vm);
        }

        public async Task<ActionResult> SubscriptionApproval2(string requestId, string page)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();

            int i = 0;
            var vm = new Models.SubscriptionIndexViewModal();
            //vm.DocumentTypes = await DocumentTypeStore.GetAllDocumentType();
            vm.Request = await RequestStore.GetRequestByRequestId(requestId);
            vm.Users = await UserStore.GetAllUser();
            vm.Users2 = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.StatusOfCases = await StatusOfCaseStore.GetAllStatusOfCase();
            vm.States = await StateCityStore.StateList();
            vm.Cities = await StateCityStore.CityList();
            //vm.Users = vm.Users.FindAll(x => x.Position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487");
            vm.UserPositions = await UserPositionStore.GetAllUserPosition();
            //vm.Debtors = await DebtorStore.GetDebtorByRequestId(requestId);
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.SubRequest = await SubscriptionStore.GetSubscriptionRequestByRequestId(requestId);
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            //vm.Negotiator = vm.Users2.FindAll(x => x.Position == "E207490E-1A8C-405D-B415-B100ADB0D04F");
            vm.Position = p;
            vm.UserId = id;
            vm.Page = page;
            //vm.Sop = await SopStore.SopList();
            //vm.SopRequests = await SopRequestsStore.GetSopRequestsByRequestsId(requestId);
            //vm.PageSegmentAccess.ProfileAddressPersonAccess = true;
            //if (vm.Request.Stage < 7 || vm.Request.Stage == 10)
            //{
            //    vm.PageSegmentAccess.ProfileAddressPersonAccess = true;
            //    vm.PageSegmentAccess.AppointmentDocAccess = true;
            //    vm.PageSegmentAccess.CreditorDocAccess = true;
            //    vm.PageSegmentAccess.CtosDocAccess = true;
            //    vm.PageSegmentAccess.DebtorAccess = true;
            //    vm.PageSegmentAccess.CFIAccess = true;
            //    vm.PageSegmentAccess.LRAccess = true;
            //    vm.PageSegmentAccess.BRAccess = true;
            //}
            //else if (vm.Request.Stage == 12)
            //{
            //    vm.PageSegmentAccess.AppointmentDocAccess = true;
            //}
            //else if (vm.Request.Stage == 13)
            //{
            //    vm.PageSegmentAccess.CreditorDocAccess = true;
            //}

            //if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            //{
            //    vm.Users.RemoveAll(x => x.UserId != id);
            //}

            if (vm != null)
            {
                vm.Profile = await ProfileStore.GetProfileByProfileId(vm.Request.ProfileId);
                vm.Address = await AddressStore.GetAddressByAddressId(vm.Request.AddressId);
                vm.Person = await PersonStore.GetPersonByPersonId(vm.Request.PersonId);
                vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByRequestId(vm.Request.RequestId);

                vm.Documents = await DocumentStore.GetDocumentByRequestId(requestId);
                vm.AuditTrails = await AuditTrailStore.GetAuditTrailByRequestId(requestId);
                vm.AuditTrails = vm.AuditTrails.FindAll(x => x.Type.TrimEnd() == "Lead" || x.Type.TrimEnd() == "Appt");

                var docs = await DocumentStore.GetDocumentByRequestId(requestId);

                //var requiredDocs = vm.ShowInSelects.FindAll(x => x.Required == true);
                //requiredDocs.RemoveAll(x => x.Stage > vm.Request.Stage);

                //if (!string.IsNullOrEmpty(vm.Request.Dispute1User) && !string.IsNullOrEmpty(vm.Request.Dispute2User))
                //{
                //    requiredDocs.RemoveAll(x => x.DocumentTypeId == "54992022-FABB-4C43-859B-459AD29B30FA");
                //}

                //if (requiredDocs.Count == 0)
                //{
                //    vm.DocRequired = true;
                //}
                //else
                //{
                //    foreach (var rd in requiredDocs)
                //    {
                //        var c = docs.Find(x => x.Type == rd.DocumentTypeId);

                //        if (c != null)
                //        {
                //            i++;
                //        }
                //    }
                //    if (i >= requiredDocs.Count)
                //    {
                //        vm.DocRequired = true;
                //    }
                //}
            }

            return View(vm);
        }
    }
}