﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using ObjectClassesLibrary;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class ConfirgurationController : Controller
    {
        DocumentTypeStore DocumentTypeStore;
        ShowInSelectStore ShowInSelectStore;
        UserStore UserStore;
        UserPositionStore UserPositionStore;
        UserAccessStore UserAccessStore;
        AccessStore AccessStore;
        lawFirmLawFirmProfileStore lawFirmLawFirmProfileStore;
        SopStore SopStore;
        PositionsStore PositionsStore;
        PackageStore PackageStore;

        public ConfirgurationController()
        {
            DocumentTypeStore = DependencyResolver.Current.GetService<DocumentTypeStore>();
            ShowInSelectStore = DependencyResolver.Current.GetService<ShowInSelectStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            UserPositionStore = DependencyResolver.Current.GetService<UserPositionStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            AccessStore = DependencyResolver.Current.GetService<AccessStore>();
            lawFirmLawFirmProfileStore = DependencyResolver.Current.GetService<lawFirmLawFirmProfileStore>();
            SopStore = DependencyResolver.Current.GetService<SopStore>();
            PositionsStore = DependencyResolver.Current.GetService<PositionsStore>();
            PackageStore = DependencyResolver.Current.GetService<PackageStore>();
        }
        // GET: Confirguration
        public async Task<ActionResult> Index(string module)
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var vm = new Models.ConfigViewModel();
            vm.Module = module;
            vm.Sops = await SopStore.SopList();
            if (!string.IsNullOrEmpty(module))
            {
                module = null;
            }

            return View(vm);
        }
        public async Task<ActionResult> LoadConfigurationPage(string module)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.ConfigViewModel();
            vm.Users = await UserStore.GetAllUser();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);

            switch (module)
            {
                case "_DocType":
                    vm.DocumentType = await DocumentTypeStore.GetAllDocumentType();
                    vm.ShowInSelects = await ShowInSelectStore.GetAllShowInSelect();
                    vm.DocumentType.RemoveAll(x => x.Deleted == true);
                    vm.ShowInSelects.RemoveAll(x => x.Deleted == true);
                    break;
                case "_User":
                    vm.Users = vm.Users.OrderBy(x => x.Name).ToList();
                    vm.UserPositions = await UserPositionStore.GetAllUserPosition();
                    vm.UserPositions.RemoveAll(x => x.Deleted == true);
                    vm.Positions = await PositionsStore.GetPositions();
                    break;
                case "_Group":
                    vm.UserPositions = await UserPositionStore.GetAllUserPosition();
                    vm.UserPositions.RemoveAll(x => x.Deleted == true);
                    break;
                case "_Packages":
                    vm.Packages = await PackageStore.GetPackages();
                    break;
                case "_PackageServices":
                    vm.PackageServices = await PackageStore.GetPackageServices();
                    vm.ConditionTypes = await PackageStore.GetConditionTypes();
                    break;
                case "_PackageServicesConditionType":
                    vm.ConditionTypes = await PackageStore.GetConditionTypes();
                    break;
            }


            return PartialView(module, vm);
        }
        public async Task<ActionResult> CreateGroup()
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.GroupViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.AccessTypes = await AccessStore.AccessList();
            vm.UserAccess = await UserAccessStore.GetAllUserAccess();

            return View(vm);
        }
        public async Task<ActionResult> EditGroup(string groupId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.GroupViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.Group = await UserPositionStore.GetUserPositionByUserPositionId(groupId);
            vm.UserAccess = await UserAccessStore.GetUserAccessByUserPositionId(groupId);
            vm.AccessTypes = await AccessStore.AccessList();

            return View(vm);
        }
        public async Task<ActionResult> EditGroupPreview(string groupId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.GroupViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.Group = await UserPositionStore.GetUserPositionByUserPositionId(groupId);
            vm.UserAccess = await UserAccessStore.GetUserAccessByUserPositionId(groupId);
            vm.AccessTypes = await AccessStore.AccessList();

            return View(vm);
        }
        public async Task<ActionResult> AddNewPackage()
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.PackageViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            //vm.Groups = await UserPositionStore.GetAllUserPosition();
            vm.AccessTypes = await AccessStore.AccessList();
            //vm.Groups.RemoveAll(x => x.Deleted == true);
            //vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            //vm.Positions = await PositionsStore.GetPositions();

            return View(vm);
        }
        public async Task<ActionResult> EditPackage(string packageId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.PackageViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.AccessTypes = await AccessStore.AccessList();
            vm.Package = await PackageStore.GetPackagesByPackageId(packageId);

            return View(vm);
        }

        public async Task<ActionResult> EditPackageService(string serviceId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.ConfigViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.AccessTypes = await AccessStore.AccessList();
            vm.PackageService = await PackageStore.GetPackageServiceByServiceId(serviceId);
            vm.PackageServiceConditions = await PackageStore.GetPackageServiceConditionsByServiceId(serviceId);
            vm.ConditionTypes = await PackageStore.GetConditionTypes();

            return View(vm);
        }

        public async Task<JsonResult> updateConditionSelect(string serviceId)
        {
            var vm = new Models.ConfigViewModel();
            vm.PackageServiceConditions = await PackageStore.GetPackageServiceConditionsByServiceId(serviceId);
            vm.PackageService = await PackageStore.GetPackageServiceByServiceId(serviceId);
            return Json(vm);
        }


        public async Task<ActionResult> AddNewPackageService()
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.ConfigViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            //vm.Groups = await UserPositionStore.GetAllUserPosition();
            vm.AccessTypes = await AccessStore.AccessList();
            //vm.Groups.RemoveAll(x => x.Deleted == true);
            //vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            //vm.Positions = await PositionsStore.GetPositions();
            vm.ConditionTypes = await PackageStore.GetConditionTypes();
            return View(vm);
        }
        public async Task<JsonResult> CreateNewConditionType(string condName)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await PackageStore.CreateNewConditionType(condName, id);


                r.Message = "Condition type added successfully.";

                r.RedirectUrl = "/Confirguration?module=PackageServicesConditionType";
            return Json(r);
        }
        public async Task<ActionResult> CreateUser()
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.UserViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.Groups = await UserPositionStore.GetAllUserPosition();
            vm.AccessTypes = await AccessStore.AccessList();
            vm.Groups.RemoveAll(x => x.Deleted == true);
            vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            vm.Positions = await PositionsStore.GetPositions();

            return View(vm);
        }
        public async Task<ActionResult> EditUser(string userId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.UserViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.Groups = await UserPositionStore.GetAllUserPosition();
            vm.AccessTypes = await AccessStore.AccessList();
            vm.Groups.RemoveAll(x => x.Deleted == true);
            vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            vm.User = await UserStore.GetUserByUserid(userId);
            vm.Positions = await PositionsStore.GetPositions();

            return View(vm);
        }
        public async Task<ActionResult> EditUserPreview(string userId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.UserViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.Groups = await UserPositionStore.GetAllUserPosition();
            vm.AccessTypes = await AccessStore.AccessList();
            vm.Groups.RemoveAll(x => x.Deleted == true);
            vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            vm.User = await UserStore.GetUserByUserid(userId);
            vm.Positions = await PositionsStore.GetPositions();

            return View(vm);
        }
        public async Task<JsonResult> CreateNewDocType(DocumentType docType)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            docType = await DocumentTypeStore.CreateNewDocType(docType, id);

            if (!string.IsNullOrEmpty(docType.DocumentTypeId))
            {
                r.Message = "Doc type saved successfully.";
                r.RedirectUrl = "/Confirguration?module=DocType";
            }
            else
            {
                r.Error = "Unable to save doc type, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> CreateNewGroupType(UserPosition groupType)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            groupType = await UserPositionStore.CreateNewUserPosition(groupType, id);

            if (!string.IsNullOrEmpty(groupType.UserPositionId))
            {
                r.Message = "Group type saved successfully.";
                r.RedirectUrl = "/Confirguration?module=Group";
            }
            else
            {
                r.Error = "Unable to save group type, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> CreateNewGroup(UserPosition group, List<UserAccess> access)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            group = await UserPositionStore.CreateNewUserPosition(group, id);

            if (!string.IsNullOrEmpty(group.UserPositionId))
            {
                bool b = await AccessStore.CreateGroupAccess(access, id, group.UserPositionId);

                if (b)
                {
                    r.Message = "Group type saved successfully.";
                    r.RedirectUrl = "/Confirguration?module=Group";
                }
            }
            else
            {
                r.Error = "Unable to save group type, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> CreateNewPackage(Packages package)
        {
            string id = Utils.GetUserId();
            var r = new Response();


                package.PackageId = await PackageStore.CreateNewPackage(package, id);

                if (!string.IsNullOrEmpty(package.PackageId))
                {
                    r.Message = "Package added successfully.";
                    r.RedirectUrl = "/Confirguration?module=Packages";
                }
                else
                {
                    r.Error = "Unable to save package, please try again later.";
                }

            return Json(r);
        }

        public async Task<JsonResult> CreateNewPackageService(PackageServices service, PackageServiceConditions condition)
        {
            string id = Utils.GetUserId();
            var r = new Response();


            service.ServiceId = await PackageStore.CreateNewPackageService(service, condition, id);

            if (!string.IsNullOrEmpty(service.ServiceId))
            {
                r.Message = "Service added successfully.";
                r.RedirectUrl = "/Confirguration?module=PackageServices";
            }
            else
            {
                r.Error = "Unable to save service, please try again later.";
            }

            return Json(r);
        }

        public async Task<JsonResult> UpdatePackage(Packages package)
        {
            string id = Utils.GetUserId();
            var r = new Response();


            package.PackageId = await PackageStore.UpdatePackage(package, id);

            if (!string.IsNullOrEmpty(package.PackageId))
            {
                r.Message = "Package updated successfully.";
               // r.RedirectUrl = "/Confirguration?module=Packages";
            }
            else
            {
                r.Error = "Unable to update package, please try again later.";
            }

            return Json(r);
        }

        //public async Task<JsonResult> UpdatePackageServices(PackageServices services)
        //{
        //    string id = Utils.GetUserId();
        //    var r = new Response();


        //    services.ServiceId = await PackageStore.UpdatePackage(package, id);

        //    if (!string.IsNullOrEmpty(package.PackageId))
        //    {
        //        r.Message = "Package updated successfully.";
        //        // r.RedirectUrl = "/Confirguration?module=Packages";
        //    }
        //    else
        //    {
        //        r.Error = "Unable to update package, please try again later.";
        //    }

        //    return Json(r);
        //}

        public async Task<JsonResult> DeletePackage(string packageId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            string b = await PackageStore.DeletePackage(packageId, id);

            if (!string.IsNullOrEmpty(b))
            {
                r.Message = "Package deleted successfully.";
                r.RedirectUrl = "/Confirguration?module=Packages";
            }
            else
            {
                r.Error = "Unable to delete package, please try again later.";
            }

            return Json(r);
        }

        public async Task<JsonResult> DeleteConditionType(string CondTypeId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await PackageStore.DeleteConditionType(CondTypeId, id);

            //if (!string.IsNullOrEmpty(a.DocumentTypeId))
            //{
            r.Message = "Condition type removed successfully.";
            r.RedirectUrl = "/Confirguration?module=PackageServicesConditionType";
            //}
            //else
            //{
            //    r.Error = "Unable to save record, please try again later.";
            //}

            return Json(r);
        }

        public async Task<JsonResult> UpdateConditionType(ConditionTypes CondTypes)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await PackageStore.UpdateConditionType(CondTypes, id);

            //if (!string.IsNullOrEmpty(a.DocumentTypeId))
            //{
            r.Message = "Condition type updated successfully.";
            r.RedirectUrl = "/Confirguration?module=PackageServicesConditionType";
            //}
            //else
            //{
            //    r.Error = "Unable to save record, please try again later.";
            //}

            return Json(r);
        }

        public async Task<JsonResult> CreateNewUser(User user)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            bool check = await UserStore.CheckUserName(user.Name);

            if (!check)
            {
                user.UserId = await UserStore.CreateNewUser(user, id);

                if (!string.IsNullOrEmpty(user.UserId))
                {
                    r.Message = "User saved successfully.";
                    r.RedirectUrl = "/Confirguration?module=User";
                }
                else
                {
                    r.Error = "Unable to save user, please try again later.";
                }
            }
            else {
                r.Error = "Username taken, please try again.";
            }

            return Json(r);
        }
        public async Task<JsonResult> CreateNewSegmentDocType(ShowInSelect showInSelect)
        {
            string id = Utils.GetUserId();
            var r = new Response();

            if (!string.IsNullOrEmpty(showInSelect.ShowInSelectId = await ShowInSelectStore.CreateShowInSelect(showInSelect, id)))
            {
                r.RedirectUrl = "null";
                r.Message = "Segment doc type saved successfully.";
            }
            else
            {
                r.Error = "Unable to save segment doc type, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> DeleteGroupType(string groupTypeId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await UserPositionStore.DeleteUserPosition(groupTypeId, id);

            //if (!string.IsNullOrEmpty(a.DocumentTypeId))
            //{
            r.Message = "Group type removed successfully.";
            r.RedirectUrl = "/Confirguration?module=Group";
            //}
            //else
            //{
            //    r.Error = "Unable to save record, please try again later.";
            //}

            return Json(r);
        }
        public async Task<JsonResult> DeleteDocType(string docTypeId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await DocumentTypeStore.DeleteDocType(docTypeId, id);

            //if (!string.IsNullOrEmpty(a.DocumentTypeId))
            //{
                r.Message = "Doc type removed successfully.";
                r.RedirectUrl = "/Confirguration?module=DocType";
            //}
            //else
            //{
            //    r.Error = "Unable to save record, please try again later.";
            //}

            return Json(r);
        }
        public async Task<JsonResult> DeleteSegmentDocType(string showInSelectId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await ShowInSelectStore.DeleteShowInSelect(showInSelectId, id);

            //if (!string.IsNullOrEmpty(a.DocumentTypeId))
            //{
            r.Message = "Segment doc type removed successfully.";
            r.RedirectUrl = "null";
            //r.RedirectUrl = "/Confirguration?module=DocType";
            //}
            //else
            //{
            //    r.Error = "Unable to save record, please try again later.";
            //}

            return Json(r);
        }
        public async Task<JsonResult> UpdateDocType(DocumentType docType)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await DocumentTypeStore.UpdateDocType(docType, id);

            if (!string.IsNullOrEmpty(a.DocumentTypeId))
            {
                r.Message = "Doc type edited successfully.";
                r.RedirectUrl = "/Confirguration?module=DocType";
            }
            else
            {
                r.Error = "Unable to save doc type, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateGroup(UserPosition group, List<UserAccess> access)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await UserPositionStore.UpdateUserPosition(group, id);
            bool b = await AccessStore.UpdateGroupAccess(access, id, group.UserPositionId);

            if (!string.IsNullOrEmpty(a.UserPositionId) && b)
            {
                r.Message = "Group type edited successfully.";
            }
            else
            {
                r.Error = "Unable to save group type, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateSegmentDocType(ShowInSelect showInSelect)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await ShowInSelectStore.UpdateShowInSelect(showInSelect, id);

            if (!string.IsNullOrEmpty(a))
            {
                r.Message = "Segment doc type edited successfully.";
                r.RedirectUrl = "null";
            }
            else
            {
                r.Error = "Unable to save segment doc type, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateUser(User user)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            bool check = true;

            if (user.Oldname != user.Name)
            {
                check = await UserStore.CheckUserName(user.Name);
            }
            else
            {
                check = false;
            }

            if (!check)
            {
                user.UserId = await UserStore.UpdateUserByUserId(user, id);

                if (!string.IsNullOrEmpty(user.UserId))
                {
                    r.Message = "User saved successfully.";
                }
                else
                {
                    r.Error = "Unable to save user, please try again later.";
                }
            }
            else {
                r.Error = "Username taken, please try again.";
            }

            return Json(r);
        }
        public async Task<JsonResult> ResetUserPassword(string userId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            bool b = await UserStore.ResetUserPassword(userId, id);

            if (b)
            {
                r.Message = "Password saved successfully.";
            }
            else
            {
                r.Error = "Unable to reset password, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> DeleteUser(string userId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            bool b = await UserStore.DeleteUser(userId, id);

            if (b)
            {
                r.Message = "User deleted successfully.";
            }
            else
            {
                r.Error = "Unable to delete user, please try again later.";
            }

            return Json(r);
        }
    }
}