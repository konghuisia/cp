﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class AppointmentController : Controller
    {
        DocumentStore DocumentStore;
        DocumentTypeStore DocumentTypeStore;
        RequestStore RequestStore;
        AppointmentStore AppointmentStore;
        CommentRemarkStore CommentRemarkStore;
        UserStore UserStore;
        LeadStore LeadStore;
        AuditTrailStore AuditTrailStore;
        UserPositionStore UserPositionStore;
        AppointmentTypeStore AppointmentTypeStore;
        UserAccessStore UserAccessStore;
        public AppointmentController()
        {
            DocumentStore = DependencyResolver.Current.GetService<DocumentStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            AppointmentStore = DependencyResolver.Current.GetService<AppointmentStore>();
            CommentRemarkStore = DependencyResolver.Current.GetService<CommentRemarkStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            LeadStore = DependencyResolver.Current.GetService<LeadStore>();
            AuditTrailStore = DependencyResolver.Current.GetService<AuditTrailStore>();
            DocumentTypeStore = DependencyResolver.Current.GetService<DocumentTypeStore>();
            UserPositionStore = DependencyResolver.Current.GetService<UserPositionStore>();
            AppointmentTypeStore = DependencyResolver.Current.GetService<AppointmentTypeStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
        }
        public async Task<ActionResult> _Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var vm = new Models.AppointmentViewModel();
            vm.Position = p;
            vm.Appointments = await AppointmentStore.GetAllAppointment();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Leads = await LeadStore.GetAllLead();
            vm.AppointmentTypes = await AppointmentTypeStore.GetAllAppointmentType();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);

            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Leads.RemoveAll(x => x.AssignTo != id);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Leads.RemoveAll(x => x.AssignTo == null);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }
            else
            {
                vm.Appointments = new List<Appointment>();
            }


            if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5" || p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.AppointmentTypes.RemoveAll(x => x.Name.Substring(0, 3) == "Pre");
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1" || p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.AppointmentTypes.RemoveAll(x => x.Name.Substring(0, 3) != "Pre");
            }
            else
            {
                vm.AppointmentTypes = new List<AppointmentType>();
            }
            vm.Appointments.RemoveAll(x => !vm.AppointmentTypes.Select(z => z.AppointmentTypeId).Contains(x.AppointmentType));

            vm.Appointments = vm.Appointments.FindAll(x => x.RequestId != null);

            return PartialView(vm);
        }
        public async Task<ActionResult> CreateAppointment(string requestId, Appointment a)
        {
            var position = Utils.GetUserGroup();
            var vm = new Models.AppointmentViewModel();
            vm.RequestId = requestId;
            vm.UserId = Utils.GetUserId();
            vm.UserName = Utils.GetUsername();
            vm.Request = await RequestStore.GetRequestByRequestId(requestId);
            vm.UserPosition = await UserPositionStore.GetUserPositionByUserPositionId(position);
            vm.AppointmentTypes = await AppointmentTypeStore.GetAllAppointmentType();
            vm.CommentRemarks = await CommentRemarkStore.GetCommentRemarkByRequestId(vm.Request.RequestId);
            vm.AuditTrails = await AuditTrailStore.GetAuditTrailByRequestId(requestId);
            vm.AuditTrails.RemoveAll(x => x.Type.TrimEnd() != "Appt");
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.CommentRemarks = vm.CommentRemarks.FindAll(x => x.Type.TrimEnd() == "Appt");
            vm.Users = await UserStore.GetAllUser();

            switch (vm.Request.Stage)
            {
                case 2:
                    vm.AppointmentType = vm.AppointmentTypes.Find(x => x.AppointmentTypeId == "3E653BCE-AE1B-46C4-840D-A75A5B4EDF15");
                    break;
                case 11:
                    //switch (vm.Request.RejectWithProposal)
                    //{
                    //    case true:
                    //        vm.AppointmentType = vm.AppointmentTypes.Find(x => x.AppointmentTypeId == "811B6DB1-E243-4829-AC24-B92597D36305");
                    //        break;
                    //    default:
                            vm.AppointmentType = vm.AppointmentTypes.Find(x => x.AppointmentTypeId == "95CBF154-443F-4E49-B33F-F647E9BE5B1B");
                    //        break;
                    //}
                    break;
                case 23:
                    vm.AppointmentType = vm.AppointmentTypes.Find(x => x.AppointmentTypeId == "8307A31F-33C7-44F9-A498-93387E4E918A");
                    break;
                case 30:
                    vm.AppointmentType = vm.AppointmentTypes.Find(x => x.AppointmentTypeId == "7F240754-470A-4EDE-AD18-C2B63CA56271");
                    break;
            }

            if (position == "E207490E-1A8C-405D-B415-B100ADB0D04F" || position == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.CanSee = false;
                vm.CommentRemarks.RemoveAll(x => x.Stage < 15);
                vm.AuditTrails.RemoveAll(x => x.Stage < 15);
                vm.CurrentUser.Where(x => x.Keyword == "Case").Select(z => { z.IsCreate = true; return z; }).ToList();
            }
            else
            {
                vm.CanSee = true;
            }

            return View(vm);
        }
        public async Task<ActionResult> EditAppointment(string appointmentId, string cameFrom)
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.AppointmentViewModel();
            vm.Appointment = await AppointmentStore.GetAppointmentByAppointmentId(appointmentId);
            vm.Request = await RequestStore.GetRequestByRequestId(vm.Appointment.RequestId);
            vm.Documents = await DocumentStore.GetDocumentsByAppointmentId(appointmentId);
            //vm.Documents = vm.Documents.FindAll(x => x.Type == "C56ADBB5-2520-483B-BA80-1DA8CE61E461" && x.RequestId == vm.Request.RequestId);
            vm.CommentRemarks = await CommentRemarkStore.GetCommentRemarkByRequestId(vm.Request.RequestId);
            vm.AuditTrails = await AuditTrailStore.GetAuditTrailByRequestId(vm.Request.RequestId);
            vm.AuditTrails.RemoveAll(x => x.Type.TrimEnd() != "Appt");
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.CommentRemarks = vm.CommentRemarks.FindAll(x => x.Type.TrimEnd() == "Appt");
            vm.Users = await UserStore.GetAllUser();
            vm.UserPositions = await UserPositionStore.GetAllUserPosition();
            vm.AppointmentTypes = await AppointmentTypeStore.GetAllAppointmentType();
            vm.Page = cameFrom;

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4" || p == "E207490E-1A8C-405D-B415-B100ADB0D04F" || p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.CanSee = false;
                vm.CommentRemarks.RemoveAll(x => x.Stage < 15);
                vm.AuditTrails.RemoveAll(x => x.Stage < 15);
            }
            else
            {
                vm.CanSee = true;
            }

            return View(vm);
        }
        public async Task<ActionResult> EditAppointmentPreview(string appointmentId, string cameFrom)
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.AppointmentViewModel();
            vm.Appointment = await AppointmentStore.GetAppointmentByAppointmentId(appointmentId);
            vm.Request = await RequestStore.GetRequestByRequestId(vm.Appointment.RequestId);
            vm.Documents = await DocumentStore.GetAllDocument();
            vm.Documents = vm.Documents.FindAll(x => x.Type == "C56ADBB5-2520-483B-BA80-1DA8CE61E461" && x.RequestId == vm.Request.RequestId);
            vm.CommentRemarks = await CommentRemarkStore.GetCommentRemarkByRequestId(vm.Request.RequestId);
            vm.AuditTrails = await AuditTrailStore.GetAuditTrailByRequestId(vm.Request.RequestId);
            vm.AuditTrails.RemoveAll(x => x.Type.TrimEnd() != "Appt");
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.CommentRemarks = vm.CommentRemarks.FindAll(x => x.Type.TrimEnd() == "Appt");
            vm.Users = await UserStore.GetAllUser();
            vm.UserPositions = await UserPositionStore.GetAllUserPosition();
            vm.AppointmentTypes = await AppointmentTypeStore.GetAllAppointmentType();
            vm.Page = cameFrom;

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4" || p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.CanSee = false;
                vm.CommentRemarks.RemoveAll(x => x.Stage < 15);
                vm.AuditTrails.RemoveAll(x => x.Stage < 15);
            }
            else
            {
                vm.CanSee = true;
            }

            return View(vm);
        }
        public async Task<JsonResult> CreateNewAppointment(string requestId, Appointment appointment, Request request, CommentRemark cr, List<Document> document, AuditTrail auditTrail, string startTime, string endTime)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            cr.CreatedBy = id;
            
            var r = new Response();
            var rStage = await RequestStore.GetRequestByRequestId(request.RequestId);

            if (startTime == "")
            {
                startTime = DateTime.Today.Date.ToString();
                endTime = DateTime.Today.Date.ToString();
                appointment.Date = DateTime.Today.Date;
                appointment.EndDate = DateTime.Today.Date;
            }

            if (!appointment.IsAllDay && !string.IsNullOrEmpty(startTime))
            {
                var v1 = DateTime.Parse(startTime);
                var v2 = DateTime.Parse(endTime);

                var ts1 = TimeSpan.Parse(v1.TimeOfDay.ToString());
                var ts2 = TimeSpan.Parse(v2.TimeOfDay.ToString());

                DateTime startDate = DateTime.Parse(appointment.Date.ToString());
                appointment.Date = startDate.Add(ts1);
                DateTime endDate = DateTime.Parse(appointment.EndDate.ToString());
                appointment.EndDate = endDate.Add(ts2);
            }
            else
            {
                appointment.IsAllDay = true;
            }

            //cr.ProfileId = request.ProfileId;
            cr.RequestId = request.RequestId;
            cr.Stage = rStage.Stage;
            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

            var appts = await AppointmentTypeStore.GetAllAppointmentType();

            if (appts != null || appts.Count != 0)
            {
                var appt = appts.Find(x => x.AppointmentTypeId == appointment.AppointmentType);

                if (appt != null)
                {
                    request.Stage = appt.Stage;
                    request.Status = appt.StatusName;
                    appointment.Status = appt.StatusName;
                }
            }

            appointment.RequestId = request.RequestId;
            appointment.CommentRemarkId = cr.CommentRemarkId;
            appointment.CreatedBy = id;
            appointment.AppointmentId = await AppointmentStore.CreateAppointment(appointment);
            string status = request.Status.Remove(0, 6);
            status = status.Remove(status.Length - 1, 1);

            if (!string.IsNullOrEmpty(appointment.AppointmentId))
            {
                bool b = await DocumentStore.UpdateDocumentsAppointmentIdByDocumentId(document, appointment.AppointmentId);
                request.AppointmentId = appointment.AppointmentId;
                request.LawFirm = rStage.LawFirm;
                request.NegotiatorId = rStage.NegotiatorId;
                request.AppointmentId = await RequestStore.UpdateRequest(request);
                

                request.AppointmentId = appointment.AppointmentId;
                var rr = await RequestStore.UpdateRequest(request);

                if (string.IsNullOrEmpty(rr))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(request.RequestId, id, p, rStage.Stage);
                    auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Appt", status, cr.Comment, rStage.Stage);
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Case/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            
            return Json(r);
        }
        public async Task<JsonResult> UpdateAppointment(string requestId, Appointment appointment, Request request, CommentRemark cr, List<Document> document, AuditTrail auditTrail, string startTime, string endTime)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            cr.CreatedBy = id;

            var r = new Response();
            var rStage = await RequestStore.GetRequestByRequestId(request.RequestId);

            if (!appointment.IsAllDay && !string.IsNullOrEmpty(startTime))
            {
                var v1 = DateTime.Parse(startTime);
                var v2 = DateTime.Parse(endTime);

                var ts1 = TimeSpan.Parse(v1.TimeOfDay.ToString());
                var ts2 = TimeSpan.Parse(v2.TimeOfDay.ToString());

                DateTime startDate = DateTime.Parse(appointment.Date.ToString());
                appointment.Date = startDate.Add(ts1);
                DateTime endDate = DateTime.Parse(appointment.EndDate.ToString());
                appointment.EndDate = endDate.Add(ts2);
            }
            else
            {
                appointment.IsAllDay = true;
            }

            cr.RequestId = request.RequestId;
            cr.Stage = rStage.Stage;
            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

            var appts = await AppointmentTypeStore.GetAllAppointmentType();

            if (appts != null || appts.Count != 0)
            {
                var appt = appts.Find(x => x.AppointmentTypeId == appointment.AppointmentType);

                if (appt != null)
                {
                    request.Stage = rStage.Stage;
                    request.Status = appt.StatusName;
                    appointment.Status = appt.StatusName;
                }
            }

            appointment.CommentRemarkId = cr.CommentRemarkId;
            appointment.RequestId = request.RequestId;
            appointment.CreatedBy = id;
            appointment.AppointmentId = await AppointmentStore.UpdateAppointment(appointment);

            if (!string.IsNullOrEmpty(appointment.AppointmentId))
            {
                bool b = await DocumentStore.UpdateDocumentsAppointmentIdByDocumentId(document, appointment.AppointmentId);
                var rr = await RequestStore.UpdateRequest(request);

                if (string.IsNullOrEmpty(rr))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(request.RequestId, id, p, rStage.Stage);
                    auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Appt", "Appointment Updated", cr.Comment, rStage.Stage);
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Case/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> DeleteAppointment(string appointmentId)
        {
            var r = new Response();
            var dd = await AppointmentStore.DeleteAppointment(appointmentId);
            r.Message = "Appointment successfully deleted.";

            return Json(r);
        }
        public async Task<JsonResult> AttachmentUpload()
        {
            var RequestId = Request.Params.GetValues("RequestId")[0];
            var Type = Request.Params.GetValues("Type")[0];
            var folder = "Documents";
            var docType = await DocumentTypeStore.GetAllDocumentType();

            string Flocation = docType.Find(x => x.DocumentTypeId == Type).Name;

            HttpPostedFileBase file = Request.Files[0]; //Uploaded file
                                                        //Use the following properties to get file's name, size and MIMEType
            int fileSize = file.ContentLength;
            string fileName = file.FileName;
            string ext = fileName.Substring(fileName.LastIndexOf('.'));
            string mimeType = file.ContentType;
            string path = folder + "/" + RequestId + "/" + Flocation + "/";
            string location = Server.MapPath("~") + path;
            System.IO.Stream fileContent = file.InputStream;
            System.IO.Directory.CreateDirectory(location);
            //To save file, use SaveAs method
            file.SaveAs(location + fileName); //File will be saved in application root

            var d = new ObjectClassesLibrary.Classes.Document()
            {
                Path = path,
                RequestId = RequestId,
                Type = Type,
                FileName = fileName,
                Size = fileSize,
                CreatedDate = DateTime.Now,
            };

            var dd = await DocumentStore.CreateAppointmentDocument(d);
            dd.CreatedDateString = d.CreatedDate.ToString("dd MMM yyyy");
            dd.Type = docType.Find(x => x.DocumentTypeId == Type).Name;

            return Json(dd);
        }
        public async Task<JsonResult> DeleteAttachment(string documentId)
        {
            var r = new Response();

            var d = await DocumentStore.DeleteAppointmentDocument(documentId);
            r.Message = "Document successfully removed.";

            return Json(r);
        }
    }
}