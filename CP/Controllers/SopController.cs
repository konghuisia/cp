﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class SopController : Controller
    {
        SopRequestsStore SopRequestsStore;
        AuditTrailStore AuditTrailStore;
        SopStageStore SopStageStore;
        SopShowInSelectStore SopShowInSelectStore;
        SopDocumentsStore SopDocumentsStore;
        DocumentTypeStore DocumentTypeStore;
        ProfileStore ProfileStore;
        RequestStore RequestStore;
        LeadStore LeadStore;
        DraftStore DraftStore;
        AddressStore AddressStore;
        CommentRemarkStore CommentRemarkStore;
        PersonStore PersonStore;
        AssetForInvestigationStore AssetForInvestigationStore;
        CompanyFinancialInformationStore CompanyFinancialInformationStore;
        LitigationRecordStore LitigationRecordStore;
        BlackListingRecordStore BlackListingRecordStore;

        public SopController()
        {
            SopRequestsStore = DependencyResolver.Current.GetService<SopRequestsStore>();
            AuditTrailStore = DependencyResolver.Current.GetService<AuditTrailStore>();
            SopStageStore = DependencyResolver.Current.GetService<SopStageStore>();
            SopShowInSelectStore = DependencyResolver.Current.GetService<SopShowInSelectStore>();
            SopDocumentsStore = DependencyResolver.Current.GetService<SopDocumentsStore>();
            DocumentTypeStore = DependencyResolver.Current.GetService<DocumentTypeStore>();
            ProfileStore = DependencyResolver.Current.GetService<ProfileStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            LeadStore = DependencyResolver.Current.GetService<LeadStore>();
            DraftStore = DependencyResolver.Current.GetService<DraftStore>();
            AddressStore = DependencyResolver.Current.GetService<AddressStore>();
            CommentRemarkStore = DependencyResolver.Current.GetService<CommentRemarkStore>();
            PersonStore = DependencyResolver.Current.GetService<PersonStore>();
            AssetForInvestigationStore = DependencyResolver.Current.GetService<AssetForInvestigationStore>();
            CompanyFinancialInformationStore = DependencyResolver.Current.GetService<CompanyFinancialInformationStore>();
            LitigationRecordStore = DependencyResolver.Current.GetService<LitigationRecordStore>();
            BlackListingRecordStore = DependencyResolver.Current.GetService<BlackListingRecordStore>();
        }
        public async Task<JsonResult> CreateSopRequest(SopRequests sopRequests, string requestId, AuditTrail auditTrail)
        {
            var id = Utils.GetUserId();
            var sopStage = new List<SopStage>();
            var r = new Response();

            sopStage = await SopStageStore.GetSopStageBySopId(sopRequests.SopId);

            var request = await RequestStore.GetRequestByRequestId(requestId);
            request.Status = "Case (" + sopStage[0].Status + ")";
            
            var rr = await RequestStore.UpdateRequest(request);

            if (sopStage != null)
            {
                sopRequests.SopRequestid = await SopRequestsStore.CreateSopRequests(sopRequests, id);

                if (!string.IsNullOrEmpty(sopRequests.SopRequestid) && !string.IsNullOrEmpty(rr))
                {
                    auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(requestId, id, "Lead", sopStage[0].Status, "", 14);
                }
                else
                {
                    r.Error = "";
                }
            }

            return Json(r);
        }
        // GET: Sop
        public async Task<ActionResult> LoadSop(string page, string requestId, string sopId, string sopRequestId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.SopViewModel();
            vm.SopShowInSelect = await SopShowInSelectStore.GetSopShowInSelectBySopId(sopId);
            vm.SopDocuments = await SopDocumentsStore.GetSopDocumentBySopId(sopRequestId);
            vm.DocumentTypes = await DocumentTypeStore.GetAllDocumentType();
            vm.SopStage = await SopStageStore.GetSopStageBySopId(sopId);
            vm.SopRequest = await SopRequestsStore.GetSopRequestsBySopRequestId(sopRequestId);
            vm.SopDocuments = vm.SopDocuments.OrderByDescending(x => x.CreatedDate).ToList();

            switch (page)
            {
                case "_WSS":
                    break;
                case "_WSSPreview":
                    break;
            }


            return PartialView(page, vm);
        }
        public async Task<JsonResult> AttachmentUpload()
        {
            string id = Utils.GetUserId();

            var RequestId = Request.Params.GetValues("RequestId")[0];
            var Type = Request.Params.GetValues("Type")[0];
            var ProfileId = Request.Params.GetValues("ProfileId")[0];
            var SopRequestId = Request.Params.GetValues("SopRequestId")[0];

            var folder = "Documents";
            var docType = await DocumentTypeStore.GetAllDocumentType();

            string Flocation = docType.Find(x => x.DocumentTypeId == Type).Name;

            HttpPostedFileBase file = Request.Files[0]; //Uploaded file
                                                        //Use the following properties to get file's name, size and MIMEType
            int fileSize = file.ContentLength;
            string fileName = file.FileName;
            string ext = fileName.Substring(fileName.LastIndexOf('.'));
            string mimeType = file.ContentType;
            string path = folder + "/" + RequestId + "/" + Flocation + "/";
            string location = Server.MapPath("~") + path;
            System.IO.Stream fileContent = file.InputStream;
            System.IO.Directory.CreateDirectory(location);
            //To save file, use SaveAs method
            file.SaveAs(location + fileName); //File will be saved in application root

            var d = new ObjectClassesLibrary.Classes.SopDocuments()
            {
                Path = path,
                SopId = SopRequestId,
                RequestId = RequestId,
                Type = Type,
                FileName = fileName,
                Size = fileSize,
                CreatedDate = DateTime.Now,
            };

            var dd = await SopDocumentsStore.CreateSopDocuments(d);

            dd.CreatedDateString = d.CreatedDate.ToString("dd MMM yyyy");
            dd.Type = docType.Find(x => x.DocumentTypeId == Type).Name;
            dd.Path = d.Path;

            return Json(dd);
        }
        public async Task<JsonResult> DeleteAttachment(string documentId)
        {
            var r = new Response();

            var d = await SopDocumentsStore.DeleteSopDocuments(documentId);
            r.Message = "Document successfully removed.";

            return Json(r);
        }
        public async Task<JsonResult> UpdateRequestSop(Profile profile, Address address, Person person, CommentRemark cr, Request request, Lead lead, AssetForInvestigation afi, IList<CompanyFinancialInformation> cfi, IList<LitigationRecord> lr, IList<BlackListingRecord> br, bool boo, CommentRemark crRmo, AuditTrail auditTrail, string stage, Draft draft, string SopRequestId, string SopId)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();

            cr.RequestId = request.RequestId;
            cr.CreatedBy = id;

            draft = await DraftStore.GetDraftByRequestId(request.RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }

            var rStage = await RequestStore.GetRequestByRequestId(request.RequestId);
            bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(request.RequestId, id, p, rStage.Stage);

            List<SopDocuments> documents = await SopDocumentsStore.GetSopDocumentBySopId(SopRequestId);
            List<SopShowInSelect> showInSelects = await SopShowInSelectStore.GetSopShowInSelectBySopId(SopId);
            showInSelects.RemoveAll(x => x.Deleted == true);

            address.ProfileId = profile.ProfileId;
            person.ProfileId = profile.ProfileId;
            cr.ProfileId = profile.ProfileId;
            lead.RequestId = request.RequestId;
            cr.Stage = rStage.Stage;

            profile.ProfileId = await ProfileStore.UpdateProfile(profile);
            address.AddressId = await AddressStore.UpdateAddress(address);
            person.PersonId = await PersonStore.UpdatePerson(person);
            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
            lead.LeadId = await LeadStore.UpdateLead(lead);

            request.ProfileId = profile.ProfileId;
            request.PersonId = person.PersonId;
            request.AddressId = address.AddressId;
            request.LeadId = lead.LeadId;
            afi.AssetForInvestigationId = await AssetForInvestigationStore.UpdateAssetForInvestigation(afi, id, profile.ProfileId);
            bool c = await CompanyFinancialInformationStore.UpdateCompanyFinancialInformation(cfi, id, profile.ProfileId);
            bool l = await LitigationRecordStore.UpdateLitigationRecord(lr, id, profile.ProfileId);
            bool b = await BlackListingRecordStore.UpdateBlackListingRecord(br, id, profile.ProfileId);

            var docs = await SopDocumentsStore.GetSopDocumentBySopId(SopRequestId);
            var doc1 = docs.ToList();

            var requiredDocs = showInSelects.FindAll(x => x.Required == true);
            var soprequest = await SopRequestsStore.GetSopRequestsBySopRequestId(SopRequestId);
            var rd1 = requiredDocs;
            rd1.RemoveAll(x => x.Stage > soprequest.Stage);

            boo = false;
            if (requiredDocs.Count == 0)
            {
                boo = true;
            }
            else
            {
                int i = 0;
                foreach (var rd in rd1)
                {
                    var cc = doc1.Find(x => x.Type == rd.DocumentTypeId);

                    if (cc != null)
                    {
                        doc1.RemoveAll(x => x.DocumentId == cc.DocumentId);
                        i++;
                    }
                }
                if (i >= rd1.Count)
                {
                    boo = true;
                }
            }

            var sops = await SopStageStore.GetSopStageBySopId(SopId);

            if (soprequest != null)
            {
                var doc2 = docs.ToList();
                int sopstage = soprequest.Stage + 1;
                var i = sops.Find(x => x.Stage == sopstage);
                request.Stage = rStage.Stage;
                request.Status = rStage.Status;

                if (boo)
                {
                    if (i != null)
                    {
                        soprequest.Stage = i.Stage;
                        request.Status = "Case (" + i.Status + ")";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", i.Status + ".", "", 14);
                    }
                    else
                    {
                        soprequest.Status = true;
                        request.Status = "Case (SOP - Submit to close case)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "SOP - Submit to close case.", "", 14);
                    }
                }
                else
                {
                    if (soprequest.Stage > 1)
                    {
                        bool boo2 = false;
                        var rd2 = requiredDocs;
                        rd2.RemoveAll(x => x.Stage > soprequest.Stage - 1);

                        int i2 = 0;
                        foreach (var rd in rd2)
                        {
                            var cc = doc2.Find(x => x.Type == rd.DocumentTypeId);

                            if (cc != null)
                            {
                                doc2.RemoveAll(x => x.DocumentId == cc.DocumentId);
                                i2++;
                            }
                        }
                        if (i2 >= rd2.Count)
                        {
                            boo2 = true;
                        }

                        if (!boo2)
                        {
                            var sop = sops.Find(x => x.Stage == soprequest.Stage - 1);
                            request.Status = "Case (" + sop.Status + ")";
                            soprequest.Stage = sop.Stage;
                            auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", sop.Status + ".", "", 14);
                        }
                    }
                }

                soprequest.SopRequestid = await SopRequestsStore.UpdateSopRequests(soprequest, id);
            }


            var rr = await RequestStore.UpdateRequest(request);

            if (string.IsNullOrEmpty(rr))
            {
                r.Error = "Unable to save record, please try again later.";
            }
            else
            {
                r.Message = "Record saved successfully.";

                if (request.Stage < 5)
                {
                    r.RedirectUrl = "/Case/Choice?module=Lead";
                }
                else
                {
                    r.RedirectUrl = "/Case/Choice?module=Case";
                }
            }
            return Json(r);
        }
    }
}