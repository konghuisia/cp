﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class CloseController : Controller
    {
        UserStore UserStore;
        RequestStore RequestStore;
        ProfileStore ProfileStore;
        LeadStore LeadStore;
        SourceStore SourceStore;
        StatusStore StatusStore;
        UserAccessStore UserAccessStore;

        public CloseController()
        {
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            ProfileStore = DependencyResolver.Current.GetService<ProfileStore>();
            LeadStore = DependencyResolver.Current.GetService<LeadStore>();
            SourceStore = DependencyResolver.Current.GetService<SourceStore>();
            StatusStore = DependencyResolver.Current.GetService<StatusStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
        }
        // GET: Close
        public async Task<ActionResult> _Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.LeadIndexViewModel();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Leads = await LeadStore.GetAllLead();
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.Statuses = await StatusStore.StatusList();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.UserId = id;

            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);
            vm.Requests.RemoveAll(x => x.Status == null);
            vm.Requests = vm.Requests.FindAll(x => x.Stage == 21 || x.Stage == 35);
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Leads.RemoveAll(x => x.AssignTo != id);
                vm.Requests.RemoveAll(x => x.Stage < 1);
            }
            else if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                vm.Requests.RemoveAll(x => x.Approval1User == null && x.Approval2User == null);
            }
            else if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                vm.Requests.RemoveAll(x => x.LawFirm != pid);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")
            {
                vm.Requests.RemoveAll(x => x.Stage < 33);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Leads.RemoveAll(x => x.AssignTo == null);
                vm.Requests.RemoveAll(x => x.Stage < 1);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }
            else if (p == "A4C2EBBE-9391-44AF-BD49-572BBC9EB22E")
            {
                vm.Requests.RemoveAll(x => x.Stage == 1);
            }
            foreach (var v in vm.Requests)
            {
                v.AccessStatus = new ObjectClassesLibrary.Classes.LeadUserHasAccess().checkAccess(p, v.Stage, id);
            }

            return PartialView(vm);
        }
    }
}
