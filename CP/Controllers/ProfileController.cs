﻿using DatabaseHelper.Store;
using IdentityManagement.Utilities;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class ProfileController : Controller
    {
        AddressStore AddressStore;
        CommentRemarkStore CommentRemarkStore;
        PersonStore PersonStore;
        ProfileStore ProfileStore;
        SourceStore SourceStore;
        UserPositionStore UserPositionStore;
        lawFirmLawFirmProfileStore lawFirmLawFirmProfileStore;
        NegotiatorProfileStore NegotiatorProfileStore;
        AuditTrailStore AuditTrailStore;
        UserStore UserStore;
        DebtorStore DebtorStore;
        UserAccessStore UserAccessStore;
        StateCityStore StateCityStore;
        public ProfileController()
        {
            AddressStore = DependencyResolver.Current.GetService<AddressStore>();
            CommentRemarkStore = DependencyResolver.Current.GetService<CommentRemarkStore>();
            PersonStore = DependencyResolver.Current.GetService<PersonStore>();
            ProfileStore = DependencyResolver.Current.GetService<ProfileStore>();
            SourceStore = DependencyResolver.Current.GetService<SourceStore>();
            UserPositionStore = DependencyResolver.Current.GetService<UserPositionStore>();
            lawFirmLawFirmProfileStore = DependencyResolver.Current.GetService<lawFirmLawFirmProfileStore>();
            NegotiatorProfileStore = DependencyResolver.Current.GetService<NegotiatorProfileStore>();
            AuditTrailStore = DependencyResolver.Current.GetService<AuditTrailStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            DebtorStore = DependencyResolver.Current.GetService<DebtorStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            StateCityStore = DependencyResolver.Current.GetService<StateCityStore>();
        }
        public async Task<ActionResult> _Creditor()
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.ProfileIndexViewModel();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Profiles.RemoveAll(x => x.ProfileType == 2);
            vm.Sources = await SourceStore.GetAllSource();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            return PartialView(vm);
        }
        public async Task<ActionResult> _Debtor()
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.ProfileIndexViewModel();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Profiles.RemoveAll(x => x.ProfileType == 1);
            vm.Sources = await SourceStore.GetAllSource();
            vm.Debtors = await DebtorStore.GetAllDebtor();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);

            return PartialView(vm);
        }
        public async Task<ActionResult> _LawFirm()
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.ProfileIndexViewModel();
            vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            //vm.Profiles.RemoveAll(x => x.ProfileType == 2);
            return PartialView(vm);
        }
        public async Task<ActionResult> _Negotiator()
        {
            var vm = new Models.ProfileIndexViewModel();
            vm.NegotiatorProfiles = await NegotiatorProfileStore.GetAllNegotiatorProfile();
            //vm.Profiles.RemoveAll(x => x.ProfileType == 2);
            return PartialView(vm);
        }
        public async Task<ActionResult> Index()
        {
            var vm = new Models.ProfileIndexViewModel();
            vm.Profiles = await ProfileStore.GetAllProfile();

            return View(vm);
        }
        public async Task<ActionResult> CreateProfile(string page, int profileType)
        {
            var position = Utils.GetUserGroup();
            var vm = new Models.CreateProfileViewModel();
            var users = await UserStore.GetAllUser();

            vm.UserPosition = await UserPositionStore.GetUserPositionByUserPositionId(position);
            vm.Sources = await SourceStore.GetAllSource();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.User = users.Find(x => x.UserId == Utils.GetUserId());
            vm.State = await StateCityStore.StateList();
            vm.City = await StateCityStore.CityList();
            vm.ProfileType = profileType;
            vm.Page = page;

            return View(vm);
        }
        public async Task<ActionResult> CreateLawFirmProfile()
        {
            var position = Utils.GetUserGroup();
            var vm = new Models.CreateProfileViewModel();
            var users = await UserStore.GetAllUser();
            vm.UserPosition = await UserPositionStore.GetUserPositionByUserPositionId(position);
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.User = users.Find(x => x.UserId == Utils.GetUserId());
            vm.State = await StateCityStore.StateList();
            vm.City = await StateCityStore.CityList();

            return View(vm);
        }
        public async Task<ActionResult> CreateNegotiatorProfile()
        {
            var position = Utils.GetUserGroup();
            var vm = new Models.CreateProfileViewModel();
            vm.UserPosition = await UserPositionStore.GetUserPositionByUserPositionId(position);
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);

            return View(vm);
        }
        public JsonResult NewSubscription(Profile profile, Address address, Person person, CommentRemark cr, ProfileAuditTrail profileAuditTrail, Lead lead, Request request)
        {
            var id = Utils.GetUserId();
            var r = new Response();

            TempData["profile"] = profile;
            TempData["address"] = address;
            TempData["person"] = person;
            TempData["cr"] = cr;
            //TempData["audittrail"] = profileAuditTrail;
            TempData["request"] = request;
            TempData["lead"] = lead;
            
            r.RedirectUrl = "/Subscriptions/NewSubscription";
            return Json(r);
        }
        public async Task<ActionResult> Choice(string page)
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var vm = new Models.HomePageViewModel();

            vm.Page = page;
            if (string.IsNullOrEmpty(page))
            {
                vm.Page = "Creditor";
            }

            return View(vm);
        }
        public async Task<ActionResult> EditProfile(string ProfileId, string page)
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.UpdateProfileViewModel();
            vm.Profile = await ProfileStore.GetProfileByProfileId(ProfileId);
            vm.Address = await AddressStore.GetAddressByProfileId(ProfileId);
            vm.Person = await PersonStore.GetPersonByProfileId(ProfileId);
            vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByProfileId(ProfileId);
            vm.ProfileAuditTrails = await AuditTrailStore.GetProfileAuditTrailByProfileId(ProfileId);
            vm.Positions = await UserPositionStore.GetAllUserPosition();
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.CommentRemark.RemoveAll(x => x.Type.TrimEnd() != "Profile");
            vm.State = await StateCityStore.StateList();
            vm.City = await StateCityStore.CityList();
            vm.Page = page;

            return View(vm);
        }
        public async Task<ActionResult> EditProfilePreview(string ProfileId, string page)
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.UpdateProfileViewModel();
            vm.Profile = await ProfileStore.GetProfileByProfileId(ProfileId);
            vm.Address = await AddressStore.GetAddressByProfileId(ProfileId);
            vm.Person = await PersonStore.GetPersonByProfileId(ProfileId);
            vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByProfileId(ProfileId);
            vm.ProfileAuditTrails = await AuditTrailStore.GetProfileAuditTrailByProfileId(ProfileId);
            vm.Positions = await UserPositionStore.GetAllUserPosition();
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.CommentRemark.RemoveAll(x => x.Type.TrimEnd() != "Profile");
            vm.State = await StateCityStore.StateList();
            vm.City = await StateCityStore.CityList();
            vm.Page = page;

            return View(vm);
        }
        public async Task<ActionResult> EditLawFirmProfile(string ProfileId)
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.UpdateProfileViewModel();
            vm.Users = await UserStore.GetAllUser();
            vm.LawFirmProfile = await lawFirmLawFirmProfileStore.GetLawFirmProfileByLawFirmProfileId(ProfileId);
            vm.Address = await AddressStore.GetAddressByProfileId(ProfileId);
            vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByProfileId(ProfileId);
            vm.Positions = await UserPositionStore.GetAllUserPosition();
            vm.ProfileAuditTrails = await AuditTrailStore.GetProfileAuditTrailByProfileId(ProfileId);
            vm.Users = await UserStore.GetAllUser();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.CommentRemark.RemoveAll(x => x.Type.TrimEnd() != "Profile");
            vm.State = await StateCityStore.StateList();
            vm.City = await StateCityStore.CityList();

            return View(vm);
        }
        public async Task<ActionResult> EditLawFirmProfilePreview(string ProfileId)
        {
            var p = Utils.GetUserGroup();
            var vm = new Models.UpdateProfileViewModel();
            vm.Users = await UserStore.GetAllUser();
            vm.LawFirmProfile = await lawFirmLawFirmProfileStore.GetLawFirmProfileByLawFirmProfileId(ProfileId);
            vm.Address = await AddressStore.GetAddressByProfileId(ProfileId);
            vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByProfileId(ProfileId);
            vm.Positions = await UserPositionStore.GetAllUserPosition();
            vm.ProfileAuditTrails = await AuditTrailStore.GetProfileAuditTrailByProfileId(ProfileId);
            vm.Users = await UserStore.GetAllUser();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.CommentRemark.RemoveAll(x => x.Type.TrimEnd() != "Law Firm");
            vm.State = await StateCityStore.StateList();
            vm.City = await StateCityStore.CityList();

            return View(vm);
        }
        public async Task<ActionResult> EditNegotiatorProfile(string ProfileId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.UpdateProfileViewModel();
            vm.Users = await UserStore.GetAllUser();
            vm.NegotiatorProfile = await NegotiatorProfileStore.GetNegotiatorProfileByNegotiatorProfileId(ProfileId);
            vm.Address = await AddressStore.GetAddressByProfileId(ProfileId);
            vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByProfileId(ProfileId);
            vm.Position = await UserPositionStore.GetUserPositionByUserPositionId(position);
            vm.ProfileAuditTrails = await AuditTrailStore.GetProfileAuditTrailByProfileId(ProfileId);
            vm.CommentRemark.RemoveAll(x => x.Type.TrimEnd() != "Law Firm");

            return View(vm);
        }
        public async Task<ActionResult> EditNegotiatorProfilePreview(string ProfileId)
        {
            string position = Utils.GetUserGroup();
            var vm = new Models.UpdateProfileViewModel();
            vm.Users = await UserStore.GetAllUser();
            vm.NegotiatorProfile = await NegotiatorProfileStore.GetNegotiatorProfileByNegotiatorProfileId(ProfileId);
            vm.Address = await AddressStore.GetAddressByProfileId(ProfileId);
            vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByProfileId(ProfileId);
            vm.Position = await UserPositionStore.GetUserPositionByUserPositionId(position);
            vm.ProfileAuditTrails = await AuditTrailStore.GetProfileAuditTrailByProfileId(ProfileId);
            vm.CommentRemark.RemoveAll(x => x.Type != "Profile");

            return View(vm);
        }
        public async Task<JsonResult> CreateNewProfile(Profile profile, Address address, Person person, CommentRemark cr, ProfileAuditTrail profileAuditTrail)
        {
            var id = Utils.GetUserId();
            var r = new Response();
            profile.ProfileId = await ProfileStore.CreateProfile(profile,id);

            if (!string.IsNullOrEmpty(profile.ProfileId))
            {
                address.ProfileId = profile.ProfileId;
                person.ProfileId = profile.ProfileId;
                cr.ProfileId = profile.ProfileId;

                address.AddressId = await AddressStore.CreateAddress(address);
                person.PersonId = await PersonStore.CreatePerson(person);
                cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

                profileAuditTrail.AuditTrailId = await AuditTrailStore.CreateProfileAuditTrail(address.ProfileId, id, "Profile", "Profile created.", cr.Comment);

                if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(person.PersonId) || string.IsNullOrEmpty(profileAuditTrail.AuditTrailId))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Profile/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }

        public JsonResult NewSubscription(Profile profile, Address address, Person person, CommentRemark cr, ProfileAuditTrail profileAuditTrail)
        {
            var id = Utils.GetUserId();
            var r = new Response();
            //profile.ProfileId = await ProfileStore.CreateProfile(profile, id);

            TempData["profile"] = profile;
            TempData["address"] = address;
            TempData["person"] = person;
            TempData["cr"] = cr;
            TempData["audittrail"] = profileAuditTrail;

            //if (!string.IsNullOrEmpty(profile.ProfileId))
            //{
            //    address.ProfileId = profile.ProfileId;
            //    person.ProfileId = profile.ProfileId;
            //    cr.ProfileId = profile.ProfileId;

            //    address.AddressId = await AddressStore.CreateAddress(address);
            //    person.PersonId = await PersonStore.CreatePerson(person);
            //    cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

            //profileAuditTrail.AuditTrailId = await AuditTrailStore.CreateProfileAuditTrail(address.ProfileId, id, "Profile", "Profile created.", cr.Comment);

            //    if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(person.PersonId) || string.IsNullOrEmpty(profileAuditTrail.AuditTrailId))
            //    {
            //        r.Error = "Unable to save record, please try again later.";
            //    }
            //    else
            //    {
            //        r.Message = "Record saved successfully.";
            //        r.RedirectUrl = "/Profile/Choice";
            //    }
            //}
            //else
            //{
            //    r.Error = "Unable to save record, please try again later.";
            //}
            r.RedirectUrl = "/Subscriptions/NewSubscription";
            return Json(r);
        }
        public async Task<JsonResult> CreateNewLawFirm(LawFirmProfile profile, Address address, CommentRemark cr, ProfileAuditTrail profileAuditTrail, User user)
        {
            var id = Utils.GetUserId();
            var r = new Response();
            profile.LawFirmProfileId = await lawFirmLawFirmProfileStore.CreateLawFirmProfile(profile, id);

            if (!string.IsNullOrEmpty(profile.LawFirmProfileId))
            {
                //user.Name = profile.Name;
                //user.Position = "47461275-5753-47D1-8A8B-9A26D91E03A4";
                //user.ProfileId = profile.LawFirmProfileId;
                //user.Password = profile.Name;
                //user.Active = profile.Active;
                //user.UserId = await UserStore.CreateNewUser(user, id);

                address.ProfileId = profile.LawFirmProfileId;
                cr.ProfileId = profile.LawFirmProfileId;

                address.AddressId = await AddressStore.CreateAddress(address);
                cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                profileAuditTrail.AuditTrailId = await AuditTrailStore.CreateProfileAuditTrail(address.ProfileId, id, "Profile", "Law Firm created.", cr.Comment);


                if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(profileAuditTrail.AuditTrailId))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Profile/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> CreateNewNegotiator(NegotiatorProfile profile, Address address, CommentRemark cr, ProfileAuditTrail profileAuditTrail, User user)
        {
            var id = Utils.GetUserId();
            var r = new Response();
            profile.NegotiatorProfileId = await NegotiatorProfileStore.CreateNegotiatorProfile(profile, id);

            if (!string.IsNullOrEmpty(profile.NegotiatorProfileId))
            {
                //user.Name = profile.Name;
                //user.Position = "E207490E-1A8C-405D-B415-B100ADB0D04F";
                //user.ProfileId = profile.NegotiatorProfileId;
                //user.Password = profile.Name;
                //user.Active = profile.Active;
                //user.UserId = await UserStore.CreateNewUser(user, id);

                address.ProfileId = profile.NegotiatorProfileId;
                cr.ProfileId = profile.NegotiatorProfileId;

                address.AddressId = await AddressStore.CreateAddress(address);
                cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                profileAuditTrail.AuditTrailId = await AuditTrailStore.CreateProfileAuditTrail(address.ProfileId, id, "Profile", "Law Firm created.", cr.Comment);


                if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(profileAuditTrail.AuditTrailId))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Profile/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateProfile(Profile profile, Address address, Person person, CommentRemark cr, ProfileAuditTrail profileAuditTrail)
        {
            var id = Utils.GetUserId();
            var r = new Response();
            profile.ProfileId = await ProfileStore.UpdateProfile(profile);

            if (!string.IsNullOrEmpty(profile.ProfileId))
            {
                address.ProfileId = profile.ProfileId;
                person.ProfileId = profile.ProfileId;
                cr.ProfileId = profile.ProfileId;

                address.AddressId = await AddressStore.UpdateAddress(address);
                person.PersonId = await PersonStore.UpdatePerson(person);
                cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                profileAuditTrail.AuditTrailId = await AuditTrailStore.CreateProfileAuditTrail(address.ProfileId, id, "Profile", "Profile updated.", cr.Comment);

                if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(person.PersonId) || string.IsNullOrEmpty(profileAuditTrail.AuditTrailId))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Profile/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateLawFirmProfile(LawFirmProfile profile, Address address, CommentRemark cr, ProfileAuditTrail profileAuditTrail, User user)
        {
            var id = Utils.GetUserId();
            var r = new Response();
            profile.LawFirmProfileId = await lawFirmLawFirmProfileStore.UpdateLawFirmProfile(profile, id);

            if (!string.IsNullOrEmpty(profile.LawFirmProfileId))
            {
                //user = await UserStore.GetUserByProfileId(profile.LawFirmProfileId);

                //user.Name = profile.Name;
                //user.Position = "47461275-5753-47D1-8A8B-9A26D91E03A4";
                //user.ProfileId = profile.LawFirmProfileId;
                //user.Password = profile.Name;
                //user.Active = profile.Active;
                //user.UserId = await UserStore.UpdateUserByUserId(user, id);

                address.ProfileId = profile.LawFirmProfileId;
                cr.ProfileId = profile.LawFirmProfileId;

                address.AddressId = await AddressStore.UpdateAddress(address);
                cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                profileAuditTrail.AuditTrailId = await AuditTrailStore.CreateProfileAuditTrail(address.ProfileId, id, "Profile", "Law Firm updated.", cr.Comment);

                if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(profileAuditTrail.AuditTrailId))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Profile/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateNegotiatorProfile(NegotiatorProfile profile, Address address, CommentRemark cr, ProfileAuditTrail profileAuditTrail, User user)
        {
            var id = Utils.GetUserId();
            var r = new Response();
            profile.NegotiatorProfileId = await NegotiatorProfileStore.UpdateNegotiatorProfile(profile, id);

            if (!string.IsNullOrEmpty(profile.NegotiatorProfileId))
            {
                //user = await UserStore.GetUserByProfileId(profile.NegotiatorProfileId);

                //user.Name = profile.Name;
                //user.Position = "E207490E-1A8C-405D-B415-B100ADB0D04F";
                //user.ProfileId = profile.NegotiatorProfileId;
                //user.Password = profile.Name;
                //user.Active = profile.Active;
                //user.UserId = await UserStore.UpdateUserByUserId(user, id);

                address.ProfileId = profile.NegotiatorProfileId;
                cr.ProfileId = profile.NegotiatorProfileId;

                address.AddressId = await AddressStore.UpdateAddress(address);
                cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                profileAuditTrail.AuditTrailId = await AuditTrailStore.CreateProfileAuditTrail(address.ProfileId, id, "Profile", "Law Firm updated.", cr.Comment);

                if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(profileAuditTrail.AuditTrailId))
                {
                    r.Error = "Unable to save record, please try again later.";
                }
                else
                {
                    r.Message = "Record saved successfully.";
                    r.RedirectUrl = "/Profile/Choice";
                }
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> DeleteProfile(string profileId)
        {
            var r = new Response();
            var p = await ProfileStore.DeleteProfile(profileId);

            r.Message = "Profile successfully deleted.";
            r.RedirectUrl = "/Profile/Index";
            return Json(r);
        }
        public async Task<JsonResult> DeleteLawfirm(string profileId)
        {
            var r = new Response();
            var p = await ProfileStore.DeleteLawfirmProfile(profileId);

            r.Message = "Profile successfully deleted.";
            r.RedirectUrl = "/Profile/Index";
            return Json(r);
        }
    }
}