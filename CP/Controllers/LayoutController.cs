﻿using CP.Models;
using IdentityManagement.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CP.Controllers
{
    public class LayoutController : Controller
    {
        // GET: Layout
        public ActionResult _HeaderIcons()
        {
            var vm = new HeaderIconViewModel();

            if (Utils.IfUserAuthenticated())
            {
                vm.Name = Utils.GetUsername();
                vm.Position = Utils.GetUserGroup();
            }

            return PartialView(vm);
        }
    }
}