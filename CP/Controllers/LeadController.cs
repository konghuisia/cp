﻿using DatabaseHelper.Store;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using WordToPDF;
using System.Text;
using ObjectClassesLibrary;
using IdentityManagement.Utilities;

namespace CP.Controllers
{
    public class LeadController : Controller
    {
        AddressStore AddressStore;
        CommentRemarkStore CommentRemarkStore;
        DocumentStore DocumentStore;
        DocumentTypeStore DocumentTypeStore;
        PersonStore PersonStore;
        ProfileStore ProfileStore;
        RequestStore RequestStore;
        LeadStore LeadStore;
        UserStore UserStore;
        AppointmentStore AppointmentStore;
        StatusOfCaseStore StatusOfCaseStore;
        AssetForInvestigationStore AssetForInvestigationStore;
        CompanyFinancialInformationStore CompanyFinancialInformationStore;
        LitigationRecordStore LitigationRecordStore;
        BlackListingRecordStore BlackListingRecordStore;
        SourceStore SourceStore;
        AuditTrailStore AuditTrailStore;
        UserPositionStore UserPositionStore;
        DebtorStore DebtorStore;
        AppointmentTypeStore AppointmentTypeStore;
        ShowInSelectStore ShowInSelectStore;
        lawFirmLawFirmProfileStore lawFirmLawFirmProfileStore;
        DraftStore DraftStore;
        StateCityStore StateCityStore;
        StatusStore StatusStore;
        UserAccessStore UserAccessStore;
        SopStore SopStore;
        SopRequestsStore SopRequestsStore;
        RequestServiceStore RequestServiceStore;
        PackageStore PackageStore;
        SubscriptionStore SubscriptionStore;

        public LeadController()
        {
            AddressStore = DependencyResolver.Current.GetService<AddressStore>();
            CommentRemarkStore = DependencyResolver.Current.GetService<CommentRemarkStore>();
            DocumentStore = DependencyResolver.Current.GetService<DocumentStore>();
            DocumentTypeStore = DependencyResolver.Current.GetService<DocumentTypeStore>();
            PersonStore = DependencyResolver.Current.GetService<PersonStore>();
            ProfileStore = DependencyResolver.Current.GetService<ProfileStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            LeadStore = DependencyResolver.Current.GetService<LeadStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            AppointmentStore = DependencyResolver.Current.GetService<AppointmentStore>();
            StatusOfCaseStore = DependencyResolver.Current.GetService<StatusOfCaseStore>();
            AssetForInvestigationStore = DependencyResolver.Current.GetService<AssetForInvestigationStore>();
            CompanyFinancialInformationStore = DependencyResolver.Current.GetService<CompanyFinancialInformationStore>();
            LitigationRecordStore = DependencyResolver.Current.GetService<LitigationRecordStore>();
            BlackListingRecordStore = DependencyResolver.Current.GetService<BlackListingRecordStore>();
            SourceStore = DependencyResolver.Current.GetService<SourceStore>();
            AuditTrailStore = DependencyResolver.Current.GetService<AuditTrailStore>();
            UserPositionStore = DependencyResolver.Current.GetService<UserPositionStore>();
            DebtorStore = DependencyResolver.Current.GetService<DebtorStore>();
            AppointmentTypeStore = DependencyResolver.Current.GetService<AppointmentTypeStore>();
            ShowInSelectStore = DependencyResolver.Current.GetService<ShowInSelectStore>();
            lawFirmLawFirmProfileStore = DependencyResolver.Current.GetService<lawFirmLawFirmProfileStore>();
            DraftStore = DependencyResolver.Current.GetService<DraftStore>();
            StateCityStore = DependencyResolver.Current.GetService<StateCityStore>();
            StatusStore = DependencyResolver.Current.GetService<StatusStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            SopStore = DependencyResolver.Current.GetService<SopStore>();
            SopRequestsStore = DependencyResolver.Current.GetService<SopRequestsStore>();
            RequestServiceStore = DependencyResolver.Current.GetService<RequestServiceStore>();
            PackageStore = DependencyResolver.Current.GetService<PackageStore>();
            SubscriptionStore = DependencyResolver.Current.GetService<SubscriptionStore>();
        }

        public async Task<ActionResult> _Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                var u = await UserStore.GetUserByUserid(Utils.GetUserId());
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var pid = "";

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                pid = Utils.GetProfileId();
            }

            var vm = new Models.LeadIndexViewModel();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Leads = await LeadStore.GetAllLead();
            vm.Users = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.Statuses = await StatusStore.StatusList();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.Drafts = await DraftStore.GetDraftByCreatedById(id);
            vm.AuditTrails = await AuditTrailStore.GetAllAuditTrail();
            vm.UserId = id;

            vm.Requests.RemoveAll(x => x.ProfileId == null && x.AddressId == null && x.PersonId == null && x.CommentRemarkId == null);
            vm.Requests.RemoveAll(x => x.Status == null);
            vm.Requests.RemoveAll(x => x.Status.Length >= 4 && x.Status.Substring(0, 4) == "Case");
            vm.Statuses.RemoveAll(x => x.Name.Length >= 4 && x.Name.Substring(0, 4) == "Case");
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Leads.RemoveAll(x => x.AssignTo != id);
                vm.Requests.RemoveAll(x => x.Stage < 1);
            }
            else if (p == "22C63EE9-290E-4160-A293-6ECB49B4677F")
            {
                vm.Requests.RemoveAll(x => x.Approval1User == null && x.Approval2User == null);
            }
            else if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4")
            {
                vm.Requests.RemoveAll(x => x.LawFirm != pid);
            }
            else if (p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId != id);
            }
            else if (p == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")
            {
                vm.Requests.RemoveAll(x => x.Stage < 27);
            }
            else if (p == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")
            {
                vm.Leads.RemoveAll(x => x.AssignTo == null);
                vm.Requests.RemoveAll(x => x.Stage < 1);
            }
            else if (p == "7D68595F-726A-4D9F-8BF2-42DA814901C5")
            {
                vm.Requests.RemoveAll(x => x.NegotiatorId == null);
            }
            else if (p == "A4C2EBBE-9391-44AF-BD49-572BBC9EB22E")
            {
                vm.Requests.RemoveAll(x => x.Stage == 1);
            }

            foreach (var v in vm.Requests)
            {
                v.AccessStatus = new ObjectClassesLibrary.Classes.LeadUserHasAccess().checkAccess(p, v.Stage, id);
            }

            return PartialView(vm);
        }
        public async Task<ActionResult> CreateLead()
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            var userName = Utils.GetDisplayName();

            var vm = new Models.CreateLeadViewModel();
            //vm.Request.CreatedBy = id;
            //vm.Request.RequestId = await RequestStore.CreateRequest(vm.Request);
            vm.User = await UserStore.GetAllUser();
            vm.StatusOfCases = await StatusOfCaseStore.GetAllStatusOfCase();
            vm.Sources = await SourceStore.GetAllSource();
            vm.Sources.RemoveAll(x => x.Deleted == true);
            vm.UserPosition = await UserPositionStore.GetUserPositionByUserPositionId(p);
            vm.States = await StateCityStore.StateList();
            vm.Cities = await StateCityStore.CityList();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.UserId = id;
            vm.Name = userName;
            vm.Users = await UserStore.GetAllUser();

            vm.User = vm.User.FindAll(x => x.Position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487");
            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.User.RemoveAll(x => x.UserId != id);
            }

            return View(vm);
        }
        public async Task<JsonResult> CreateNewLead(Profile profile, Address address, Person person, CommentRemark cr, Request request, Lead lead, AssetForInvestigation afi, IList<CompanyFinancialInformation> cfi, IList<LitigationRecord> lr, IList<BlackListingRecord> br, IList<ObjectClassesLibrary.Classes.Document> documents, AuditTrail auditTrail)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            cr.CreatedBy = id;

            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                lead.AssignTo = id;
            }

            string caseSwitch = lead.AssignTo;
            switch (caseSwitch)
            {
                case null:
                case "":
                    request.Stage = 0;
                    break;
                default:
                    request.Stage = 2;
                    break;
            }


            var r = new Response();
            request.CreatedBy = id;
            request.Type = "Lead";
            request.RequestId = await RequestStore.CreateRequest(request);
            cr.Stage = request.Stage;
            cr.RequestId = request.RequestId;


            if (!string.IsNullOrEmpty(request.RequestId))
            {
                if (!string.IsNullOrEmpty(profile.ProfileId) && profile.ProfileId != "null")
                {
                    address.ProfileId = profile.ProfileId;
                    person.ProfileId = profile.ProfileId;
                    cr.ProfileId = profile.ProfileId;
                    request.ProfileId = profile.ProfileId;
                    lead.RequestId = request.RequestId;
                    afi.ProfileId = request.ProfileId;

                    if (!string.IsNullOrEmpty(address.AddressId))
                    {
                        address.AddressId = await AddressStore.UpdateAddressByAddressId(address);
                    }
                    else
                    {
                        address.AddressId = await AddressStore.CreateAddress(address);
                    }

                    if (!string.IsNullOrEmpty(person.PersonId))
                    {
                        person.PersonId = await PersonStore.UpdatePerson(person);
                    }
                    else
                    {
                        person.PersonId = await PersonStore.CreatePerson(person);
                    }

                    cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                    lead.LeadId = await LeadStore.CreateLead(lead);
                    afi.AssetForInvestigationId = await AssetForInvestigationStore.CreateAssetForInvestigation(afi, id, profile.ProfileId);
                    //bool c = await CompanyFinancialInformationStore.CreateCompanyFinancialInformation(cfi, id, profile.ProfileId);
                    //bool l = await LitigationRecordStore.CreateLitigationRecord(lr, id, profile.ProfileId);
                    //bool b = await BlackListingRecordStore.CreateBlackListingRecord(br, id, profile.ProfileId);
                    bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                    request.PersonId = person.PersonId;
                    request.AddressId = address.AddressId;
                    request.LeadId = lead.LeadId;
                    if (!string.IsNullOrEmpty(lead.AssignTo))
                    {
                        request.Status = "Lead (Assigned)";
                    }
                    else
                    {
                        request.Status = "Lead (New)";
                    }
                    request.ProfileId = profile.ProfileId;
                    request.PersonId = person.PersonId;
                    request.AddressId = address.AddressId;
                    request.LeadId = lead.LeadId;

                    var rr = await RequestStore.UpdateRequest(request);

                    if (string.IsNullOrEmpty(rr))
                    {
                        r.Error = "Unable to save record, please try again later.";
                    }
                    else
                    {
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Lead Created.", cr.Comment, request.Stage);
                        r.Message = "Record saved successfully.";
                        r.RedirectUrl = "/Case/Choice";
                    }
                }
                else
                {
                    profile.ProfileId = await ProfileStore.CreateProfile(profile, id);

                    if (!string.IsNullOrEmpty(profile.ProfileId))
                    {
                        address.ProfileId = profile.ProfileId;
                        person.ProfileId = profile.ProfileId;
                        cr.ProfileId = profile.ProfileId;
                        lead.RequestId = request.RequestId;
                        afi.ProfileId = request.ProfileId;

                        address.AddressId = await AddressStore.CreateAddress(address);
                        person.PersonId = await PersonStore.CreatePerson(person);
                        cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                        lead.LeadId = await LeadStore.CreateLead(lead);
                        afi.AssetForInvestigationId = await AssetForInvestigationStore.CreateAssetForInvestigation(afi, id, profile.ProfileId);
                        //bool c = await CompanyFinancialInformationStore.CreateCompanyFinancialInformation(cfi, id, profile.ProfileId);
                        //bool l = await LitigationRecordStore.CreateLitigationRecord(lr, id, profile.ProfileId);
                        //bool b = await BlackListingRecordStore.CreateBlackListingRecord(br, id, profile.ProfileId);
                        bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                        if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(person.PersonId) /*|| string.IsNullOrEmpty(cr.CommentRemarkId)*/ || string.IsNullOrEmpty(lead.LeadId))
                        {
                            r.Error = "Unable to save record, please try again later.";
                        }
                        else
                        {
                            request.ProfileId = profile.ProfileId;
                            request.PersonId = person.PersonId;
                            request.AddressId = address.AddressId;
                            request.LeadId = lead.LeadId;
                            if (!string.IsNullOrEmpty(lead.AssignTo))
                            {
                                request.Status = "Lead (Assigned)";
                            }
                            else
                            {
                                request.Status = "Lead (New)";
                            }

                            var rr = await RequestStore.UpdateRequest(request);

                            if (!string.IsNullOrEmpty(rr))
                            {
                                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Lead Created.", cr.Comment, request.Stage);
                                r.Message = "Record saved successfully.";
                                r.RedirectUrl = "/Case/Choice?module=Lead";
                            }
                            else
                            {
                                r.Error = "Unable to save record, please try again later.";
                            }
                        }
                    }
                }
            }

            return Json(r);
        }
        public async Task<JsonResult> CreateNewLeadDraft(Profile profile, Address address, Person person, CommentRemark cr, Request request, Lead lead, AssetForInvestigation afi, IList<CompanyFinancialInformation> cfi, IList<LitigationRecord> lr, IList<BlackListingRecord> br, IList<ObjectClassesLibrary.Classes.Document> documents, AuditTrail auditTrail, Draft draft)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();
            cr.CreatedBy = id;

            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                lead.AssignTo = id;
                request.Stage = 1;
            }
            else
            {
                request.Stage = 0;
            }

            request.Status = "Lead (New)";


            var r = new Response();
            request.CreatedBy = id;
            request.Type = "Lead";
            request.RequestId = await RequestStore.CreateRequest(request);
            cr.Stage = request.Stage;
            cr.RequestId = request.RequestId;

            if (!string.IsNullOrEmpty(request.RequestId))
            {
                if (!string.IsNullOrEmpty(profile.ProfileId) && profile.ProfileId != "null")
                {
                    address.ProfileId = profile.ProfileId;
                    person.ProfileId = profile.ProfileId;
                    cr.ProfileId = profile.ProfileId;
                    request.ProfileId = profile.ProfileId;
                    lead.RequestId = request.RequestId;
                    afi.ProfileId = request.ProfileId;

                    if (!string.IsNullOrEmpty(address.AddressId))
                    {
                        address.AddressId = await AddressStore.UpdateAddressByAddressId(address);
                    }
                    else
                    {
                        address.AddressId = await AddressStore.CreateAddress(address);
                    }

                    if (!string.IsNullOrEmpty(person.PersonId))
                    {
                        person.PersonId = await PersonStore.UpdatePerson(person);
                    }
                    else
                    {
                        person.PersonId = await PersonStore.CreatePerson(person);
                    }

                    cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                    lead.LeadId = await LeadStore.CreateLead(lead);
                    afi.AssetForInvestigationId = await AssetForInvestigationStore.CreateAssetForInvestigation(afi, id, profile.ProfileId);
                    //bool c = await CompanyFinancialInformationStore.CreateCompanyFinancialInformation(cfi, id, profile.ProfileId);
                    //bool l = await LitigationRecordStore.CreateLitigationRecord(lr, id, profile.ProfileId);
                    //bool b = await BlackListingRecordStore.CreateBlackListingRecord(br, id, profile.ProfileId);
                    bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                    request.PersonId = person.PersonId;
                    request.AddressId = address.AddressId;
                    request.LeadId = lead.LeadId;

                    var rr = await RequestStore.UpdateRequest(request);

                    if (!string.IsNullOrEmpty(rr))
                    {
                        draft.CreatedBy = id;
                        draft.RequestId = rr;
                        draft.Active = true;
                        draft.DraftId = await DraftStore.CreateDraft(draft);

                        if (!string.IsNullOrEmpty(draft.DraftId))
                        {
                            r.Message = "Draft saved successfully.";
                            r.RedirectUrl = "/Home/MyTask";
                        }
                        else
                        {
                            r.Error = "Unable to save draft, please try again later.";
                        }
                    }
                    else
                    {
                        r.Error = "Unable to save draft, please try again later.";
                    }
                }
                else
                {
                    profile.ProfileId = await ProfileStore.CreateProfile(profile, id);

                    if (!string.IsNullOrEmpty(profile.ProfileId))
                    {
                        address.ProfileId = profile.ProfileId;
                        person.ProfileId = profile.ProfileId;
                        cr.ProfileId = profile.ProfileId;
                        lead.RequestId = request.RequestId;
                        afi.ProfileId = request.ProfileId;

                        address.AddressId = await AddressStore.CreateAddress(address);
                        person.PersonId = await PersonStore.CreatePerson(person);
                        cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
                        lead.LeadId = await LeadStore.CreateLead(lead);
                        afi.AssetForInvestigationId = await AssetForInvestigationStore.CreateAssetForInvestigation(afi, id, profile.ProfileId);
                        //bool c = await CompanyFinancialInformationStore.CreateCompanyFinancialInformation(cfi, id, profile.ProfileId);
                        //bool l = await LitigationRecordStore.CreateLitigationRecord(lr, id, profile.ProfileId);
                        //bool b = await BlackListingRecordStore.CreateBlackListingRecord(br, id, profile.ProfileId);
                        bool d = await DocumentStore.UpdateDocumentsRequestIdByDocumentId(documents, request.RequestId);

                        if (string.IsNullOrEmpty(address.AddressId) || string.IsNullOrEmpty(person.PersonId) /*|| string.IsNullOrEmpty(cr.CommentRemarkId)*/ || string.IsNullOrEmpty(lead.LeadId))
                        {
                            r.Error = "Unable to save draft, please try again later.";
                        }
                        else
                        {
                            request.ProfileId = profile.ProfileId;
                            request.PersonId = person.PersonId;
                            request.AddressId = address.AddressId;
                            request.LeadId = lead.LeadId;

                            var rr = await RequestStore.UpdateRequest(request);

                            if (!string.IsNullOrEmpty(rr))
                            {
                                draft.CreatedBy = id;
                                draft.RequestId = rr;
                                draft.Active = true;
                                draft.DraftId = await DraftStore.CreateDraft(draft);

                                if (!string.IsNullOrEmpty(draft.DraftId))
                                {
                                    r.Message = "Draft saved successfully.";
                                    r.RedirectUrl = "/Home/MyTask";
                                }
                                else
                                {
                                    r.Error = "Unable to save draft, please try again later.";
                                }

                            }
                            else
                            {
                                r.Error = "Unable to save draft, please try again later.";
                            }
                        }
                    }
                }
            }

            return Json(r);
        }
        public async Task<JsonResult> RequestMore(CommentRemark cr, string RequestId, string ProfileId, AuditTrail auditTrail, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();

            var stage = 0;
            var rStage = await RequestStore.GetRequestByRequestId(RequestId);

            if (rStage.Stage == 5 || rStage.Stage == 10)
            {
                stage = 6;
                rStage.Status = "Lead (Audit Check Request More)";
                var rrr = await RequestStore.UpdateRequest(rStage);
                bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(RequestId, id, p, rStage.Stage);
            }
            else if (rStage.Stage == 7 || rStage.Stage == 8)
            {
                stage = 10;
                rStage.Status = "Case (Approval Request More)";
                var rrr = await RequestStore.UpdateRequest(rStage);
                bool ap = await RequestStore.UpdateAllRequestApprovalToFalse(RequestId, id, "Request More");
            }
            else if (rStage.Stage == 23 || rStage.Stage == 24)
            {
                stage = 26;
                bool ap = await RequestStore.UpdateAllRequestDecisionToFalse(RequestId, id);
            }
            else if (rStage.Stage == 28)
            {
                stage = 29;
                bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(RequestId, id, p, rStage.Stage);
            }

            draft = await DraftStore.GetDraftByRequestId(RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }

            cr.RequestId = RequestId;
            cr.CreatedBy = id;
            cr.ProfileId = ProfileId;
            cr.Stage = stage;

            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
            bool re = await RequestStore.UpdateRequestStage(RequestId, stage, rStage.Stage);

            if (!string.IsNullOrEmpty(cr.CommentRemarkId) && re && stage > 9)
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Case Request More", cr.Comment, stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Case";
            }
            else if (!string.IsNullOrEmpty(cr.CommentRemarkId) && re && stage < 9)
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Lead Request More", cr.Comment, stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Lead";
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> RejectWithProposal(CommentRemark cr, string RequestId, string ProfileId, AuditTrail auditTrail, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();

            var stage = 0;
            var rStage = await RequestStore.GetRequestByRequestId(RequestId);

            if (rStage.Stage == 7 || rStage.Stage == 8)
            {
                stage = 10;
                rStage.Status = "Case (Reject With Proposal)";
            }
            var rrr = await RequestStore.UpdateRequest(rStage);
            bool ap = await RequestStore.UpdateAllRequestApprovalToFalse(RequestId, id, "Reject With Proposal");
            //else if (rStage.Stage == 28)
            //{
            //    stage = 29;
            //    bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(RequestId, id, p, rStage.Stage);
            //}

            draft = await DraftStore.GetDraftByRequestId(RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }

            cr.RequestId = RequestId;
            cr.CreatedBy = id;
            cr.ProfileId = ProfileId;
            cr.Stage = rStage.Stage;

            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
            bool re = await RequestStore.UpdateRequestStage(RequestId, stage, rStage.Stage);

            if (!string.IsNullOrEmpty(cr.CommentRemarkId) && re)
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Case Reject With Proposal", cr.Comment, rStage.Stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Case";
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> Rejected(CommentRemark cr, string RequestId, string ProfileId, AuditTrail auditTrail, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();

            var r = new Response();
            cr.CreatedBy = id;
            cr.ProfileId = ProfileId;
            var rr = await RequestStore.GetRequestByRequestId(RequestId);
            bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(RequestId, id, p, rr.Stage);

            cr.RequestId = RequestId;
            rr.Stage = 21;
            rr.Status = "Case (Closed)";
            cr.Stage = rr.Stage;

            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

            draft = await DraftStore.GetDraftByRequestId(RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }
            var rrr = await RequestStore.UpdateRequest(rr);
            if (!string.IsNullOrEmpty(cr.CommentRemarkId) && !string.IsNullOrEmpty(rrr))
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Case Rejected", cr.Comment, rr.Stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Closed";
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> Release(CommentRemark cr, string RequestId, string ProfileId, AuditTrail auditTrail, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();
            Lead lead = new Lead();

            cr.RequestId = RequestId;
            cr.CreatedBy = id;
            cr.ProfileId = ProfileId;
            var rr = await RequestStore.GetRequestByRequestId(RequestId);
            lead = await LeadStore.GetLeadByLeadId(rr.LeadId);
            bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(RequestId, id, p, rr.Stage);

            rr.Stage = 0;
            rr.Status = "Lead (New)";

            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

            draft = await DraftStore.GetDraftByRequestId(RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }
            var rrr = await RequestStore.UpdateRequest(rr);
            var rc = await LeadStore.UpdateRecycleCount(lead.LeadId, lead.RecycledCount + 1);
            lead.AssignTo = null;
            lead.LeadId = await LeadStore.UpdateLead(lead);

            if (!string.IsNullOrEmpty(cr.CommentRemarkId) && !string.IsNullOrEmpty(rrr) && !string.IsNullOrEmpty(lead.LeadId))
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Non Contactable", cr.Comment, rr.Stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Lead";
            }
            else if (string.IsNullOrEmpty(cr.CommentRemarkId) && !string.IsNullOrEmpty(rrr) && !string.IsNullOrEmpty(lead.LeadId))
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Released", cr.Comment, rr.Stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Lead";
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> Dispute(CommentRemark cr, string RequestId, string ProfileId, string SopRequestId, AuditTrail auditTrail, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();

            cr.RequestId = RequestId;
            cr.CreatedBy = id;
            cr.ProfileId = ProfileId;
            var rr = await RequestStore.GetRequestByRequestId(RequestId);
            bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(RequestId, id, p, rr.Stage);

            rr.Stage = 18;
            rr.Status = "Case (Dispute)";
            cr.Stage = rr.Stage;

            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

            draft = await DraftStore.GetDraftByRequestId(RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }
            var rrr = await RequestStore.UpdateRequest(rr);
            bool re = await RequestStore.CreateRequestDisputeUser("2AA5715A-AB61-4861-A26B-5A125FB69D5D", "31316B60-95E1-4727-9242-CD7B51B47469", RequestId);


            var soprequest = await SopRequestsStore.GetSopRequestsBySopRequestId(SopRequestId);
            if (soprequest != null)
            {
                soprequest.Status = 2;
                soprequest.SopRequestid = await SopRequestsStore.UpdateSopRequests(soprequest, id);
            }

            if (!string.IsNullOrEmpty(cr.CommentRemarkId) && !string.IsNullOrEmpty(rrr) && re)
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Case Dispute", cr.Comment, rr.Stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Case";
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> Complete(CommentRemark cr, string RequestId, string ProfileId, AuditTrail auditTrail, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();

            cr.RequestId = RequestId;
            cr.CreatedBy = id;
            cr.ProfileId = ProfileId;
            var rr = await RequestStore.GetRequestByRequestId(RequestId);
            bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(RequestId, id, p, rr.Stage);

            rr.Stage = 35;
            rr.Status = "Case (Close Complete)";

            var rrr = await RequestStore.UpdateRequest(rr);
            cr.Stage = rr.Stage;
            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);

            draft = await DraftStore.GetDraftByRequestId(RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }

            if (!string.IsNullOrEmpty(rrr))
            {
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Case Invoice Document", cr.Comment, rr.Stage);
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(RequestId, id, "Lead", "Case Close Complete", cr.Comment, rr.Stage);
                r.Message = "Record saved successfully.";
                r.RedirectUrl = "/Case/Choice?module=Case";
            }
            else
            {
                r.Error = "Unable to save record, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateLead(Profile profile, Address address, Person person, CommentRemark cr, Request request, Lead lead, AssetForInvestigation afi, IList<CompanyFinancialInformation> cfi, IList<LitigationRecord> lr, IList<BlackListingRecord> br, bool boo, CommentRemark crRmo, AuditTrail auditTrail, string stage, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();

            cr.RequestId = request.RequestId;
            cr.CreatedBy = id;

            draft = await DraftStore.GetDraftByRequestId(request.RequestId);
            if (draft != null)
            {
                draft.Active = false;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }

            var rStage = await RequestStore.GetRequestByRequestId(request.RequestId);
            bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(request.RequestId, id, p, rStage.Stage);

            List<ObjectClassesLibrary.Classes.Document> PreAndSoaDoc = await DocumentStore.GetAllDocument();
            List<ShowInSelect> showInSelects = await ShowInSelectStore.GetAllShowInSelect();
            showInSelects.RemoveAll(x => x.Deleted == true);

            PreAndSoaDoc = PreAndSoaDoc.FindAll(x => x.Type == "E11A487A-27EB-4F27-88B8-556B6D5643ED" || x.Type == "55E243C2-AD01-4869-8B46-675D7632211A");
            PreAndSoaDoc = PreAndSoaDoc.FindAll(x => x.RequestId == request.RequestId);

            address.ProfileId = profile.ProfileId;
            person.ProfileId = profile.ProfileId;
            cr.ProfileId = profile.ProfileId;
            lead.RequestId = request.RequestId;
            cr.Stage = rStage.Stage;

            profile.ProfileId = await ProfileStore.UpdateProfile(profile);
            address.AddressId = await AddressStore.UpdateAddress(address);
            person.PersonId = await PersonStore.UpdatePerson(person);
            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
            lead.LeadId = await LeadStore.UpdateLead(lead);

            request.ProfileId = profile.ProfileId;
            request.PersonId = person.PersonId;
            request.AddressId = address.AddressId;
            request.LeadId = lead.LeadId;
            afi.ProfileId = profile.ProfileId;
            afi.AssetForInvestigationId = await AssetForInvestigationStore.UpdateAssetForInvestigation(afi, id, profile.ProfileId);
            bool c = await CompanyFinancialInformationStore.UpdateCompanyFinancialInformation(cfi, id, profile.ProfileId);
            bool l = await LitigationRecordStore.UpdateLitigationRecord(lr, id, profile.ProfileId);
            bool b = await BlackListingRecordStore.UpdateBlackListingRecord(br, id, profile.ProfileId);

            var docs = await DocumentStore.GetDocumentByRequestId(request.RequestId);

            var requiredDocs = showInSelects.FindAll(x => x.Required == true);
            requiredDocs.RemoveAll(x => x.Stage > rStage.Stage);

            if (!string.IsNullOrEmpty(rStage.Dispute1User) && !string.IsNullOrEmpty(rStage.Dispute2User))
            {
                requiredDocs.RemoveAll(x => x.DocumentTypeId == "54992022-FABB-4C43-859B-459AD29B30FA");
            }

            if (requiredDocs.Count == 0)
            {
                boo = true;
            }
            else
            {
                int i = 0;
                foreach (var rd in requiredDocs)
                {
                    var cc = docs.Find(x => x.Type == rd.DocumentTypeId);

                    if (cc != null)
                    {
                        docs.RemoveAll(x => x.DocumentId == cc.DocumentId);
                        i++;
                    }
                }
                if (i >= requiredDocs.Count)
                {
                    boo = true;
                }
            }

            if (rStage.Stage == 34)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 33;
                        request.Status = "Case (Finance Document)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Invoice Document", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 34;
                        request.Status = "Case (Invoice Document)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 33)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 34;
                        request.Status = "Case (Invoice Document)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Finance Document", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 33;
                        request.Status = "Case (Finance Document)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 32)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 33;
                        request.Status = "Case (Finance Document)";
                        bool bb = await AppointmentStore.UpdateAppointmentStatus(request.RequestId, request.Stage);
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Finance Document", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 32;
                        request.Status = "Case (Settlement Uploaded)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 31)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 30;
                        request.Status = "Case (Settlement Signed)";
                        break;
                    case false:
                        request.Stage = 31;
                        request.Status = "Case (Settlement Agreement)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 30)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 32;
                        request.Status = "Case (Settlement Uploaded)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Settlement Uploaded", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 30;
                        request.Status = "Case (Settlement Signed)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 29)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 30;
                        request.Status = "Case (Settlement Signed)";
                        bool bb = await AppointmentStore.UpdateAppointmentStatus(request.RequestId, request.Stage);
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Settlement Signed", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 29;
                        request.Status = "Case (Settlement Agreement)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 28)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 25;
                        request.Status = "Case (Approval)";
                        break;
                    case false:
                        request.Stage = 28;
                        request.Status = "Case (Negotiator Assigned)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 25 || rStage.Stage == 26)
            {
                bool approvalCommitteeDecision = await RequestStore.UpdateRequestDecision(id, request.RequestId);
                rStage.Stage++;
                request.Stage = rStage.Stage;
                request.Status = "Case (Approval)";
                if (request.Stage == 26)
                {
                    request.Stage = 24;
                    request.Status = "Case (Negotiator Assigned)";
                    auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Negotiator Assigned", cr.Comment, rStage.Stage);
                }
            }
            else if (rStage.Stage == 24)
            {
                string caseSwitch = stage;
                switch (caseSwitch)
                {
                    case "":
                    case null:
                        request.Stage = 29;
                        request.Status = "Case (Settlement Agreement)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Settlement Agreement", cr.Comment, rStage.Stage);
                        break;
                    default:
                        request.Stage = 24;
                        request.Status = "Case (Approval)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Approval", cr.Comment, rStage.Stage);
                        bool re = await RequestStore.CreateRequestDecisionUser("2AA5715A-AB61-4861-A26B-5A125FB69D5D", "31316B60-95E1-4727-9242-CD7B51B47469", request.RequestId);
                        break;
                }
            }
            else if (rStage.Stage == 23)
            {
                request.Stage = 23;
                request.Status = "Case (Negotiator Assigned)";
            }
            else if (rStage.Stage == 22)
            {
                string caseSwitch = request.NegotiatorId;
                switch (caseSwitch)
                {
                    case "":
                    case null:
                        request.Stage = 22;
                        request.Status = "Case (Negotiator)";
                        break;
                    default:
                        request.Stage = 23;
                        request.Status = "Case (Negotiator Assigned)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Negotiator Assigned", cr.Comment, rStage.Stage);
                        break;
                }
            }
            else if (rStage.Stage == 20)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 22;
                        request.Status = "Case (Negotiator)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Negotiator", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 20;
                        request.Status = "Case (JID)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 18 || rStage.Stage == 19)
            {
                bool approvalCommitteeDisputeApproval = await RequestStore.UpdateRequestDispute(id, request.RequestId);
                rStage.Stage++;
                request.Stage = rStage.Stage;
                request.Status = "Case (Dispute)";
                if (request.Stage == 20)
                {
                    request.Status = "Case (JID)";
                    auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case JID", cr.Comment, rStage.Stage);
                }
                request.LawFirm = rStage.LawFirm;
            }
            else if (rStage.Stage == 16)
            {
                request.Stage = 15;
                request.Status = "Case (Law Firm Appointed)";
            }
            else if (rStage.Stage == 15)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 20;
                        request.Status = "Case (JID)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case JID", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 15;
                        request.Status = "Case (Law Firm Appointed)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 14)
            {
                string caseSwitch = request.LawFirm;
                switch (caseSwitch)
                {
                    case "":
                    case null:
                        request.Stage = 14;
                        request.Status = "Case (2nd Appointment Complete)";
                        break;
                    default:
                        request.Stage = 15;
                        request.Status = "Case (Law Firm Appointed)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Law Firm Appointed", cr.Comment, rStage.Stage);
                        break;
                }
            }
            else if (rStage.Stage == 13)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 14;
                        request.Status = "Case (2nd Appt Complete)";
                        bool bb = await AppointmentStore.UpdateAppointmentStatus(request.RequestId, request.Stage);
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Appt", "2nd Appt Complete", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 13;
                        request.Status = "Case (2nd Appt Set)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 12)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 13;
                        request.Status = "Case (2nd Appt Set)";
                        break;
                    case false:
                        request.Stage = 12;
                        request.Status = "Case (2nd Appt Set)";
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 11)
            {
                request.Stage = 11;
                request.Status = "Case (Approved)";
            }
            else if (rStage.Stage == 10)
            {
                bool re = await RequestStore.UpdateRequestStage(request.RequestId, 7, rStage.Stage);
                request.Stage = 7;
                request.Status = "Case (Approval)";
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Approval", cr.Comment, rStage.Stage);
            }
            else if (rStage.Stage == 7 || rStage.Stage == 8)
            {
                bool approvalCommitteeApproval = await RequestStore.UpdateRequestApproval(id, request.RequestId);
                if (approvalCommitteeApproval == true)
                {
                    if (rStage.Stage == 7)
                    {
                        request.Stage = 8;
                        request.Status = "Case (Approval)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Approval", cr.Comment, rStage.Stage);
                    }
                    if (rStage.Stage == 8)
                    {
                        request.Status = "Case (Approval)";
                        if (rStage.Approval1 == true && approvalCommitteeApproval == true)
                        {
                            request.Stage = 11;
                            request.Status = "Case (Approved)";
                            auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Approved", cr.Comment, rStage.Stage);
                        }
                    }
                }
                else
                {
                    request.Stage = 7;
                    request.Status = "Case (Approval)";
                    auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Approval", cr.Comment, rStage.Stage);
                }
            }
            else if (rStage.Stage == 6)
            {
                crRmo.CreatedBy = id;
                crRmo.ProfileId = profile.ProfileId;
                crRmo.Stage = rStage.Stage;
                crRmo.RequestId = request.RequestId;
                crRmo.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(crRmo, id);
                if (!string.IsNullOrEmpty(crRmo.CommentRemarkId))
                {
                    switch (boo)
                    {
                        case true:
                            request.Stage = 5;
                            request.Status = "Lead (Audit Check)";
                            auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Audit Check", cr.Comment, rStage.Stage);
                            break;
                        case false:
                            request.Stage = 6;
                            request.Status = "Lead (Audit Check Request More)";
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (boo)
                    {
                        case true:
                            request.Stage = 5;
                            request.Status = "Lead (Audit Check)";
                            auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Audit Check", cr.Comment, rStage.Stage);
                            break;
                        case false:
                            request.Stage = 6;
                            request.Status = "Lead (Audit Check Request More)";
                            break;
                        default:
                            break;
                    }
                }
            }
            else if (rStage.Stage == 5)
            {
                crRmo.CreatedBy = id;
                crRmo.ProfileId = profile.ProfileId;
                crRmo.Stage = rStage.Stage;
                crRmo.RequestId = request.RequestId;
                crRmo.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(crRmo, id);
                if (!string.IsNullOrEmpty(crRmo.CommentRemarkId))
                {
                    switch (boo)
                    {
                        case true:
                            request.Stage = 7;
                            request.Status = "Case (Approval)";
                            bool re = await RequestStore.CreateRequestApprovalUser("2AA5715A-AB61-4861-A26B-5A125FB69D5D", "31316B60-95E1-4727-9242-CD7B51B47469", request.RequestId);
                            auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Submision", cr.Comment, rStage.Stage);
                            break;
                        case false:
                            request.Stage = 5;
                            request.Status = "Lead (Audit Check)";
                            break;
                        default:
                            break;
                    }

                }
                else
                {
                    switch (boo)
                    {
                        case true:
                            request.Stage = 7;
                            request.Status = "Case (Approval)";
                            bool re = await RequestStore.CreateRequestApprovalUser("2AA5715A-AB61-4861-A26B-5A125FB69D5D", "31316B60-95E1-4727-9242-CD7B51B47469", request.RequestId);
                            auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Case Submision", cr.Comment, rStage.Stage);
                            break;
                        case false:
                            request.Stage = 5;
                            request.Status = "Lead (Audit Check)";
                            break;
                        default:
                            break;
                    }
                }
            }
            else if (rStage.Stage == 4)
            {
                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 5;
                        request.Status = "Lead (Audit Check)";
                        bool bb = await AppointmentStore.UpdateAppointmentStatus(request.RequestId, request.Stage);
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Appt", "Lead Appt Complete", cr.Comment, rStage.Stage);
                        break;
                    case false:
                        request.Stage = 4;
                        request.Status = "Lead (Appt Complete)";
                        break;
                    default:
                        break;
                }

            }
            else if (rStage.Stage == 3)
            {
                request.Status = "Lead (Appt Set)";
                request.Stage = 3;

                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 4;
                        request.Status = "Lead (Appt Complete)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Lead Pre Engagement", cr.Comment, rStage.Stage);
                        break;
                    default:
                        break;
                }
            }
            else if (rStage.Stage == 2)
            {
                request.Status = "Lead (Assigned)";
                request.Stage = 2;
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Lead Assigned", cr.Comment, rStage.Stage);
            }
            else if (!string.IsNullOrEmpty(request.AppointmentId) && request.AppointmentId != "null")
            {
                request.Status = "Lead (Appt Set)";
                request.Stage = 3;

                bool caseSwitch = boo;
                switch (caseSwitch)
                {
                    case true:
                        request.Stage = 4;
                        request.Status = "Lead (Appt Complete)";
                        auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Appt Complete", cr.Comment, rStage.Stage);
                        break;
                    default:
                        break;
                }
            }
            else if (!string.IsNullOrEmpty(lead.AssignTo) && request.AppointmentId == "null")
            {
                request.Status = "Lead (Assigned)";
                request.Stage = 2;
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Lead Assigned", cr.Comment, rStage.Stage);
            }
            else if (request.Stage == 1)
            {
                request.Status = "Lead (New)";
                request.Stage = 2;
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Lead Assigned", cr.Comment, rStage.Stage);
            }
            else if (request.Stage == 0)
            {
                request.Status = "Lead (New)";
                request.Stage = 0;
                auditTrail.AuditTrailId = await AuditTrailStore.CreateAuditTrail(request.RequestId, id, "Lead", "Lead Created", cr.Comment, rStage.Stage);
            }

            var rr = await RequestStore.UpdateRequest(request);

            if (string.IsNullOrEmpty(rr))
            {
                r.Error = "Unable to save record, please try again later.";
            }
            else
            {
                r.Message = "Record saved successfully.";
                if (request.Stage == 35)
                {
                    r.RedirectUrl = "/Case/Choice?module=Close";
                }
                else if (request.Stage < 7)
                {
                    r.RedirectUrl = "/Case/Choice?module=Lead";
                }
                else
                {
                    r.RedirectUrl = "/Case/Choice?module=Case";
                }
            }
            return Json(r);
        }
        public async Task<JsonResult> UpdateLeadDraft(Profile profile, Address address, Person person, CommentRemark cr, Request request, Lead lead, AssetForInvestigation afi, IList<CompanyFinancialInformation> cfi, IList<LitigationRecord> lr, IList<BlackListingRecord> br, bool boo, CommentRemark crRmo, AuditTrail auditTrail, string stage, Draft draft)
        {
            var id = Utils.GetUserId();
            var p = Utils.GetUserGroup();
            var r = new Response();
            cr.CreatedBy = id;
            cr.RequestId = request.RequestId;

            draft = await DraftStore.GetDraftByRequestId(request.RequestId);
            if (draft != null)
            {
                draft.CreatedBy = id;
                draft.Active = true;
                draft.DraftId = await DraftStore.UpdateDraft(draft);
            }
            else
            {
                draft = new Draft();
                draft.RequestId = request.RequestId;
                draft.CreatedBy = id;
                draft.Active = true;
                draft.DraftId = await DraftStore.CreateDraft(draft);
            }

            var rStage = await RequestStore.GetRequestByRequestId(request.RequestId);
            bool ap = await RequestStore.UpdateRequestLastUpdateActionTime(request.RequestId, id, p, rStage.Stage);

            address.ProfileId = profile.ProfileId;
            person.ProfileId = profile.ProfileId;
            cr.ProfileId = profile.ProfileId;
            lead.RequestId = request.RequestId;
            cr.Stage = rStage.Stage;

            crRmo.CreatedBy = id;
            crRmo.ProfileId = profile.ProfileId;
            crRmo.Stage = rStage.Stage;
            crRmo.RequestId = request.RequestId;
            crRmo.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(crRmo, id);

            profile.ProfileId = await ProfileStore.UpdateProfile(profile);
            address.AddressId = await AddressStore.UpdateAddress(address);
            person.PersonId = await PersonStore.UpdatePerson(person);
            cr.CommentRemarkId = await CommentRemarkStore.CreateCommentRemark(cr, id);
            lead.LeadId = await LeadStore.UpdateLead(lead);

            request.ProfileId = profile.ProfileId;
            request.PersonId = person.PersonId;
            request.AddressId = address.AddressId;
            request.LeadId = lead.LeadId;
            afi.ProfileId = profile.ProfileId;
            afi.AssetForInvestigationId = await AssetForInvestigationStore.UpdateAssetForInvestigation(afi, id, profile.ProfileId);
            bool c = await CompanyFinancialInformationStore.UpdateCompanyFinancialInformation(cfi, id, profile.ProfileId);
            bool l = await LitigationRecordStore.UpdateLitigationRecord(lr, id, profile.ProfileId);
            bool b = await BlackListingRecordStore.UpdateBlackListingRecord(br, id, profile.ProfileId);

            request.Stage = rStage.Stage;
            request.Status = rStage.Status;

            var rr = await RequestStore.UpdateRequest(request);

            if (string.IsNullOrEmpty(rr))
            {
                r.Error = "Unable to save draft, please try again later.";
            }
            else
            {
                r.Message = "Draft saved successfully.";
                r.RedirectUrl = "/Home/MyTask";
            }
            return Json(r);
        }
        public async Task<ActionResult> EditLead(string requestId, string page)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();

            int i = 0;
            var vm = new Models.UpdateLeadVIewModel();
            vm.DocumentTypes = await DocumentTypeStore.GetAllDocumentType();
            vm.Request = await RequestStore.GetRequestByRequestId(requestId);
            vm.Users = await UserStore.GetAllUser();
            vm.Users2 = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.StatusOfCases = await StatusOfCaseStore.GetAllStatusOfCase();
            vm.States = await StateCityStore.StateList();
            vm.Cities = await StateCityStore.CityList();
            vm.Users = vm.Users.FindAll(x => x.Position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487");
            vm.UserPositions = await UserPositionStore.GetAllUserPosition();
            vm.Debtors = await DebtorStore.GetDebtorByRequestId(requestId);
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.AppointmentTypes = await AppointmentTypeStore.GetAllAppointmentType();
            vm.ShowInSelects = await ShowInSelectStore.GetAllShowInSelect();
            vm.ShowInSelects.RemoveAll(x => x.Deleted == true);
            vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            vm.LawFirmProfiles.RemoveAll(x => x.Active == false);
            vm.LawFirmProfiles = vm.LawFirmProfiles.OrderBy(x => x.Name).ToList();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.Negotiator = vm.Users2.FindAll(x => x.Position == "E207490E-1A8C-405D-B415-B100ADB0D04F");
            vm.Position = p;
            vm.UserId = id;
            vm.Page = page;
            vm.Sop = await SopStore.SopList(requestId);
            vm.SopList = await SopStore.SopList();
            vm.SopRequests = await SopRequestsStore.GetSopRequestsByRequestsId(requestId);
            vm.RequestServices = await RequestServiceStore.GetRequestServicesByRequestId(requestId);

            if (vm.Request.Stage < 7 || vm.Request.Stage == 10)
            {
                vm.PageSegmentAccess.ProfileAddressPersonAccess = true;
                vm.PageSegmentAccess.AppointmentDocAccess = true;
                vm.PageSegmentAccess.CreditorDocAccess = true;
                vm.PageSegmentAccess.PropertyAccess = true;
                vm.PageSegmentAccess.CtosDocAccess = true;
                vm.PageSegmentAccess.DebtorAccess = true;
                vm.PageSegmentAccess.CFIAccess = true;
                vm.PageSegmentAccess.LRAccess = true;
                vm.PageSegmentAccess.BRAccess = true;
            }
            else if (vm.Request.Stage == 12)
            {
                vm.PageSegmentAccess.AppointmentDocAccess = true;
            }
            else if (vm.Request.Stage == 13)
            {
                vm.PageSegmentAccess.CreditorDocAccess = true;
            }

            if (p == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487")
            {
                vm.Users.RemoveAll(x => x.UserId != id);
            }

            if (vm != null)
            {
                vm.RequestServicess = await RequestServiceStore.GetRequestServicesByRequestId(requestId);
                vm.Profile = await ProfileStore.GetProfileByProfileId(vm.Request.ProfileId);
                vm.Address = await AddressStore.GetAddressByAddressId(vm.Request.AddressId);
                vm.Person = await PersonStore.GetPersonByPersonId(vm.Request.PersonId);
                vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByRequestId(requestId);
                vm.Lead = await LeadStore.GetLeadByLeadId(vm.Request.LeadId);
                vm.Appointments = await AppointmentStore.GetAppointmentByRequestId(requestId);
                vm.AppointmentDoc = await DocumentStore.GetAllDocument();
                vm.Documents = await DocumentStore.GetDocumentByRequestId(requestId);
                //vm.Documents = vm.Documents.FindAll(x => x.RequestId == vm.Request.RequestId/* && x.Type == "8705BE15-F4D5-4CFA-8ED8-32DE5E449A92"*/);
                //vm.AppointmentDoc = vm.AppointmentDoc.FindAll(x => x.RequestId == vm.Request.RequestId && x.Type == vm.ShowInSelects.FindAll(x => x.ShowInSelectName == "appointmentDocType"));
                vm.AssetForInvestigation = await AssetForInvestigationStore.GetAssetForInvestigationByProfileId(vm.Request.ProfileId);
                vm.BlackListingRecord = await BlackListingRecordStore.GetBlackListingRecordByProfileId(vm.Request.ProfileId);
                vm.CompanyFinancialInformation = await CompanyFinancialInformationStore.GetCompanyFinancialInformationByProfileId(vm.Request.ProfileId);
                vm.LitigationRecord = await LitigationRecordStore.GetLitigationRecordByProfileId(vm.Request.ProfileId);
                //vm.PreAndSoaDoc = vm.Documents.FindAll(x => x.Type == "E11A487A-27EB-4F27-88B8-556B6D5643ED" || x.Type == "55E243C2-AD01-4869-8B46-675D7632211A");
                vm.AuditTrails = await AuditTrailStore.GetAuditTrailByRequestId(requestId);
                vm.AuditTrails = vm.AuditTrails.FindAll(x => x.Type.TrimEnd() == "Lead" || x.Type.TrimEnd() == "Appt");

                var docs = await DocumentStore.GetDocumentByRequestId(requestId);

                var requiredDocs = vm.ShowInSelects.FindAll(x => x.Required == true);
                requiredDocs.RemoveAll(x => x.Stage > vm.Request.Stage);

                if (!string.IsNullOrEmpty(vm.Request.Dispute1User) && !string.IsNullOrEmpty(vm.Request.Dispute2User))
                {
                    requiredDocs.RemoveAll(x => x.DocumentTypeId == "54992022-FABB-4C43-859B-459AD29B30FA");
                }

                if (requiredDocs.Count == 0)
                {
                    vm.DocRequired = true;
                }
                else
                {
                    foreach (var rd in requiredDocs)
                    {
                        var c = docs.Find(x => x.Type == rd.DocumentTypeId);

                        if (c != null)
                        {
                            i++;
                        }
                    }
                    if (i >= requiredDocs.Count)
                    {
                        vm.DocRequired = true;
                    }
                }
            }

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4" || p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.CanSee = false;
                vm.CommentRemark.RemoveAll(x => x.Stage < 15);
                vm.AuditTrails.RemoveAll(x => x.Stage < 15);
            }
            else
            {
                vm.CanSee = true;
            }

            return View(vm);
        }
        public async Task<ActionResult> EditLeadPreview(string requestId, string page)
        {
            var p = Utils.GetUserGroup();
            var id = Utils.GetUserId();

            var vm = new Models.UpdateLeadVIewModel();
            vm.DocumentTypes = await DocumentTypeStore.GetAllDocumentType();
            vm.Request = await RequestStore.GetRequestByRequestId(requestId);
            vm.Users = await UserStore.GetAllUser();
            vm.Users2 = await UserStore.GetAllUser();
            vm.Sources = await SourceStore.GetAllSource();
            vm.StatusOfCases = await StatusOfCaseStore.GetAllStatusOfCase();
            vm.UserPositions = await UserPositionStore.GetAllUserPosition();
            vm.States = await StateCityStore.StateList();
            vm.Cities = await StateCityStore.CityList();
            vm.Debtors = await DebtorStore.GetDebtorByRequestId(requestId);
            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.AppointmentTypes = await AppointmentTypeStore.GetAllAppointmentType();
            vm.ShowInSelects = await ShowInSelectStore.GetAllShowInSelect();
            vm.ShowInSelects.RemoveAll(x => x.Deleted == true);
            vm.LawFirmProfiles = await lawFirmLawFirmProfileStore.GetAllLawFirmProfile();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(p);
            vm.Negotiator = vm.Users2.FindAll(x => x.Position == "E207490E-1A8C-405D-B415-B100ADB0D04F");
            vm.Position = p;
            vm.UserId = id;
            vm.Page = page;
            vm.Sop = await SopStore.SopList();
            vm.SopRequests = await SopRequestsStore.GetSopRequestsByRequestsId(requestId);

            if (vm.Request.Stage == 2 && p == "A4C2EBBE-9391-44AF-BD49-572BBC9EB22E")
            {
                vm.ReleaseBtn = true;
            }

            vm.Request.AccessStatus = new ObjectClassesLibrary.Classes.LeadUserHasAccess().checkAccess(p, vm.Request.Stage, id);

            if (vm != null)
            {
                vm.RequestServicess = await RequestServiceStore.GetRequestServicesByRequestId(requestId);
                vm.Profile = await ProfileStore.GetProfileByProfileId(vm.Request.ProfileId);
                vm.Address = await AddressStore.GetAddressByAddressId(vm.Request.AddressId);
                vm.Person = await PersonStore.GetPersonByPersonId(vm.Request.PersonId);
                vm.CommentRemark = await CommentRemarkStore.GetCommentRemarkByRequestId(vm.Request.RequestId);
                vm.Lead = await LeadStore.GetLeadByLeadId(vm.Request.LeadId);
                vm.Appointments = await AppointmentStore.GetAppointmentByRequestId(requestId);
                vm.AppointmentDoc = await DocumentStore.GetAllDocument();
                vm.Documents = await DocumentStore.GetDocumentByRequestId(requestId);
                //vm.Documents = await DocumentStore.GetAllDocument();
                //vm.Documents = vm.Documents.FindAll(x => x.RequestId == vm.Request.RequestId /*&& x.Type == "8705BE15-F4D5-4CFA-8ED8-32DE5E449A92"*/);
                vm.AppointmentDoc = vm.AppointmentDoc.FindAll(x => x.RequestId == vm.Request.RequestId && x.Type == "FD42D4E6-D6EA-4A79-B57A-528F93A5A3B8");
                vm.AssetForInvestigation = await AssetForInvestigationStore.GetAssetForInvestigationByProfileId(vm.Request.ProfileId);
                vm.BlackListingRecord = await BlackListingRecordStore.GetBlackListingRecordByProfileId(vm.Request.ProfileId);
                vm.CompanyFinancialInformation = await CompanyFinancialInformationStore.GetCompanyFinancialInformationByProfileId(vm.Request.ProfileId);
                vm.LitigationRecord = await LitigationRecordStore.GetLitigationRecordByProfileId(vm.Request.ProfileId);
                vm.PreAndSoaDoc = vm.Documents.FindAll(x => x.Type == "E11A487A-27EB-4F27-88B8-556B6D5643ED" || x.Type == "55E243C2-AD01-4869-8B46-675D7632211A");
                vm.AuditTrails = await AuditTrailStore.GetAuditTrailByRequestId(requestId);
                vm.AuditTrails = vm.AuditTrails.FindAll(x => x.Type.TrimEnd() == "Lead" || x.Type.TrimEnd() == "Appt");
            }

            if (p == "47461275-5753-47D1-8A8B-9A26D91E03A4" || p == "E207490E-1A8C-405D-B415-B100ADB0D04F")
            {
                vm.CanSee = false;
                vm.CommentRemark.RemoveAll(x => x.Stage < 15);
                vm.AuditTrails.RemoveAll(x => x.Stage < 15);
            }
            else
            {
                vm.CanSee = true;
            }

            return View(vm);
        }
        public async Task<JsonResult> AddDebtor(Debtor debtor)
        {
            string id = Utils.GetUserId();
            List<Profile> Profiles = new List<Profile>();
            Profiles = await ProfileStore.GetAllProfile();
            Profiles.RemoveAll(x => x.ProfileType == 1);

            bool b = Profiles.Exists(x => x.BusinessRegNo == debtor.BusinessRegNo);
            var p = Profiles.FirstOrDefault(x => x.BusinessRegNo == debtor.BusinessRegNo) ?? new Profile();

            debtor = await DebtorStore.CreateDebtor(debtor, id, b, p.ProfileId);
            return Json(debtor);
        }
        public async Task<JsonResult> DeleteDebtor(string DebtorId)
        {
            var r = new Response();
            var d = await DebtorStore.DeleteDebtor(DebtorId);
            r.Message = "Debtor successfully removed.";

            return Json(r);
        }
        public async Task<JsonResult> DeleteLead(string requestId)
        {
            var r = new Response();
            var d = await RequestStore.DeleteRequest(requestId);
            var dd = await AppointmentStore.DeleteAppointmentByRequestId(requestId);
            r.Message = "Request successfully deleted.";

            return Json(r);
        }
        public async Task<JsonResult> AttachmentUpload()
        {
            string id = Utils.GetUserId();

            var RequestId = Request.Params.GetValues("RequestId")[0];
            var Type = Request.Params.GetValues("Type")[0];
            var ProfileId = Request.Params.GetValues("ProfileId")[0];

            var folder = "Documents";
            var docType = await DocumentTypeStore.GetAllDocumentType();

            string Flocation = docType.Find(x => x.DocumentTypeId == Type).Name;

            HttpPostedFileBase file = Request.Files[0]; //Uploaded file
                                                        //Use the following properties to get file's name, size and MIMEType
            int fileSize = file.ContentLength;
            string fileName = file.FileName;
            string ext = fileName.Substring(fileName.LastIndexOf('.'));
            string mimeType = file.ContentType;
            string path = folder + "/" + RequestId + "/" + Flocation + "/";
            string location = Server.MapPath("~") + path;
            System.IO.Stream fileContent = file.InputStream;
            System.IO.Directory.CreateDirectory(location);
            //To save file, use SaveAs method
            file.SaveAs(location + fileName); //File will be saved in application root

            var d = new ObjectClassesLibrary.Classes.Document()
            {
                Path = path,
                RequestId = RequestId,
                Type = Type,
                FileName = fileName,
                Size = fileSize,
                CreatedDate = DateTime.Now,
            };

            var dd = await DocumentStore.CreateDocument(d);
            if (Type == "7F760368-629F-43A4-A2CB-5F2F2C4003B4")
            {
                CompanyFinancialInformation cfi = new CompanyFinancialInformation();
                cfi.DocumentId = dd.DocumentId;
                cfi.FinancialYearEnd1 = DateTime.Parse(Request.Params.GetValues("FYE")[0]);
                cfi.AnnualTurnover1 = float.Parse(Request.Params.GetValues("AT")[0]);
                cfi.NtaPositiveNegative1 = float.Parse(Request.Params.GetValues("NTA")[0]);
                cfi.ProfitAfterTax1 = float.Parse(Request.Params.GetValues("PAT")[0]);
                bool c = await CompanyFinancialInformationStore.CreateCompanyFinancialInformation(cfi, id, ProfileId);
            }
            else if (Type == "CE756226-33DF-4F9B-96BD-BBA9687B90ED")
            {
                LitigationRecord lr = new LitigationRecord();
                lr.DocumentId = dd.DocumentId;
                lr.ClaimAmount = float.Parse(Request.Params.GetValues("CA")[0]);
                lr.StatusOfCaseId = Request.Params.GetValues("SOC")[0];
                lr.HearingDate = DateTime.Parse(Request.Params.GetValues("HD")[0]);
                lr.Plaintiff = Request.Params.GetValues("P")[0];
                lr.Defendant = Request.Params.GetValues("D")[0];
                bool l = await LitigationRecordStore.CreateLitigationRecord(lr, id, ProfileId);
            }
            else if (Type == "AB94BE85-7EED-4307-B8CE-9A799BF090B6")
            {
                BlackListingRecord br = new BlackListingRecord();
                br.DocumentId = dd.DocumentId;
                br.Creditor1 = Request.Params.GetValues("C")[0];
                br.StatementDate = DateTime.Parse(Request.Params.GetValues("SD")[0]);
                br.ClaimAmount = float.Parse(Request.Params.GetValues("CA")[0]);
                bool b = await BlackListingRecordStore.CreateBlackListingRecord(br, id, ProfileId);
            }
            dd.CreatedDateString = d.CreatedDate.ToString("dd MMM yyyy");
            dd.Type = docType.Find(x => x.DocumentTypeId == Type).Name;
            dd.Path = d.Path;

            return Json(dd);
        }
        public async Task<JsonResult> DeleteAttachment(string documentId)
        {
            var r = new Response();

            var d = await DocumentStore.DeleteDocument(documentId);
            r.Message = "Document successfully removed.";

            return Json(r);
        }
        public async Task<ActionResult> GeneratePreEngagementLetter(string requestId)
        {
            try
            {

                var vm = new Models.UpdateLeadVIewModel();
                vm.Request = await RequestStore.GetRequestByRequestId(requestId);
                vm.Users = await UserStore.GetAllUser();

                if (vm.Request != null)
                {
                    vm.Profile = await ProfileStore.GetProfileByProfileId(vm.Request.ProfileId);
                    vm.Address = await AddressStore.GetAddressByAddressId(vm.Request.AddressId);
                    vm.Appointment = await AppointmentStore.GetAppointmentByAppointmentId(vm.Request.AppointmentId);

                    if (vm.Profile != null && vm.Address != null && vm.Appointment != null)
                    {
                        switch (vm.Profile.BusinessType)
                        {
                            case 1:
                                vm.Profile.Name += " Sdn Bhd";
                                break;
                            case 2:
                                vm.Profile.Name += " Berhad";
                                break;
                            default:
                                break;
                        }

                        string strDoc = @"C:\Users\CP\Letter-Pre-Engagement(001)-FAIR(05).docx";
                        OpenAndAddTextToWordDocument(strDoc, vm.Appointment.Date?.ToString("dd MMM yyyy"), vm.Profile.Name.ToUpper(), vm.Profile.BusinessRegNo, vm.Address, vm.Person, vm.Debtors, vm.Profiles, "Pre");

                        object fileName = @"C:\Users\CP\Pre Engagement Letter.docx";
                        object fileName2 = @"C:\Users\CP\Pre Engagement Letter.pdf";
                        //object fileName = @"C:\Users\DYM Solutions\Downloads\Pre Engagement Letter.doc";
                        //object fileName2 = @"C:\Users\DYM Solutions\Downloads\Pre Engagement Letter.pdf";
                        //object missing = System.Reflection.Missing.Value;

                        //var app = new Microsoft.Office.Interop.Word.Application();
                        //var document = app.Documents.Open(@"C:\Users\CP\Pre Engagement Letter.doc");
                        ////var document = app.Documents.Open(@"C:\Users\DYM Solutions\Documents\CP\Letter-Pre-Engagement(001)-FAIR(05).doc");
                        //document.ExportAsFixedFormat(fileName2.ToString(), WdExportFormat.wdExportFormatPDF);
                        //document.Close(ref missing, ref missing, ref missing);
                        //document = null;
                        //app.Quit(ref missing, ref missing, ref missing);
                        //app = null;

                        //server not working
                        //Word2Pdf objWorPdf = new Word2Pdf();
                        //string backfolder1 = @"C:\Users\CP";
                        //string strFileName = "Pre Engagement Letter.docx";
                        //object FromLocation = backfolder1 + "\\" + strFileName;
                        //string FileExtension = Path.GetExtension(strFileName);
                        //string ChangeExtension = strFileName.Replace(FileExtension, ".pdf");
                        //if (FileExtension == ".doc" || FileExtension == ".docx")
                        //{
                        //    object ToLocation = backfolder1 + "\\" + ChangeExtension;
                        //    objWorPdf.InputLocation = FromLocation;
                        //    objWorPdf.OutputLocation = ToLocation;
                        //    objWorPdf.Word2PdfCOnversion();
                        //}


                        byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Users\CP\Pre Engagement Letter.docx");
                        //byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Users\DYM Solutions\Downloads\Pre Engagement Letter.pdf");
                        if (System.IO.File.Exists(fileName2.ToString()))
                        {
                            System.IO.File.Delete(fileName2.ToString());
                        }
                        if (System.IO.File.Exists(fileName.ToString()))
                        {
                            System.IO.File.Delete(fileName.ToString());
                        }
                        return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "Pre Engagement Letter.docx");
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();

                string fileName = @"C:\Users\CP\Mahesh.txt";

                try
                {
                    // Check if file already exists. If yes, delete it.     
                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }

                    // Create a new file     
                    using (FileStream fs = System.IO.File.Create(fileName))
                    {
                        // Add some text to file    
                        Byte[] title = new UTF8Encoding(true).GetBytes("New Text File");
                        fs.Write(title, 0, title.Length);
                        byte[] author = new UTF8Encoding(true).GetBytes(e.ToString());
                        fs.Write(author, 0, author.Length);
                    }

                    // Open the stream and read it back.    
                    using (StreamReader sr = System.IO.File.OpenText(fileName))
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            Console.WriteLine(s);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.ToString());
                }
            }
            return RedirectToAction("EditLead", "Lead", new { requestId = requestId });
        }

        public async Task<ActionResult> GenerateLetterOfEngagement(string requestId)
        {
            try
            {

                var vm = new Models.UpdateLeadVIewModel();
                vm.Request = await RequestStore.GetRequestByRequestId(requestId);
                vm.Users = await UserStore.GetAllUser();

                if (vm.Request != null)
                {
                    vm.Profile = await ProfileStore.GetProfileByProfileId(vm.Request.ProfileId);
                    vm.Address = await AddressStore.GetAddressByAddressId(vm.Request.AddressId);
                    vm.Appointment = await AppointmentStore.GetAppointmentByAppointmentId(vm.Request.AppointmentId);
                    vm.Person = await PersonStore.GetPersonByProfileId(vm.Profile.ProfileId);
                    vm.Debtors = await DebtorStore.GetDebtorByRequestId(requestId);
                    vm.Profiles = await ProfileStore.GetAllProfile();

                    switch (vm.Profile.BusinessType)
                    {
                        case 1:
                            vm.Profile.Name += " Sdn Bhd";
                            break;
                        case 2:
                            vm.Profile.Name += " Berhad";
                            break;
                        default:
                            break;
                    }

                    if (vm.Profile != null && vm.Address != null && vm.Appointment != null)
                    {

                        string strDoc = @"C:\Users\CP\Letter-Engagement(023)-FAIR(20)-CP.docx";
                        OpenAndAddTextToWordDocument(strDoc, vm.Appointment.Date?.ToString("dd MMM yyyy"), vm.Profile.Name.ToUpper(), vm.Profile.BusinessRegNo, vm.Address, vm.Person, vm.Debtors, vm.Profiles, "Loe");

                        object fileName = @"C:\Users\CP\Letter Of Engagement.docx";
                        object fileName2 = @"C:\Users\CP\Letter Of Engagement.pdf";
                        //object fileName = @"C:\Users\DYM Solutions\Downloads\Pre Engagement Letter.doc";
                        //object fileName2 = @"C:\Users\DYM Solutions\Downloads\Pre Engagement Letter.pdf";
                        //object missing = System.Reflection.Missing.Value;

                        //var app = new Microsoft.Office.Interop.Word.Application();
                        //var document = app.Documents.Open(@"C:\Users\CP\Pre Engagement Letter.doc");
                        ////var document = app.Documents.Open(@"C:\Users\DYM Solutions\Documents\CP\Letter-Pre-Engagement(001)-FAIR(05).doc");
                        //document.ExportAsFixedFormat(fileName2.ToString(), WdExportFormat.wdExportFormatPDF);
                        //document.Close(ref missing, ref missing, ref missing);
                        //document = null;
                        //app.Quit(ref missing, ref missing, ref missing);
                        //app = null;

                        //server not working
                        //Word2Pdf objWorPdf = new Word2Pdf();
                        //string backfolder1 = @"C:\Users\CP";
                        //string strFileName = "Pre Engagement Letter.docx";
                        //object FromLocation = backfolder1 + "\\" + strFileName;
                        //string FileExtension = Path.GetExtension(strFileName);
                        //string ChangeExtension = strFileName.Replace(FileExtension, ".pdf");
                        //if (FileExtension == ".doc" || FileExtension == ".docx")
                        //{
                        //    object ToLocation = backfolder1 + "\\" + ChangeExtension;
                        //    objWorPdf.InputLocation = FromLocation;
                        //    objWorPdf.OutputLocation = ToLocation;
                        //    objWorPdf.Word2PdfCOnversion();
                        //}


                        byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Users\CP\Letter Of Engagement.docx");
                        //byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Users\DYM Solutions\Downloads\Pre Engagement Letter.pdf");
                        if (System.IO.File.Exists(fileName2.ToString()))
                        {
                            System.IO.File.Delete(fileName2.ToString());
                        }
                        if (System.IO.File.Exists(fileName.ToString()))
                        {
                            System.IO.File.Delete(fileName.ToString());
                        }
                        return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "Letter Of Engagement.docx");
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();

                string fileName = @"C:\Users\CP\Mahesh.txt";

                try
                {
                    // Check if file already exists. If yes, delete it.     
                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }

                    // Create a new file     
                    using (FileStream fs = System.IO.File.Create(fileName))
                    {
                        // Add some text to file    
                        Byte[] title = new UTF8Encoding(true).GetBytes("New Text File");
                        fs.Write(title, 0, title.Length);
                        byte[] author = new UTF8Encoding(true).GetBytes(e.ToString());
                        fs.Write(author, 0, author.Length);
                    }

                    // Open the stream and read it back.    
                    using (StreamReader sr = System.IO.File.OpenText(fileName))
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            Console.WriteLine(s);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.ToString());
                }
            }
            return RedirectToAction("EditLead", "Lead", new { requestId = requestId });
        }

        public static void OpenAndAddTextToWordDocument(string filepath, string date, string name, string businessRegNo, Address address, Person person, List<Debtor> debtors, List<Profile> profiles, string Type)
        {
            try
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(filepath);
                using (MemoryStream stream = new MemoryStream())
                {
                    stream.Write(byteArray, 0, (int)byteArray.Length);
                    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                    {
                        // Assign a reference to the existing document body.
                        Body body = wordDoc.MainDocumentPart.Document.Body;

                        var paras = body.Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>();

                        //Iterate through the paragraphs to find the bookmarks inside
                        foreach (var para in paras)
                        {
                            var bookMarkStarts = para.Elements<BookmarkStart>();
                            var bookMarkEnds = para.Elements<BookmarkEnd>();


                            foreach (BookmarkStart bookMarkStart in bookMarkStarts)
                            {
                                if (Type == "Pre")
                                {
                                    if (bookMarkStart.Name == "date1")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(date));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "company1")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(name));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "companyregno1")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(businessRegNo));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "address1")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(address.Line1 + " " + address.Line2 + " " + address.PostalCode + " " + address.City + " " + address.State + " " + address.Country));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }
                                }
                                else if (Type == "Loe")
                                {
                                    if (bookMarkStart.Name == "Address")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(address.Line1 + " " + address.Line2 + " " + address.PostalCode + " " + address.City + " " + address.State + " " + address.Country));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "CreditorCompanyNameBusinessRegNo")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(name + "(" + businessRegNo + ")"));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "CreditorCompanyNameBusinessRegNo2")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(name + "(" + businessRegNo + ")"));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "CreditorCompanyNameBusinessRegNo3")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(name + "(" + businessRegNo + ")"));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "CreditorPersonInCharge")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(person.Name + "(Director of the Company)"));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "Date")
                                    {
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(DateTime.Now.ToString("dd MMM yyyy")));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }

                                    if (bookMarkStart.Name == "DebtorCompanyNameBusinessRegNo")
                                    {
                                        var dp = profiles.Find(x => x.ProfileId == debtors[0].ProfileId);
                                        //Get the id of the bookmark start to find the bookmark end
                                        var id = bookMarkStart.Id.Value;
                                        var bookmarkEnd = bookMarkEnds.Where(i => i.Id.Value == id).First();

                                        var runElement = new Run(new Text(dp.Name + "(" + dp.BusinessRegNo + ")"));

                                        para.InsertAfter(runElement, bookmarkEnd);

                                    }
                                }
                            }
                        }
                        // Close the handle explicitly.
                        wordDoc.Close();
                    }
                    string loc = "C:\\Users\\CP";
                    string file = null;
                    if (Type == "Pre")
                    {
                        file = "Pre Engagement Letter.docx";
                    }
                    else if (Type == "Loe")
                    {
                        file = "Letter Of Engagement.docx";
                    }
                    // Save the file with the new name
                    System.IO.File.WriteAllBytes(loc + "\\" + file, stream.ToArray());
                }
            }
            catch (Exception e)
            {
                e.ToString();

                string fileName = @"C:\Users\CP\Mahesh2.txt";

                try
                {
                    // Check if file already exists. If yes, delete it.     
                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }

                    // Create a new file     
                    using (FileStream fs = System.IO.File.Create(fileName))
                    {
                        // Add some text to file    
                        Byte[] title = new UTF8Encoding(true).GetBytes("New Text File");
                        fs.Write(title, 0, title.Length);
                        byte[] author = new UTF8Encoding(true).GetBytes(e.ToString());
                        fs.Write(author, 0, author.Length);
                    }

                    // Open the stream and read it back.    
                    using (StreamReader sr = System.IO.File.OpenText(fileName))
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            Console.WriteLine(s);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.ToString());
                }
            }

        }
        public async Task<JsonResult> checkCreditorProfile(string businessRegNo)
        {
            var vm = new Models.UpdateLeadVIewModel();

            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Profiles.RemoveAll(x => x.ProfileType == 2);

            vm.ProfileExists = vm.Profiles.Exists(p => p.BusinessRegNo == businessRegNo);
            if (vm.ProfileExists)
            {
                vm.Profile = vm.Profiles.First(x => x.BusinessRegNo == businessRegNo);
                vm.Address = await AddressStore.GetAddressByProfileId(vm.Profile.ProfileId);
                vm.Person = await PersonStore.GetPersonByProfileId(vm.Profile.ProfileId);
            }
            vm.Profiles.RemoveAll(x => x.ProfileId != null);
            return Json(vm);
        }
        public async Task<JsonResult> checkDebtorProfile(string businessRegNo, string requestId)
        {
            var vm = new Models.UpdateLeadVIewModel();

            vm.Profiles = await ProfileStore.GetAllProfile();
            vm.Debtors = await DebtorStore.GetAllDebtor();
            vm.Requests = await RequestStore.GetAllRequest();
            vm.Profiles.RemoveAll(x => x.ProfileType == 1);
            vm.Requests.RemoveAll(x => x.Status == null);

            vm.ProfileExists = vm.Profiles.Exists(p => p.BusinessRegNo == businessRegNo);
            if (vm.ProfileExists)
            {
                vm.Profile = vm.Profiles.First(x => x.BusinessRegNo == businessRegNo);
                vm.Debtors = vm.Debtors.FindAll(x => x.ProfileId == vm.Profile.ProfileId);
                vm.Requests = vm.Requests.Where(x => vm.Debtors.Any(db => db.RequestId == x.RequestId)).ToList();
            }
            vm.Requests.RemoveAll(x => x.RequestId == requestId);
            vm.Profiles.RemoveAll(x => x.ProfileId != null);

            return Json(vm);
        }

        #region Request Services
        public async Task<ActionResult> EditRequestServices(string RequestId, string cameFrom, string profileId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            string position = Utils.GetUserGroup();
            var vm = new Models.RequestServicesViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.PackageServicesType = await PackageStore.GetPackageServicesType();
            vm.Sops = await SopStore.SopList();
            vm.RequestServices = await RequestServiceStore.GetRequestServicesByRequestId(RequestId);
            vm.SopRequests = await SopRequestsStore.GetSopRequestsByRequestsId(RequestId);
            vm.RequestId = RequestId;
            vm.ProfileId = profileId;

            vm.Subscriber = await SubscriptionStore.GetSubscriptionByProfileId(profileId);

            if (vm.Subscriber != null)
            {
                vm.IsSubscriber = true;
            }

            return View(vm);
        }

        public async Task<ActionResult> EditRequestServicesPreview(string RequestId, string cameFrom, string profileId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            string position = Utils.GetUserGroup();
            var vm = new Models.RequestServicesViewModel();
            vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);
            vm.PackageServicesType = await PackageStore.GetPackageServicesType();
            vm.Sops = await SopStore.SopList();
            vm.RequestServices = await RequestServiceStore.GetRequestServicesByRequestId(RequestId);
            vm.SopRequests = await SopRequestsStore.GetSopRequestsByRequestsId(RequestId);
            vm.RequestId = RequestId;
            vm.ProfileId = profileId;

            vm.Subscriber = await SubscriptionStore.GetSubscriptionByProfileId(profileId);

            if (vm.Subscriber != null)
            {
                vm.IsSubscriber = true;
            }

            return View(vm);
        }

        public async Task<JsonResult> UpdateRequestService(List<RequestServices> requestServices, string RequestId, string ProfileId, string SubscriptionId)
        {
            string id = Utils.GetUserId();
            var r = new Response();

            if (await RequestServiceStore.UpdateRequestServices(requestServices, id, RequestId, ProfileId, SubscriptionId))
            {
                r.Message = "Services saved successfully.";
                r.RedirectUrl = "url";
            }
            else
            {
                r.Error = "Unable to save services, please try again later.";
            }

            return Json(r);
        }
        #endregion
    }
}