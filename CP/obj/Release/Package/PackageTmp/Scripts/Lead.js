﻿
function init() {
    bindSaveLeadButtonClick();
    //bindUploadButtonClick();
    //bindEmploymentTypeChange();
    //bindIncentiveChange();
    //bindWageTypeChange();
    //bindProfilePictureUpload();
    initInputRestriction();
    requestMore();
    stage9();
    dispute();
    rejected();
    complete();
    release();
    checkAllRequest();
}

function checkAllRequest() {
    $('#debtorTbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            var x = $(this).children().offset();
            x.left += 49;
            x.top -= 4;
            $.ajax({
                type: "POST",
                url: "/Lead/checkDebtorProfile",
                data: '{businessRegNo: "' + $(this).data('brn') + '" , requestId: "' + $('#requestId').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.ProfileExists) {
                        //Email available.

                        if (data.Requests.length != 0) {
                            var request = "";
                            var id = data.Debtors[0].DebtorId;
                            $.each(data.Requests, function (index, value) {
                                request += 'data-value-' + index + '="' + value.RequestId + ';' + value.id + ';' + value.Status.substring(0, 4) + '"';
                            });
                            $('<a id=' + id +' onClick="showCasesPre(this)" data-id='+id+' ' + request + ' style="position:absolute;"><img src="/Image/179386.png" width="18" height="18"></a>').insertAfter($('#creditor'));
                            $('#'+id+'').css(x);
                        }
                    }
                }
            });
        });
    });
}

function initInputRestriction() {
    //var appointmentType = [], docType = [];

    ////Iterate all td's in second column$('#inptBlacklistfile tbody tr td:last-child').each(function () {
    //$('#inptRequesterfile tbody tr td:last-child').each(function () {
    //    $(this).find('a').each(function () {
    //        appointmentType.push($(this).data('type'));
    //    });
    //});

    //$('#inptAppointmentFile tbody tr td:last-child').each(function () {
    //    $(this).find('.delete').each(function () {
    //        docType.push($(this).data('type'));
    //    });
    //});

    //$.each(appointmentType, function (e) {
    //    $.each(docType, function (ee) {
    //        if (e == "3E653BCE-AE1B-46C4-840D-A75A5B4EDF15") {
    //            if (ee == "FD42D4E6-D6EA-4A79-B57A-528F93A5A3B8") {
    //                return true;
    //            }
    //        }
    //        else if (e == "95CBF154-443F-4E49-B33F-F647E9BE5B1B") {
    //            if (ee == "FB13C881-4032-4B92-B25F-6CFF7BFB55E4") {
    //                return true;
    //            }
    //        }
    //    });
    //});

    $('.datepicker').keypress(function () {
        return false;
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    $('.name').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    $('#businessRegNo').blur(function () {
        $.ajax({
            type: "POST",
            url: "/Lead/checkCreditorProfile",
            data: '{businessRegNo: "' + $('#businessRegNo').val() + '" , requestId: "' + $('#requestId').val() + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                debugger
                if (data.ProfileExists) {
                    //Email available.
                    alert("Profile Exists.");

                    $('#ProfileId').val(data.Profile.ProfileId);
                    $('#name').val(data.Profile.Name);

                    if (data.Profile.BusinessType == 1) {
                        $('#c1').prop("checked", true);
                        $('#choice').val("Company");
                    }
                    else if (data.Profile.BusinessType == 2) {
                        $('#c2').prop("checked", true);
                        $('#choice').val("Company");
                    }
                    else if (data.Profile.BusinessType == 3) {
                        $('#b1').prop("checked", true);
                        $('#choice').val("Business");
                    }
                    else if (data.Profile.BusinessType == 4) {
                        $('#b2').prop("checked", true);
                        $('#choice').val("Business");
                    }
                    $('#choice').change();

                    $('#contactNo').val(data.Profile.ContactNo);
                    $('#email').val(data.Profile.Email);

                    if (data.Profile.DMS) {
                        $('#dms').val("true");
                    }
                    else {
                        $('#dms').val("false");
                    }

                    $('#source').val(data.Profile.Source);
                    $('#website').val(data.Profile.Website);

                    $('#AddressId').val(data.Address.AddressId);
                    $('#line1').val(data.Address.Line1);
                    $('#line2').val(data.Address.Line2);
                    $('#city').val(data.Address.City);
                    $('#state').val(data.Address.State);
                    $('#postalCode').val(data.Address.PostalCode);
                    //$('#country').val(data.Address.Country);

                    $('#PersonId').val(data.Person.PersonId)
                    $('#picName').val(data.Person.Name)
                    $('#picContact').val(data.Person.ContactNo)
                    $('#picEmail').val(data.Person.Email)
                }
                else {
                    //Email not available.
                    //alert("not ok");
                }
            }
        });
    });

    $('#BusinessRegNo').blur(function () {
        $.ajax({
            type: "POST",
            url: "/Lead/checkDebtorProfile",
            data: '{businessRegNo: "' + $('#BusinessRegNo').val() + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.ProfileExists) {
                    $('#info').remove();
                    $('#Requests').remove();
                    //Email available.
                    var x = $('#addDebtor').offset();
                    x.left += 60;
                    alert("Profile Exists.");

                    //$('#ProfileId').val(data.Profile.ProfileId);
                    $('#CompanyName').val(data.Profile.Name);
                    $('#ContactNo').val(data.Profile.ContactNo);

                    if (data.Requests.length != 0) {
                        
                        var request = "";
                        $.each(data.Requests, function (index, value) {
                            request += 'data-value-' + index + '="' + value.RequestId + ';' + value.id + ';' + value.Status.substring(0, 4) + '"';
                        });
                        $('<a id="info" onClick="showCases(this)" ' + request + ' style="position:absolute;"><img src="/Image/179386.png" width="18" height="18"></a>').insertAfter($('#creditor'));
                        $('#info').css(x);
                    }
                }
                else {
                    $('#info').remove();
                    $('#Requests').remove();
                }
            }
        });
    });
    
    //$('#inptIdentityNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});

    //$('#inptAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptAddress').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptHomeAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptHomeAddress').blur(function () {
    //    trimSpacing(this);
    //});

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });
    //$('#inptOfficeContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptNationality').change(function () {
    //    if ($('#inptNationality option:selected').data('keyword') === 'MY') {
    //        $('[name=epfsocso]').prop('disabled', true);
    //    }
    //    else {
    //        $('[name=epfsocso]').prop('disabled', false);
    //    }
    //});

    //$('#inptDocumentNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});
    //$('#inptDocumentNo').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptRemark').keydown(function (e) {
    //    restrictAlphanumericInput(e);
    //});
    //$('#inptRemark').blur(function () {
    //    trimSpacing(this);
    //});

    //$('#inptEMPName').keydown(function (e) {
    //    restrictAlphabetInput(e);
    //});
    //$('#inptEMPName').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptEMPContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptAdvancePaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});

    //$('#inptMonthlyRepaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});
}

//function add() {
//    $('#assignToId').val($('#assignTo option:selected').val());
//    var r = $('#Assign tbody tr').length + 1;
//    var row = "";
//    var a = $('#assignTo option:selected').text();
//    $('#Assign tbody')[0].innerHTML = '';
//    row += '<tr>' +
//        '<td width="96">' + a + '</td>' +
//        '<td>' + "@DateTime.Now.ToShortDateString()" + '</td> ' +
//        '<td width="20" data-value="" style="text-align:center;"><a class="delete" title="Delete" onclick="remove(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
//    '</tr>';

//    $('#Assign').append(row);
//}

//function remove(e) {
//    $('#assignToId option:selected').val('');
//    $(e).parent().parent().remove();
//    var r = $('#Assign tbody tr').length + 1;
//    var row = "";
//    $('#Assign tbody')[0].innerHTML = '';
//    row += '<tr>' +
//        '<td colspan="2"><select id="assignTo" class="form-control" title="Select Risk Management Officer" @if(position != "Operation") { @disabled } > @foreach (var u in Model.User){<option value="@u.UserId">@u.Name</option>} </select></td>' +
//        '<td style="text-align:center; vertical-align:middle" width="20"><a onclick="add()" class="btn btn-xs btn-info">Add</a></td>'
//    '</tr>';

//    $('#Assign').append(row);
//}

//function uploadDoc(type) {
//    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectPex').val() !== "") {
//        var formdata = new FormData(); //FormData object
//        formdata.append("RequestId", $('#requestId').val());
//        formdata.append("UploadBy", type);
//        var fileInput;
//        if (type === "requester") {
//            fileInput = document.getElementById('fileSelectRequester');
//        }
//        else {
//            formdata.append("ClassificationId", $('input[name="classification"]:checked').val());
//            fileInput = document.getElementById('fileSelectPex');
//        }
//        //Iterating through each files selected in fileInput
//        for (var i = 0; i < fileInput.files.length; i++) {
//            //Appending each file to FormData object
//            //formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
//            formdata.append(fileInput.files[i].name, fileInput.files[i]);
//        }

//        //Creating an XMLHttpRequest and sending
//        var xhr = new XMLHttpRequest();
//        xhr.open('POST', window.location.origin + '/Lead/AttachmentUpload');
//        xhr.send(formdata);
//        xhr.onreadystatechange = function () {
//            if (xhr.readyState === 4 && xhr.status === 200) {
//                var d = JSON.parse(xhr.response);

//                debugger
//                var r = $('#inptRequesterfile tbody tr').length + 1;
//                var row = "";
//                if (type === "requester") {
//                    $('#inptRequesterfile tbody')[0].innerHTML = '';
//                    row += '<tr>' +
//                        '<td>' + d.FileName + '</td>' +
//                        '<td>' + d.Size + '</td> ' +
//                        '<td>' + d.CreatedDateString + '</td>' +
//                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
//                    '</tr>';

//                    $('#inptRequesterfile').append(row);
//                    $('#fileSelectRequester').val('');
//                }
//                else {
//                    $('#inptPexfile tbody')[0].innerHTML = '';
//                    for (i = 0; i < d.length; i++) {
//                        row += '<tr>' +
//                            '<td>' + d.fileName + '</td>' +
//                            '<td>' + d.Size + '</td> ' +
//                            '<td>' + d.CreatedDateString + '</td>' +
//                            '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
//                        '</tr>';
//                    }

//                    $('#inptPexfile').append(row);
//                    $('#fileSelectPex').val('');

//                    if ($('#inptPexfile tbody tr').length > 0) {
//                        $('#pexFileUpload').toggle();
//                    }
//                    else {
//                        $('#pexFileUpload').toggle();
//                    }
//                }
//            }
//        };
//    }
//}

//function removeAttachment(e) {
//    $.ajax({
//        url: window.location.origin + "/Lead/DeleteAttachment",
//        data: { "documentId": $(e).data('value') },
//        cache: false,
//        type: "POST",
//        dataType: "html",
//        success: function (data, textStatus, XMLHttpRequest) {
//            var d = JSON.parse(data);
//            if (d.Error === null) {
//                showToast(d.Message, "success");
//                $(e).parent().parent().remove();
//            }
//            else {
//                showToast(d.Error, "error");
//            }
//        }
//    });
//}

function showCasesPre(e) {
    $('#' + $(e).data('id') + '').attr("onclick", "closeCasesPre(this)");
    var x = $('#' + $(e).data('id') + '').offset();
    x.top += 30;
    var Requests = "<table class='table table-bordered' style='position:absolute;' id='" + $(e).data('id') + "-id' >";

    var bool = true;
    $.each(e.dataset, function (index, value) {
        if (!bool) {
            var arr = value.split(';');
            Requests += "<tr><td><a style='font-size:9px; font-style:italic; color:blue;' target='_blank' href='/Lead/EditLeadPreview?requestId=" + arr[0] + "'>" + arr[2] + "-" + arr[1] + "</a><td></tr>";
        }
        bool = false;
    });

    Requests += "</table>";
    $(Requests).insertAfter($('#' + $(e).data('id') + ''));
    $('#' + $(e).data('id') + '-id').css(x);
}

function closeCasesPre(e) {
    debugger
    $('#' + $(e).data('id') + '-id').remove();
    $('#' + $(e).data('id') + '').attr("onclick", "showCasesPre(this)");
}

function showCases(e) {
    $("#info").attr("onclick", "closeCases()");
    var x = $('#info').offset();
    x.top += 30;
    var Requests = "<table class='table table-bordered' style='position:absolute;' id='Requests'>";

    $.each(e.dataset, function (index, value) {
        var arr = value.split(';');
        Requests += "<tr><td><a style='font-size:9px; font-style:italic; color:blue;' target='_blank' href='/Lead/EditLeadPreview?requestId="+arr[0]+"'>"+arr[2]+"-"+arr[1]+"</a><td></tr>";
    });

    Requests += "</table>";
    $(Requests).insertAfter($('#info'));
    $('#Requests').css(x);
}

function closeCases() {
    $('#Requests').remove();
    $("#info").attr("onclick", "showCases(this)");
}

function addDebtor() {
    var choice = $('#choice option:selected').val();
    var cname = $('#CompanyName').val();
    var cno = $('#ContactNo').val();
    var brn = $('#BusinessRegNo').val();
    var ca = $('#ClaimAmount').val();
    var r = $('#DebtorRemark').val();
    var isrb = $('input:radio[name=InsolvencyStatusRB]:checked').val();
    var mdisrb = $('input:radio[name=MDIStatusRB]:checked').val();
    var sibrb = $('input:radio[name=StillInBusinessRB]:checked').val();
    var isrbText = $('input:radio[name=InsolvencyStatusRB]:checked').data('value');
    var mdisrbText = $('input:radio[name=MDIStatusRB]:checked').data('value');
    var sibrbText = $('input:radio[name=StillInBusinessRB]:checked').data('value');
    if (ca == "") {
        ca = "0.00";
    }

    var Debtor = {
        "RequestId": $('#requestId').val(),
        "CompanyName": cname,
        "ContactNo": cno,
        "BusinessRegNo": brn,
        "ClaimAmount": parseFloat(ca.replace(/,/g, '')),
        "StillInBusiness": sibrb,
        "Remark": r,
        "InsolvencyStatus": isrb,
        "MDIStatus": mdisrb
    };

    $.ajax({
        url: window.location.origin + "/Lead/AddDebtor",
        data: { "Debtor": Debtor },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);

            var row = "";

            var row2 = "";
            if (choice == "Company") {
                row = '<tr>' +
                    '<td><a data-type="show" data-value="' + d.DebtorId + '" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + ca + '" data-r="' + r + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" onclick="Debtor(this)">' + brn + '</a></td>' +
                    '<td>' + cname + '</td>' +
                    '<td>' + ca + '</td>' +
                    '<td>' + isrbText + '</td>' +
                    '<td>' + mdisrbText + '</td>' +
                    '<td>' + sibrbText + '</td>' +
                    '<td style="text-align:center;"><a class="delete" title="Delete" onclick="removeDebtor(this)" data-value="' + d.DebtorId + '" data-doc="debtor" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" data-r="' + r + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                '</tr>';
            }
            else {
                row = '<tr>' +
                    '<td><a data-type="show" data-value="' + d.DebtorId + '" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + ca + '" data-r="' + r + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" onclick="Debtor(this)">' + brn + '</a></td>' +
                    '<td>' + cname + '</td>' +
                    '<td>' + ca + '</td>' +
                    '<td>' + sibrbText + '</td>' +
                    '<td style="text-align:center;"><a class="delete" title="Delete" onclick="removeDebtor(this)" data-value="' + d.DebtorId + '" data-doc="debtor" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" data-r="' + r + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                '</tr>';
            }
            $('#inptDebtor').last().append(row);
        }
    });

    $('#CompanyName').val('');
    $('#ContactNo').val('');
    $('#BusinessRegNo').val('');
    $('#ClaimAmount').val('');
    $('#DebtorRemark').val('');
    $("input[name=InsolvencyStatusRB][value=false]").prop('checked', true);
    $("input[name=MDIStatusRB][value=false]").prop('checked', true);
    $("input[name=StillInBusinessRB][value=false]").prop('checked', true);

    $('#info').remove();
    $('#Requests').remove();
}

function removeDebtor(e) {
    $.ajax({
        url: window.location.origin + "/Lead/DeleteDebtor",
        data: { "DebtorId": $(e).data('value') },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                $(e).parent().parent().remove();
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function Debtor(e) {
    if ($(e).data('type') == "show") {
        debugger
        $('#addDebtor').css('display', 'none');
        $('#hideDebtor').css('display', 'inline');

        var cname = $(e).data('cname');
        var cno = $(e).data('cno');
        var brn = $(e).data('brn');
        var ca = $(e).data('ca');
        var r = $(e).data('r');
        var isrb = $(e).data('isrb');
        var mdisrb = $(e).data('mdisrb');
        var sibrb = $(e).data('sibrb');
        if (ca == "") {
            ca = "0.00";
        }

        $('#CompanyName').val(cname);
        $('#ContactNo').val(cno);
        $('#BusinessRegNo').attr('disabled', 'disabled');
        $('#BusinessRegNo').val(brn);
        $('#ClaimAmount').val(ca);
        $('#DebtorRemark').val(r);
        $("input[name=InsolvencyStatusRB][value=" + isrb + "]").prop('checked', true);
        $("input[name=MDIStatusRB][value=" + mdisrb + "]").prop('checked', true);
        $("input[name=StillInBusinessRB][value=" + sibrb + "]").prop('checked', true);

        $('#info').remove();
        $('#Requests').remove();
    }
    else {
        $('#addDebtor').css('display', 'inline');
        $('#hideDebtor').css('display', 'none');

        $('#CompanyName').val('');
        $('#ContactNo').val('');
        $('#BusinessRegNo').val('');
        $('#BusinessRegNo').removeAttr('disabled');
        $('#ClaimAmount').val('');
        $('#DebtorRemark').val('');

        $("input[name=InsolvencyStatusRB][value=False]").prop('checked', true);
        $("input[name=MDIStatusRB][value=False]").prop('checked', true);
        $("input[name=StillInBusinessRB][value=False]").prop('checked', true);
    }
}

function uploadDoc(doctype) {
    var docType2 = null;
    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectPex').val() !== "") {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#requestId').val());
        var fileInput;
        if (doctype === "65FECCE2-AA95-412D-95D2-0A271273B08F") {
            fileInput = document.getElementById('fileSelectRequester');
        }
        else if (doctype === "7F760368-629F-43A4-A2CB-5F2F2C4003B4") {
            fileInput = document.getElementById('fileSelectFinancialStatement');
            formdata.append("FYE", $('#FinancialYearEnd1').val());
            formdata.append("AT", $('#AnnualTurnover1').val());
            formdata.append("NTA", $('#NtaPositiveNegative1').val());
            formdata.append("PAT", $('#ProfitAfterTax1').val());
        }
        else if (doctype === "CE756226-33DF-4F9B-96BD-BBA9687B90ED") {
            var ca = $('#LRClaimAmount').val();
            if (ca == "") {
                ca = "0.00";
            }
            fileInput = document.getElementById('fileSelectLitigation');
            formdata.append("CA", ca);
            formdata.append("SOC", $('#StatusOfCase1').val());
            formdata.append("HD", $('#HearingDate').val());
            formdata.append("P", $('#Plaintiff').val());
            formdata.append("D", $('#Defendant').val());
        }
        else if (doctype === "AB94BE85-7EED-4307-B8CE-9A799BF090B6") {
            var ca = $('#BRClaimAmount').val();
            if (ca == "") {
                ca = "0.00";
            }
            fileInput = document.getElementById('fileSelectBlacklist');
            formdata.append("C", $('#Creditor1').val());
            formdata.append("SD", $('#StatementDate').val());
            formdata.append("CA", ca);
        }
        else if (doctype === "AppointmentDoc") {
            fileInput = document.getElementById('fileSelectAppointment');
            doctype = $('#appointmentDocType').val();
            docType2 = "AppointmentDoc";
        }
        else if (doctype === "PreAndSoa") {
            fileInput = document.getElementById('fileSelectPreAndSoa');
            doctype = $('#preAndSoaDocType').val();
            docType2 = "PreAndSoa";
        }
        else if (doctype === "B8084C0D-1096-4F0F-A747-8B76DB48B1DC") {
            fileInput = document.getElementById('fileSelectCtos');
        }
        else if (doctype === "lawyerDoc") {
            fileInput = document.getElementById('fileSelectLawyerDoc');
            doctype = $('#lawyerDocType').val();
            docType2 = "lawyerDoc";
        }
        else if (doctype === "D307B976-AF64-4B34-8CED-FE3DD69A2195") {
            fileInput = document.getElementById('fileSelectDispute');
        }
        else if (doctype === "6EB5BD98-1305-42FD-BED8-919F03BFD78B") {
            fileInput = document.getElementById('fileSelectJid');
        }
        else if (doctype === "settlementAgreement") {
            fileInput = document.getElementById('fileSelectSettlement');
            doctype = $('#settlementDocType').val();
            docType2 = "settlementAgreement";
        }
        else if (doctype === "financeDoc") {
            fileInput = document.getElementById('fileSelectFinanceDoc');
            doctype = $('#financeDocType').val();
            docType2 = "financeDoc";
        }
        else if (doctype === "272B6077-CBA6-4855-80EB-9D52E5B76D22") {
            fileInput = document.getElementById('fileSelectInvoiceDoc');
        }

        formdata.append("Type", doctype);
        formdata.append("ProfileId", $('#ProfileId').val());

        //Iterating through each files selected in fileInput
        for (var i = 0; i < fileInput.files.length; i++) {
            //Appending each file to FormData object
            //formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/Lead/AttachmentUpload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);

                debugger
                if (doctype === "65FECCE2-AA95-412D-95D2-0A271273B08F") {
                    var row = '<tr>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Type + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptRequesterfile').last().append(row);
                    $('#fileSelectRequester').val('');

                    //location.reload();
                }
                else if (doctype === "7F760368-629F-43A4-A2CB-5F2F2C4003B4") {
                    var fye = $('#FinancialYearEnd1').val();
                    var at = $('#AnnualTurnover1').val();
                    var npn = $('#NtaPositiveNegative1').val();
                    var pat = $('#ProfitAfterTax1').val();

                    var row = '<tr>' +
                        '<td>' + fye + '</td>' +
                        '<td>' + at + '</td> ' +
                        '<td>' + npn + '</td>' +
                        '<td>' + pat + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '" data-fye="' + fye + '" data-at="' + parseFloat(at.replace(/,/g, '')) + '" data-npn="' + parseFloat(npn.replace(/,/g, '')) + '" data-pat="' + parseFloat(pat.replace(/,/g, '')) + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptFinancialStatementfile').last().append(row);
                    $('#fileSelectFinancialStatement').val('');
                    $('#FinancialYearEnd1').val('');
                    $('#AnnualTurnover1').val('');
                    $('#NtaPositiveNegative1').val('');
                    $('#ProfitAfterTax1').val('');

                }
                else if (doctype === "CE756226-33DF-4F9B-96BD-BBA9687B90ED") {
                    var ca = $('#LRClaimAmount').val();
                    if (ca == "") {
                        ca = "0.00";
                    }
                    var socN = $('#StatusOfCase1 option:selected').text();
                    var soc = $('#StatusOfCase1').val();
                    var hd = $('#HearingDate').val();
                    var p = $('#Plaintiff').val();
                    var dd = $('#Defendant').val();

                    var row = '<tr>' +
                        '<td>' + ca + '</td>' +
                        '<td>' + socN + '</td> ' +
                        '<td>' + hd + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-doc="litigation" data-value="' + d.DocumentId + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '" data-soc="' + soc + '" data-hd="' + hd + '" data-p="' + p + '" data-dd="' + dd + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    var row2 = '<tr>' +
                        '<td>' + p + '</td>' +
                        '<td data-value"' + d.DocumentId + '">' + dd + '<span class="l" hidden data-value="' + d.DocumentId + '"></span></td>' +
                        '</tr>';

                    $('#inptLitigationfile').last().append(row);
                    $('#inptLitigationfile2').last().append(row2);
                    $('#fileSelectLitigation').val('');
                    $('#LRClaimAmount').val('');
                    $('#StatusOfCase1').val($("#StatusOfCase1 option:first").val());
                    $('#HearingDate').val('');
                    $('#Plaintiff').val('');
                    $('#Defendant').val('');

                }
                else if (doctype === "AB94BE85-7EED-4307-B8CE-9A799BF090B6") {
                    var c = $('#Creditor1').val();
                    var sd = $('#StatementDate').val();
                    var ca = $('#BRClaimAmount').val();
                    if (ca == "") {
                        ca = "0.00";
                    }

                    var row = '<tr>' +
                        '<td>' + c + '</td>' +
                        '<td>' + sd + '</td> ' +
                        '<td>' + ca + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '" data-c="' + c + '" data-sd="' + sd + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptBlacklistfile').last().append(row);
                    $('#fileSelectBlacklist').val('');
                    $('#Creditor1').val('');
                    $('#StatementDate').val('');
                    $('#BRClaimAmount').val('');

                }
                else if (docType2 == "AppointmentDoc") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptAppointmentFile').append(row);
                    $('#fileSelectAppointment').val('');
                    $('#preEngagement').remove();
                    $('#letterOfEngagement').remove();

                    //location.reload();
                }
                else if (docType2 == "PreAndSoa") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptPreAndSoa').last().append(row);
                    $('#fileSelectPreAndSoa').val('');

                    //location.reload();
                }
                else if (doctype === "B8084C0D-1096-4F0F-A747-8B76DB48B1DC") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptCtos').last().append(row);
                    $('#fileSelectCtos').val('');

                    //location.reload();
                }
                else if (docType2 == "lawyerDoc") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptLawyerDoc').last().append(row);
                    $('#fileSelectLawyerDoc').val('');

                    location.reload();
                }
                else if (doctype === "D307B976-AF64-4B34-8CED-FE3DD69A2195") {
                    $('#inptDispute').next('.invalid-alert').remove();

                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptDispute').last().append(row);
                    $('#fileSelectDispute').val('');
                }
                else if (doctype === "6EB5BD98-1305-42FD-BED8-919F03BFD78B") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptJid').last().append(row);
                    $('#fileSelectJid').val('');

                    //location.reload();
                }
                else if (docType2 == "settlementAgreement") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptSettlement').append(row);
                    $('#fileSelectSettlement').val('');

                    //location.reload();
                }
                else if (docType2 == "financeDoc") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptFinanceDoc').last().append(row);
                    $('#fileSelectFinanceDoc').val('');

                    //location.reload();
                }
                else if (doctype === "272B6077-CBA6-4855-80EB-9D52E5B76D22") {
                    var row = '<tr>' +
                        '<td>' + d.Type + '</td>' +
                        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptInvoiceDoc').last().append(row);
                    $('#fileSelectInvoiceDoc').val('');

                    location.reload();
                }
            }
        };
    }
}

function removeAttachment(e, ee) {
    if (confirm("Please click OK to confirm delete.")) {
        $.ajax({
            url: window.location.origin + "/Lead/DeleteAttachment",
            data: { "documentId": $(e).data('value') },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if ($(e).data('doc') != null) {
                        var l = $(e).data('value');
                        $('#inptLitigationfile2 tbody tr td:last-child').each(function () {
                            $(this).find('.l').each(function () {
                                if ($(this).data('value') == l) {
                                    $(this).parent().parent().remove();
                                }
                            });
                        });
                    }
                    $(e).parent().parent().remove();
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function bindSaveLeadButtonClick() {
    var page = $('#page').val();
    var bb = $('#bool').val();
    debugger
    $('#btnSave').click(function () {
        if (inputValidation()) {

            var assign = $('#assignToId').val();
            if (assign == " ") {
                assign = null;
            }

            var lawFirm = $('#lawFirmId').val();
            if (lawFirm == " ") {
                lawFirm = null;
            }

            var negotiator = $('#negotiatorId').val();
            if (negotiator == " ") {
                negotiator = null;
            }

            var profile = {
                "ProfileId": $('#ProfileId').val(),
                "Name": $('#name').val(),
                "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
                "ProfileType": "1",
                "ContactNo": $('#contactNo').val(),
                "Email": $('#email').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "DMS": $('#dms').val(),
                "Source": $('#source').val(),
                "NatureOfBusiness": $('#natureOfBusiness').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val()
            };

            var address = {
                "AddressId": $('#AddressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var person = {
                "PersonId": $('#PersonId').val(),
                "Name": $('#picName').val(),
                "ContactNo": $('#picContact').val(),
                "Email": $('#picEmail').val()
            };

            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            var lead = {
                "LeadId": $('#LeadId').val(),
                "AssignTo": assign
            };

            var request = {
                "AppointmentId": $('#AppointmentId').val(),
                "RequestId": $('#requestId').val(),
                "LawFirm": lawFirm,
                "NegotiatorId": negotiator
            };

            var afi = {
                "AssetForInvestigationId": $('#AssetForInvestigationId').val(),
                "PropertySearchResult1": $('#PropertySearchResult1').val(),
                "PropertySearchResult2": $('#PropertySearchResult2').val(),
                "VehicleDetails1": $('#VehicleDetails1').val(),
                "VehicleDetails2": $('#VehicleDetails2').val(),
                "SiteInvestigation": $('#SiteInvestigation').val()
            };

            var cfi = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    cfi.push({
                        "FinancialYearEnd1": $(this).data('fye'), "AnnualTurnover1": $(this).data('at'), "NtaPositiveNegative1": $(this).data('npn'),
                        "ProfitAfterTax1": $(this).data('pat'), "DocumentId": $(this).data('value'), "CompanyFinancialInformationId": $(this).data('cfid')
                    });
                });
            });

            var lr = [];
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    lr.push({
                        "ClaimAmount": $(this).data('ca'), "StatusOfCaseId": $(this).data('soc'), "HearingDate": $(this).data('hd'),
                        "Plaintiff": $(this).data('p'), "Defendant": $(this).data('dd'), "DocumentId": $(this).data('value'), "LitigationRecordId": $(this).data("lrid")
                    })
                });
            });

            var br = [];
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    br.push({
                        "Creditor1": $(this).data('c'), "StatementDate": $(this).data('sd'), "ClaimAmount": $(this).data('ca'), "DocumentId": $(this).data('value'),
                        "BlackListingRecordId": $(this).data('brid')
                    })
                });
            })

            var documents = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    });
                });
            });
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            });
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })
            $('#inptRequesterfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })

            var crRmo = {
                "Comment": $('#commentRmo').val(),
                "Type": "RmoLead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/"+ page +"",
                data: { "profile": profile, "address": address, "person": person, "cr": cr, "request": request, "lead": lead, "afi": afi, "cfi": cfi, "lr": lr, "br": br, "boo":bb, "crRmo":crRmo, "documents":documents },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function requestMore() {
    $('#btnRequestMore').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/RequestMore",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function rejected() {
    $('#btnRejected').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Rejected",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function release() {
    $('#btnRelease').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Release",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function dispute() {
    $('#btnDispute').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Dispute",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function complete() {
    $('#btnComplete').click(function () {
        if (confirm("Please click OK to confirm settlement.")) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Complete",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function stage9() {
    var page = $('#page').val();
    var bb = $('#bool').val();
    debugger
    $('#btnSave10').click(function () {
        if (inputValidation($('#stage').val())) {

            var assign = $('#assignToId').val();
            if (assign == " ") {
                assign = null;
            }
            var lawFirm = $('#lawFirmId').val();
            if (lawFirm == " ") {
                lawFirm = null;
            }

            var negotiator = $('#negotiatorId').val();
            if (negotiator == " ") {
                negotiator = null;
            }

            var profile = {
                "ProfileId": $('#ProfileId').val(),
                "Name": $('#name').val(),
                "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
                "ProfileType": "1",
                "ContactNo": $('#contactNo').val(),
                "Email": $('#email').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "DMS": $('#dms').val(),
                "Source": $('#source').val(),
                "NatureOfBusiness": $('#natureOfBusiness').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val()
            };

            var address = {
                "AddressId": $('#AddressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var person = {
                "PersonId": $('#PersonId').val(),
                "Name": $('#picName').val(),
                "ContactNo": $('#picContact').val(),
                "Email": $('#picEmail').val()
            };

            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            var lead = {
                "LeadId": $('#LeadId').val(),
                "AssignTo": assign
            };

            var request = {
                "AppointmentId": $('#AppointmentId').val(),
                "RequestId": $('#requestId').val(),
                "LawFirm": lawFirm,
                "NegotiatorId": negotiator
            };

            var afi = {
                "AssetForInvestigationId": $('#AssetForInvestigationId').val(),
                "PropertySearchResult1": $('#PropertySearchResult1').val(),
                "PropertySearchResult2": $('#PropertySearchResult2').val(),
                "VehicleDetails1": $('#VehicleDetails1').val(),
                "VehicleDetails2": $('#VehicleDetails2').val(),
                "SiteInvestigation": $('#SiteInvestigation').val()
            };

            var cfi = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    cfi.push({
                        "FinancialYearEnd1": $(this).data('fye'), "AnnualTurnover1": $(this).data('at'), "NtaPositiveNegative1": $(this).data('npn'),
                        "ProfitAfterTax1": $(this).data('pat'), "DocumentId": $(this).data('value'), "CompanyFinancialInformationId": $(this).data('cfid')
                    });
                });
            });

            var lr = [];
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    lr.push({
                        "ClaimAmount": $(this).data('ca'), "StatusOfCaseId": $(this).data('soc'), "HearingDate": $(this).data('hd'),
                        "Plaintiff": $(this).data('p'), "Defendant": $(this).data('dd'), "DocumentId": $(this).data('value'), "LitigationRecordId": $(this).data("lrid")
                    })
                });
            });

            var br = [];
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    br.push({
                        "Creditor1": $(this).data('c'), "StatementDate": $(this).data('sd'), "ClaimAmount": $(this).data('ca'), "DocumentId": $(this).data('value'),
                        "BlackListingRecordId": $(this).data('brid')
                    })
                });
            })

            var documents = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    });
                });
            });
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            });
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })
            $('#inptRequesterfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })

            var crRmo = {
                "Comment": $('#commentRmo').val(),
                "Type": "RmoLead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/" + page + "",
                data: { "profile": profile, "address": address, "person": person, "cr": cr, "request": request, "lead": lead, "afi": afi, "cfi": cfi, "lr": lr, "br": br, "boo": bb, "crRmo": crRmo, "documents": documents, "stage": $('#stage').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function saveDraft(e) {
    var bb = $('#bool').val();

    var assign = $('#assignToId').val();
    if (assign == " ") {
        assign = null;
    }
    var lawFirm = $('#lawFirmId').val();
    if (lawFirm == " ") {
        lawFirm = null;
    }

    var negotiator = $('#negotiatorId').val();
    if (negotiator == " ") {
        negotiator = null;
    }

    var profile = {
        "ProfileId": $('#ProfileId').val(),
        "Name": $('#name').val(),
        "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
        "ProfileType": "1",
        "ContactNo": $('#contactNo').val(),
        "Email": $('#email').val(),
        "BusinessRegNo": $('#businessRegNo').val(),
        "DMS": $('#dms').val(),
        "Source": $('#source').val(),
        "NatureOfBusiness": $('#natureOfBusiness').val(),
        "Website": $('#website').val(),
        "Remark": $('#remark').val()
    };

    var address = {
        "AddressId": $('#AddressId').val(),
        "Line1": $('#line1').val(),
        "Line2": $('#line2').val(),
        "City": $('#city').val(),
        "State": $('#state').val(),
        "PostalCode": $('#postalCode').val(),
        "Country": "Malaysia"
    };

    var person = {
        "PersonId": $('#PersonId').val(),
        "Name": $('#picName').val(),
        "ContactNo": $('#picContact').val(),
        "Email": $('#picEmail').val()
    };

    var cr = {
        "CommentRemarkId": $('#CommentRemarkId').val(),
        "Comment": $('#comment').val(),
        "Type": "Lead"
    };

    var lead = {
        "LeadId": $('#LeadId').val(),
        "AssignTo": assign
    };

    var request = {
        "AppointmentId": $('#AppointmentId').val(),
        "RequestId": $('#requestId').val(),
        "LawFirm": lawFirm,
        "NegotiatorId": negotiator
    };

    var afi = {
        "AssetForInvestigationId": $('#AssetForInvestigationId').val(),
        "PropertySearchResult1": $('#PropertySearchResult1').val(),
        "PropertySearchResult2": $('#PropertySearchResult2').val(),
        "VehicleDetails1": $('#VehicleDetails1').val(),
        "VehicleDetails2": $('#VehicleDetails2').val(),
        "SiteInvestigation": $('#SiteInvestigation').val()
    };

    var cfi = [];
    $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            cfi.push({
                "FinancialYearEnd1": $(this).data('fye'), "AnnualTurnover1": $(this).data('at'), "NtaPositiveNegative1": $(this).data('npn'),
                "ProfitAfterTax1": $(this).data('pat'), "DocumentId": $(this).data('value'), "CompanyFinancialInformationId": $(this).data('cfid')
            });
        });
    });

    var lr = [];
    $('#inptLitigationfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            lr.push({
                "ClaimAmount": $(this).data('ca'), "StatusOfCaseId": $(this).data('soc'), "HearingDate": $(this).data('hd'),
                "Plaintiff": $(this).data('p'), "Defendant": $(this).data('dd'), "DocumentId": $(this).data('value'), "LitigationRecordId": $(this).data("lrid")
            })
        });
    });

    var br = [];
    $('#inptBlacklistfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            br.push({
                "Creditor1": $(this).data('c'), "StatementDate": $(this).data('sd'), "ClaimAmount": $(this).data('ca'), "DocumentId": $(this).data('value'),
                "BlackListingRecordId": $(this).data('brid')
            })
        });
    })

    var documents = [];
    $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            documents.push({
                "DocumentId": $(this).data('value')
            });
        });
    });
    $('#inptLitigationfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            documents.push({
                "DocumentId": $(this).data('value')
            })
        });
    });
    $('#inptBlacklistfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            documents.push({
                "DocumentId": $(this).data('value')
            })
        });
    })
    $('#inptRequesterfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            documents.push({
                "DocumentId": $(this).data('value')
            })
        });
    })

    var crRmo = {
        "Comment": $('#commentRmo').val(),
        "Type": "RmoLead"
    };

    $.ajax({
        url: window.location.origin + "/Lead/" + e + "",
        data: { "profile": profile, "address": address, "person": person, "cr": cr, "request": request, "lead": lead, "afi": afi, "cfi": cfi, "lr": lr, "br": br, "boo": bb, "crRmo": crRmo, "documents": documents, "stage": $('#stage').val() },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function inputValidation(stage) {
    var isValid = true;
    debugger

    if (stage === "5") {
        if ($('#commentRmo').val() === '') {
            if ($('#commentRmo').next('.invalid-alert').length < 1) {
                $('<label class="invalid-alert">Please enter comment remark to proceed.</label>').insertAfter('#commentRmo');
            }
            isValid = false;
        }
        else {
            if ($('#commentRmo').next('.invalid-alert').length > 0) {
                $('#commentRmo').next('.invalid-alert').remove();
            }
        }
    }
    else if (stage === "1" || stage === "6" || stage === "7" || stage === "9" || stage === "15" || stage === "16" || stage === "21" || stage === "22" || stage === "23" || stage === "25" || stage === "27" || stage === "28") {
        if ($('#comment').val() === '') {
            if ($('#comment').next('.invalid-alert').length < 1) {
                $('<label class="invalid-alert">Please enter comment remark to proceed.</label>').insertAfter('#comment');
            }
            isValid = false;
        }
        else {
            if ($('#comment').next('.invalid-alert').length > 0) {
                $('#comment').next('.invalid-alert').remove();
            }
        }
    }
    else if (stage === "14") {
        if ($('#comment').val() === '') {
            if ($('#comment').next('.invalid-alert').length < 1) {
                $('<label class="invalid-alert">Please enter comment remark to proceed.</label>').insertAfter('#comment');
            }
            isValid = false;
        }
        else {
            if ($('#comment').next('.invalid-alert').length > 0) {
                $('#comment').next('.invalid-alert').remove();
            }
        }
        if ($('#inptDispute tbody').children().length === 0) {
            if ($('#inptDispute').next('.invalid-alert').length < 1) {
                $('<label class="invalid-alert">Please attach dispute attachment to proceed.</label>').insertAfter('#inptDispute');
            }
            isValid = false;
        }
        else {
            if ($('#inptDispute').next('.invalid-alert').length > 0) {
                $('#inptDispute').next('.invalid-alert').remove();
            }
        }
    }
    else {

    }
    //if ($('#inptAgency option:selected').data('keyword') === 'OTHR') {
    //    if ($('#inptOtherAgency').val() === '') {
    //        if ($('#inptOtherAgency').next('.invalid-alert').length < 1) {
    //            $('<label class="invalid-alert">Please select employment to proceed.</label>').insertAfter('#inptOtherAgency');
    //        }
    //        isValid = false;
    //    }
    //    else {
    //        if ($('#inptOtherAgency').next('.invalid-alert').length > 0) {
    //            $('#inptOtherAgency').next('.invalid-alert').remove();
    //        }
    //    }
    //}
    //else {
    //    if ($('#inptOtherAgency').next('.invalid-alert').length > 0) {
    //        $('#inptOtherAgency').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEmploymentType option:selected').val() === '0') {
    //    if ($('#inptEmploymentType').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select employment to proceed.</label>').insertAfter('#inptEmploymentType');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEmploymentType').next('.invalid-alert').length > 0) {
    //        $('#inptEmploymentType').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptPosition option:selected').val() === '0') {
    //    if ($('#inptPosition').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select position to proceed.</label>').insertAfter('#inptPosition');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptPosition').next('.invalid-alert').length > 0) {
    //        $('#inptPosition').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEMPName').val() === '') {
    //    if ($('#inptEMPName').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter emergency contact person name to proceed.</label>').insertAfter('#inptEMPName');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEMPName').next('.invalid-alert').length > 0) {
    //        $('#inptEMPName').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEMPContactNo').val() === '') {
    //    if ($('#inptEMPContactNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter emergency contact person contact no to proceed.</label>').insertAfter('#inptEMPContactNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEMPContactNo').next('.invalid-alert').length > 0) {
    //        $('#inptEMPContactNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptRelationship option:selected').val() === '0') {
    //    if ($('#inptRelationship').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select emergency contact person relationship to proceed.</label>').insertAfter('#inptRelationship');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptRelationship').next('.invalid-alert').length > 0) {
    //        $('#inptRelationship').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptIdentityNo').val() === '') {
    //    if ($('#inptIdentityNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter ic / passport to proceed.</label>').insertAfter('#inptIdentityNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptIdentityNo').next('.invalid-alert').length > 0) {
    //        $('#inptIdentityNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptNationality option:selected').val() === '0') {
    //    if ($('#inptNationality').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select nationality to proceed.</label>').insertAfter('#inptNationality');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptNationality').next('.invalid-alert').length > 0) {
    //        $('#inptNationality').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptDOB').val() === '') {
    //    if ($('#inptDOB').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter daate of birth to proceed.</label>').insertAfter('#inptDOB');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptDOB').next('.invalid-alert').length > 0) {
    //        $('#inptDOB').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptJoinDate').val() === '') {
    //    if ($('#inptJoinDate').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter join date to proceed.</label>').insertAfter('#inptJoinDate');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptJoinDate').next('.invalid-alert').length > 0) {
    //        $('#inptJoinDate').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptWageRate').val() === '') {
    //    if ($('#inptWageRate').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert" style="display:block;">Please enter rate to proceed.</label>').insertAfter('#inptWageRate');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptWageRate').next('.invalid-alert').length > 0) {
    //        $('#inptWageRate').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('[name=wagetype]:checked').data('keyword') === 'MONTHLY') {
    //    if ($('[name=monthlywagetype]:checked').length < 1) {
    //        if ($('#monthlywagetypealert').hasClass('hidden')) {
    //            $('#monthlywagetypealert').toggleClass('hidden');
    //        }
    //        isValid = false;
    //    }
    //    else {
    //        if (!$('#monthlywagetypealert').hasClass('hidden')) {
    //            $('#monthlywagetypealert').toggleClass('hidden');
    //        }
    //    }
    //}

    //if ($('#inptName').val() === '') {
    //    if ($('#inptName').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#inptName');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptName').next('.invalid-alert').length > 0) {
    //        $('#inptName').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEmail').val() === '') {
    //    if ($('#inptEmail').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter email to proceed.</label>').insertAfter('#inptEmail');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if (validateEmail($('#inptEmail').val())) {
    //        if ($('#inptEmail').next('.invalid-alert').length > 0) {
    //            $('#inptEmail').next('.invalid-alert').remove();
    //        }
    //    }
    //    else {
    //        isValid = false;
    //    }
    //}

    //if ($('[name=wagetype]:checked').length < 1) {
    //    if ($('.wagetypeselection').find('.invalid-alert').length < 1) {
    //        $('.wagetypeselection').append('<label class="invalid-alert">Please select wage type to proceed.</label>');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('.wagetypeselection').find('.invalid-alert').length > 0) {
    //        $('.wagetypeselection').find('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptContactNo').val() === '') {
    //    if ($('#inptContactNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter contact number to proceed.</label>').insertAfter('#inptContactNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptContactNo').next('.invalid-alert').length > 0) {
    //        $('#inptContactNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptAddress').val() === '') {
    //    if ($('#inptAddress').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter address to proceed.</label>').insertAfter('#inptAddress');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptAddress').next('.invalid-alert').length > 0) {
    //        $('#inptAddress').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('[name=incentive]:checked').val() === 'Y') {
    //    saveIncentive();
    //    if (employeeIncentives.length < 1) {
    //        if ($('#btnIncentive').next('.invalid-alert').length < 1) {
    //            $('<label class="invalid-alert">Please update employee incentive.</label>').insertAfter('#btnIncentive');
    //        }
    //        isValid = false;
    //    }
    //}
    //else {
    //    if ($('#btnIncentive').next('.invalid-alert').length > 0) {
    //        $('#btnIncentive').next('.invalid-alert').remove();
    //    }
    //}
    return isValid;
}

