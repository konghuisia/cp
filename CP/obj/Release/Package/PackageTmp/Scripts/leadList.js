﻿function init() {
    bindSearch();
    //bindCheckAll();
    //bindButtonPrint();
}

//function bindCheckAll() {
//    $('#ckbxAll').change(function () {
//        if ($('#ckbxAll').prop('checked')) {
//            $('[name=employees]').prop('checked', true);
//        }
//        else {
//            $('[name=employees]').prop('checked', false);
//        }
//    });
//}

//function bindButtonPrint() {
//    $('#btnPrint').click(function () {
//        var employees = [];
//        $('[name=employees]:checked').each(function () {
//            employees.push($(this).val());
//        });

//        if (employees.length > 0) {
//            $('#printSubmit').trigger('click');
//        }
//    });
//}
//function redirectPost(url, data) {
//    var form = document.createElement('form');
//    document.body.appendChild(form);
//    form.method = 'post';
//    form.action = url;
//    for (var name in data) {
//        var input = document.createElement('input');
//        input.type = 'hidden';
//        input.name = name;
//        input.value = JSON.stringify(data[name]);
//        form.appendChild(input);
//    }
//    form.submit();
//}

function bindSearch() {
    $('#inptSearch').change(function () {
        searchList('tblLeadList ', $('#inptSearch').val());
    });
    $('#btnOrder').click(function () {
        var order = $('#sortBy option:selected').val();
        if (order !== '0') {
            sortTable('tblLeadList', order, $(this));
        }
    });
}
function deleteLead(e) {
    if (confirm("Please click OK to confirm delete.")) {
        $.ajax({
            url: window.location.origin + "/Lead/DeleteLead",
            data: { "requestId": $(e).data('value') },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    $(e).parent().parent().remove();
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}
