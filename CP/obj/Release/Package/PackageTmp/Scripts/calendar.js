﻿function init() {
    initInputRestriction();
}

function initInputRestriction() {
    $('.email').keydown(function (e) {
        restrictMultipleEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateMultipleEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                    $('#btnSend').css('display','inline');
                }
                $('#btnSend').css('display', 'inline');
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                    $('#btnSend').css('display', 'none');
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });
}

function inviteThem(e) {
    if (inputValidation()) {
        var recepients = $('#invite').val();
        var split = recepients.split(',');

        debugger
        var emailModel = {
            "Subject": $(e).data('subject'),
            "Description": $(e).data('description'),
            "Location": $(e).data('venue'),
            "DateStart": $(e).data('datestart'),
            "DateEnd": $(e).data('dateend')
        };

        //var address = {
        //    "AddressId": $('#AddressId').val(),
        //    "Line1": $('#line1').val(),
        //    "Line2": $('#line2').val(),
        //    "City": $('#city').val(),
        //    "State": $('#state').val(),
        //    "PostalCode": $('#postalCode').val(),
        //    "Country": $('#country').val()
        //};

        //var cr = {
        //    "Comment": $('#comment').val(),
        //    "Type": "Law Firm"
        //};

        //debugger
        $.ajax({
            url: window.location.origin + "/Home/SendEmailInvite",
            data: { "emailModel": emailModel, "recipients": split },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
};

function inputValidation() {
    var isValid = true;

    return isValid;
}
