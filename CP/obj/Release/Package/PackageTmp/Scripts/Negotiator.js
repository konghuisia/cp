﻿
function init() {
    bindSaveProfileButtonClick();
    initInputRestriction();
}

function initInputRestriction() {

    $('.datepicker').keypress(function () {
        return false;
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    $('.name').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });
}

function bindSaveProfileButtonClick() {
    var page = $('#page').val();
    debugger
    $('#btnSave').click(function () {
        if (inputValidation()) {

            var profile = {
                "NegotiatorProfileId": $('#NegotiatorProfileId').val(),
                "Name": $('#name').val(),
                "Email": $('#email').val(),
                "ContactNo": $('#contactNo').val(),
                "Remark": $('#remark').val(),
                "Active": $('input:radio[name=activeRb]:checked').val(),
            };

            var address = {
                "AddressId": $('#AddressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": $('#country').val()
            };

            var cr = {
                "Comment": $('#comment').val(),
                "Type": "Law Firm"
            };

            debugger
            $.ajax({
                url: window.location.origin + "/Profile/" + page + "",
                data: { "profile": profile, "address": address, "cr": cr },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;
    //if ($('#inptAgency option:selected').data('keyword') === 'OTHR') {
    //    if ($('#inptOtherAgency').val() === '') {
    //        if ($('#inptOtherAgency').next('.invalid-alert').length < 1) {
    //            $('<label class="invalid-alert">Please select employment to proceed.</label>').insertAfter('#inptOtherAgency');
    //        }
    //        isValid = false;
    //    }
    //    else {
    //        if ($('#inptOtherAgency').next('.invalid-alert').length > 0) {
    //            $('#inptOtherAgency').next('.invalid-alert').remove();
    //        }
    //    }
    //}
    //else {
    //    if ($('#inptOtherAgency').next('.invalid-alert').length > 0) {
    //        $('#inptOtherAgency').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEmploymentType option:selected').val() === '0') {
    //    if ($('#inptEmploymentType').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select employment to proceed.</label>').insertAfter('#inptEmploymentType');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEmploymentType').next('.invalid-alert').length > 0) {
    //        $('#inptEmploymentType').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptPosition option:selected').val() === '0') {
    //    if ($('#inptPosition').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select position to proceed.</label>').insertAfter('#inptPosition');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptPosition').next('.invalid-alert').length > 0) {
    //        $('#inptPosition').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEMPName').val() === '') {
    //    if ($('#inptEMPName').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter emergency contact person name to proceed.</label>').insertAfter('#inptEMPName');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEMPName').next('.invalid-alert').length > 0) {
    //        $('#inptEMPName').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEMPContactNo').val() === '') {
    //    if ($('#inptEMPContactNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter emergency contact person contact no to proceed.</label>').insertAfter('#inptEMPContactNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEMPContactNo').next('.invalid-alert').length > 0) {
    //        $('#inptEMPContactNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptRelationship option:selected').val() === '0') {
    //    if ($('#inptRelationship').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select emergency contact person relationship to proceed.</label>').insertAfter('#inptRelationship');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptRelationship').next('.invalid-alert').length > 0) {
    //        $('#inptRelationship').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptIdentityNo').val() === '') {
    //    if ($('#inptIdentityNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter ic / passport to proceed.</label>').insertAfter('#inptIdentityNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptIdentityNo').next('.invalid-alert').length > 0) {
    //        $('#inptIdentityNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptNationality option:selected').val() === '0') {
    //    if ($('#inptNationality').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select nationality to proceed.</label>').insertAfter('#inptNationality');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptNationality').next('.invalid-alert').length > 0) {
    //        $('#inptNationality').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptDOB').val() === '') {
    //    if ($('#inptDOB').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter daate of birth to proceed.</label>').insertAfter('#inptDOB');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptDOB').next('.invalid-alert').length > 0) {
    //        $('#inptDOB').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptJoinDate').val() === '') {
    //    if ($('#inptJoinDate').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter join date to proceed.</label>').insertAfter('#inptJoinDate');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptJoinDate').next('.invalid-alert').length > 0) {
    //        $('#inptJoinDate').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptWageRate').val() === '') {
    //    if ($('#inptWageRate').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert" style="display:block;">Please enter rate to proceed.</label>').insertAfter('#inptWageRate');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptWageRate').next('.invalid-alert').length > 0) {
    //        $('#inptWageRate').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('[name=wagetype]:checked').data('keyword') === 'MONTHLY') {
    //    if ($('[name=monthlywagetype]:checked').length < 1) {
    //        if ($('#monthlywagetypealert').hasClass('hidden')) {
    //            $('#monthlywagetypealert').toggleClass('hidden');
    //        }
    //        isValid = false;
    //    }
    //    else {
    //        if (!$('#monthlywagetypealert').hasClass('hidden')) {
    //            $('#monthlywagetypealert').toggleClass('hidden');
    //        }
    //    }
    //}

    //if ($('#inptName').val() === '') {
    //    if ($('#inptName').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#inptName');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptName').next('.invalid-alert').length > 0) {
    //        $('#inptName').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEmail').val() === '') {
    //    if ($('#inptEmail').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter email to proceed.</label>').insertAfter('#inptEmail');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if (validateEmail($('#inptEmail').val())) {
    //        if ($('#inptEmail').next('.invalid-alert').length > 0) {
    //            $('#inptEmail').next('.invalid-alert').remove();
    //        }
    //    }
    //    else {
    //        isValid = false;
    //    }
    //}

    //if ($('[name=wagetype]:checked').length < 1) {
    //    if ($('.wagetypeselection').find('.invalid-alert').length < 1) {
    //        $('.wagetypeselection').append('<label class="invalid-alert">Please select wage type to proceed.</label>');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('.wagetypeselection').find('.invalid-alert').length > 0) {
    //        $('.wagetypeselection').find('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptContactNo').val() === '') {
    //    if ($('#inptContactNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter contact number to proceed.</label>').insertAfter('#inptContactNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptContactNo').next('.invalid-alert').length > 0) {
    //        $('#inptContactNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptAddress').val() === '') {
    //    if ($('#inptAddress').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter address to proceed.</label>').insertAfter('#inptAddress');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptAddress').next('.invalid-alert').length > 0) {
    //        $('#inptAddress').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('[name=incentive]:checked').val() === 'Y') {
    //    saveIncentive();
    //    if (employeeIncentives.length < 1) {
    //        if ($('#btnIncentive').next('.invalid-alert').length < 1) {
    //            $('<label class="invalid-alert">Please update employee incentive.</label>').insertAfter('#btnIncentive');
    //        }
    //        isValid = false;
    //    }
    //}
    //else {
    //    if ($('#btnIncentive').next('.invalid-alert').length > 0) {
    //        $('#btnIncentive').next('.invalid-alert').remove();
    //    }
    //}
    return isValid;
}

