﻿
function init() {
    bindSaveAppointmentButtonClick();
    //bindUploadButtonClick();
    //bindEmploymentTypeChange();
    //bindIncentiveChange();
    //bindWageTypeChange();
    //bindProfilePictureUpload();
    initInputRestriction();
}

function initInputRestriction() {
    $('.datepicker').keypress(function () {
        return false;
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    //$('#inptName').keydown(function (e) {
    //    restrictAlphabetInput(e);
    //});
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    //$('#inptIdentityNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});

    //$('#inptAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptAddress').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptHomeAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptHomeAddress').blur(function () {
    //    trimSpacing(this);
    //});

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });
    //$('#inptOfficeContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptNationality').change(function () {
    //    if ($('#inptNationality option:selected').data('keyword') === 'MY') {
    //        $('[name=epfsocso]').prop('disabled', true);
    //    }
    //    else {
    //        $('[name=epfsocso]').prop('disabled', false);
    //    }
    //});

    //$('#inptDocumentNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});
    //$('#inptDocumentNo').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptRemark').keydown(function (e) {
    //    restrictAlphanumericInput(e);
    //});
    //$('#inptRemark').blur(function () {
    //    trimSpacing(this);
    //});

    //$('#inptEMPName').keydown(function (e) {
    //    restrictAlphabetInput(e);
    //});
    //$('#inptEMPName').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptEMPContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptAdvancePaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});

    //$('#inptMonthlyRepaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});
}

function uploadDoc(doctype) {
    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectPex').val() !== "") {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#RequestId').val());
        formdata.append("Type", doctype);
        var fileInput;
        if (doctype === "C56ADBB5-2520-483B-BA80-1DA8CE61E461") {
            fileInput = document.getElementById('fileSelectRequester');
        }
        
        //Iterating through each files selected in fileInput
        for (var i = 0; i < fileInput.files.length; i++) {
            //Appending each file to FormData object
            //formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/Appointment/AttachmentUpload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);

                debugger
                if (doctype === "C56ADBB5-2520-483B-BA80-1DA8CE61E461") {
                    var row = '<tr>' +
                        '<td>' + d.FileName + '</td>' +
                        '<td>' + d.Size + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptRequesterfile').last().append(row);
                    $('#fileSelectRequester').val('');
                }
            }
        };
    }
}

function removeAttachment(e, ee) {
    $.ajax({
        url: window.location.origin + "/Appointment/DeleteAttachment",
        data: { "documentId": $(e).data('value') },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if ($(e).data('doc') != null) {
                    var l = $(e).data('value');
                    $('#inptLitigationfile2 tbody tr td:last-child').each(function () {
                        $(this).find('.l').each(function () {
                            if ($(this).data('value') == l) {
                                $(this).parent().parent().remove();
                            }
                        });
                    });
                }
                $(e).parent().parent().remove();
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function bindSaveAppointmentButtonClick() {
    var page = $('#page').val();

    $('#btnSave').click(function () {
        if (inputValidation()) {
            var appointment = {
                "AppointmentId": $("#AppointmentId").val(),
                "AppointmentType": $("#AppointmentType").val(),
                "Subject": $('#Subject').val(),
                "Description": $('#Description').val(),
                "Date": $('#Date').val(),
                "EndDate": $('#EndDate').val(),
                "IsAllDay": $('input:radio[name=isAllDayRb]:checked').val(),
                "Venue": $('#Venue').val()
            };

            var request = {
                "RequestId": $("#RequestId").val(),
                "ProfileId": $("#ProfileId").val(),
                "AddressId": $("#AddressId").val(),
                "PersonId": $("#PersonId").val(),
                //"CommentRemarkId": "@request.CommentRemarkId",
                //"DocumentId": "@request.DocumentId",
                "LeadId": $("#LeadId").val(),
            }

            var requestId = {
                "requestId": $("#RequestId").val()
            };

            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "ProfileId": $("#ProfileId").val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Appointment/" + page + "",
                data: { "appointment": appointment, "request": request, "requestId": requestId, "cr": cr, "startTime": $('#StartTime').val(), "endTime": $('#EndTime').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                opener.location.reload();
                                window.close(); 
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;
    //if ($('#inptAgency option:selected').data('keyword') === 'OTHR') {
    //    if ($('#inptOtherAgency').val() === '') {
    //        if ($('#inptOtherAgency').next('.invalid-alert').length < 1) {
    //            $('<label class="invalid-alert">Please select employment to proceed.</label>').insertAfter('#inptOtherAgency');
    //        }
    //        isValid = false;
    //    }
    //    else {
    //        if ($('#inptOtherAgency').next('.invalid-alert').length > 0) {
    //            $('#inptOtherAgency').next('.invalid-alert').remove();
    //        }
    //    }
    //}
    //else {
    //    if ($('#inptOtherAgency').next('.invalid-alert').length > 0) {
    //        $('#inptOtherAgency').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEmploymentType option:selected').val() === '0') {
    //    if ($('#inptEmploymentType').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select employment to proceed.</label>').insertAfter('#inptEmploymentType');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEmploymentType').next('.invalid-alert').length > 0) {
    //        $('#inptEmploymentType').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptPosition option:selected').val() === '0') {
    //    if ($('#inptPosition').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select position to proceed.</label>').insertAfter('#inptPosition');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptPosition').next('.invalid-alert').length > 0) {
    //        $('#inptPosition').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEMPName').val() === '') {
    //    if ($('#inptEMPName').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter emergency contact person name to proceed.</label>').insertAfter('#inptEMPName');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEMPName').next('.invalid-alert').length > 0) {
    //        $('#inptEMPName').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEMPContactNo').val() === '') {
    //    if ($('#inptEMPContactNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter emergency contact person contact no to proceed.</label>').insertAfter('#inptEMPContactNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptEMPContactNo').next('.invalid-alert').length > 0) {
    //        $('#inptEMPContactNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptRelationship option:selected').val() === '0') {
    //    if ($('#inptRelationship').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select emergency contact person relationship to proceed.</label>').insertAfter('#inptRelationship');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptRelationship').next('.invalid-alert').length > 0) {
    //        $('#inptRelationship').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptIdentityNo').val() === '') {
    //    if ($('#inptIdentityNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter ic / passport to proceed.</label>').insertAfter('#inptIdentityNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptIdentityNo').next('.invalid-alert').length > 0) {
    //        $('#inptIdentityNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptNationality option:selected').val() === '0') {
    //    if ($('#inptNationality').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please select nationality to proceed.</label>').insertAfter('#inptNationality');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptNationality').next('.invalid-alert').length > 0) {
    //        $('#inptNationality').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptDOB').val() === '') {
    //    if ($('#inptDOB').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter daate of birth to proceed.</label>').insertAfter('#inptDOB');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptDOB').next('.invalid-alert').length > 0) {
    //        $('#inptDOB').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptJoinDate').val() === '') {
    //    if ($('#inptJoinDate').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter join date to proceed.</label>').insertAfter('#inptJoinDate');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptJoinDate').next('.invalid-alert').length > 0) {
    //        $('#inptJoinDate').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptWageRate').val() === '') {
    //    if ($('#inptWageRate').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert" style="display:block;">Please enter rate to proceed.</label>').insertAfter('#inptWageRate');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptWageRate').next('.invalid-alert').length > 0) {
    //        $('#inptWageRate').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('[name=wagetype]:checked').data('keyword') === 'MONTHLY') {
    //    if ($('[name=monthlywagetype]:checked').length < 1) {
    //        if ($('#monthlywagetypealert').hasClass('hidden')) {
    //            $('#monthlywagetypealert').toggleClass('hidden');
    //        }
    //        isValid = false;
    //    }
    //    else {
    //        if (!$('#monthlywagetypealert').hasClass('hidden')) {
    //            $('#monthlywagetypealert').toggleClass('hidden');
    //        }
    //    }
    //}

    //if ($('#inptName').val() === '') {
    //    if ($('#inptName').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#inptName');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptName').next('.invalid-alert').length > 0) {
    //        $('#inptName').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptEmail').val() === '') {
    //    if ($('#inptEmail').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter email to proceed.</label>').insertAfter('#inptEmail');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if (validateEmail($('#inptEmail').val())) {
    //        if ($('#inptEmail').next('.invalid-alert').length > 0) {
    //            $('#inptEmail').next('.invalid-alert').remove();
    //        }
    //    }
    //    else {
    //        isValid = false;
    //    }
    //}

    //if ($('[name=wagetype]:checked').length < 1) {
    //    if ($('.wagetypeselection').find('.invalid-alert').length < 1) {
    //        $('.wagetypeselection').append('<label class="invalid-alert">Please select wage type to proceed.</label>');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('.wagetypeselection').find('.invalid-alert').length > 0) {
    //        $('.wagetypeselection').find('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptContactNo').val() === '') {
    //    if ($('#inptContactNo').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter contact number to proceed.</label>').insertAfter('#inptContactNo');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptContactNo').next('.invalid-alert').length > 0) {
    //        $('#inptContactNo').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#inptAddress').val() === '') {
    //    if ($('#inptAddress').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter address to proceed.</label>').insertAfter('#inptAddress');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptAddress').next('.invalid-alert').length > 0) {
    //        $('#inptAddress').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('[name=incentive]:checked').val() === 'Y') {
    //    saveIncentive();
    //    if (employeeIncentives.length < 1) {
    //        if ($('#btnIncentive').next('.invalid-alert').length < 1) {
    //            $('<label class="invalid-alert">Please update employee incentive.</label>').insertAfter('#btnIncentive');
    //        }
    //        isValid = false;
    //    }
    //}
    //else {
    //    if ($('#btnIncentive').next('.invalid-alert').length > 0) {
    //        $('#btnIncentive').next('.invalid-alert').remove();
    //    }
    //}
    return isValid;
}