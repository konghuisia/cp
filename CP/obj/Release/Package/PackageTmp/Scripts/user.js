﻿function init() {
    bindSaveAccountButtonClick();
    initInputRestriction();
    initResetPasswordButtonClick();
}

function initInputRestriction() {
    $('#inptName').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('#inptName').blur(function () {
        trimSpacing(this);
    });

    $('#inptEmail').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('#inptEmail').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    $('#inptUsername').keydown(function (e) {
        restrictCapitalLetter(e);
    });
    $('#inptUsername').blur(function () {
        trimSpacing(this);
        convertToLower(this);
    });
    $('#inptRemark').keydown(function (e) {
        restrictAlphanumericInput(e);
    });
}


function bindSaveAccountButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {
            

            var user = {
                "UserId": $('#userId').val(),
                "Fullname": $('#inptName').val(),
                "Name": $('#inptUsername').val(),
                "Email": $('#inptEmail').val(),
                "Remark": $('#inptRemark').val(),
                "Active": $('[name=status]:checked').val(),
                "Position": $('#inptGroup option:selected').val(),
                "ProfileId": $('#inptProfile option:selected').val(),
                "Oldname": $('#oldName').val(),
            };


            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/Confirguration/" + url + "User",
                data: { "user": user },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#inptEmail').val() === '') {
        if ($('#inptEmail').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter email to proceed.</label>').insertAfter('#inptEmail');
        }
        isValid = false;
    }
    else {
        if (validateEmail($('#inptEmail').val())) {
            if ($('#inptEmail').next('.invalid-alert').length > 0) {
                $('#inptEmail').next('.invalid-alert').remove();
            }
        }
        else {
            isValid = false;
        }
    }

    if ($('#inptName').val() === '') {
        if ($('#inptName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#inptName');
        }
        isValid = false;
    }
    else {
        if ($('#inptName').next('.invalid-alert').length > 0) {
            $('#inptName').next('.invalid-alert').remove();
        }
    }

    if ($('#inptUsername').val() === '') {
        if ($('#inptUsername').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter user id to proceed.</label>').insertAfter('#inptUsername');
        }
        isValid = false;
    }
    else {
        if ($('#inptUsername').next('.invalid-alert').length > 0) {
            $('#inptUsername').next('.invalid-alert').remove();
        }
    }

    return isValid;
}

function initResetPasswordButtonClick() {
    $('#btnResetPassword').click(function () {
        if (confirm("Please click OK to confirm delete.")) {
            $.ajax({
                url: window.location.origin + "/Confirguration/ResetUserPassword",
                data: { "userId": $('#userId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function clearInput() {
    $('#inptUsername').val('');
    $('#inptName').val('');
    $('#inptGroup').val('');
    $('#inptPosition').val('');
    $('#inptProfile').val('');
    $('#inptEmail').val('');
    $('#inptRemark').val('');
    $($('[name=status]')[0]).prop('checked', true);
    $('[type=checkbox]').prop('checked', false);
    $('textarea').val('');
}
