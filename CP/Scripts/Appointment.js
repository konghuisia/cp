﻿
function init() {
    bindSaveAppointmentButtonClick();
    //bindUploadButtonClick();
    //bindEmploymentTypeChange();
    //bindIncentiveChange();
    //bindWageTypeChange();
    //bindProfilePictureUpload();
    initInputRestriction();
}

function initInputRestriction() {
    $('.datepicker').keypress(function () {
        return false;
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    //$('#inptName').keydown(function (e) {
    //    restrictAlphabetInput(e);
    //});
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    //$('#inptIdentityNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});

    //$('#inptAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptAddress').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptHomeAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptHomeAddress').blur(function () {
    //    trimSpacing(this);
    //});

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });
    //$('#inptOfficeContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptNationality').change(function () {
    //    if ($('#inptNationality option:selected').data('keyword') === 'MY') {
    //        $('[name=epfsocso]').prop('disabled', true);
    //    }
    //    else {
    //        $('[name=epfsocso]').prop('disabled', false);
    //    }
    //});

    //$('#inptDocumentNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});
    //$('#inptDocumentNo').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptRemark').keydown(function (e) {
    //    restrictAlphanumericInput(e);
    //});
    //$('#inptRemark').blur(function () {
    //    trimSpacing(this);
    //});

    //$('#inptEMPName').keydown(function (e) {
    //    restrictAlphabetInput(e);
    //});
    //$('#inptEMPName').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptEMPContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptAdvancePaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});

    //$('#inptMonthlyRepaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});
}

function uploadDoc(doctype) {
    
        $('#inptRequesterfile').next('.invalid-alert').remove();

        if ($('#fileSelectRequester').val() !== "") {
            var formdata = new FormData(); //FormData object
            formdata.append("RequestId", $('#RequestId').val());
            formdata.append("Type", doctype);
            var fileInput;
            if (doctype === "C56ADBB5-2520-483B-BA80-1DA8CE61E461") {
                fileInput = document.getElementById('fileSelectRequester');
            }
            
            if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
                $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptRequesterfile');
                return false;
            }

            //Iterating through each files selected in fileInput
            for (var i = 0; i < fileInput.files.length; i++) {
                //Appending each file to FormData object
                //formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }

            //Creating an XMLHttpRequest and sending
            var xhr = new XMLHttpRequest();
            xhr.open('POST', window.location.origin + '/Appointment/AttachmentUpload');
            xhr.send(formdata);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var d = JSON.parse(xhr.response);

                    debugger
                    if (doctype === "C56ADBB5-2520-483B-BA80-1DA8CE61E461") {
                        var row = '<tr>' +
                            '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                            '<td class=alignRight>' + d.Size + '</td> ' +
                            '<td>' + d.CreatedDateString + '</td>' +
                            '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                        '</tr>';

                        $('#inptRequesterfile').last().append(row);
                        $('#fileSelectRequester').val('');
                    }
                }
            };
        }
}

function removeAttachment(e, ee) {
    $.ajax({
        url: window.location.origin + "/Appointment/DeleteAttachment",
        data: { "documentId": $(e).data('value') },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                $(e).parent().parent().remove();
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function bindSaveAppointmentButtonClick() {
    var page = $('#page').val();

    $('#btnSave').click(function () {
        if (inputValidation()) {
            var appointment = {
                "AppointmentId": $("#AppointmentId").val(),
                "AppointmentType": $("#AppointmentType").val(),
                "Subject": $('#Subject').val(),
                "Description": $('#Description').val(),
                "Date": $('#Date').val(),
                "EndDate": $('#EndDate').val(),
                "IsAllDay": $('input:radio[name=isAllDayRb]:checked').val(),
                "Venue": $('#Venue').val()
            };

            var request = {
                "RequestId": $("#RequestId").val(),
                "ProfileId": $("#ProfileId").val(),
                "AddressId": $("#AddressId").val(),
                "PersonId": $("#PersonId").val(),
                //"CommentRemarkId": "@request.CommentRemarkId",
                //"DocumentId": "@request.DocumentId",
                "LeadId": $("#LeadId").val(),
            }

            var requestId = {
                "requestId": $("#RequestId").val()
            };

            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "ProfileId": $("#ProfileId").val(),
                "Comment": $('#comment').val(),
                "Type": "Appt"
            };
            
            var documents = [];
            $('#inptRequesterfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    });
                });
            });

            $.ajax({
                url: window.location.origin + "/Appointment/" + page + "",
                data: { "appointment": appointment, "request": request, "requestId": requestId, "cr": cr, "startTime": $('#StartTime').val(), "endTime": $('#EndTime').val(), "document": documents },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                opener.location.reload();
                                window.close(); 
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#Subject').val() === '') {
        if ($('#Subject').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Subject to proceed.</label>').insertAfter('#Subject');
        }
        isValid = false;
    }
    else {
        if ($('#Subject').next('.invalid-alert').length > 0) {
            $('#Subject').next('.invalid-alert').remove();
        }
    }

    if ($('#Description').val() === '') {
        if ($('#Description').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Description to proceed.</label>').insertAfter('#Description');
        }
        isValid = false;
    }
    else {
        if ($('#Description').next('.invalid-alert').length > 0) {
            $('#Description').next('.invalid-alert').remove();
        }
    }

    if ($('#Date').val() === '') {
        if ($('#Date').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Start Date to proceed.</label>').insertAfter('#Date');
        }
        isValid = false;
    }
    else {
        if ($('#Date').next('.invalid-alert').length > 0) {
            $('#Date').next('.invalid-alert').remove();
        }
    }

    if ($('#EndDate').val() === '') {
        if ($('#EndDate').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter End Date to proceed.</label>').insertAfter('#EndDate');
        }
        isValid = false;
    }
    else {
        if ($('#EndDate').next('.invalid-alert').length > 0) {
            $('#EndDate').next('.invalid-alert').remove();
        }
    }

    if ($('#Venue').val() === '') {
        if ($('#Venue').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Venue to proceed.</label>').insertAfter('#Venue');
        }
        isValid = false;
    }
    else {
        if ($('#Venue').next('.invalid-alert').length > 0) {
            $('#Venue').next('.invalid-alert').remove();
        }
    }
    
    return isValid;
}