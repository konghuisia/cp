﻿

function init() {
    initInputRestriction();
    bindSavePackageButtonClick();
    //bindSavePackageServiceButtonClick();
}

function initInputRestriction() {
    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });

    $('.datepicker').keypress(function () {
        return false;
    });

    $('.money').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.money').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#Name').val() === '') {
        if ($('#Name').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter SOP Service Name to proceed.</label>').insertAfter('#Name');
        }
        isValid = false;
    } else {
        if ($('#Name').next('.invalid-alert').length > 0) {
            $('#Name').next('.invalid-alert').remove();
        }
    }

    if ($('#Point').val() === '' || $('#Point').val() === "0.00" || $('#Price').val() === '' || $('#Price').val() === "0.00") {
        if ($('#divFirstLevel').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter a valid Price & Conditions to proceed.</label>').insertAfter('#divFirstLevel');
        }
        isValid = false;
    }
    else {
        if ($('#divFirstLevel').next('.invalid-alert').length > 0) {
            $('#divFirstLevel').next('.invalid-alert').remove();
        }
    }

    return isValid;
}

function clearInput() {
    $('#Name').val('');
    $('#Description').val('');
    $($('.thisCb')).prop('checked', true);
    $('#Point').val('');
    $('#Price').val('');
}



function bindSavePackageButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {
            var c1 = $('#Point').val();
            var p1 = $('#Price').val();



            var Sop = {
                "SopId": $('#SopId').val(),
                "Name": $('#Name').val(),
                "Description": $('#Description').val(),
                "Status": $('[name=status]:checked').val(),
                "Price": parseFloat(p1.replace(/,/g, '')),
                "Point": parseFloat(c1.replace(/,/g, ''))
            };


            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/Confirguration/" + url + "Sop",
                data: { "sop": Sop },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}