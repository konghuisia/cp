﻿
var btn = null;


function init() {
    bindSaveProfileButtonClick();
    initInputRestriction();
}

function initInputRestriction() {

    btn = $('#btnSubscription').detach();

    $('.datepicker').keypress(function () {
        return false;
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    $('.Pname').keyup(onlyAllowAlphanumeric);
    $('.Pname').blur(function () {
        trimSpacing(this);
    });

    $('.name').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });
}


function dmsSelectOnChanged() {
    if ($('dms').val() == "true") {
        div.appendTo($('#dms'))
        div = null;
    } else {
        btn = $('#btnSubscription').detach();
    }
}

function bindSaveProfileButtonClick() {
    var page = $('#page').val();

    $('#btnSave').click(function () {
        if (inputValidation()) {

            var profile = {
                "LawFirmProfileId": $('#LawFirmProfileId').val(),
                "Name": $('#name').val(),
                "Email": $('#email').val(),
                "ContactNo": $('#contactNo').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val(),
                "Active": $('input:radio[name=activeRb]:checked').val(),
            };

            var address = {
                "AddressId": $('#AddressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var cr = {
                "Comment": $('#comment').val(),
                "Type": "Law Firm"
            };
            
            $.ajax({
                url: window.location.origin + "/Profile/" + page + "",
                data: { "profile": profile, "address": address, "cr": cr },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;
    if ($('#name').val() === '') {
        if ($('#name').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Profile Name to proceed.</label>').insertAfter('#name');
        }
        isValid = false;
    }
    else {
        if ($('#name').next('.invalid-alert').length > 0) {
            $('#name').next('.invalid-alert').remove();
        }
    }

    if ($('#businessRegNo').val() === '') {
        if ($('#businessRegNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Business Reg No to proceed.</label>').insertAfter('#businessRegNo');
        }
        isValid = false;
    }
    else {
        if ($('#businessRegNo').next('.invalid-alert').length > 0) {
            $('#businessRegNo').next('.invalid-alert').remove();
        }
    }

    if ($('#contactNo').val() === '') {
        if ($('#contactNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Contact No to proceed.</label>').insertAfter('#contactNo');
        }
        isValid = false;
    }
    else {
        if ($('#contactNo').next('.invalid-alert').length > 0) {
            $('#contactNo').next('.invalid-alert').remove();
        }
    }

    if ($('#email').val() === '') {
        if ($('#email').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Email to proceed.</label>').insertAfter('#email');
        }
        isValid = false;
    }
    else {
        if ($('#email').next('.invalid-alert').length > 0) {
            $('#email').next('.invalid-alert').remove();
        }
    }

    if ($('#line1').val() === '') {
        if ($('#line1').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Line 1 to proceed.</label>').insertAfter('#line1');
        }
        isValid = false;
    }
    else {
        if ($('#line1').next('.invalid-alert').length > 0) {
            $('#line1').next('.invalid-alert').remove();
        }
    }

    if ($('#line2').val() === '') {
        if ($('#line2').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Line 2 to proceed.</label>').insertAfter('#line2');
        }
        isValid = false;
    }
    else {
        if ($('#line2').next('.invalid-alert').length > 0) {
            $('#line2').next('.invalid-alert').remove();
        }
    }

    if ($('#postalCode').val() === '') {
        if ($('#postalCode').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert" style="display:block;">Please enter Postal Code to proceed.</label>').insertAfter('#postalCode');
        }
        isValid = false;
    }
    else {
        if ($('#postalCode').next('.invalid-alert').length > 0) {
            $('#postalCode').next('.invalid-alert').remove();
        }
    }

    //if ($('#picName').val() === '') {
    //    if ($('#picName').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#picName');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#picName').next('.invalid-alert').length > 0) {
    //        $('#picName').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#picContact').val() === '') {
    //    if ($('#picContact').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter contact number to proceed.</label>').insertAfter('#picContact');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#picContact').next('.invalid-alert').length > 0) {
    //        $('#picContact').next('.invalid-alert').remove();
    //    }
    //}

    //if ($('#picEmail').val() === '') {
    //    if ($('#picEmail').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter email to proceed.</label>').insertAfter('#picEmail');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#picEmail').next('.invalid-alert').length > 0) {
    //        $('#picEmail').next('.invalid-alert').remove();
    //    }
    //}
    return isValid;
}

