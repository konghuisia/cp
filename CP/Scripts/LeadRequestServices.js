﻿

function init() {
    //bindSavePackageServiceButtonClick();
}


function inputValidation() {
    var isValid = true;

    

    return isValid;
}

function clearInput() {
    $($('.thisCb')).prop('checked', false);
}

function CloseThisTab() {
    window.close();
}

function bindSaveButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {
            debugger
            
            var requestServices = [];

            var Checked = $('.thisCb:checkbox:checked');
            $(Checked).each(function (index, value) {
                if (value.dataset.type === "Sop") {
                    requestServices.push({ "RequestServicesId": value.dataset.requestservicesid, "SopId": value.value, "Type": "Sop", "Active": true });
                }
                else if (value.dataset.type === "Service") {
                    requestServices.push({ "RequestServicesId": value.dataset.requestservicesid, "ServiceTypeId": value.value, "Type": "Service", "Active": true });
                }
            });

            var unChecked = $('.thisCb:checkbox:not(:checked)');
            $(unChecked).each(function (index, value) {
                if (value.dataset.type === "Sop") {
                    requestServices.push({ "RequestServicesId": value.dataset.requestservicesid, "SopId": value.value, "Type": "Sop", "Active": false });
                }
                else if (value.dataset.type === "Service") {
                    requestServices.push({ "RequestServicesId": value.dataset.requestservicesid, "ServiceTypeId": value.value, "Type": "Service", "Active": false });
                }
            });

            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/Lead/" + url + "RequestService",
                data: { "requestServices": requestServices, "RequestId": $("#RequestId").val(), "ProfileId": $('#ProfileId').val(), "SubscriptionId": $('#SubscriptionId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                opener.location.reload();
                                window.close(); 
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}
