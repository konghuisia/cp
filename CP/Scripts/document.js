﻿"use strict";

function InputValidate(status) {
    var isValid = true;
    if (status === "New" || status === "Draft" || status === "Pending Requester") {
        if ($('#inptSubject').val() === "") {
            if ($('#inptSubject').next().length < 1) {
                $('<p style="color:red;">Please enter subject to proceed</p>').insertAfter('#inptSubject');
            }
            isValid = false;
        }
        else {
            $('#inptSubject').next().remove();
        }

        if ($('#inptDescription').val() === "") {
            if ($('#inptDescription').next().length < 1) {
                $('<p style="color:red;">Please enter description to proceed</p>').insertAfter('#inptDescription');
            }
            isValid = false;
        }
        else {
            $('#inptDescription').next().remove();
        }

        if ($('#tblReviewer tbody tr').length < 1) {
            if ($('#tblReviewer tfoot tr').length < 2) {
                $('<tr><td colspan="3"><p style="color:red;">Please select reviewer to proceed</p></td></tr>').insertAfter('#tblReviewer tfoot tr');
            }
            isValid = false;
        }
        else {
            $('#tblReviewer tfoot tr:nth-child(2)').remove();
        }
    }
    if (status === "Pending Requester" || (status === "Pending PEx Team" && $('#previousState').val() === "REQUEST_MORE")) {
        if ($('#inptComment').val() === "") {
            if ($('#inptComment').next().length < 1) {
                $('<p style="color:red;">Please enter comment to proceed</p>').insertAfter('#inptComment');
            }
            isValid = false;
        }
        else {
            $('#inptComment').next().remove();
        }
    }
    if (status === "Pending PEx Team") {
        if ($('input[name="department"]:checked').val() === undefined) {
            if ($('#inptDepartmentApp').next().length < 1) {
                $('<p class="row col-lg-12" style="color:red;">Please select department applicability to proceed</p>').insertAfter('#inptDepartmentApp');
            }
            isValid = false;
        }
        else {
            $('#inptDepartmentApp').next().remove();
        }

        if ($('input[name="location"]:checked').val() === undefined) {
            if ($('#inptLocationApp').next().length < 1) {
                $('<p class="row col-lg-12" style="color:red;">Please select location applicability to proceed</p>').insertAfter('#inptLocationApp');
            }
            isValid = false;
        }
        else {
            $('#inptLocationApp').next().remove();
        }

        if ($('#inptSOPName').val() === '') {
            if ($('#inptSOPName').next().length < 1) {
                $('<p style="color:red;">Please enter sop name to proceed</p>').insertAfter('#inptSOPName');
            }
            isValid = false;
        }
        else {
            $('#inptSOPName').next().remove();
        }

        if ($('#inptCode').val() === '') {
            if ($('#inptCode').next().length < 1) {
                $('<p style="color:red;">Please enter code to proceed</p>').insertAfter('#inptCode');
            }
            isValid = false;
        }
        else {
            $('#inptCode').next().remove();
        }

        if ($('#inptSOPVersion').val() === '') {
            if ($('#inptSOPVersion').next().length < 1) {
                $('<p style="color:red;">Please enter version to proceed</p>').insertAfter('#inptSOPVersion');
            }
            isValid = false;
        }
        else {
            $('#inptSOPVersion').next().remove();
        }

        if ($('#inptValidity').val() === '') {
            if ($('#inptValidity').next().length < 1) {
                $('<p style="color:red;">Please enter validity to proceed</p>').insertAfter('#inptValidity');
            }
            isValid = false;
        }
        else {
            $('#inptValidity').next().remove();
        }
        if ($('#inptPexfile tbody tr').length < 1) {
            if ($('#inptPexfile').next().length < 1) {
                $('<p style="color:red;">Please upload document to proceed</p>').insertAfter('#inptPexfile');
            }
            isValid = false;
        }
        else {
            $('#inptPexfile').next().remove();
        }
    }
    return isValid;
}

function SetRequest() {
    if ($('#inptCommentRemark').val() !== "" && $('#inptCommentRemark').val() !== undefined) {
        comment = {
            "Id": "",
            "Description": $('#inptCommentRemark').val()
        };
    }
}

function Submit(actionId) {
    if (InputValidate($('#lblStatus').text())) {
        var DA = '';
        var da = $('input[name="department"]:checked');
        for (var i = 0; i < da.length; i++) {
            DA += da[i].value + ';';
        }

        var LA = '';
        var la = $('input[name="location"]:checked');
        for (i = 0; i < la.length; i++) {
            LA += la[i].value + ';';
        }

        var sop = null;
        if (DA !== '') {
            sop = {
                "DepartmentApplicability": DA,
                "LocationApplicability": LA,
                "SOPName": $('#inptSOPName').val(),
                "SOPVersion": $('#inptSOPVersion').val(),
                "UserOwnerId": $('#inptUserOwner option:selected').val(),
                "DepartmentOwnerId": $('#inptDepartmentOwner option:selected').val(),
                "Validity": $('#inptValidity').val(),
                "Code": $('#inptCode').val()
            }
        }

        var reviewers = [];
        if ($('#lblStatus').text() === "Pending Requester" || $('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft") {
            $('#tblReviewer tbody tr').each(function () {
                reviewers.push({ "UserId": $(this).find('label').data('value') });
            })
        }

        var approvers = [];
        if ($('#lblStatus').text() === "Pending PEx Team") {
            $('#tblApprover tbody tr').each(function () {
                approvers.push({ "UserId": $(this).find('label').data('value') });
            });
        }

        var data = {
            "req": {
                "ActionId": actionId,
                "RoleId": $('#roleId').val(),
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "RequestTypeId": $('input[name="requesttype"]:checked').val(),
                "Comment": $('#inptComment').val(),
                "Reviewers": reviewers,
                "Approvers": approvers
            },
            "sop": sop
        };
        $.ajax({
            url: window.location.origin + "/ProcessVault/Document/Submit",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}
function SaveDraft(actionId) {
    if (InputValidate()) {
        var DA = '';
        var da = $('input[name="department"]:checked');
        for (var i = 0; i < da.length; i++) {
            DA += da[i].value + ';';
        }

        var LA = '';
        var la = $('input[name="location"]:checked');
        for (i = 0; i < la.length; i++) {
            LA += la[i].value + ';';
        }

        var sop = null
        if (DA !== '') {
            sop = {
                "DepartmentApplicability": DA,
                "LocationApplicability": LA,
                "SOPName": $('#inptSOPName').val(),
                "SOPVersion": $('#inptSOPVersion').val(),
                "UserOwnerId": '',
                "DepartmentOwnerId": '',
            }
        }
        var reviewers = []
        $('#tblReviewer tbody tr').each(function () {
            reviewers.push({ "UserId": $(this).find('label').data('value') });
        })

        var data = {
            "req": {
                "ActionId": actionId,
                "RequestId": $('#requestId').val(),
                "ReferenceNo": $('#referenceNo').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "RequestTypeId": $('input[name="requesttype"]:checked').val(),
                "Comment": $('#inptComment').val(),
                "Reviewers": reviewers
            },
            "sop": sop
        };
        $.ajax({
            url: window.location.origin + "/ProcessVault/Document/SaveDraft",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}
function SubmitSOP(actionId) {
    var data = {
        "sop": {
            "Id": $('#requestId').val(),
            "ActionId": actionId,
            "OwnedByDepartment": $('#imptsubject').val(),
            "DepartmentApplicabilityIds": [],
            "Validity": $('#imptrequesttype').val(),
            "CommentRemarks": $('#imptrequestcomment').val(),
        }
    }
    $.ajax({
        url: window.location.origin + "/ProcessVault/Home/SubmitSOP",
        data: data,
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function Approve(actionId) {
    var data = {
        "req": {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "ReferenceNo": $('#referenceNo').val(),
            "Comment": $('#inptComment').val()
        }
    }
    $.ajax({
        url: window.location.origin + "/ProcessVault/Document/Approve",
        data: data,
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}
function Reject(actionId) {
    if ($('#inptComment').val() !== "") {
        var data = {
            "req": {
                "ActionId": actionId,
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Comment": $('#inptComment').val()
            }
        }
        $.ajax({
            url: window.location.origin + "/ProcessVault/Document/Reject",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        $('<p style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
    }
}
function RequestMore(actionId) {
    if ($('#inptComment').val() !== "") {
        var data = {
            "req": {
                "ActionId": actionId,
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Comment": $('#inptComment').val()
            }
        }
        $.ajax({
            url: window.location.origin + "/ProcessVault/Document/RequestMore",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        if ($('#inptComment').next().length < 1) {
            $('<p style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
        }
    }
}
function uploadDoc(type) {
    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectPex').val() !== "") {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#requestId').val());
        formdata.append("UploadBy", type);
        var fileInput;
        if (type === "requester") {
            fileInput = document.getElementById('fileSelectRequester');
        }
        else {
            formdata.append("ClassificationId", $('input[name="classification"]:checked').val());
            fileInput = document.getElementById('fileSelectPex');
        }
        //Iterating through each files selected in fileInput
        for (var i = 0; i < fileInput.files.length; i++) {
            //Appending each file to FormData object
            alert(fileInput.files[i].name);
            //formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/ProcessVault/Document/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);

                var r = $('#inptRequesterfile tbody tr').length + 1;

                var row = "";
                if (type === "requester") {
                    $('#inptRequesterfile tbody')[0].innerHTML = '';
                    for (var i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name + '</td>' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td>' + d[i].Size + '</td> ' +
                            '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                        '</tr>';
                    }

                    $('#inptRequesterfile').append(row);
                    $('#fileSelectRequester').val('');
                }
                else {
                    $('#inptPexfile tbody')[0].innerHTML = '';
                    for (i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name + '</td>' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td>' + d[i].Size + '</td> ' +
                            '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                        '</tr>';
                    }

                    $('#inptPexfile').append(row);
                    $('#fileSelectPex').val('');

                    if ($('#inptPexfile tbody tr').length > 0) {
                        $('#pexFileUpload').toggle();
                    }
                    else {
                        $('#pexFileUpload').toggle();
                    }
                }
            }
        };
    }
}

function removeAttachment(e) {
    var attachmentId = $(e).parent().data('value');

    $.ajax({
        url: window.location.origin + "/ProcessVault/Document/DeleteAttachment",
        data: { "attachmentId": attachmentId },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            showToast(d.Message, "success");
            $(e).parent().parent().remove();
            if ($('#inptPexFile tbody tr').length > 0) {
                $('#pexFileUpload').toggle();
            }
            else {
                $('#pexFileUpload').toggle();
            }
        }
    });
}

function CloseForm() {
    $.ajax({
        url: window.location.origin + "/ProcessVault/Home/Index",
        data: "",
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            //setTimeout(function () {
            window.location.href = window.location.origin + d.RedirectUrl;
            //}, 2000);
        }
    });
}

function selectReviewer() {
    if ($('#inptReviewer option:selected').val() !== "0") {
        var a = $('#inptReviewer option:selected');
        $('#tblReviewer').append('<tr><td colspan="2"><label data-value="' + a.val() + '">' + a.text() + '</label></td>' +
            '<td style="text-align: center">' +
            '<a class="delete" onclick="removeReviewer(this)" data-value="' + a.val() + '" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>');
    }
}

function removeReviewer(e) {
    $.ajax({
        url: window.location.origin + "/ProcessVault/Document/RemoveReviewer",
        data: { "userId": $(e).data('value'), "requestId": $('#requestId').val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            $(e).parent().parent().remove();
        }
    });
}


function selectApprover() {
    if ($('#inptApprover option:selected').val() !== "0") {
        var a = $('#inptApprover option:selected');
        $('#tblApprover').append('<tr><td colspan="2"><label data-value="' + a.val() + '">' + a.text() + '</label></td>' +
            '<td style="text-align: center">' +
            '<a class="delete" onclick="removeApprover(this)" data-value="' + a.val() + '" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>');
    }
}

function removeApprover(e) {
    $.ajax({
        url: window.location.origin + "/ProcessVault/Document/RemoveApprover",
        data: { "userId": $(e).data('value'), "requestId": $('#requestId').val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            $(e).parent().parent().remove();
        }
    });
}

$('#impt :checkbox').change(function () {
    // this will contain a reference to the checkbox   
    if (this.checked) {
        // the checkbox is now checked 
    } else {
        // the checkbox is now no longer checked
    }
});

function onlyOne(c) {
    $('#tblApprover').append('<tr><td>HOD-' + c.val() + '<input type="hidden" id="approverId" value="' + c.val() + '"/></td></tr>')
}

function selectDepartment(e) {
    $.ajax({
        url: window.location.origin + "/ProcessVault/Document/GetHOD",
        data: { "departmentId": $(e).val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            var hod = JSON.parse(d.Data);
            if ($(e).prop('checked')) {
                $('#tblApprover').append('<tr><td colspan="2"><label data-value="' + hod.UserId + '">' + hod.Name + '</label></td>' +
                    '<td style="text-align: center">' +
                    '<a class="delete" onclick="removeApprover(this)" data-value="' + hod.UserId + '" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>');
            }
            else {
                $('#tblApprover tbody tr').each(function () {
                    if ($(this).find('label').data('value') === hod.UserId) {
                        $(this).remove();
                    }
                });
            }
        }
    });


}

function ExportPDF() {
    $.ajax({
        url: window.location.origin + "/ProcessVault/Document/PdfSharpConvert",
        data: { "html": document.documentElement.innerHTML },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
          
        }
    });
}