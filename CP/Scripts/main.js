﻿
function initMain() {
    initRestriction();
    bindSaveButtonClick();
}
function initRestriction() {
    if ($('#lastlogindate').val() === '') {
        $('#updatePassword').css('display', 'block');
        $('#updatePassword').toggleClass('in');
        $('<div class="modal-backdrop fade in"></div>').insertAfter('#updatePassword');
    }

    $('#inptPass').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validatePasswordPattern($(this).val())) {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid password to proceed.</p>').insertAfter($(this));
                }
            } else {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                    $('#inptPass').blur();
                }
            }
        }
        else {
            $(this).next().remove();
        }
        validatePassword();
    });

    $('#inptPass1').blur(function () {
        if ($(this).val().trim() !== '') {
            if ($(this).val() !== $('#inptPass').val()) {
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Password doesn&acute;t match.</p>').insertAfter($(this));
                }
            }
            else {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            }
        }
        else {
            $(this).next().remove();
        }

        validatePassword();
    });
}

function bindSaveButtonClick() {
    $('#btnUpdate').click(function () {
        var p = $('#inptPass').val().trim();
        var p1 = $('#inptPass1').val().trim();

        var url = "ChangePassword";
        var data;
        if ($('#lastlogindate').val() === '') {
            url = "UpdatePassword";
            data = { "pass": p };
        }
        else {
            data = { 'curPass': $('#inptCurPass').val().trim(), 'pass': p };
        }
        
        if (p === p1 && !validatePasswordPattern(p)) {
            $.ajax({
                url: window.location.origin + "/Home/" + url,
                data: data,
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        $('#updatePassword').css('display', 'none');
                        $('#updatePassword').toggleClass('in');
                        $('#updatePassword').next().remove();
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function validatePasswordPattern(p) {
    var regx = new RegExp('^(.{0,7}|[^0-9]*|[^A-Z]*|[a-zA-Z0-9]*)$');
    return regx.test(p);
}

function validatePassword() {
    if ($('#inptCurPass').val() !== '' && $('#inptPass').val() !== '' && $('#inptPas1').val() !== '' && $('#inptPass').val() === $('#inptPass1').val()) {
        $('#btnUpdate').prop('disabled', false);
    }
    else {
        $('#btnUpdate').prop('disabled', true);
    }
}


function showToast(msg, type) {
    if (type === "success") {
        $.toast({
            heading: 'Success',
            text: msg,
            position: 'top-roght',
            stack: false
        });
    }
    else if (type === "error") {
        $.toast({
            heading: 'Error',
            text: msg,
            position: 'top-roght',
            icon: 'error'
        });
    }
}