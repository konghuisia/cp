﻿

function init() {
    //bindUploadButtonClick();
    //bindEmploymentTypeChange();
    //bindIncentiveChange();
    //bindWageTypeChange();
    //bindProfilePictureUpload();
    initInputRestriction();
    requestMore();
    stage9();
    dispute();
    rejected();
    complete();
    release();
    checkAllRequest();
    bindSaveSOP();
}

function clearPackageInfo(cb) {

    var btdrAmt = document.getElementById("btdrAmt");
    btdrAmt.value = ""

    var bprAmt = document.getElementById("bprAmt");
    bprAmt.value = ""

    var rnrAmt = document.getElementById("rnrAmt");
    rnrAmt.value = ""

    var iolAmt = document.getElementById("iolAmt");
    iolAmt.value = ""

    var dsAmt = document.getElementById("dsAmt");
    dsAmt.value = ""

    var dtrlAmt = document.getElementById("dtrlAmt");
    dtrlAmt.value = ""

    var cicAmt = document.getElementById("cicAmt");
    cicAmt.value = ""

    var cfaAmt = document.getElementById("cfaAmt");
    cfaAmt.value = ""
    
    var cbarray = document.getElementsByName(cb);
    for (var i = 0; i < cbarray.length; i++) {
        cbarray[i].checked = false
    }
}


function SetTotalCredit(selectID) {
    var selection = document.getElementById(selectID);
    var strValue = selection.options[selection.selectedIndex].value;

    span = document.getElementById("spanTotal");
    clearPackageInfo("sub_services");
    if (strValue === "DMS-5") {
        span.textContent = "/5,000"
    } else {
        span.textContent = "/3,000"
    }
}

function checkAllRequest() {
    $('#debtorTbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            var x = $(this).children().offset();
            x.left += 49;
            x.top -= 4;
            $.ajax({
                type: "POST",
                url: "/Lead/checkDebtorProfile",
                data: '{businessRegNo: "' + $(this).data('brn') + '" , requestId: "' + $('#requestId').val() + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.ProfileExists) {
                        //Email available.

                        if (data.Requests.length != 0) {
                            var request = "";
                            var id = data.Debtors[0].DebtorId;
                            $.each(data.Requests, function (index, value) {
                                request += 'data-value-' + index + '="' + value.RequestId + ';' + value.id + ';' + value.Status.substring(0, 4) + '"';
                            });
                            $('<a id=' + id + ' onClick="showCasesPre(this)" data-id=' + id + ' ' + request + ' style="position:absolute;"><img src="/Image/179386.png" width="18" height="18"></a>').insertAfter($('#creditor'));
                            $('#' + id + '').css(x);
                        }
                    }
                }
            });
        });
    });
}

function initInputRestriction() {
    //var appointmentType = [], docType = [];

    ////Iterate all td's in second column$('#inptBlacklistfile tbody tr td:last-child').each(function () {
    //$('#inptRequesterfile tbody tr td:last-child').each(function () {
    //    $(this).find('a').each(function () {
    //        appointmentType.push($(this).data('type'));
    //    });
    //});

    //$('#inptAppointmentFile tbody tr td:last-child').each(function () {
    //    $(this).find('.delete').each(function () {
    //        docType.push($(this).data('type'));
    //    });
    //});

    //$.each(appointmentType, function (e) {
    //    $.each(docType, function (ee) {
    //        if (e == "3E653BCE-AE1B-46C4-840D-A75A5B4EDF15") {
    //            if (ee == "FD42D4E6-D6EA-4A79-B57A-528F93A5A3B8") {
    //                return true;
    //            }
    //        }
    //        else if (e == "95CBF154-443F-4E49-B33F-F647E9BE5B1B") {
    //            if (ee == "FB13C881-4032-4B92-B25F-6CFF7BFB55E4") {
    //                return true;
    //            }
    //        }
    //    });
    //});

    $('.datepicker').keypress(function () {
        return false;
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    $('.name').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.Pname').keyup(onlyAllowAlphanumeric);
    $('.Pname').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    //$('#businessRegNo').blur(function () {
    //    $.ajax({
    //        type: "POST",
    //        url: "/Subscriptions/checkCreditorProfile",
    //        data: '{businessRegNo: "' + $('#businessRegNo').val() + '" , requestId: "' + $('#requestId').val() + '" }',
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (data) {
    //            if ($('#businessRegNo').val() != "") {
    //                if (data.ProfileExists && data.isSubscriber) {
    //                    //Email available.

    //                    var r = confirm('Profile is already a DMS subscriber. Press "OK" to go to subscription page. Press "Cancel" to key in other Business Registration No.');
    //                    if (r == true) {
    //                        window.location.href = '/Subscriptions/Subscription/?profileId=' + data.Profile.ProfileId;
    //                    } else {
    //                        $('#businessRegNo').val('');
    //                        $('#businessRegNo').focus();
    //                    }
    //                } else if (data.ProfileExists && !data.isSubscriber) {
    //                    $('#ProfileId').val(data.Profile.ProfileId);
    //                    $('#name').val(data.Profile.Name);
    //                    $('#contactNo').val(data.Profile.ContactNo);
    //                    $('#email').val(data.Profile.Email);
    //                    $('#PersonId').val(data.Person.PersonId)
    //                    $('#picName').val(data.Person.Name)
    //                    $('#picContact').val(data.Person.ContactNo)
    //                    $('#picEmail').val(data.Person.Email)
    //                }
    //                else {
    //                    var t = confirm('Profile is not yet registered yet. Register this profile?');
    //                    if (t == true) {
    //                        window.location.href = 'Profile/CreateProfile?page=Creditor';
    //                    } else {
    //                        $('#businessRegNo').val('');
    //                        $('#businessRegNo').focus();
    //                    }
    //                }
    //            }

    //        }
    //    });
    //});

    //$('#inptIdentityNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});

    //$('#inptAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptAddress').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptHomeAddress').keydown(function (e) {
    //    restrictAddressAlphanumericInput(e);
    //});
    //$('#inptHomeAddress').blur(function () {
    //    trimSpacing(this);
    //});

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });
    //$('#inptOfficeContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptNationality').change(function () {
    //    if ($('#inptNationality option:selected').data('keyword') === 'MY') {
    //        $('[name=epfsocso]').prop('disabled', true);
    //    }
    //    else {
    //        $('[name=epfsocso]').prop('disabled', false);
    //    }
    //});

    //$('#inptDocumentNo').keydown(function (e) {
    //    restrictAlphanumericDashInput(e);
    //});
    //$('#inptDocumentNo').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptRemark').keydown(function (e) {
    //    restrictAlphanumericInput(e);
    //});
    //$('#inptRemark').blur(function () {
    //    trimSpacing(this);
    //});

    //$('#inptEMPName').keydown(function (e) {
    //    restrictAlphabetInput(e);
    //});
    //$('#inptEMPName').blur(function () {
    //    trimSpacing(this);
    //});
    //$('#inptEMPContactNo').keydown(function (e) {
    //    restrictIntegerInput(e);
    //});

    //$('#inptAdvancePaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});

    //$('#inptMonthlyRepaymentAmount').blur(function (e) {
    //    $(this).val($(this).val().replace(/\,/g, ''));
    //});
}

//function add() {
//    $('#assignToId').val($('#assignTo option:selected').val());
//    var r = $('#Assign tbody tr').length + 1;
//    var row = "";
//    var a = $('#assignTo option:selected').text();
//    $('#Assign tbody')[0].innerHTML = '';
//    row += '<tr>' +
//        '<td width="96">' + a + '</td>' +
//        '<td>' + "@DateTime.Now.ToShortDateString()" + '</td> ' +
//        '<td width="20" data-value="" style="text-align:center;"><a class="delete" title="Delete" onclick="remove(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
//    '</tr>';

//    $('#Assign').append(row);
//}

//function remove(e) {
//    $('#assignToId option:selected').val('');
//    $(e).parent().parent().remove();
//    var r = $('#Assign tbody tr').length + 1;
//    var row = "";
//    $('#Assign tbody')[0].innerHTML = '';
//    row += '<tr>' +
//        '<td colspan="2"><select id="assignTo" class="form-control" title="Select Risk Management Officer" @if(position != "Operation") { @disabled } > @foreach (var u in Model.User){<option value="@u.UserId">@u.Name</option>} </select></td>' +
//        '<td style="text-align:center; vertical-align:middle" width="20"><a onclick="add()" class="btn btn-xs btn-info">Add</a></td>'
//    '</tr>';

//    $('#Assign').append(row);
//}

//function uploadDoc(type) {
//    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectPex').val() !== "") {
//        var formdata = new FormData(); //FormData object
//        formdata.append("RequestId", $('#requestId').val());
//        formdata.append("UploadBy", type);
//        var fileInput;
//        if (type === "requester") {
//            fileInput = document.getElementById('fileSelectRequester');
//        }
//        else {
//            formdata.append("ClassificationId", $('input[name="classification"]:checked').val());
//            fileInput = document.getElementById('fileSelectPex');
//        }
//        //Iterating through each files selected in fileInput
//        for (var i = 0; i < fileInput.files.length; i++) {
//            //Appending each file to FormData object
//            //formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
//            formdata.append(fileInput.files[i].name, fileInput.files[i]);
//        }

//        //Creating an XMLHttpRequest and sending
//        var xhr = new XMLHttpRequest();
//        xhr.open('POST', window.location.origin + '/Lead/AttachmentUpload');
//        xhr.send(formdata);
//        xhr.onreadystatechange = function () {
//            if (xhr.readyState === 4 && xhr.status === 200) {
//                var d = JSON.parse(xhr.response);

//                debugger
//                var r = $('#inptRequesterfile tbody tr').length + 1;
//                var row = "";
//                if (type === "requester") {
//                    $('#inptRequesterfile tbody')[0].innerHTML = '';
//                    row += '<tr>' +
//                        '<td>' + d.FileName + '</td>' +
//                        '<td>' + d.Size + '</td> ' +
//                        '<td>' + d.CreatedDateString + '</td>' +
//                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
//                    '</tr>';

//                    $('#inptRequesterfile').append(row);
//                    $('#fileSelectRequester').val('');
//                }
//                else {
//                    $('#inptPexfile tbody')[0].innerHTML = '';
//                    for (i = 0; i < d.length; i++) {
//                        row += '<tr>' +
//                            '<td>' + d.fileName + '</td>' +
//                            '<td>' + d.Size + '</td> ' +
//                            '<td>' + d.CreatedDateString + '</td>' +
//                            '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
//                        '</tr>';
//                    }

//                    $('#inptPexfile').append(row);
//                    $('#fileSelectPex').val('');

//                    if ($('#inptPexfile tbody tr').length > 0) {
//                        $('#pexFileUpload').toggle();
//                    }
//                    else {
//                        $('#pexFileUpload').toggle();
//                    }
//                }
//            }
//        };
//    }
//}

//function removeAttachment(e) {
//    $.ajax({
//        url: window.location.origin + "/Lead/DeleteAttachment",
//        data: { "documentId": $(e).data('value') },
//        cache: false,
//        type: "POST",
//        dataType: "html",
//        success: function (data, textStatus, XMLHttpRequest) {
//            var d = JSON.parse(data);
//            if (d.Error === null) {
//                showToast(d.Message, "success");
//                $(e).parent().parent().remove();
//            }
//            else {
//                showToast(d.Error, "error");
//            }
//        }
//    });
//}

function showCasesPre(e) {
    $('#' + $(e).data('id') + '').attr("onclick", "closeCasesPre(this)");
    var x = $('#' + $(e).data('id') + '').offset();
    x.top += 30;
    var Requests = "<table class='table table-bordered' style='position:absolute;' id='" + $(e).data('id') + "-id' >";

    var bool = true;
    $.each(e.dataset, function (index, value) {
        if (!bool) {
            var arr = value.split(';');
            Requests += "<tr><td><a style='font-size:9px; font-style:italic; color:blue;' target='_blank' href='/Lead/EditLeadPreview?requestId=" + arr[0] + "'>" + arr[2] + "-" + arr[1] + "</a><td></tr>";
        }
        bool = false;
    });

    Requests += "</table>";
    $(Requests).insertAfter($('#' + $(e).data('id') + ''));
    $('#' + $(e).data('id') + '-id').css(x);
}

function closeCasesPre(e) {
    $('#' + $(e).data('id') + '-id').remove();
    $('#' + $(e).data('id') + '').attr("onclick", "showCasesPre(this)");
}

function showCases(e) {
    $("#info").attr("onclick", "closeCases()");
    var x = $('#info').offset();
    x.top += 30;
    var Requests = "<table class='table table-bordered' style='position:absolute;' id='Requests'>";

    $.each(e.dataset, function (index, value) {
        var arr = value.split(';');
        Requests += "<tr><td><a style='font-size:9px; font-style:italic; color:blue;' target='_blank' href='/Lead/EditLeadPreview?requestId=" + arr[0] + "'>" + arr[2] + "-" + arr[1] + "</a><td></tr>";
    });

    Requests += "</table>";
    $(Requests).insertAfter($('#info'));
    $('#Requests').css(x);
}

function closeCases() {
    $('#Requests').remove();
    $("#info").attr("onclick", "showCases(this)");
}
function checkDebtorField() {
    var isValid = true;

    if ($('#BusinessRegNo').val() === '') {
        if ($('#BusinessRegNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please insert Business Reg Number to proceed.</label>').insertAfter('#BusinessRegNo');
        }
        isValid = false;
    }
    else {
        if ($('#BusinessRegNo').next('.invalid-alert').length > 0) {
            $('#BusinessRegNo').next('.invalid-alert').remove();
        }
    }

    if ($('#CompanyName').val() === '') {
        if ($('#CompanyName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please insert Name to proceed.</label>').insertAfter('#CompanyName');
        }
        isValid = false;
    }
    else {
        if ($('#CompanyName').next('.invalid-alert').length > 0) {
            $('#CompanyName').next('.invalid-alert').remove();
        }
    }

    if ($('#ContactNo').val() === '') {
        if ($('#ContactNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please insert Contact No to proceed.</label>').insertAfter('#ContactNo');
        }
        isValid = false;
    }
    else {
        if ($('#ContactNo').next('.invalid-alert').length > 0) {
            $('#ContactNo').next('.invalid-alert').remove();
        }
    }

    if ($('#ClaimAmount').val() === '') {
        if ($('#ClaimAmount').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Claim Amount to proceed.</label>').insertAfter('#ClaimAmount');
        }
        isValid = false;
    }
    else {
        if ($('#ClaimAmount').next('.invalid-alert').length > 0) {
            $('#ClaimAmount').next('.invalid-alert').remove();
        }
    }
    return isValid;
}
function addDebtor() {

    if (!checkDebtorField()) {
        return false;
    }

    var choice = $('#choice option:selected').val();
    var cname = $('#CompanyName').val();
    var cno = $('#ContactNo').val();
    var brn = $('#BusinessRegNo').val();
    var ca = $('#ClaimAmount').val();
    var r = $('#DebtorRemark').val();
    var isrb = $('#InsolvencyStatusRB').is(':checked');
    var mdisrb = $('#MDIStatusRB').is(':checked');
    var sibrb = $('#StillInBusinessRB').is(':checked');
    var isrbText = "No";
    var mdisrbText = "No";
    var sibrbText = "No";

    if (isrb) {
        isrbText = "Yes";
        isrb = "True";
    }
    else {
        isrb = "False";
    }

    if (mdisrb) {
        mdisrbText = "Yes";
        mdisrb = "True";
    }
    else {
        mdisrb = "False";
    }

    if (sibrb) {
        sibrbText = "Yes";
        sibrb = "True";
    }
    else {
        sibrb = "False";
    }

    if (ca === "") {
        ca = "0.00";
    }

    var Debtor = {
        "RequestId": $('#requestId').val(),
        "CompanyName": cname,
        "ContactNo": cno,
        "BusinessRegNo": brn,
        "ClaimAmount": parseFloat(ca.replace(/,/g, '')),
        "StillInBusiness": sibrb,
        "Remark": r,
        "InsolvencyStatus": isrb,
        "MDIStatus": mdisrb
    };

    $.ajax({
        url: window.location.origin + "/Lead/AddDebtor",
        data: { "Debtor": Debtor },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);

            var row = "";

            var row2 = "";
            if (choice == "Company") {
                row = '<tr>' +
                    '<td><a data-type="show" data-value="' + d.DebtorId + '" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + ca + '" data-r="' + r + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" onclick="Debtor(this)">' + brn + '</a></td>' +
                    '<td>' + cno + '</td>' +
                    '<td>' + cname + '</td>' +
                    '<td class=alignRight>' + ca + '</td>' +
                    '<td>' + r + '</td>' +
                    '<td>' + isrbText + '</td>' +
                    '<td>' + mdisrbText + '</td>' +
                    '<td>' + sibrbText + '</td>' +
                    '<td style="text-align:center;"><a class="delete" title="Delete" onclick="removeDebtor(this)" data-value="' + d.DebtorId + '" data-doc="debtor" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" data-r="' + r + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                '</tr>';
            }
            else {
                row = '<tr>' +
                    '<td><a data-type="show" data-value="' + d.DebtorId + '" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + ca + '" data-r="' + r + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" onclick="Debtor(this)">' + brn + '</a></td>' +
                    '<td>' + cno + '</td>' +
                    '<td>' + cname + '</td>' +
                    '<td class=alignRight>' + ca + '</td>' +
                    '<td>' + r + '</td>' +
                    '<td>' + sibrbText + '</td>' +
                    '<td style="text-align:center;"><a class="delete" title="Delete" onclick="removeDebtor(this)" data-value="' + d.DebtorId + '" data-doc="debtor" data-cname="' + cname + '" data-cno="' + cno + '" data-brn="' + brn + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '" data-isrb="' + isrb + '" data-mdisrb="' + mdisrb + '" data-sibrb="' + sibrb + '" data-r="' + r + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                '</tr>';
            }
            $('#inptDebtor').last().append(row);
        }
    });

    $('#CompanyName').val('');
    $('#ContactNo').val('');
    $('#BusinessRegNo').val('');
    $('#ClaimAmount').val('');
    $('#DebtorRemark').val('');
    $("#InsolvencyStatusRB").prop('checked', false);
    $("#MDIStatusRB").prop('checked', false);
    $("#StillInBusinessRB").prop('checked', false);

    $('#info').remove();
    $('#Requests').remove();

    if ($('#BusinessRegNo').next('.invalid-alert').length > 0) {
        $('#BusinessRegNo').next('.invalid-alert').remove();
    }
    if ($('#CompanyName').next('.invalid-alert').length > 0) {
        $('#CompanyName').next('.invalid-alert').remove();
    }
    if ($('#ContactNo').next('.invalid-alert').length > 0) {
        $('#ContactNo').next('.invalid-alert').remove();
    }
    if ($('#ClaimAmount').next('.invalid-alert').length > 0) {
        $('#ClaimAmount').next('.invalid-alert').remove();
    }
}

function removeDebtor(e) {
    $.ajax({
        url: window.location.origin + "/Lead/DeleteDebtor",
        data: { "DebtorId": $(e).data('value') },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                $(e).parent().parent().remove();
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function Debtor(e) {
    if ($(e).data('type') == "show") {
        $('#addDebtor').css('display', 'none');
        $('#hideDebtor').css('display', 'inline');

        var cname = $(e).data('cname');
        var cno = $(e).data('cno');
        var brn = $(e).data('brn');
        var ca = $(e).data('ca');
        var r = $(e).data('r');
        var isrb = $(e).data('isrb');
        var mdisrb = $(e).data('mdisrb');
        var sibrb = $(e).data('sibrb');
        if (ca === "") {
            ca = "0.00";
        }

        $('#CompanyName').val(cname);
        $('#ContactNo').val(cno);
        $('#BusinessRegNo').attr('disabled', 'disabled');
        $('#BusinessRegNo').val(brn);
        $('#ClaimAmount').val(ca);
        $('#DebtorRemark').val(r);
        debugger
        if (isrb === "True") {
            $("#InsolvencyStatusRB").prop('checked', true);
        }
        else {
            $("#InsolvencyStatusRB").prop('checked', false);
        }

        if (mdisrb === "True") {
            $("#MDIStatusRB").prop('checked', true);
        }
        else {
            $("#MDIStatusRB").prop('checked', false);
        }

        if (sibrb === "True") {
            $("#StillInBusinessRB").prop('checked', true);
        }
        else {
            $("#StillInBusinessRB").prop('checked', false);
        }

        $('#info').remove();
        $('#Requests').remove();
    }
    else {
        $('#addDebtor').css('display', 'inline');
        $('#hideDebtor').css('display', 'none');

        $('#CompanyName').val('');
        $('#ContactNo').val('');
        $('#BusinessRegNo').val('');
        $('#BusinessRegNo').removeAttr('disabled');
        $('#ClaimAmount').val('');
        $('#DebtorRemark').val('');

        $("#InsolvencyStatusRB").prop('checked', false);
        $("#MDIStatusRB").prop('checked', false);
        $("#StillInBusinessRB").prop('checked', false);
    }
}

function uploadDoc(doctype) {
    var docType2 = null;
    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectPex').val() !== "") {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#requestId').val());
        var fileInput;
        if (doctype === "AB7FF5C1-7D88-4BAE-AAEC-85561463026F") {
            fileInput = document.getElementById('fileSelectRequester');
            if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
                $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptRequesterfile');
                return false;
            }
            else {
                $('#inptRequesterfile').next('.invalid-alert').remove();
            }
        }
        //if (doctype === "65FECCE2-AA95-412D-95D2-0A271273B08F") {
        //    fileInput = document.getElementById('fileSelectRequester');
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptRequesterfile');
        //        return false;
        //    }
        //    else {
        //        $('#inptRequesterfile').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "7F760368-629F-43A4-A2CB-5F2F2C4003B4") {
        //    fileInput = document.getElementById('fileSelectFinancialStatement');
        //    formdata.append("FYE", $('#FinancialYearEnd1').val());
        //    formdata.append("AT", $('#AnnualTurnover1').val());
        //    formdata.append("NTA", $('#NtaPositiveNegative1').val());
        //    formdata.append("PAT", $('#ProfitAfterTax1').val());
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptFinancialStatementfile');
        //        return false;
        //    }
        //    else {
        //        $('#inptFinancialStatementfile').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "CE756226-33DF-4F9B-96BD-BBA9687B90ED") {
        //    var ca = $('#LRClaimAmount').val();
        //    if (ca == "") {
        //        ca = "0.00";
        //    }
        //    fileInput = document.getElementById('fileSelectLitigation');
        //    formdata.append("CA", ca);
        //    formdata.append("SOC", $('#StatusOfCase1').val());
        //    formdata.append("HD", $('#HearingDate').val());
        //    formdata.append("P", $('#Plaintiff').val());
        //    formdata.append("D", $('#Defendant').val());
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptLitigationfile2');
        //        return false;
        //    }
        //    else {
        //        $('#inptLitigationfile').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "AB94BE85-7EED-4307-B8CE-9A799BF090B6") {
        //    var ca = $('#BRClaimAmount').val();
        //    if (ca == "") {
        //        ca = "0.00";
        //    }
        //    fileInput = document.getElementById('fileSelectBlacklist');
        //    formdata.append("C", $('#Creditor1').val());
        //    formdata.append("SD", $('#StatementDate').val());
        //    formdata.append("CA", ca);
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptBlacklistfile');
        //        return false;
        //    }
        //    else {
        //        $('#inptBlacklistfile').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "AppointmentDoc") {
        //    fileInput = document.getElementById('fileSelectAppointment');
        //    doctype = $('#appointmentDocType').val();
        //    docType2 = "AppointmentDoc";
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptAppointmentFile');
        //        return false;
        //    }
        //    else {
        //        $('#inptAppointmentFile').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "PreAndSoa") {
        //    fileInput = document.getElementById('fileSelectPreAndSoa');
        //    doctype = $('#preAndSoaDocType').val();
        //    docType2 = "PreAndSoa";
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptPreAndSoa');
        //        return false;
        //    }
        //    else {
        //        $('#inptPreAndSoa').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "B8084C0D-1096-4F0F-A747-8B76DB48B1DC") {
        //    fileInput = document.getElementById('fileSelectCtos');
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptCtos');
        //        return false;
        //    }
        //    else {
        //        $('#inptCtos').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "D307B976-AF64-4B34-8CED-FE3DD69A2195") {
        //    fileInput = document.getElementById('fileSelectDispute');
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptDispute');
        //        return false;
        //    }
        //    else {
        //        $('#inptDispute').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "6EB5BD98-1305-42FD-BED8-919F03BFD78B") {
        //    fileInput = document.getElementById('fileSelectJid');
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptJid');
        //        return false;
        //    }
        //    else {
        //        $('#inptJid').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "settlementAgreement") {
        //    fileInput = document.getElementById('fileSelectSettlement');
        //    doctype = $('#settlementDocType').val();
        //    docType2 = "settlementAgreement";
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptSettlement');
        //        return false;
        //    }
        //    else {
        //        $('#inptSettlement').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "financeDoc") {
        //    fileInput = document.getElementById('fileSelectFinanceDoc');
        //    doctype = $('#financeDocType').val();
        //    docType2 = "financeDoc";
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptFinanceDoc');
        //        return false;
        //    }
        //    else {
        //        $('#inptFinanceDoc').next('.invalid-alert').remove();
        //    }
        //}
        //else if (doctype === "272B6077-CBA6-4855-80EB-9D52E5B76D22") {
        //    fileInput = document.getElementById('fileSelectInvoiceDoc');
        //    if (Math.floor(fileInput.files[0].size / 1000) > 4096) {
        //        $('<label class="invalid-alert">File larger than 4mb. Maximum file upload size is 4mb.</label>').insertAfter('#inptInvoiceDoc');
        //        return false;
        //    }
        //    else {
        //        $('#inptInvoiceDoc').next('.invalid-alert').remove();
        //    }
        //}

        formdata.append("Type", doctype);
        formdata.append("ProfileId", $('#ProfileId').val());

        //Iterating through each files selected in fileInput
        for (var i = 0; i < fileInput.files.length; i++) {
            //Appending each file to FormData object
            //formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/Lead/AttachmentUpload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);
                if (doctype === "AB7FF5C1-7D88-4BAE-AAEC-85561463026F") {
                    var row = '<tr>' +
                        //'<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                        '<td>' + d.FileName + '</td>' +
                        '<td>' + d.Type + '</td> ' +
                        '<td>' + d.CreatedDateString + '</td>' +
                        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';

                    $('#inptRequesterfile').last().append(row);
                    $('#fileSelectRequester').val('');
                    //location.reload();
                }
                //if (doctype === "65FECCE2-AA95-412D-95D2-0A271273B08F") {
                //    var row = '<tr>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td>' + d.Type + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptRequesterfile').last().append(row);
                //    $('#fileSelectRequester').val('');
                //    //location.reload();
                //}
                //else if (doctype === "7F760368-629F-43A4-A2CB-5F2F2C4003B4") {
                //    var fye = $('#FinancialYearEnd1').val();
                //    var at = $('#AnnualTurnover1').val();
                //    var npn = $('#NtaPositiveNegative1').val();
                //    var pat = $('#ProfitAfterTax1').val();

                //    var row = '<tr>' +
                //        '<td>' + fye + '</td>' +
                //        '<td class=alignRight>' + at + '</td> ' +
                //        '<td class=alignRight>' + npn + '</td>' +
                //        '<td class=alignRight>' + pat + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '" data-fye="' + fye + '" data-at="' + parseFloat(at.replace(/,/g, '')) + '" data-npn="' + parseFloat(npn.replace(/,/g, '')) + '" data-pat="' + parseFloat(pat.replace(/,/g, '')) + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptFinancialStatementfile').last().append(row);
                //    $('#fileSelectFinancialStatement').val('');
                //    $('#FinancialYearEnd1').val('');
                //    $('#AnnualTurnover1').val('');
                //    $('#NtaPositiveNegative1').val('');
                //    $('#ProfitAfterTax1').val('');

                //}
                //else if (doctype === "CE756226-33DF-4F9B-96BD-BBA9687B90ED") {
                //    var ca = $('#LRClaimAmount').val();
                //    if (ca == "") {
                //        ca = "0.00";
                //    }
                //    var socN = $('#StatusOfCase1 option:selected').text();
                //    var soc = $('#StatusOfCase1').val();
                //    var hd = $('#HearingDate').val();
                //    var p = $('#Plaintiff').val();
                //    var dd = $('#Defendant').val();

                //    var row = '<tr>' +
                //        '<td class=alignRight>' + ca + '</td>' +
                //        '<td>' + socN + '</td> ' +
                //        '<td>' + hd + '</td>' +
                //        '<td>' + p + '</td> ' +
                //        '<td>' + dd + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-doc="litigation" data-value="' + d.DocumentId + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '" data-soc="' + soc + '" data-hd="' + hd + '" data-p="' + p + '" data-dd="' + dd + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptLitigationfile').last().append(row);
                //    $('#fileSelectLitigation').val('');
                //    $('#LRClaimAmount').val('');
                //    $('#StatusOfCase1').val($("#StatusOfCase1 option:first").val());
                //    $('#HearingDate').val('');
                //    $('#Plaintiff').val('');
                //    $('#Defendant').val('');

                //}
                //else if (doctype === "AB94BE85-7EED-4307-B8CE-9A799BF090B6") {
                //    var c = $('#Creditor1').val();
                //    var sd = $('#StatementDate').val();
                //    var ca = $('#BRClaimAmount').val();
                //    if (ca == "") {
                //        ca = "0.00";
                //    }

                //    var row = '<tr>' +
                //        '<td>' + c + '</td>' +
                //        '<td>' + sd + '</td> ' +
                //        '<td class=alignRight>' + ca + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '" data-c="' + c + '" data-sd="' + sd + '" data-ca="' + parseFloat(ca.replace(/,/g, '')) + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptBlacklistfile').last().append(row);
                //    $('#fileSelectBlacklist').val('');
                //    $('#Creditor1').val('');
                //    $('#StatementDate').val('');
                //    $('#BRClaimAmount').val('');
                //}
                //else if (docType2 == "AppointmentDoc") {

                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-seg="AppointmentDoc" data-type="' + $("#appointmentDocType option:selected").val() + '" data-value="' + d.DocumentId + '" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptAppointmentFile').append(row);
                //    $('#fileSelectAppointment').val('');
                //    $('#preEngagement').css('display', 'none');
                //    $('#letterOfEngagement').css('display', 'none');

                //    location.reload();
                //    $("#appointmentDocType option:selected").attr('disabled', 'disabled');

                //    inputValidation("S-" + $('#stage').val());
                //}
                //else if (docType2 == "PreAndSoa") {
                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-seg="PreAndSoaDoc" data-type="' + $("#preAndSoaDocType option:selected").val() + '" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptPreAndSoa').last().append(row);
                //    $('#fileSelectPreAndSoa').val('');

                //    location.reload();
                //    $("#preAndSoaDocType option:selected").attr('disabled', 'disabled');
                //    inputValidation("S-" + $('#stage').val());
                //}
                //else if (doctype === "B8084C0D-1096-4F0F-A747-8B76DB48B1DC") {
                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptCtos').last().append(row);
                //    $('#fileSelectCtos').val('');
                //    inputValidation("S-" + $('#stage').val());

                //    location.reload();
                //}
                //else if (doctype === "D307B976-AF64-4B34-8CED-FE3DD69A2195") {
                //    $('#inptDispute').next('.invalid-alert').remove();

                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptDispute').last().append(row);
                //    $('#fileSelectDispute').val('');
                //}
                //else if (doctype === "6EB5BD98-1305-42FD-BED8-919F03BFD78B") {
                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptJid').last().append(row);
                //    $('#fileSelectJid').val('');

                //    location.reload();
                //}
                //else if (docType2 == "settlementAgreement") {
                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-seg="SettlementDoc" data-type="' + $("#settlementDocType option:selected").val() + '" data-value="' + d.DocumentId + '" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptSettlement').append(row);
                //    $('#fileSelectSettlement').val('');

                //    location.reload();
                //    $("#settlementDocType option:selected").attr('disabled', 'disabled');
                //}
                //else if (docType2 == "financeDoc") {
                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptFinanceDoc').last().append(row);
                //    $('#fileSelectFinanceDoc').val('');

                //    location.reload();
                //}
                //else if (doctype === "272B6077-CBA6-4855-80EB-9D52E5B76D22") {
                //    var row = '<tr>' +
                //        '<td>' + d.Type + '</td>' +
                //        '<td><a href="/' + d.Path + '' + d.FileName + '" target="_blank">' + d.FileName + '</a></td>' +
                //        '<td class=alignRight>' + d.Size + '</td> ' +
                //        '<td>' + d.CreatedDateString + '</td>' +
                //        '<td data-value="' + d.DocumentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)" data-value="' + d.DocumentId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                //    '</tr>';

                //    $('#inptInvoiceDoc').last().append(row);
                //    $('#fileSelectInvoiceDoc').val('');

                //    location.reload();
                //}
            }
        };
    }
}

function removeAttachment(e, ee) {
    if (confirm("Please click OK to confirm delete.")) {
        var seg = $(e).data('seg');
        var type = $(e).data('type');


        $.ajax({
            url: window.location.origin + "/Lead/DeleteAttachment",
            data: { "documentId": $(e).data('value') },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {

                    if (seg !== "") {
                        if (seg === "AppointmentDoc") {
                            $('#appointmentDocType option[value=' + type + ']').prop('disabled', false);

                            if (type === "FD42D4E6-D6EA-4A79-B57A-528F93A5A3B8") {
                                $('#preEngagement').removeAttr('style');
                            }
                            else if (type === "FB13C881-4032-4B92-B25F-6CFF7BFB55E4") {
                                $('#letterOfEngagement').removeAttr('style');
                            }
                        }
                        if (seg === "PreAndSoaDoc") {
                            $('#preAndSoaDocType option[value=' + type + ']').prop('disabled', false);
                        }
                        if (seg === "SettlementDoc") {
                            $('#settlementDocType option[value=' + type + ']').prop('disabled', false);
                        }
                    }

                    showToast(d.Message, "success");
                    $(e).parent().parent().remove();
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function bindSaveButtonClick() {
    var page = $('#page').val();
    console.log($('#packageType').val())
        if (inputValidation()) {
           var assign = $('#assignToId').val();
            console.log(assign)

            var profile = {
                "ProfileId": $('#profileId').val(),
                "Name": $('#name').val(),
                "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
                "ProfileType": $('#profileType').val(),
                "ContactNo": $('#contactNo').val(),
                "Email": $('#email').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "DMS": $('#dms').val(),
                "Source": $('#source').val(),
                "NatureOfBusiness": $('#natureOfBusiness').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val()
            };

            var address = {
                "AddressId": $('#addressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var subscription = {
                "Approver1_Id": assign,
                "PackageId": $('#packageType').val()
            }

            var person = {
                "PersonId": $('#personId').val(),
                "Name": $('#picName').val(),
                "ContactNo": $('#picContact').val(),
                "Email": $('#picEmail').val()
            };

            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Subscriptions"
            };

            var request = {
                "RequestId": $('#requestId').val(),
                "Stage": "34"
            };

            var documents = [];
            $('#inptRequesterfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            });

            $.ajax({
                url: window.location.origin + "/Subscriptions/" + page ,
                data: { "profile": profile, "address": address, "person": person, "cr": cr, "request": request, "documents": documents, "subscription": subscription },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
}


function bindSaveSOP() {
    var page = $('#page').val();
    var bb = $('#bool').val();

    $('#btnSaveSop').click(function () {
        if (inputValidation()) {

            var assign = $('#assignToId').val();
            if (assign == " ") {
                assign = null;
            }

            var lawFirm = $('#lawFirmId').val();
            if (lawFirm == " ") {
                lawFirm = null;
            }

            var negotiator = $('#negotiatorId').val();
            if (negotiator == " ") {
                negotiator = null;
            }

            var profile = {
                "ProfileId": $('#ProfileId').val(),
                "Name": $('#name').val(),
                "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
                "ProfileType": "1",
                "ContactNo": $('#contactNo').val(),
                "Email": $('#email').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "DMS": $('#dms').val(),
                "Source": $('#source').val(),
                "NatureOfBusiness": $('#natureOfBusiness').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val()
            };

            var address = {
                "AddressId": $('#AddressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var person = {
                "PersonId": $('#PersonId').val(),
                "Name": $('#picName').val(),
                "ContactNo": $('#picContact').val(),
                "Email": $('#picEmail').val()
            };

            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            var lead = {
                "LeadId": $('#LeadId').val(),
                "AssignTo": assign
            };

            var request = {
                "AppointmentId": $('#AppointmentId').val(),
                "RequestId": $('#requestId').val(),
                "LawFirm": lawFirm,
                "NegotiatorId": negotiator
            };

            var afi = {
                "AssetForInvestigationId": $('#AssetForInvestigationId').val(),
                "PropertySearchResult1": $('#PropertySearchResult1').val(),
                "PropertySearchResult2": $('#PropertySearchResult2').val(),
                "VehicleDetails1": $('#VehicleDetails1').val(),
                "VehicleDetails2": $('#VehicleDetails2').val(),
                "SiteInvestigation": $('#SiteInvestigation').val()
            };

            var cfi = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    cfi.push({
                        "FinancialYearEnd1": $(this).data('fye'), "AnnualTurnover1": $(this).data('at'), "NtaPositiveNegative1": $(this).data('npn'),
                        "ProfitAfterTax1": $(this).data('pat'), "DocumentId": $(this).data('value'), "CompanyFinancialInformationId": $(this).data('cfid')
                    });
                });
            });

            var lr = [];
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    lr.push({
                        "ClaimAmount": $(this).data('ca'), "StatusOfCaseId": $(this).data('soc'), "HearingDate": $(this).data('hd'),
                        "Plaintiff": $(this).data('p'), "Defendant": $(this).data('dd'), "DocumentId": $(this).data('value'), "LitigationRecordId": $(this).data("lrid")
                    })
                });
            });

            var br = [];
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    br.push({
                        "Creditor1": $(this).data('c'), "StatementDate": $(this).data('sd'), "ClaimAmount": $(this).data('ca'), "DocumentId": $(this).data('value'),
                        "BlackListingRecordId": $(this).data('brid')
                    })
                });
            })

            var documents = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    });
                });
            });
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            });
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })
            $('#inptRequesterfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })

            var crRmo = {
                "Comment": $('#commentRmo').val(),
                "Type": "RmoLead"
            };

            $.ajax({
                url: window.location.origin + "/Sop/UpdateRequestSop",
                data: { "profile": profile, "address": address, "person": person, "cr": cr, "request": request, "lead": lead, "afi": afi, "cfi": cfi, "lr": lr, "br": br, "boo": bb, "crRmo": crRmo, "documents": documents, "sopRequestId": $('#sopRequestId').val(), "SopId": $('#sopId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function requestMore() {
    $('#btnRequestMore').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/RequestMore",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function rejected() {
    $('#btnRejected').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Rejected",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function release() {
    $('#btnRelease').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Release",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function dispute() {
    $('#btnDispute').click(function () {
        if (inputValidation($('#stage').val())) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Dispute",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function complete() {
    $('#btnComplete').click(function () {
        if (confirm("Please click OK to confirm settlement.")) {
            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/Complete",
                data: { "cr": cr, "RequestId": $('#requestId').val(), "ProfileId": $('#ProfileId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function stage9() {
    var page = $('#page').val();
    var bb = $('#bool').val();

    $('#btnSave10').click(function () {
        if (inputValidation($('#stage').val())) {

            var assign = $('#assignToId').val();
            if (assign == " ") {
                assign = null;
            }
            var lawFirm = $('#lawFirmId').val();
            if (lawFirm == " ") {
                lawFirm = null;
            }

            var negotiator = $('#negotiatorId').val();
            if (negotiator == " ") {
                negotiator = null;
            }

            var profile = {
                "ProfileId": $('#ProfileId').val(),
                "Name": $('#name').val(),
                "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
                "ProfileType": "1",
                "ContactNo": $('#contactNo').val(),
                "Email": $('#email').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "DMS": "2",
                "Source": $('#source').val(),
                "NatureOfBusiness": $('#natureOfBusiness').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val()
            };

            var address = {
                "AddressId": $('#AddressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var person = {
                "PersonId": $('#PersonId').val(),
                "Name": $('#picName').val(),
                "ContactNo": $('#picContact').val(),
                "Email": $('#picEmail').val()
            };

            var cr = {
                "CommentRemarkId": $('#CommentRemarkId').val(),
                "Comment": $('#comment').val(),
                "Type": "Lead"
            };

            var lead = {
                "LeadId": $('#LeadId').val(),
                "AssignTo": assign
            };

            var request = {
                "AppointmentId": $('#AppointmentId').val(),
                "RequestId": $('#requestId').val(),
                "LawFirm": lawFirm,
                "NegotiatorId": negotiator
            };

            var afi = {
                "AssetForInvestigationId": $('#AssetForInvestigationId').val(),
                "PropertySearchResult1": $('#PropertySearchResult1').val(),
                "PropertySearchResult2": $('#PropertySearchResult2').val(),
                "VehicleDetails1": $('#VehicleDetails1').val(),
                "VehicleDetails2": $('#VehicleDetails2').val(),
                "SiteInvestigation": $('#SiteInvestigation').val()
            };

            var cfi = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    cfi.push({
                        "FinancialYearEnd1": $(this).data('fye'), "AnnualTurnover1": $(this).data('at'), "NtaPositiveNegative1": $(this).data('npn'),
                        "ProfitAfterTax1": $(this).data('pat'), "DocumentId": $(this).data('value'), "CompanyFinancialInformationId": $(this).data('cfid')
                    });
                });
            });

            var lr = [];
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    lr.push({
                        "ClaimAmount": $(this).data('ca'), "StatusOfCaseId": $(this).data('soc'), "HearingDate": $(this).data('hd'),
                        "Plaintiff": $(this).data('p'), "Defendant": $(this).data('dd'), "DocumentId": $(this).data('value'), "LitigationRecordId": $(this).data("lrid")
                    })
                });
            });

            var br = [];
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    br.push({
                        "Creditor1": $(this).data('c'), "StatementDate": $(this).data('sd'), "ClaimAmount": $(this).data('ca'), "DocumentId": $(this).data('value'),
                        "BlackListingRecordId": $(this).data('brid')
                    })
                });
            })

            var documents = [];
            $('#inptFinancialStatementfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    });
                });
            });
            $('#inptLitigationfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            });
            $('#inptBlacklistfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })
            $('#inptRequesterfile tbody tr td:last-child').each(function () {
                $(this).find('.delete').each(function () {
                    documents.push({
                        "DocumentId": $(this).data('value')
                    })
                });
            })

            var crRmo = {
                "Comment": $('#commentRmo').val(),
                "Type": "RmoLead"
            };

            $.ajax({
                url: window.location.origin + "/Lead/" + page + "",
                data: { "profile": profile, "address": address, "person": person, "cr": cr, "request": request, "lead": lead, "afi": afi, "cfi": cfi, "lr": lr, "br": br, "boo": bb, "crRmo": crRmo, "documents": documents, "stage": $('#stage').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function saveDraft(controller, action) {
    var bb = $('#bool').val();

    var assign = $('#assignToId').val();
    if (assign == " ") {
        assign = null;
    }

    var profile = {
        "ProfileId": $('#ProfileId').val(),
        "Name": $('#name').val(),
        "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
        "ProfileType": "1",
        "ContactNo": $('#contactNo').val(),
        "Email": $('#email').val(),
        "BusinessRegNo": $('#businessRegNo').val(),
        "DMS": $('#dms').val(),
        "Source": $('#source').val(),
        "NatureOfBusiness": $('#natureOfBusiness').val(),
        "Website": $('#website').val(),
        "Remark": $('#remark').val()
    };

    var person = {
        "PersonId": $('#PersonId').val(),
        "Name": $('#picName').val(),
        "ContactNo": $('#picContact').val(),
        "Email": $('#picEmail').val()
    };

    var cr = {
        "CommentRemarkId": $('#CommentRemarkId').val(),
        "Comment": $('#comment').val(),
        "Type": "Subscriptions"
    };

    var request = {
        "RequestId": $('#requestId').val()
    };

    var documents = [];
    $('#inptRequesterfile tbody tr td:last-child').each(function () {
        $(this).find('.delete').each(function () {
            documents.push({
                "DocumentId": $(this).data('value')
            })
        });
    })

    $.ajax({
        url: window.location.origin + "/" + controller + "/" + action + "",
        data: { "profile": profile, "person": person, "cr": cr, "request": request, "boo": bb, "documents": documents, "stage": $('#stage').val() },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

//function inputValidation(stage) {
//    var isValid = true;

//    if (stage === "S-3") {
//        if ($('#inptAppointmentFile tbody tr').length === 0) {
//            if ($('#inptAppointmentFile').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please upload Pre Engagament Letter.</label>').insertAfter('#inptAppointmentFile');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#inptAppointmentFile').next('.invalid-alert').length > 0) {
//                $('#inptAppointmentFile').next('.invalid-alert').remove();
//            }
//        }
//    }

//    if (stage === "S-4") {
//        if ($('#inptPreAndSoa tbody tr').length < 2) {
//            if ($('#inptPreAndSoa').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please upload mandatory documents.</label>').insertAfter('#inptPreAndSoa');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#inptPreAndSoa').next('.invalid-alert').length > 0) {
//                $('#inptPreAndSoa').next('.invalid-alert').remove();
//            }
//        }
//    }

//    if (stage === "S-5") {
//        if ($('#inptCtos tbody tr').length === 0) {
//            if ($('#inptCtos').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please upload CTOS.</label>').insertAfter('#inptCtos');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#inptCtos').next('.invalid-alert').length > 0) {
//                $('#inptCtos').next('.invalid-alert').remove();
//            }
//        }
//    }

//    if (stage === "S-12") {
//        if ($('#inptAppointmentFile tbody tr').length === 0 || $('#inptAppointmentFile tbody tr').length === 1) {
//            if ($('#inptAppointmentFile').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please upload Letter of Engagement.</label>').insertAfter('#inptAppointmentFile');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#inptAppointmentFile').next('.invalid-alert').length > 0) {
//                $('#inptAppointmentFile').next('.invalid-alert').remove();
//            }
//        }
//    }

//    if (stage === "S-13") {
//        if ($('#inptPreAndSoa tbody tr').length < 3 || $('#inptPreAndSoa tbody tr').length === 1) {
//            if ($('#inptPreAndSoa').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please upload Letter of Engagement (Signed).</label>').insertAfter('#inptPreAndSoa');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#inptPreAndSoa').next('.invalid-alert').length > 0) {
//                $('#inptPreAndSoa').next('.invalid-alert').remove();
//            }
//        }
//    }

//    if (stage === "S-14") {
//        if ($('#lawFirmId').val() === null || $('#lawFirmId').val() === "") {
//            $('#LawFirmAssign').css('margin-bottom', 0);
//            if ($('#LawFirmAssign').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please select Law Firm to proceed.</label>').insertAfter('#LawFirmAssign');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#LawFirmAssign').next('.invalid-alert').length > 0) {
//                $('#LawFirmAssign').next('.invalid-alert').remove();
//            }
//        }
//    }

//    if (stage === "S-20") {
//        if ($('#negotiatorId').val() === null || $('#negotiatorId').val() === "") {
//            $('#negotiatorAssign').css('margin-bottom', 0);
//            if ($('#negotiatorAssign').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please select Negotiator to proceed.</label>').insertAfter('#negotiatorAssign');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#negotiatorAssign').next('.invalid-alert').length > 0) {
//                $('#negotiatorAssign').next('.invalid-alert').remove();
//            }
//        }
//    }

//    if (stage === "6") {
//        if ($('#commentRmo').val() === '') {
//            if ($('#commentRmo').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please enter RMO Comment/Remark to proceed.</label>').insertAfter('#commentRmo');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#commentRmo').next('.invalid-alert').length > 0) {
//                $('#commentRmo').next('.invalid-alert').remove();
//            }
//        }
//        if ($('#debtorTbody tr').length < 1) {
//            checkDebtorField();
//        }
//    }
//    else if (stage === "2" || stage === "7" || stage === "8" || stage === "10" || stage === "16" || stage === "17" || stage === "22" || stage === "23" || stage === "24" || stage === "26" || stage === "28" || stage === "29") {
//        if ($('#comment').val() === '') {
//            if ($('#comment').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please enter Comment/Remark to proceed.</label>').insertAfter('#comment');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#comment').next('.invalid-alert').length > 0) {
//                $('#comment').next('.invalid-alert').remove();
//            }
//        }
//    }
//    else if (stage === "15") {
//        $('#disputetable').removeAttr('hidden');
//        if ($('#comment').val() === '') {
//            if ($('#comment').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please enter Comment/Remark to proceed.</label>').insertAfter('#comment');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#comment').next('.invalid-alert').length > 0) {
//                $('#comment').next('.invalid-alert').remove();
//            }
//        }
//        if ($('#inptDispute tbody').children().length === 0) {
//            if ($('#inptDispute').next('.invalid-alert').length < 1) {
//                $('<label class="invalid-alert">Please attach Dispute Attachment to proceed.</label>').insertAfter('#inptDispute');
//            }
//            isValid = false;
//        }
//        else {
//            if ($('#inptDispute').next('.invalid-alert').length > 0) {
//                $('#inptDispute').next('.invalid-alert').remove();
//            }
//        }
//    }


//    if ($('#name').val() === '') {
//        if ($('#name').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Profile Name to proceed.</label>').insertAfter('#name');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#name').next('.invalid-alert').length > 0) {
//            $('#name').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#businessRegNo').val() === '') {
//        if ($('#businessRegNo').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Business Reg No to proceed.</label>').insertAfter('#businessRegNo');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#businessRegNo').next('.invalid-alert').length > 0) {
//            $('#businessRegNo').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#contactNo').val() === '') {
//        if ($('#contactNo').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Contact No to proceed.</label>').insertAfter('#contactNo');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#contactNo').next('.invalid-alert').length > 0) {
//            $('#contactNo').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#email').val() === '') {
//        if ($('#email').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Email to proceed.</label>').insertAfter('#email');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#email').next('.invalid-alert').length > 0) {
//            $('#email').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#line1').val() === '') {
//        if ($('#line1').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Line 1 to proceed.</label>').insertAfter('#line1');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#line1').next('.invalid-alert').length > 0) {
//            $('#line1').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#line2').val() === '') {
//        if ($('#line2').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Line 2 to proceed.</label>').insertAfter('#line2');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#line2').next('.invalid-alert').length > 0) {
//            $('#line2').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#postalCode').val() === '') {
//        if ($('#postalCode').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert" style="display:block;">Please enter Postal Code to proceed.</label>').insertAfter('#postalCode');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#postalCode').next('.invalid-alert').length > 0) {
//            $('#postalCode').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#picName').val() === '') {
//        if ($('#picName').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Name to proceed.</label>').insertAfter('#picName');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#picName').next('.invalid-alert').length > 0) {
//            $('#picName').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#picContact').val() === '') {
//        if ($('#picContact').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Contact No to proceed.</label>').insertAfter('#picContact');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#picContact').next('.invalid-alert').length > 0) {
//            $('#picContact').next('.invalid-alert').remove();
//        }
//    }

//    if ($('#picEmail').val() === '') {
//        if ($('#picEmail').next('.invalid-alert').length < 1) {
//            $('<label class="invalid-alert">Please enter Email to proceed.</label>').insertAfter('#picEmail');
//        }
//        isValid = false;
//    }
//    else {
//        if ($('#picEmail').next('.invalid-alert').length > 0) {
//            $('#picEmail').next('.invalid-alert').remove();
//        }
//    }
//    //if ($('[name=wagetype]:checked').data('keyword') === 'MONTHLY') {
//    //    if ($('[name=monthlywagetype]:checked').length < 1) {
//    //        if ($('#monthlywagetypealert').hasClass('hidden')) {
//    //            $('#monthlywagetypealert').toggleClass('hidden');
//    //        }
//    //        isValid = false;
//    //    }
//    //    else {
//    //        if (!$('#monthlywagetypealert').hasClass('hidden')) {
//    //            $('#monthlywagetypealert').toggleClass('hidden');
//    //        }
//    //    }
//    //}

//    //if ($('[name=wagetype]:checked').length < 1) {
//    //    if ($('.wagetypeselection').find('.invalid-alert').length < 1) {
//    //        $('.wagetypeselection').append('<label class="invalid-alert">Please select wage type to proceed.</label>');
//    //    }
//    //    isValid = false;
//    //}
//    //else {
//    //    if ($('.wagetypeselection').find('.invalid-alert').length > 0) {
//    //        $('.wagetypeselection').find('.invalid-alert').remove();
//    //    }
//    //}



//    //if ($('#inptAddress').val() === '') {
//    //    if ($('#inptAddress').next('.invalid-alert').length < 1) {
//    //        $('<label class="invalid-alert">Please enter address to proceed.</label>').insertAfter('#inptAddress');
//    //    }
//    //    isValid = false;
//    //}
//    //else {
//    //    if ($('#inptAddress').next('.invalid-alert').length > 0) {
//    //        $('#inptAddress').next('.invalid-alert').remove();
//    //    }
//    //}

//    //if ($('[name=incentive]:checked').val() === 'Y') {
//    //    saveIncentive();
//    //    if (employeeIncentives.length < 1) {
//    //        if ($('#btnIncentive').next('.invalid-alert').length < 1) {
//    //            $('<label class="invalid-alert">Please update employee incentive.</label>').insertAfter('#btnIncentive');
//    //        }
//    //        isValid = false;
//    //    }
//    //}
//    //else {
//    //    if ($('#btnIncentive').next('.invalid-alert').length > 0) {
//    //        $('#btnIncentive').next('.invalid-alert').remove();
//    //    }
//    //}
//    return isValid;
//}
function inputValidation() {
    var isValid = true;
    if ($('#name').val() === '') {
        if ($('#name').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Profile Name to proceed.</label>').insertAfter('#name');
        }
        isValid = false;
    }
    else {
        if ($('#name').next('.invalid-alert').length > 0) {
            $('#name').next('.invalid-alert').remove();
        }
    }

    if ($('#businessRegNo').val() === '') {
        if ($('#businessRegNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Business Reg No to proceed.</label>').insertAfter('#businessRegNo');
        }
        isValid = false;
    }
    else {
        if ($('#businessRegNo').next('.invalid-alert').length > 0) {
            $('#businessRegNo').next('.invalid-alert').remove();
        }
    }

    if ($('#contactNo').val() === '') {
        if ($('#contactNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Contact Number to proceed.</label>').insertAfter('#contactNo');
        }
        isValid = false;
    }
    else {
        if ($('#contactNo').next('.invalid-alert').length > 0) {
            $('#contactNo').next('.invalid-alert').remove();
        }
    }

    if ($('#email').val() === '') {
        if ($('#email').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Email to proceed.</label>').insertAfter('#email');
        }
        isValid = false;
    }
    else {
        if ($('#email').next('.invalid-alert').length > 0) {
            $('#email').next('.invalid-alert').remove();
        }
    }

    if ($('#line1').val() === '') {
        if ($('#line1').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Line 1 to proceed.</label>').insertAfter('#line1');
        }
        isValid = false;
    }
    else {
        if ($('#line1').next('.invalid-alert').length > 0) {
            $('#line1').next('.invalid-alert').remove();
        }
    }

    if ($('#line2').val() === '') {
        if ($('#line2').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Line 2 to proceed.</label>').insertAfter('#line2');
        }
        isValid = false;
    }
    else {
        if ($('#line2').next('.invalid-alert').length > 0) {
            $('#line2').next('.invalid-alert').remove();
        }
    }

    if ($('#postalCode').val() === '') {
        if ($('#postalCode').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert" style="display:block;">Please enter Postal Code to proceed.</label>').insertAfter('#postalCode');
        }
        isValid = false;
    }
    else {
        if ($('#postalCode').next('.invalid-alert').length > 0) {
            $('#postalCode').next('.invalid-alert').remove();
        }
    }

    if ($('#picName').val() === '') {
        if ($('#picName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Name to proceed.</label>').insertAfter('#picName');
        }
        isValid = false;
    }
    else {
        if ($('#picName').next('.invalid-alert').length > 0) {
            $('#picName').next('.invalid-alert').remove();
        }
    }

    if ($('#picContact').val() === '') {
        if ($('#picContact').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Contact No to proceed.</label>').insertAfter('#picContact');
        }
        isValid = false;
    }
    else {
        if ($('#picContact').next('.invalid-alert').length > 0) {
            $('#picContact').next('.invalid-alert').remove();
        }
    }

    if ($('#picEmail').val() === '') {
        if ($('#picEmail').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Email to proceed.</label>').insertAfter('#picEmail');
        }
        isValid = false;
    }
    else {
        if ($('#picEmail').next('.invalid-alert').length > 0) {
            $('#picEmail').next('.invalid-alert').remove();
        }
    }
    if ($('#assignToId').val() === '') {
        $('#Assign').css('margin-bottom', 0);
        if ($('#Assign').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please select approval committee to proceed.</label>').insertAfter('#Assign');
        }
        isValid = false;
    }
    else {
        if ($('#Assign').next('.invalid-alert').length > 0) {
            $('#Assign').next('.invalid-alert').remove();
        }
    }
    return isValid;
}

