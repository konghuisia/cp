﻿
var btn = null;

$(window).on('load', function () {
    btn = $('#btnSubscription').detach();
    $('#dms').val("false")
});

function init() {
    bindSaveProfileButtonClick();
    initInputRestriction();
}
function initInputRestriction() {
    //btn = $('#btnSubscription').detach();

    $('.name').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });

    $('.Pname').keyup(onlyAllowAlphanumeric);
    $('.Pname').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });
}

function dmsSelectOnChanged() {
    var x = document.getElementById("dms").value;
    if (x == "true") {
        btn.appendTo($('#firstDiv'))
        btn = null;
    } else {
        btn = $('#btnSubscription').detach();
    }
}

$('#choice').on('change', function () {
    if (this.value == "Company") {
        $('#companyBox').show();
        $('#c1').prop('checked', true);
        $('#businessBox').hide();
    }
    else {
        $('#companyBox').hide();
        $('#b1').prop('checked', true);
        $('#businessBox').show();
    }
});

function bindSaveProfileButtonClick() {
    var page = $('#page').val();

    $('#btnSaveCompany').click(function () {
        if (inputValidation()) {
            var profile = {
                "ProfileId": $('#profileId').val(),
                "Name": $('#name').val(),
                "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
                "ProfileType": $('#profileType').val(),
                "ContactNo": $('#contactNo').val(),
                "Email": $('#email').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "DMS": $('#dms').val(),
                "Source": $('#source').val(),
                "NatureOfBusiness": $('#natureOfBusiness').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val()
            };

            var address = {
                "AddressId": $('#addressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var person = {
                "PersonId": $('#personId').val(),
                "Name": $('#picName').val(),
                "ContactNo": $('#picContact').val(),
                "Email": $('#picEmail').val()
            };

            var cr = {
                "Comment": $('#comment').val(),
                "Type": "Profile"
            };

            $.ajax({
                url: window.location.origin + "/Profile/" + page + "",
                data: { "profile": profile, "address": address, "person": person, "cr": cr },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function btnSubscriptionOnClick() {
    var profile = {
                "ProfileId": $('#profileId').val(),
                "Name": $('#name').val(),
                "BusinessType": $("input:radio[name=businessTypeRB]:checked").val(),
                "ProfileType": $('#profileType').val(),
                "ContactNo": $('#contactNo').val(),
                "Email": $('#email').val(),
                "BusinessRegNo": $('#businessRegNo').val(),
                "DMS": $('#dms').val(),
                "Source": $('#source').val(),
                "NatureOfBusiness": $('#natureOfBusiness').val(),
                "Website": $('#website').val(),
                "Remark": $('#remark').val()
            };

            var address = {
                "AddressId": $('#addressId').val(),
                "Line1": $('#line1').val(),
                "Line2": $('#line2').val(),
                "City": $('#city').val(),
                "State": $('#state').val(),
                "PostalCode": $('#postalCode').val(),
                "Country": "Malaysia"
            };

            var person = {
                "PersonId": $('#personId').val(),
                "Name": $('#picName').val(),
                "ContactNo": $('#picContact').val(),
                "Email": $('#picEmail').val()
            };

            var cr = {
                "Comment": $('#comment').val(),
                "Type": "Profile"
            };

            $.ajax({
                url: window.location.origin + "/Profile/" + "NewSubscription" + "",
                data: { "profile": profile, "address": address, "person": person, "cr": cr },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        //showToast(d.Message, "success");
                        //console.log(d.RedirectUrl)
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
}

function inputValidation() {
    var isValid = true;
    if ($('#name').val() === '') {
        if ($('#name').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Profile Name to proceed.</label>').insertAfter('#name');
        }
        isValid = false;
    }
    else {
        if ($('#name').next('.invalid-alert').length > 0) {
            $('#name').next('.invalid-alert').remove();
        }
    }

    if ($('#businessRegNo').val() === '') {
        if ($('#businessRegNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Business Reg No to proceed.</label>').insertAfter('#businessRegNo');
        }
        isValid = false;
    }
    else {
        if ($('#businessRegNo').next('.invalid-alert').length > 0) {
            $('#businessRegNo').next('.invalid-alert').remove();
        }
    }

    if ($('#contactNo').val() === '') {
        if ($('#contactNo').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Contact Number to proceed.</label>').insertAfter('#contactNo');
        }
        isValid = false;
    }
    else {
        if ($('#contactNo').next('.invalid-alert').length > 0) {
            $('#contactNo').next('.invalid-alert').remove();
        }
    }

    if ($('#email').val() === '') {
        if ($('#email').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Email to proceed.</label>').insertAfter('#email');
        }
        isValid = false;
    }
    else {
        if ($('#email').next('.invalid-alert').length > 0) {
            $('#email').next('.invalid-alert').remove();
        }
    }

    if ($('#line1').val() === '') {
        if ($('#line1').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Line 1 to proceed.</label>').insertAfter('#line1');
        }
        isValid = false;
    }
    else {
        if ($('#line1').next('.invalid-alert').length > 0) {
            $('#line1').next('.invalid-alert').remove();
        }
    }

    if ($('#line2').val() === '') {
        if ($('#line2').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Line 2 to proceed.</label>').insertAfter('#line2');
        }
        isValid = false;
    }
    else {
        if ($('#line2').next('.invalid-alert').length > 0) {
            $('#line2').next('.invalid-alert').remove();
        }
    }

    if ($('#postalCode').val() === '') {
        if ($('#postalCode').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert" style="display:block;">Please enter Postal Code to proceed.</label>').insertAfter('#postalCode');
        }
        isValid = false;
    }
    else {
        if ($('#postalCode').next('.invalid-alert').length > 0) {
            $('#postalCode').next('.invalid-alert').remove();
        }
    }

    if ($('#picName').val() === '') {
        if ($('#picName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Name to proceed.</label>').insertAfter('#picName');
        }
        isValid = false;
    }
    else {
        if ($('#picName').next('.invalid-alert').length > 0) {
            $('#picName').next('.invalid-alert').remove();
        }
    }

    if ($('#picContact').val() === '') {
        if ($('#picContact').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Contact No to proceed.</label>').insertAfter('#picContact');
        }
        isValid = false;
    }
    else {
        if ($('#picContact').next('.invalid-alert').length > 0) {
            $('#picContact').next('.invalid-alert').remove();
        }
    }

    if ($('#picEmail').val() === '') {
        if ($('#picEmail').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Email to proceed.</label>').insertAfter('#picEmail');
        }
        isValid = false;
    }
    else {
        if ($('#picEmail').next('.invalid-alert').length > 0) {
            $('#picEmail').next('.invalid-alert').remove();
        }
    }
    return isValid;
}