﻿var div = "";

function init() {
    initInputRestriction();
    bindSavePackageButtonClick();
    bindSavePackageServiceButtonClick();
}

function initInputRestriction() {
    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });

    $('.datepicker').keypress(function () {
        return false;
    });

    $("#tblSub").find("input").attr("disabled", "disabled");

    div = $('#divSecondLevel').detach();
}

function initSubServices() {
    if (document.getElementById('subYes').checked) {
        $("#tblSub").find("input").attr("disabled", false);
        $("#tblSub").removeClass("grayout");
    } else if (document.getElementById('subNo').checked) {
        $("#tblSub").find("input").attr("disabled", "disabled");
        $("#tblSub").addClass("grayout");
    }
}

function initSecondLevelCondition() {
    if (document.getElementById('YesSecond').checked) {
        div.appendTo($('#divFirstLevel'))
        div = null;
    } else if (document.getElementById('NoSecond').checked) {
        div = $('#divSecondLevel').detach();
    }
}

function inputValidation() {
    var isValid = true;

    if ($('#txtPackageName').val() === '') {
        if ($('#txtPackageName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter package name to proceed.</label>').insertAfter('#txtPackageName');
        }
        isValid = false;
    } else {
        if ($('#txtPackageName').next('.invalid-alert').length > 0) {
            $('#txtPackageName').next('.invalid-alert').remove();
        }
    }

    if ($('#txtPrice').val() === '') {
        if ($('#txtPrice').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter a valid fee amount to proceed.</label>').insertAfter('#txtFees');
        }
        isValid = false;
    }
    else {
        if ($('#inptName').next('.invalid-alert').length > 0) {
            $('#inptName').next('.invalid-alert').remove();
        }
    }

    if ($('#txtSC').val() === '') {
        if ($('#txtSC').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter the service credit which is included in the package to proceed.</label>').insertAfter('#txtSC');
        }
        isValid = false;
    }
    else {
        if ($('#txtSC').next('.invalid-alert').length > 0) {
            $('#txtSC').next('.invalid-alert').remove();
        }

    }

    if ($('#txtValidityPeriod').val() === '') {
        if ($('#txtValidityPeriod').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please select a valid period to proceed.</label>').insertAfter('#txtValidityPeriod');
        }
        isValid = false;
    }
    else {
        if ($('#txtValidityPeriod').next('.invalid-alert').length > 0) {
            $('#txtValidityPeriod').next('.invalid-alert').remove();
        }
    }

    if ($('#txtValidityPeriod').val() === '0') {
        if ($('#txtValidityPeriod').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please select a valid period to proceed.</label>').insertAfter('#txtValidityPeriod');
        }
        isValid = false;
    }
    else {
        if ($('#txtValidityPeriod').next('.invalid-alert').length > 0) {
            $('#txtValidityPeriod').next('.invalid-alert').remove();
        }
    }

    return isValid;
}

function clearInput() {
    $('#txtPackageName').val('');
    $('#txtDesc').val('');
    $('#txtSC').val('');
    $('#txtPrice').val('');
    $('#txtValidityPeriod').val('');
    $($('[name=multipleP]')[1]).prop('checked', true);
    $($('[name=status]')[0]).prop('checked', true);
}



function bindSavePackageButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {


            var package = {
                "PackageId": $('#txtPackageId').val(),
                "PackageName": $('#txtPackageName').val(),
                "PackageDesc": $('#txtDesc').val(),
                "CreditGiven": $('#txtSC').val(),
                "SubscriptionFee_RM": $('#txtPrice').val(),
                "multiplePurchase": $('[name=multipleP]:checked').val(),
                "status": $('[name=status]:checked').val(),
                "Validity_Months": $('#txtValidityPeriod').val(),
                "SubServices": $('[name=sub]:checked').val()
            };

            console.log(package)

            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/Confirguration/" + url + "Package",
                data: { "package": package },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

var service = {
    "serviceId": getUrlParameter("serviceId")
};

$(window).on('load', function () {
    $.ajax({
        type: "POST",
        url: "/Confirguration/updateConditionSelect",
        data: JSON.stringify(service),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#condition').val(data.PackageServiceConditions[0].CondTypeId);

            if (data.PackageService.AmtOfConditionLevel == 2) {
                div.appendTo($('#divFirstLevel'))
                div = null;
                $('#condition2').val(data.PackageServiceConditions[1].CondTypeId);
            }
            //console.log(data.PackageServiceConditions[0].CondTypeId);
        }
    });
});

function bindSavePackageServiceButtonClick() {
    $('#btnSavePS').click(function () {
        if (inputValidation()) {


            var service = {
                "ServiceId": $('#txtServiceId').val(),
                "ServiceName": $('#txtServiceName').val(),
                "ServiceDesc": $('#txtServiceDesc').val(),
                "Status": $('[name=status]:checked').val(),
                "AmtOfConditionLevel": $('[name=cond]:checked').val()
            };

            var condition = {}

            if (div == null || div == "") {
                condition = {
                    "Price_RM2": $('#txtRM2').val(),
                    "Price_SC2": $('#txtSC2').val(),
                    "CondTypeId2": $('#condition2').val(),
                    "Price_RM": $('#txtRM').val(),
                    "Price_SC": $('#txtSC').val(),
                    "CondTypeId": $('#condition').val()
                }
            } else {
                condition = {
                    "Price_RM": $('#txtRM').val(),
                    "Price_SC": $('#txtSC').val(),
                    "CondTypeId": $('#condition').val()
                }
            }

            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/Confirguration/" + url + "PackageService",
                data: { "service": service, "condition": condition },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}
