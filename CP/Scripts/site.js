﻿"use strict";
var sop;
var doc;
var comment;
var reviewers;
function formatDate(d) {
    var date = new Date(d);

    var hour = date.getHours();     // yields hours 
    var minute = date.getMinutes(); // yields minutes
    var second = date.getSeconds(); // yields seconds

    // After this construct a string with the above results as below
    return date.toShortFormat();
}

function formatDateTime(d) {
    var date = new Date(d);

    var hour = date.getHours().padLeft();     // yields hours 
    var minute = date.getMinutes().padLeft(); // yields minutes
    var second = date.getSeconds().padLeft(); // yields seconds

    // After this construct a string with the above results as below
    return date.toShortFormat() + ' ' + hour + ':' + minute + ':' + second;
}


Number.prototype.padLeft = function (base, chr) {
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + this : this;
};
Date.prototype.toShortFormat = function () {

    var month_names = ["Jan", "Feb", "Mar",
        "Apr", "May", "Jun",
        "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec"];

    var day = this.getDate();
    var month_index = this.getMonth();
    var year = this.getFullYear();

    return "" + day + " " + month_names[month_index] + " " + year;
};
const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

function restrictIntegerInput(keydownEvent) {
    var e = keydownEvent;

    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function restrictDecimalInput(keydownEvent) {
    var e = keydownEvent;
    if ($(e.target).val().indexOf('.') > 0) {
        if (e.key === '.') {
            e.preventDefault();
        }
    }
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function restrictAlphanumericSymbolInput(keydownEvent) {
    var e = keydownEvent;
    if (e.shiftKey && e.keyCode === 190) {
        e.preventDefault();
    }

    // Allow: backspace, delete, tab, escape, enter and.
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 32, 189, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }

    if ((e.shiftKey && (e.keyCode <= 57 && e.keyCode >= 48)) || (e.keyCode < 48 || e.keyCode > 90) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function restrictAlphanumericInput(keydownEvent) {
    var e = keydownEvent;
    // Allow: backspace, delete, tab, escape, enter and.
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 32]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }

    if ((e.shiftKey && (e.keyCode <= 57 && e.keyCode >= 48)) || (e.keyCode < 48 || e.keyCode > 90) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}
function restrictAlphanumericSlashInput(keydownEvent) {
    var e = keydownEvent;

    if ((e.shiftKey && e.keyCode === 191) || (e.shiftKey && e.keyCode === 190)) {
        e.preventDefault();
    }

    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 32, 189, 190, 191]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }

    if ((e.shiftKey && (e.keyCode <= 57 && e.keyCode >= 48)) || (e.keyCode < 48 || e.keyCode > 90) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}
function restrictAlphanumericDashInput(keydownEvent) {
    var e = keydownEvent;
    if (e.shiftKey && e.keyCode === 189) {
        e.preventDefault();
    }
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 32, 189, 109]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }

    if ((e.shiftKey && (e.keyCode <= 57 && e.keyCode >= 48)) || (e.keyCode < 48 || e.keyCode > 90) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function onlyAllowAlphanumeric() {
    this.value = this.value.replace(/[^a-zA-Z0-9 _]/g, '');
}

function restrictAddressAlphanumericInput(keydownEvent) {
    var e = keydownEvent;

    if ((e.shiftKey && e.keyCode === 55) || (e.shiftKey && e.keyCode === 191) || (e.shiftKey && e.keyCode === 189) || (e.shiftKey && e.keyCode === 188) || (e.shiftKey && e.keyCode === 190)) {
        e.preventDefault();
    }

    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 32, 191, 189, 188, 190, 109]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: @alias
        (e.keyCode === 50 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }


    if ((e.shiftKey && (e.keyCode <= 57 && e.keyCode >= 48)) || (e.keyCode < 48 || e.keyCode > 90) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function restrictEmailInput(keydownEvent) {
    var e = keydownEvent;
    if ($(e.target).val().indexOf('@') > -1) {
        if (e.keyCode === 50 && (e.shiftKey === true || e.metaKey === true)) {
            e.preventDefault();
        }
    }
    // Allow: backspace, delete, tab, escape, enter , dash and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 189, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: alias @
        (e.keyCode === 50 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: underscore _
        (e.keyCode === 189 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 90)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function restrictMultipleEmailInput(keydownEvent) {
    var e = keydownEvent;

    // Allow: backspace, delete, tab, escape, enter, space, comma, dash and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 32, 188, 189, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: alias @
        (e.keyCode === 50 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: comma _
        (e.keyCode === 188 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: underscore _
        (e.keyCode === 189 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 90)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}
function validateMultipleEmail(email) {
    var re = /^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$/;
    return re.test(email);
}
function restrictAlphabetInput(keydownEvent) {
    var e = keydownEvent;
    if ($(e.target).val().indexOf('.') > 0) {
        if (e.key === '.') {
            return false;
        }
    }
    if (e.shiftKey && e.keyCode === 190) {
        e.preventDefault();
    }

    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190, 32]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: alias @
        (e.keyCode === 50 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    if ((e.shiftKey && (e.keyCode === 190)) || (e.keyCode > 64 && e.keyCode < 91) || (e.keyCode > 96 && e.keyCode < 123) || e.keyCode === 8) {
        return;
    }
    else {
        e.preventDefault();
    }
}


function restrictCapitalLetter(keydownEvent) {
    var e = keydownEvent;
    if (e < 65 || e > 90) return;
}

function showToast(msg, type) {
    if (type === "success") {
        $.toast({
            heading: 'Success',
            text: msg,
            position: 'top-roght',
            stack: false
        });
    }
    else if (type === "error") {
        $.toast({
            heading: 'Error',
            text: msg,
            position: 'top-roght',
            icon: 'error'
        });
    }
}
function searchList(tableId, input) {
    $('#' + tableId + ' tbody tr').each(function () {
        var isHide = true;
        $(this).find('p').each(function () {
            if (input !== '' && input !== undefined) {
                if ($(this).text().toLowerCase().indexOf(input.toLowerCase()) > -1) {
                    isHide = false;
                }
            }
            else {
                isHide = false;
            }
        });

        if (isHide) {
            $(this).css('display', 'none');
        }
        else {
            $(this).css('display', 'table-row');
        }
    });
}

function sortTable(e) {
    var table, rows, switching, i, x, y, shouldSwitch, type, elemId, col;
    table = document.getElementById($(e).parent().parent().parent().parent().prop('id'));
    type = $(e).data('type');
    elemId = $(e).prop('id');
    col = $(e).parent().index();
    switching = true;
    debugger
    var start = 2;
    if ($(e).parent().parent().parent().parent().attr('id') != "tblLeadList") {
        start = 1;
    }
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = start; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            //rows[i].getElementsByTagName("td")[0].innerHTML = i;

            x = rows[i].getElementsByTagName("td")[col];
            y = rows[i + 1].getElementsByTagName("td")[col];
            //check if the two rows should switch place:
            if (type === 'asc') {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            else {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
    if (type === 'asc') {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="desc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-bottom"></span>');
    }
    else
    {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="asc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-top"></span>');
    }
}

//function containsObject(obj, list) {
//    var i;
//    for (i = 0; i < list.length; i++) {
//        if (list[i] === obj) {
//            return true;
//        }
//    }

//    return false;
//}
function containsObject(obj, list) {
    var x;
    for (x in list) {
        if (list.hasOwnProperty(x) && list[x] === obj) {
            return true;
        }
    }

    return false;
}
function trimSpacing(e) {
    $(e).val($(e).val().trim());
}

function convertToLower(e) {
    $(e).val($(e).val().toLowerCase());
}

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}
