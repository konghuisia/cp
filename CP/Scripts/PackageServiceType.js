﻿

function init() {
    initInputRestriction();
    bindSavePackageButtonClick();
    //bindSavePackageServiceButtonClick();
}

function initSecondLevelCondition() {
    if (document.getElementById('YesSecond').checked) {
        $('#divSecondLevel').show();
    } else if (document.getElementById('NoSecond').checked) {
        $('#divSecondLevel').hide();
        if ($('#divSecondLevel').next('.invalid-alert').length > 0) {
            $('#divSecondLevel').next('.invalid-alert').remove();
        }
    }
}

function initInputRestriction() {
    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });

    $('.datepicker').keypress(function () {
        return false;
    });

    $('.money').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.money').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#txtServiceName').val() === '') {
        if ($('#txtServiceName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter Service Name to proceed.</label>').insertAfter('#txtServiceName');
        }
        isValid = false;
    } else {
        if ($('#txtServiceName').next('.invalid-alert').length > 0) {
            $('#txtServiceName').next('.invalid-alert').remove();
        }
    }

    if ($('#txtSC').val() === '' || $('#txtSC').val() === "0.00" || $('#txtRM').val() === '' || $('#txtRM').val() === "0.00") {
        if ($('#divFirstLevel').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter a valid Price & Conditions to proceed.</label>').insertAfter('#divFirstLevel');
        }
        isValid = false;
    }
    else {
        if ($('#divFirstLevel').next('.invalid-alert').length > 0) {
            $('#divFirstLevel').next('.invalid-alert').remove();
        }
    }

    if (document.getElementById('YesSecond').checked) {
        if ($('#txtSC2').val() === '' || $('#txtSC2').val() === "0.00" || $('#txtRM2').val() === '' || $('#txtRM2').val() === "0.00") {
            if ($('#divSecondLevel').next('.invalid-alert').length < 1) {
                $('<label class="invalid-alert">Please enter a valid Price & Conditions to proceed.</label>').insertAfter('#divSecondLevel');
            }
            isValid = false;
        }
        else {
            if ($('#divSecondLevel').next('.invalid-alert').length > 0) {
                $('#divSecondLevel').next('.invalid-alert').remove();
            }
        }
    }
    else {
        if ($('#divSecondLevel').next('.invalid-alert').length > 0) {
            $('#divSecondLevel').next('.invalid-alert').remove();
        }
    }
    
    return isValid;
}

function clearInput() {
    $('#txtServiceName').val('');
    $('#txtServiceDesc').val('');
    $($('.thisCb')).prop('checked', true);
    $('#txtSC').val('');
    $('#txtRM').val('');
    $('#txtSC2').val('');
    $('#txtRM2').val('');
    $(".thisSelect").val($(".thisSelect option:first").val());
    $('#divSecondLevel').hide();
}



function bindSavePackageButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {
            var c1 = $('#txtSC').val();
            var p1 = $('#txtRM').val();
            var c2 = $('#txtSC2').val();
            var p2 = $('#txtRM2').val();



            var PackageServices = {
                "ServiceId": $('#txtServiceId').val(),
                "ServiceName": $('#txtServiceName').val(),
                "ServiceDesc": $('#txtServiceDesc').val(),
                "Condition2": $('[name=cond]:checked').val(),
                "Status": $('[name=status]:checked').val(),
                "Price1": parseFloat(p1.replace(/,/g, '')),
                "Point1": parseFloat(c1.replace(/,/g, '')),
                "Price2": parseFloat(p2.replace(/,/g, '')),
                "Point2": parseFloat(c2.replace(/,/g, '')),
                "ServiceConditionId1": $('#condition').val(),
                "ServiceConditionId2": $('#condition2').val()
            };

            
            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/Confirguration/" + url + "PackageServiceType",
                data: { "PackageServices": PackageServices },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}