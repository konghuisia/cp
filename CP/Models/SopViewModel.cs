﻿using System.Collections.Generic;
using ObjectClassesLibrary.Classes;

namespace CP.Models
{
    public class SopViewModel
    {
        public List<SopShowInSelect> SopShowInSelect { get; set; }
        public List<SopDocuments> SopDocuments { get; set; }
        public List<DocumentType> DocumentTypes { get; set; }
        public List<SopStage> SopStage { get; set; }
        public SopRequests SopRequest { get; set; }
    }
}