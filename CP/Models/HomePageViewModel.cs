﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObjectClassesLibrary.Classes;

namespace CP.Models
{
    public class HomePageViewModel
    {
        public List<UserAccess> CurrentUser { get; set; }
        public List<User> Users { get; set; }
        public List<Profile> Profiles { get; set; }
        public List<Request> Requests { get; set; }
        public List<Debtor> Debtors { get; set; }
        public List<Lead> Leads { get; set; }
        public List<Source> Sources { get; set; }
        public List<User> RMO { get; set; }
        public string Module { get; set; }
        public string Position { get; set; }
        public string UserId { get; set; }
        public string Page { get; set; }
    }
}