﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class ProfileIndexViewModel
    {
        public List<Profile> Profiles { get; set; }
        public List<Source> Sources { get; set; }
        public List<LawFirmProfile> LawFirmProfiles { get; set; }
        public List<NegotiatorProfile> NegotiatorProfiles { get; set; }
        public List<Debtor> Debtors { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
    }
}