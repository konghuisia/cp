﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class UpdateProfileViewModel
    {
        public UpdateProfileViewModel()
        {
            Address = new Address();
            Person = new Person();
        }
        public Profile Profile { get; set; }
        public Address Address { get; set; }
        public Person Person { get; set; }
        public List<CommentRemark> CommentRemark { get; set; }
        public List<User> Users { get; set; }
        public LawFirmProfile LawFirmProfile { get; set; }
        public NegotiatorProfile NegotiatorProfile { get; set; }
        public List<UserPosition> UserPositions { get; set; }
        public UserPosition Position { get; set; }
        public List<ProfileAuditTrail> ProfileAuditTrails { get; set; }
        public List<UserPosition> Positions { get; set; }
        public List<Source> Sources { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
        public string Module { get; set; }
        public string Page { get; internal set; }
        public List<State> State { get; internal set; }
        public List<City> City { get; internal set; }
    }
}