﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class PackageViewModel
    {
        public List<UserAccess> CurrentUser { get; set; }
        public List<Access> AccessTypes { get; set; }
        public List<PackageServices> PackageServices { get; set; }
        public User User { get; set; }
        public Packages Package { get; set; }
        public PackageServices PackageService { get; set; }
        public PackageServiceConditions PackageServiceConditions { get; set; }
    }
}