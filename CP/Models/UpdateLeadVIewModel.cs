﻿using ObjectClassesLibrary;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class UpdateLeadVIewModel
    {
        public UpdateLeadVIewModel()
        {
            Appointment = new Appointment();
            PageSegmentAccess = new PageSegmentAccess();
            AssetForInvestigation = new AssetForInvestigation();
        }
        public Profile Profile { get; set; }
        public Address Address { get; set; }
        public Person Person { get; set; }
        public List<CommentRemark> CommentRemark { get; set; }
        public Request Request { get; set; }
        public Lead Lead { get; set; }
        public List<User> Users { get; set; }
        public List<User> Users2 { get; set; }
        public List<Document> Documents { get; set; }
        public Appointment Appointment { get; set; }
        public List<Appointment> Appointments { get; set; }
        public List<Document> AppointmentDoc { get; set; }
        public List<DocumentType> DocumentTypes { get; set; }
        public bool DocRequired { get; set; }
        public List<Source> Sources { get; set; }
        public List<StatusOfCase> StatusOfCases { get; set; }
        public AssetForInvestigation AssetForInvestigation { get; set; }
        public List<BlackListingRecord> BlackListingRecord { get; set; }
        public List<CompanyFinancialInformation> CompanyFinancialInformation { get; set; }
        public List<LitigationRecord> LitigationRecord { get; set; }
        public bool PreAndSoaBool { get; set; }
        public List<Document> PreAndSoaDoc { get; set; }
        public List<AuditTrail> AuditTrails { get; set; }
        public List<UserPosition> UserPositions { get; set; }
        public List<Debtor> Debtors { get; set; }
        public List<Profile> Profiles { get; set; }
        public List<AppointmentType> AppointmentTypes { get; set; }
        public List<ShowInSelect> ShowInSelects { get; set; }
        public List<LawFirmProfile> LawFirmProfiles { get; set; }
        public List<User> Negotiator { get; set; }
        public bool CanSee { get; set; }
        public bool ProfileExists { get; set; }
        public List<Address> Addresses { get; set; }
        public List<Request> Requests { get; set; }
        public string UserId { get; set; }
        public string Position { get; set; }
        public List<State> States { get; set; }
        public List<City> Cities { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
        public string Page { get; set; }
        public List<Sop> Sop { get; set; }
        public List<Sop> SopList { get; set; }
        public List<SopRequests> SopRequests { get; set; }
        public PageSegmentAccess PageSegmentAccess { get; set; }
        public bool ReleaseBtn { get; set; }
        public List<RequestServices> RequestServicess { get; set; }
        public List<RequestServices> RequestServices { get; set; }
    }
}