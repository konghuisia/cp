﻿using ObjectClassesLibrary;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class ConfigViewModel
    {
        public string Module { get; set; }
        public List<DocumentType> DocumentType { get; set; }
        public List<ShowInSelect> ShowInSelects { get; set; }
        public List<Access> AccessTypes { get; set; }
        public List<User> Users { get; set; }
        public List<Packages> Packages { get; set; }
        public List<PackageServices> PackageServices { get; set; }
        public List<ConditionTypes> ConditionTypes { get; set; }
        public List<UserPosition> UserPositions { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
        public List<Request> Requests { get; internal set; }
        public List<Profile> Profiles { get; internal set; }
        public List<Lead> Leads { get; internal set; }
        public List<Source> Sources { get; internal set; }
        public List<Sop> Sops { get; internal set; }
        public List<Positions> Positions { get; internal set; }
        public PackageServices PackageService {get; set;}
        public List<PackageServiceConditions> PackageServiceConditions { get; set; }
    }
}