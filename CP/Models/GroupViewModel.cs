﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class GroupViewModel
    {
        public UserPosition Group { get; set; }
        public List<UserAccess> UserAccess { get; set; }
        public List<Access> AccessTypes { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
    }
}