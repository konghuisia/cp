﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class AppointmentViewModel
    {
        public AppointmentViewModel()
        {
            Requests = new List<Request>();
            Documents = new List<Document>();
            Appointments = new List<Appointment>();
            CommentRemarks = new List<CommentRemark>();
            Users = new List<User>();
            Leads = new List<Lead>();
            AppointmentType = new AppointmentType();

        }
        public List<Request> Requests { get; set; }
        public List<Document> Documents { get; set; }
        public List<Appointment> Appointments { get; set; }
        public List<CommentRemark> CommentRemarks { get; set; }
        public List<Lead> Leads { get; set; }
        public List<User> Users { get; set; }
        public Request Request { get; set; }
        public Document Document { get; set; }
        public Appointment Appointment { get; set; }
        public CommentRemark CommentRemark { get; set; }
        public string RequestId { get; set; }
        public UserPosition UserPosition { get; set; }
        public List<UserPosition> UserPositions { get; set; }
        public List<AppointmentType> AppointmentTypes { get; set; }
        public AppointmentType AppointmentType { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
        public bool CanSee { get; set; }
        public List<AuditTrail> AuditTrails { get; set; }
        public string Position { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Page { get; internal set; }
    }
}