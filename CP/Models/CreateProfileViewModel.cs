﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class CreateProfileViewModel
    {
        public UserPosition UserPosition { get; set; }
        public List<Source> Sources { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
        public string UserName { get; set; }
        public string Module { get; set; }
        public string Page { get; internal set; }
        public List<State> State { get; internal set; }
        public List<City> City { get; internal set; }
        public User User { get; internal set; }
        public int ProfileType { get; internal set; }
    }
}