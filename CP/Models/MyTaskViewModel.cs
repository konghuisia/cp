﻿
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CP.Models
{
    public class MyTaskViewModel
    {
        public List<Draft> Drafts { get; set; }
        public List<Request> Requests { get; set; }
        public List<User> Users { get; set; }
        public List<Lead> Leads { get; set; }
        public List<SubscriptionRequest> SubscriptionRequest { get; set; }
        public List<Profile> Profiles { get; set; }
        public string Page { get; set; }
        public List<AuditTrail> AuditTrails { get; internal set; }
        public string UserId { get; internal set; }
    }
}