﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class LeadIndexViewModel
    {
        public List<UserAccess> CurrentUser;

        public List<Request> Requests { get; set; }
        public List<Profile> Profiles { get; set; }
        public List<Lead> Leads { get; set; }
        public List<User> Users { get; set; }
        public List<Source> Sources { get; set; }
        public List<Debtor> Debtors { get; set; }
        public List<User> RMO { get; set; }
        public string Module { get; set; }
        public List<Status> Statuses { get; set; }
        public List<Draft> Drafts { get; internal set; }
        public string UserId { get; internal set; }
        public string Position { get; internal set; }
        public List<AuditTrail> AuditTrails { get; internal set; }
    }
}