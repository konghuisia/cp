﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class UserViewModel
    {
        public List<UserAccess> CurrentUser { get; set; }
        public List<UserPosition> Groups { get; set; }
        public List<Access> AccessTypes { get; set; }
        public List<LawFirmProfile> LawFirmProfiles { get; set; }
        public User User { get; set; }
        public List<Positions> Positions { get; internal set; }
    }
}