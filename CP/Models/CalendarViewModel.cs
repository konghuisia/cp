﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class CalendarViewModel
    {
        public List<Request> Requests { get; set; }
        public List<Lead> Leads { get; set; }
        public List<Appointment> Appointments { get; set; }
        public List<AppointmentType> AppointmentTypes { get; set; }
        public DateTime Date { get; set; }
    }
}