﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class HeaderIconViewModel
    {
        public string Name { get; set; }
        public string Position { get; set; }
    }
}