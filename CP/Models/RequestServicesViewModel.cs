﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObjectClassesLibrary.Classes;

namespace CP.Models
{
    public class RequestServicesViewModel
    {
        public List<UserAccess> CurrentUser { get; set; }
        public List<PackageServices> PackageServicesType { get; set; }
        public List<Sop> Sops { get; set; }
        public List<RequestServices> RequestServices { get; set; }
        public string RequestId { get; set; }
        public string ProfileId { get; set; }
        public Subscriptions Subscriber { get; set; }
        public bool IsSubscriber { get; set; }
        public List<SopRequests> SopRequests { get; set; }
    }
}