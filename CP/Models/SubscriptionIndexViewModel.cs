﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class SubscriptionIndexViewModal
    {
        public List<Subscriptions> Subscriptions { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
        public List<Profile> Profiles { get; set; }
        public Subscriptions Subscription { get; set; }
        public Profile Profile { get; set; }
        public List<RedemptionHistory> RedemptionHistory { get; set; }
        public List<SubscriptionAuditTrail> SubscriptionAuditTrails { get; set; }
        public string Module { get; set; }
        public string ProfileId { get; set; }
        public string Page { get; internal set; }
        public User User { get; set; }
        public bool ProfileExists { get; set; }
        public bool isSubscriber { get; set; }
        public Person Person { get; set; }
        public Request Request { get; set; }
        public SubscriptionRequest SubRequest { get; set; }
        public List<User> Users { get; set; }
        public List<User> Users2 { get; set; }
        public List<Source> Sources { get; set; }
        public List<StatusOfCase> StatusOfCases { get; set; }
        public List<State> States { get; set; }
        public List<City> Cities { get; set; }
        public List<UserPosition> UserPositions { get; set; }
        public string UserId { get; set; }
        public string Position { get; set; }
        public Address Address { get; set; }
        public List<CommentRemark> CommentRemark { get; set; }
        public List<Document> Documents { get; set; }
        public List<AuditTrail> AuditTrails { get; set; }
        public PageSegmentAccess PageSegmentAccess { get; set; }
        public List<SubscriptionRequest> SubscriptionRequest { get; set; }
        public List<Packages> Packages { get; set; }
        public List<SubscriptionStatus> SubStatus { get; set; }
    }
}