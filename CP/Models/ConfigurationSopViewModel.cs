﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class ConfigurationSopViewModel
    {
        public ConfigurationSopViewModel()
        {
            Sop = new Sop();
            SopShowInSelects = new List<SopShowInSelect>();
        }

        public Sop Sop { get; set; }
        public List<SopShowInSelect> SopShowInSelects { get; set; }
        public List<UserAccess> CurrentUser { get; set; }
    }
}