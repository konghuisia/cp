﻿using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CP.Models
{
    public class CreateLeadViewModel
    {
        public CreateLeadViewModel()
        {
            Address = new Address();
            CommentRemark = new CommentRemark();
            Document = new Document();
            Person = new Person();
            Profile = new Profile();
            Request = new Request();
            Appointment = new Appointment();
            User = new List<User>();
            Sources = new List<Source>();
        }
        public Address Address { get; set; }
        public CommentRemark CommentRemark { get; set; }
        public Document Document { get; set; }
        public Person Person { get; set; }
        public Profile Profile { get; set; }
        public Request Request { get; set; }
        public Appointment Appointment { get; set; }
        public List<User> User { get; set; }
        public List<StatusOfCase> StatusOfCases { get; set; }
        public AssetForInvestigation AssetForInvestigation { get; set; }
        public BlackListingRecord BlackListingRecord { get; set; }
        public CompanyFinancialInformation CompanyFinancialInformation { get; set; }
        public LitigationRecord LitigationRecord { get; set; }
        public List<Source> Sources { get; set; }
        public string Position { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public UserPosition UserPosition { get; set; }
        public List<City> Cities { get; set; }
        public List<State> States { get; set; }
        public List<UserAccess> CurrentUser { get; internal set; }
        public List<User> Users { get; internal set; }
    }
}