﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class PageSegmentAccess
    {
        public bool ProfileAddressPersonAccess { get; set; }
        public bool AppointmentDocAccess { get; set; }
        public bool CreditorDocAccess { get; set; }
        public bool PropertyAccess { get; set; }
        public bool CtosDocAccess { get; set; }
        public bool DebtorAccess { get; set; }
        public bool CFIAccess { get; set; }
        public bool LRAccess { get; set; }
        public bool BRAccess { get; set; }
    }
}
