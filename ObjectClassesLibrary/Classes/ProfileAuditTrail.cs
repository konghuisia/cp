﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class ProfileAuditTrail
    {
        public string AuditTrailId { get; set; }
        public string ProfileId { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
        public string Action { get; set; }
        public string Comment { get; set; }
        public DateTime Date { get; set; }
    }
}
