﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class SopDocuments
    {
        public int Id { get; set; }
        public string DocumentId { get; set; }
        public string Path { get; set; }
        public string RequestId { get; set; }
        public string SopId { get; set; }
        public string Type { get; set; }
        public string FileName { get; set; }
        public int Size { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateString { get; set; }
        public bool Deleted { get; set; }
    }
}
