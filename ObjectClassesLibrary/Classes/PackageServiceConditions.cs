﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class PackageServiceConditions
    {
        public string ServiceId { get; set; }
        public string ConditionId { get; set; }
        public string CondTypeId { get; set; }
        public string Price_RM { get; set; }
        public string Price_SC { get; set; }
        public string CondTypeId2 { get; set; }
        public string Price_RM2 { get; set; }
        public string Price_SC2 { get; set; }
        public string Status { get; set; }
        public string CondName { get; set; }

    }
}
