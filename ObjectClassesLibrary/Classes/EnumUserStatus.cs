﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public enum EnumUserStatus
    {
        Pending = 0,
        Active,
        LockedOut,
        Inactive,
        Closed,
        Banned
    }
}
