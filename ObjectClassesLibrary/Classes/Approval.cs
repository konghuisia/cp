﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Approval
    {
        public int id { get; set; }
        public string ApprovalId { get; set; }
        public string RequestId { get; set; }
        public string ApprovalUserId { get; set; }
        public string ApprovalType { get; set; }
        public bool Action { get; set; }
        public bool Status { get; set; }
        public DateTime LastUpdateActionTime { get; set; }
        public DateTime AssignedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
