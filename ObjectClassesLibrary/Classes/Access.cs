﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Access
    {
        public string AccessId { get; set; }
        public string Name { get; set; }
        public string Keyword { get; set; }
        public string Description { get; set; }
    }
}
