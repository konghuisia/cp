﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class PackageService_Sop
    {
        public int id { get; set; }
        public int AutoId { get; set; }
        public string PackageId { get; set; }
        public string PackageServiceId { get; set; }
        public string ServiceTypeId { get; set; }
        public string SopId { get; set; }
        public string Type { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Active { get; set; }
    }
}
