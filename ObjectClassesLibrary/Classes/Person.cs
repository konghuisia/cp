﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectClassesLibrary.Classes
{
    public class Person
    {
        public string ProfileId { get; set; }
        public string PersonId { get; set; }
        public string Name { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
