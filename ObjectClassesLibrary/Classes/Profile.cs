﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectClassesLibrary.Classes
{
    public class Profile
    {
        public string ProfileId { get; set; }
        public string Name { get; set; }
        public int BusinessType { get; set; }
        public int ProfileType { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string BusinessRegNo { get; set; }
        public int DMS { get; set; }
        public string Source { get; set; }
        public string NatureOfBusiness { get; set; }
        public string Website { get; set; }
        public string Remark { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string DMSStatus { get; set; }
        public string SubscriptionRequestId { get; set; }
    }
}
