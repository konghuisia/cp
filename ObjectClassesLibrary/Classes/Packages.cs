﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Packages
    {
        public string PackageId { get; set; }
        public string PackageName { get; set; }
        public string PackageDesc { get; set; }
        public string CreditGiven { get; set; }
        public string SubscriptionFee_RM { get; set; }
        public DateTime createDate { get; set; }
        public DateTime updateDate { get; set; }
        public string createBy { get; set; }
        public string updateBy { get; set; }
        public string Validity_Months { get; set; }
        public DateTime startDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool status { get; set; }
        public bool SubServices { get; set; }
    }
}
