﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class BlackListingRecord
    {
        public string BlackListingRecordId { get; set; }
        public string ProfileId { get; set; }
        public string DocumentId { get; set; }
        public string Creditor1 { get; set; }
        public DateTime? StatementDate { get; set; }
        public double ClaimAmount { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedAt { get; set; }
    }
}
