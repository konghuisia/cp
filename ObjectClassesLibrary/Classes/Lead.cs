﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Lead
    {
        public string LeadId { get; set; }
        public string RequestId { get; set; }
        public string AssignTo { get; set; }
        public bool Delete { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public int RecycledCount { get; set; }
        public DateTime LastUpdateActionTime { get; set; }
    }
}
