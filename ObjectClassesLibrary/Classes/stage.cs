﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Stage
    {
        public int id { get; set; }
        public int StageIntId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AccessByUserGroup { get; set; }
        public string StageId { get; set; }
        public string NextStageId { get; set; }
    }
}
