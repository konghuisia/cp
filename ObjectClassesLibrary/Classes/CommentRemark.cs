﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectClassesLibrary.Classes
{
    public class CommentRemark
    {
        public string RequestId { get; set; }
        public string CommentRemarkId { get; set; }
        public string ProfileId { get; set; }
        public string Comment { get; set; }
        public bool Deleted { get; set; }
        public string Type { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public int Stage { get; set; }
    }
}
