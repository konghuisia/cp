﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class SubscriptionStatus
    {
        public string Name { get; set; }
        public string StageId { get; set; }
        public string NextStageId { get; set; }
    }
}
