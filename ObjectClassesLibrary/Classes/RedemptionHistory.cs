﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class RedemptionHistory
    {
        public DateTime TranDate { get; set; }
        public string TotalAmount { get; set; }
        public string TranType { get; set; }
        public string WalletBalance { get; set; }
        public string Description { get; set; }
        public string AmountPerItem { get; set; }
        public int Amount { get; set; }
    }
}
