﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class LawFirmProfile
    {
        public string LawFirmProfileId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string BusinessRegNo { get; set; }
        public string Website { get; set; }
        public string Remark { get; set; }
        public bool Deleted { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
    }
}
