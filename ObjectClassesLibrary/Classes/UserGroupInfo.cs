﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class UserGroupInfo
    {
        public string Id { get; set; }
        public string GroupId { get; set; }
        public string GroupType { get; set; }
    }
}
