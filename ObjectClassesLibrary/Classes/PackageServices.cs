﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class PackageServices
    {
        public string ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ServiceDesc { get; set; }
        public int AmtOfConditionLevel { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Boolean Status { get; set; }
    }
}
