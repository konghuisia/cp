﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class SopRequests
    {
        public int id { get; set; }
        public string SopRequestid { get; set; }
        public string SopId { get; set; }
        public string RequestId { get; set; }
        public DateTime CretedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Deleted { get; set; }
        public int Status { get; set; }//0 = PENDING, 1 = STARTED, 2 = COMPLETED
        public int Stage { get; set; }
    }
}
