﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Appointment
    {
        public int id { get; set; }
        public string AppointmentId { get; set; }
        public string RequestId { get; set; }
        public string Status { get; set; }
        public string AppointmentType { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsAllDay { get; set; }
        public string Venue { get; set; }
        public string DocumentId { get; set; }
        public string CommentRemarkId { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Color { get; set; }
        public DateTime calendarDate { get; set; }
        public string calendarView { get; set; }
    }
}
