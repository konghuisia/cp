﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Transactions
    {
        public int id { get; set; }
        public string SubscriptionId { get; set; }
        public string RequestId { get; set; }
        public string ProfileId { get; set; }
        public string TransactionId { get; set; }
        public string TransactionType { get; set; }//Top-Up, Deduct-Sop-Point, Deduct-Sop-Price, Deduct-Service-Point, Deduct-Service-Price
        public string TransactionDescription { get; set; }
        public string SopId { get; set; }
        public string ServiceTypeId { get; set; }
        public double Amount { get; set; }
        public double TotalAmount { get; set; }
        public double WalletBalance { get; set; }//for subscriber
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Deleted { get; set; }
    }
}
