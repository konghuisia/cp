﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary
{
    public class ShowInSelect
    {
        public string ShowInSelectId { get; set; }
        public string ShowInSelectName { get; set; }
        public string DocumentTypeId { get; set; }
        public bool Required { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int Stage { get; set; }
    }
}
