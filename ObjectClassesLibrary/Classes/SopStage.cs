﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class SopStage
    {
        public int id { get; set; }
        public string SopId { get; set; }
        public string SopStageId { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public int Stage { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Deleted { get; set; }
    }
}
