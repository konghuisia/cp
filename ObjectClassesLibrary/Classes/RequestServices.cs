﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class RequestServices
    {
        public int id { get; set; }
        public string RequestId { get; set; }
        public string ProfileId { get; set; }
        public string SubscriptionId { get; set; }
        public string RequestServicesId { get; set; }
        public string SopId { get; set; }
        public string ServiceTypeId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool Deleted { get; set; }
        public bool Active { get; set; }
        public string Type { get; set; }
    }
}
