﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectClassesLibrary.Classes
{
    public class Request
    {
        public Request()
        {
            Type = "Lead";
        }
        public int id { get; set; }
        public string RequestId { get; set; }
        public string ProfileId { get; set; }
        public string AddressId { get; set; }
        public string PersonId { get; set; }
        public string CommentRemarkId { get; set; }
        public string DocumentId { get; set; }
        public string AppointmentId { get; set; }
        public string LeadId { get; set; }
        public string sName { get; set; }
        public bool Deleted { get; set; }
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public int Stage { get; set; }
        public bool AccessStatus { get; set; }
        public bool Approval1 { get; set; }
        public string Approval1User { get; set; }
        public DateTime Approval1LastUpdateActionTime { get; set; }
        public bool Approval2 { get; set; }
        public string Approval2User { get; set; }
        public DateTime Approval2LastUpdateActionTime { get; set; }
        public DateTime ApprovalAssignDate { get; set; }
        public string LawFirm { get; set; }
        public DateTime LawFirmLastUpdateActionTime { get; set; }
        public DateTime LawFirmAssignDate { get; set; }
        public bool Dispute1 { get; set; }
        public string Dispute1User { get; set; }
        public DateTime Dispute1LastUpdateActionTime { get; set; }
        public bool Dispute2 { get; set; }
        public string Dispute2User { get; set; }
        public DateTime Dispute2LastUpdateActionTime { get; set; }
        public DateTime DisputeAssignDate { get; set; }
        public string NegotiatorId { get; set; }
        public DateTime NegotiatorLastUpdateActionTime { get; set; }
        public DateTime NegotiatorAssignDate { get; set; }
        public bool Decision1 { get; set; }
        public string Decision1User { get; set; }
        public DateTime Decision1LastUpdateActionTime { get; set; }
        public bool Decision2 { get; set; }
        public string Decision2User { get; set; }
        public DateTime Decision2LastUpdateActionTime { get; set; }
        public DateTime DecisionAssignDate { get; set; }
        public string BoxStatus { get; set; }
        public DateTime OperationLastUpdateActionTime { get; set; }
        public string OperationUpdatedBy { get; set; }
        public DateTime FinanceLastUpdateActionTime { get; set; }
        public string FinanceUpdatedBy { get; set; }
        public string Approval1UserAction { get; set; }
        public string Approval2UserAction { get; set; }
        public string Dispute1UserAction { get; set; }
        public string Dispute2UserAction { get; set; }
        public string Decision1UserAction { get; set; }
        public string Decision2UserAction { get; set; }
        public bool RejectWithProposal { get; set; }
        public string Type { get; set; }
        public string SubscriptionRequestId { get; set; }
    }
}
