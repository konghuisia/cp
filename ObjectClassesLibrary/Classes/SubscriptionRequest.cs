﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class SubscriptionRequest
    {
        public string RequestId { get; set; }
        public string RequesterId { get; set; }
        public DateTime RequestDate { get; set; }
        public string SubsId { get; set; }
        public string PackageId { get; set; }
        public string PackageName { get; set; }
        public string Approver1_id { get; set; }
        public string Approver2_id { get; set; }
        public string Status { get; set; }
        public string Status_Desc { get; set; }
        public string sName { get; set; }
        public string Name { get; set; }
        public string BusinessRegNo { get; set; }
        public int Stage { get; set; }
        public DateTime Approval1_Date { get; set; }
        public DateTime Final_Approval_Date { get; set; }
        public bool Approval1 { get; set; }
        public bool FinalApproval { get; set; }
    }
}
