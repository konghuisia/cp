﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class AssetForInvestigation
    {
        public string AssetForInvestigationId { get; set; }
        public string ProfileId { get; set; }
        public string VehicleDetails1 { get; set; }
        public string VehicleDetails2 { get; set; }
        public string PropertySearchResult1 { get; set; }
        public string PropertySearchResult2 { get; set; }
        public string SiteInvestigation { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
