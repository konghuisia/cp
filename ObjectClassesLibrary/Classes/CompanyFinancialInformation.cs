﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class CompanyFinancialInformation
    {
        public string CompanyFinancialInformationId { get; set; }
        public string ProfileId { get; set; }
        public string DocumentId { get; set; }
        public bool LastFinancialStatementSubmitted { get; set; }
        public DateTime? LastFinancialStatementSubmittedDate { get; set; }
        public DateTime? FinancialYearEnd1 { get; set; }
        public DateTime FinancialYearEnd2 { get; set; }
        public DateTime FinancialYearEnd3 { get; set; }
        public float AnnualTurnover1 { get; set; }
        public float AnnualTurnover2 { get; set; }
        public float AnnualTurnover3 { get; set; }
        public float NtaPositiveNegative1 { get; set; }
        public float NtaPositiveNegative2 { get; set; }
        public float NtaPositiveNegative3 { get; set; }
        public float ProfitAfterTax1 { get; set; }
        public float ProfitAfterTax2 { get; set; }
        public float ProfitAfterTax3 { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool B { get; set; }
    }
}
