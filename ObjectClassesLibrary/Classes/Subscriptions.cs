﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Subscriptions
    {
        public string ProfileId { get; set; }
        public string Name { get; set; }
        public string SubsId { get; set; }
        public string SubscriptionCreditBalance { get; set; }
        //public string Subscription { get; set; }
        public string Remark { get; set; }
        public DateTime SubscribeDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime createdDate { get; set; }
        public int Status { get; set; }
        public string RequestId { get; set; }
        public string CreatedBy { get; set; }
        public string Approver1_Id { get; set; }
        public string Approver2_Id { get; set; }
        public string PackageId { get; set; }
    }
}
