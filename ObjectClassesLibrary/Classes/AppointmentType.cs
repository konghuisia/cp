﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class AppointmentType
    {
        public string AppointmentTypeId { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Deleted { get; set; }
        public string StatusName { get; set; }
        public int Stage { get; set; }
    }
}
