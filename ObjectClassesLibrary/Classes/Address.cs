﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectClassesLibrary.Classes
{
    public class Address
    {
        public string ProfileId { get; set; }
        public string AddressId { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
