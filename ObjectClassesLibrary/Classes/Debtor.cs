﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class Debtor
    {
        public string DebtorId { get; set; }
        public string RequestId { get; set; }
        public string ProfileId { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public bool Deleted { get; set; }
        public string CompanyName { get; set; }
        public string ContactNo { get; set; }
        public string BusinessRegNo { get; set; }
        public double ClaimAmount { get; set; }
        public bool StillInBusiness { get; set; }
        public string Remark { get; set; }
        public bool? InsolvencyStatus { get; set; }
        public bool? MDIStatus { get; set; }
    }
}
