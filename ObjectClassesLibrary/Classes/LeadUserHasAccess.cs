﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class LeadUserHasAccess
    {
        public bool checkAccess(string position, int stage, string id, string apAction)
        {
            bool boo = false;
            if (position == "A4C2EBBE-9391-44AF-BD49-572BBC9EB22E")//operations
            {
                if(stage == 0 || stage == 5 || stage == 14 || stage == 20)
                {
                    boo = true;
                }
            }
            else if (position == "011C9D8F-A82A-4BE3-9F30-4C2CEDE0D487" || position == "2788371D-8EC5-4ABE-A189-2A3B8562CAF1")//rmo //rmo supervisor
            {
                if (stage == 1 || stage == 2 || stage == 3 || stage == 4 || stage == 6 || stage == 9 || stage == 10 || stage == 11 || stage == 12 || stage == 13)
                {
                    boo = true;
                }
            }
            else if (position == "22C63EE9-290E-4160-A293-6ECB49B4677F")//approval committee
            {
                if (id == "2AA5715A-AB61-4861-A26B-5A125FB69D5D")//A
                {
                    if (stage == 7 || stage == 16 || stage == 23 || stage == 34)
                    {
                        boo = true;
                    }
                }
                
                if (id == "31316B60-95E1-4727-9242-CD7B51B47469")//B
                {
                    if (stage == 7 && !string.IsNullOrEmpty(apAction) || stage == 8 || stage == 17 || stage == 24)
                    {
                        boo = true;
                    }
                }
            }
            else if (position == "47461275-5753-47D1-8A8B-9A26D91E03A4")//law firm
            {
                if (stage == 15 || stage == 18 || stage == 27 || stage == 29 || stage == 31)
                { 
                    boo = true;
                }
            }
            else if (position == "E207490E-1A8C-405D-B415-B100ADB0D04F" || position == "7D68595F-726A-4D9F-8BF2-42DA814901C5")//negotiator //negitiator supervisor
            {
                if (stage == 21 || stage == 22 || stage == 26 || stage == 28 || stage == 30)
                {
                    boo = true;
                }
            }
            else if (position == "FD23EA98-5D5E-464C-B109-EE7F8E5211C5")//Finance 
            {
                if (stage == 32)
                {
                    boo = true;
                }
            }
            return boo;
        }
    }
}
