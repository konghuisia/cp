﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClassesLibrary.Classes
{
    public class LitigationRecord
    {
        public string LitigationRecordId { get; set; }
        public string ProfileId { get; set; }
        public string DocumentId { get; set; }
        public string Plaintiff { get; set; }
        public string Defendant { get; set; }
        public double ClaimAmount { get; set; }
        public string StatusOfCaseId { get; set; }
        public DateTime? HearingDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
