﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class RequestServiceStore
    {
        public Task<bool> UpdateRequestServices(List<RequestServices> rs, string UserId, string RequestId, string ProfileId, string SubscriptionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestServicesControl.UpdateRequestServices(rs, UserId, RequestId, ProfileId, SubscriptionId);
            });
        }

        public Task<bool> CreateRequestServices(List<RequestServices> rs, string UserId, string RequestId, string ProfileId, string SubscriptionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestServicesControl.CreateRequestServices(rs, UserId, RequestId, ProfileId, SubscriptionId);
            });
        }
        public Task<List<RequestServices>> GetAllRequestServices()
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestServicesControl.GetAllRequestServices()?.ToList() ?? new List<RequestServices>();
            });
        }

        public Task<List<RequestServices>> GetRequestServicesByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestServicesControl.GetRequestServicesByRequestId(RequestId)?.ToList() ?? new List<RequestServices>();
            });
        }
    }
}
