﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class AddressStore
    {
        public Task<string> UpdateAddress(Address a)
        {
            return Task.Factory.StartNew(() =>
            {
                return AddressControl.UpdateAddress(a);
            });
        }

        public Task<string> CreateAddress(Address a)
        {
            return Task.Factory.StartNew(() =>
            {
                return AddressControl.CreateAddress(a);
            });
        }
        public Task<List<Address>> GetAllAddress()
        {
            return Task.Factory.StartNew(() =>
            {
                return AddressControl.GetAllAddress()?.ToList() ?? new List<Address>();
            });
        }

        public Task<Address> GetAddressByAddressId(string AddressId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AddressControl.GetAddressByAddressId(AddressId);
            });
        }

        public Task<Address> GetAddressByProfileId(string profileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AddressControl.GetAddressByProfileId(profileId) ?? new Address();
            });
        }

        public Task<string> UpdateAddressByProfileId(Address a)
        {
            return Task.Factory.StartNew(() =>
            {
                return AddressControl.UpdateAddressByProfileId(a);
            });
        }

        public Task<string> UpdateAddressByAddressId(Address address)
        {
            return Task.Factory.StartNew(() =>
            {
                return AddressControl.UpdateAddressByAddressId(address);
            });
        }
    }
}
