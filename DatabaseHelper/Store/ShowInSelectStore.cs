﻿using DatabaseHelper.Control;
using ObjectClassesLibrary;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class ShowInSelectStore
    {
        public Task<List<ShowInSelect>> GetAllShowInSelect()
        {
            return Task.Factory.StartNew(() =>
            {
                return ShowInSelectControl.GetAllShowInSelect()?.ToList() ?? new List<ShowInSelect>();
            });
        }

        //public Task<ShowInSelect> CreateNewShowInSelect(ShowInSelect s)
        //{
        //    return Task.Factory.StartNew(() =>
        //    {
        //        return ShowInSelectControl.CreateNewShowInSelect(s);
        //    });
        //}
        //public Task<ShowInSelect> UpdateShowInSelect(ShowInSelect s)
        //{
        //    return Task.Factory.StartNew(() =>
        //    {
        //        return ShowInSelectControl.UpdateShowInSelect(s);
        //    });
        //}
        public Task<ShowInSelect> DeleteShowInSelect(string ShowInSelectId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return ShowInSelectControl.DeleteShowInSelect(ShowInSelectId, id);
            });
        }

        public Task<string> UpdateShowInSelect(ShowInSelect showInSelect, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return ShowInSelectControl.UpdateShowInSelect(showInSelect, id);
            });
        }

        public Task<string> CreateShowInSelect(ShowInSelect showInSelect, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return ShowInSelectControl.CreateShowInSelect(showInSelect, id);
            });
        }
    }
}
