﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class LitigationRecordStore
    {
        public Task<bool> CreateLitigationRecord(LitigationRecord lr, string id, string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LitigationRecordControl.CreateLitigationRecord(lr,id, ProfileId);
            });
        }
        public Task<List<LitigationRecord>> GetAllLitigationRecord()
        {
            return Task.Factory.StartNew(() =>
            {
                return LitigationRecordControl.GetAllLitigationRecord()?.ToList() ?? new List<LitigationRecord>();
            });
        }

        public Task<LitigationRecord> GetLitigationRecordByLitigationRecordId(string LitigationRecordId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LitigationRecordControl.GetLitigationRecordByLitigationRecordId(LitigationRecordId);
            });
        }
        public Task<bool> UpdateLitigationRecord(IList<LitigationRecord> lr, string id, string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LitigationRecordControl.UpdateLitigationRecord(lr, id, ProfileId);
            });
        }
        public Task<List<LitigationRecord>> GetLitigationRecordByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LitigationRecordControl.GetLitigationRecordByProfileId(ProfileId)?.ToList() ?? new List<LitigationRecord>();
            });
        }
    }
}
