﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class UserAccessStore
    {
        public Task<List<UserAccess>> GetAllUserAccess()
        {
            return Task.Factory.StartNew(() =>
            {
                return UserAccessControl.GetAllUserAccess()?.ToList() ?? new List<UserAccess>();
            });
        }

        //public Task<UserAccess> CreateNewUserAccess(UserAccess UserAccess, string id)
        //{
        //    return Task.Factory.StartNew(() =>
        //    {
        //        return UserAccessControl.CreateNewUserAccess(UserAccess, id);
        //    });
        //}
        //public Task<UserAccess> UpdateUserAccess(UserAccess UserAccess, string id)
        //{
        //    return Task.Factory.StartNew(() =>
        //    {
        //        return UserAccessControl.UpdateUserAccess(UserAccess, id);
        //    });
        //}
        //public Task<UserAccess> DeleteUserAccess(string UserAccessId, string id)
        //{
        //    return Task.Factory.StartNew(() =>
        //    {
        //        return UserAccessControl.DeleteUserAccess(UserAccessId, id);
        //    });
        //}
        public Task<UserAccess> GetUserAccessByUserAccessId(string UserAccessId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserAccessControl.GetUserAccessByUserAccessId(UserAccessId);
            });
        }
        public Task<List<UserAccess>> GetUserAccessByUserPositionId(string UserPositionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserAccessControl.GetUserAccessByUserPositionId(UserPositionId)?.ToList() ?? new List<UserAccess>();
            });
        }
    }
}
