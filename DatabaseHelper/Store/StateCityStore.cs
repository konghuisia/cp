﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class StateCityStore
    {
        public Task<List<City>> CityList()
        {
            return Task.Factory.StartNew(() =>
            {
                return StateCityControl.CityList()?.ToList() ?? new List<City>();
            });
        }
        public Task<List<State>> StateList()
        {
            return Task.Factory.StartNew(() =>
            {
                return StateCityControl.StateList()?.ToList() ?? new List<State>();
            });
        }
    }
}
