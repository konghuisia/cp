﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class PositionsStore
    {
        public Task<List<Positions>> GetPositions()
        {
            return Task.Factory.StartNew(() =>
            {
                return PositionsControl.GetAllPositions()?.ToList() ?? new List<Positions>();
            });
        }
    }
}
