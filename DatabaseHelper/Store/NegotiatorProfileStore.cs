﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class NegotiatorProfileStore
    {
        public Task<string> UpdateNegotiatorProfile(NegotiatorProfile p, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return NegotiatorProfileControl.UpdateNegotiatorProfile(p, id);
            });
        }

        public Task<string> CreateNegotiatorProfile(NegotiatorProfile p, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return NegotiatorProfileControl.CreateNegotiatorProfile(p, id);
            });
        }
        public Task<List<NegotiatorProfile>> GetAllNegotiatorProfile()
        {
            return Task.Factory.StartNew(() =>
            {
                return NegotiatorProfileControl.GetAllNegotiatorProfile()?.ToList() ?? new List<NegotiatorProfile>();
            });
        }

        public Task<NegotiatorProfile> GetNegotiatorProfileByNegotiatorProfileId(string NegotiatorProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return NegotiatorProfileControl.GetNegotiatorProfileByNegotiatorProfileId(NegotiatorProfileId);
            });
        }

        public Task<string> DeleteNegotiatorProfile(string NegotiatorProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return NegotiatorProfileControl.DeleteNegotiatorProfile(NegotiatorProfileId);
            });
        }

        public Task<string> GetNegotiatorProfileByNegotiatorProfileName(NegotiatorProfile p)
        {
            return Task.Factory.StartNew(() =>
            {
                return NegotiatorProfileControl.GetNegotiatorProfileByNegotiatorProfileName(p);
            });
        }
    }
}
