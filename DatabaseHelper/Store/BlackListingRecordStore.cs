﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class BlackListingRecordStore
    {
        public Task<bool> CreateBlackListingRecord(BlackListingRecord br, string id, string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return BlackListingRecordControl.CreateBlackListingRecord(br, id, ProfileId);
            });
        }
        public Task<List<BlackListingRecord>> GetAllBlackListingRecord()
        {
            return Task.Factory.StartNew(() =>
            {
                return BlackListingRecordControl.GetAllBlackListingRecord()?.ToList() ?? new List<BlackListingRecord>();
            });
        }

        public Task<BlackListingRecord> GetBlackListingRecordByBlackListingRecordId(string BlackListingRecordId)
        {
            return Task.Factory.StartNew(() =>
            {
                return BlackListingRecordControl.GetBlackListingRecordByBlackListingRecordId(BlackListingRecordId);
            });
        }
        public Task<bool> UpdateBlackListingRecord(IList<BlackListingRecord> br, string id, string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return BlackListingRecordControl.UpdateBlackListingRecord(br, id, ProfileId);
            });
        }
        public Task<List<BlackListingRecord>> GetBlackListingRecordByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return BlackListingRecordControl.GetBlackListingRecordByProfileId(ProfileId)?.ToList() ?? new List<BlackListingRecord>();
            });
        }
    }
}
