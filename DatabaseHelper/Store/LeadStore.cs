﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class LeadStore
    {
        public Task<string> UpdateLead(Lead l)
        {
            return Task.Factory.StartNew(() =>
            {
                return LeadControl.UpdateLead(l);
            });
        }

        public Task<string> CreateLead(Lead l)
        {
            return Task.Factory.StartNew(() =>
            {
                return LeadControl.CreateLead(l);
            });
        }
        public Task<List<Lead>> GetAllLead()
        {
            return Task.Factory.StartNew(() =>
            {
                return LeadControl.GetAllLead()?.ToList() ?? new List<Lead>();
            });
        }

        public Task<Lead> GetLeadByLeadId(string LeadId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LeadControl.GetLeadByLeadId(LeadId);
            });
        }

        public Task<Lead> GetLeadByRequestId(string requestId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateRecycleCount(string LeadId, int v)
        {
            return Task.Factory.StartNew(() =>
            {
                return LeadControl.UpdateRecycleCount(LeadId, v);
            });
        }
    }
}
