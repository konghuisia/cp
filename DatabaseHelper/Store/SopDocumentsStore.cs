﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class SopDocumentsStore
    {
        public Task<string> UpdateSopDocuments(SopDocuments d)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopDocumentControl.UpdateSopDocument(d);
            });
        }

        public Task<SopDocuments> CreateSopDocuments(SopDocuments d)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopDocumentControl.CreateSopDocument(d);
            });
        }
        public Task<List<SopDocuments>> GetAllSopDocuments()
        {
            return Task.Factory.StartNew(() =>
            {
                return SopDocumentControl.GetAllSopDocument()?.ToList() ?? new List<SopDocuments>();
            });
        }

        public Task<SopDocuments> GetSopDocumentsBySopDocumentsId(int SopDocumentsId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopDocumentControl.GetSopDocumentBySopDocumentId(SopDocumentsId);
            });
        }

        public Task<SopDocuments> DeleteSopDocuments(string documentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopDocumentControl.DeleteSopDocument(documentId);
            });
        }
        public Task<List<SopDocuments>> GetSopDocumentBySopId(string SopId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopDocumentControl.GetSopDocumentBySopId(SopId)?.ToList() ?? new List<SopDocuments>();
            });
        }
    }
}
