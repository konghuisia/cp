﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class lawFirmLawFirmProfileStore
    {
        public Task<string> UpdateLawFirmProfile(LawFirmProfile p, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return LawFirmProfileControl.UpdateLawFirmProfile(p, id);
            });
        }

        public Task<string> CreateLawFirmProfile(LawFirmProfile p, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return LawFirmProfileControl.CreateLawFirmProfile(p, id);
            });
        }
        public Task<List<LawFirmProfile>> GetAllLawFirmProfile()
        {
            return Task.Factory.StartNew(() =>
            {
                return LawFirmProfileControl.GetAllLawFirmProfile()?.ToList() ?? new List<LawFirmProfile>();
            });
        }

        public Task<LawFirmProfile> GetLawFirmProfileByLawFirmProfileId(string LawFirmProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LawFirmProfileControl.GetLawFirmProfileByLawFirmProfileId(LawFirmProfileId);
            });
        }

        public Task<string> DeleteLawFirmProfile(string LawFirmProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LawFirmProfileControl.DeleteLawFirmProfile(LawFirmProfileId);
            });
        }

        public Task<string> GetLawFirmProfileByLawFirmProfileName(LawFirmProfile p)
        {
            return Task.Factory.StartNew(() =>
            {
                return LawFirmProfileControl.GetLawFirmProfileByLawFirmProfileName(p);
            });
        }
    }
}
