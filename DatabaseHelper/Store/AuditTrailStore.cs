﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class AuditTrailStore
    {
        public Task<string> CreateAuditTrail(string RequestId, string UserId, string Type, string Action, string Comment, int stage)
        {
            return Task.Factory.StartNew(() =>
            {
                return AuditTrailControl.CreateAuditTrail(RequestId,UserId,Type,Action,Comment,stage);
            });
        }
        public Task<List<AuditTrail>> GetAllAuditTrail()
        {
            return Task.Factory.StartNew(() =>
            {
                return AuditTrailControl.GetAllAuditTrail()?.ToList() ?? new List<AuditTrail>();
            });
        }
        public Task<List<AuditTrail>> GetAuditTrailByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AuditTrailControl.GetAuditTrailByRequestId(RequestId)?.ToList() ?? new List<AuditTrail>();
            });
        }
        public Task<string> CreateProfileAuditTrail(string ProfileId, string UserId, string Type, string Action, string Comment)
        {
            return Task.Factory.StartNew(() =>
            {
                return AuditTrailControl.CreateProfileAuditTrail(ProfileId, UserId, Type, Action, Comment);
            });
        }
        public Task<List<ProfileAuditTrail>> GetAllProfileAuditTrail()
        {
            return Task.Factory.StartNew(() =>
            {
                return AuditTrailControl.GetAllProfileAuditTrail()?.ToList() ?? new List<ProfileAuditTrail>();
            });
        }
        public Task<List<ProfileAuditTrail>> GetProfileAuditTrailByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AuditTrailControl.GetProfileAuditTrailByProfileId(ProfileId)?.ToList() ?? new List<ProfileAuditTrail>();
            });
        }
        public Task<List<SubscriptionAuditTrail>> GetSubscriptionAuditTrailsByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AuditTrailControl.GetSubscriptionAuditTrailsByProfileId(ProfileId)?.ToList() ?? new List<SubscriptionAuditTrail>();
            });
        }
    }
}
