﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class SopStageStore
    {
        public Task<string> UpdateSopStage(SopStage sop, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopStageControl.UpdateSopStage(sop, id);
            });
        }

        public Task<string> CreateSopStage(SopStage sop, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopStageControl.CreateSopStage(sop, id);
            });
        }
        public Task<List<SopStage>> GetAllSopStage()
        {
            return Task.Factory.StartNew(() =>
            {
                return SopStageControl.SopStageList()?.ToList() ?? new List<SopStage>();
            });
        }

        public Task<List<SopStage>> GetSopStageBySopId(string SopId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopStageControl.GetSopStageBySopId(SopId)?.ToList() ?? new List<SopStage>();
            });
        }
    }
}
