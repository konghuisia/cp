﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;

namespace DatabaseHelper.Store
{
    public class ProfileStore
    {
        public Task<string> UpdateProfile(Profile p)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.UpdateProfile(p);
            });
        }

        public Task<string> CreateProfile(Profile p,string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.CreateProfile(p,id);
            });
        }
        public Task<List<Profile>> GetAllProfile()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.GetAllProfile()?.ToList() ?? new List<Profile>();
            });
        }

        public Task<Profile> GetProfileByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.GetProfileByProfileId(ProfileId) ?? new Profile();
            });
        }

        public Task<string> DeleteProfile(string profileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.DeleteProfile(profileId);
            });
        }

        public Task<string> GetProfileByProfileName(Profile p)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.GetProfileByProfileName(p);
            });
        }

        public Task<bool> DeleteLawfirmProfile(string profileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.DeleteLawfirmProfile(profileId);
            });
        }

        public Task<string> UpdateProfileSubscription(string profileId, int dms, string dmsStatus)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProfileControl.UpdateProfileSubscription(profileId, dms, dmsStatus);
            });
        }
    }
}
