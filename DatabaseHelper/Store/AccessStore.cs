﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class AccessStore
    {
        public Task<List<Access>> AccessList()
        {
            return Task.Factory.StartNew(() =>
            {
                return AccessControl.AccessList()?.ToList() ?? new List<Access>();
            });
        }

        public Task<bool> UpdateGroupAccess(List<UserAccess> access, string id, string UserPositionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AccessControl.UpdateGroupAccess(access, id, UserPositionId);
            });
        }

        public Task<bool> CreateGroupAccess(List<UserAccess> access, string id, string userPositionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AccessControl.CreateGroupAccess(access, id, userPositionId);
            });
        }
    }
}
