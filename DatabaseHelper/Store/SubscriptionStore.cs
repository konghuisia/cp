﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;

namespace DatabaseHelper.Store
{
    public class SubscriptionStore
    {
        public Task<List<Subscriptions>> GetSubscriptions()
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionsControl.GetSubscriptions()?.ToList() ?? new List<Subscriptions>();
            });
        }

        public Task<List<RedemptionHistory>> GetRedemptionHistories(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionsControl.GetRedemptionHistories(ProfileId)?.ToList() ?? new List<RedemptionHistory>();
            });
        }

        public Task<Subscriptions> GetSubscriptionByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionsControl.GetSubscriptionByProfileId(ProfileId) ?? new Subscriptions();
            });
        }

        public Task<string> CreateSubscription(Subscriptions s, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionsControl.CreateSubscription(s, id);
            });
        }

        public Task<List<SubscriptionRequest>> GetSubscriptionRequests(int status, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionsControl.GetSubscriptionRequest(status, id)?.ToList() ?? new List<SubscriptionRequest>();
            });
        }

        public Task<SubscriptionRequest> GetSubscriptionRequestByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionsControl.GetSubscriptionRequestByRequestId(RequestId) ?? new SubscriptionRequest();
            });
        }

        public Task<List<SubscriptionStatus>> GetSubscriptionStatus()
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionsControl.GetSubscriptionStatus()?.ToList() ?? new List<SubscriptionStatus>();
            });
        }
    }
}
