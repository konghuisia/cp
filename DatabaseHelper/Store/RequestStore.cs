﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class RequestStore
    {
        public Task<string> UpdateRequest(Request r)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateRequest(r);
            });
        }

        public Task<string> CreateRequest(Request r)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.CreateRequest(r);
            });
        }
        public Task<List<Request>> GetAllRequest()
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetAllRequest()?.ToList() ?? new List<Request>();
            });
        }

        public Task<Request> GetRequestByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRequestByRequestId(RequestId);
            });
        }

        public Task<bool> CreateRequestApprovalUser(string AU1, string AU2, string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.CreateRequestApprovalUser(AU1, AU2, requestId);
            });
        }

        public Task<bool> UpdateRequestApproval(string id, string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateRequestApproval(id, requestId);
            });
        }

        public Task<bool> UpdateRequestStage(string requestId, int stage, int previousStage)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateRequestStage(requestId, stage, previousStage);
            });
        }

        public Task<bool> UpdateAllRequestApprovalToFalse(string requestId, string id, string status)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateAllRequestApprovalToFalse(requestId, id, status);
            });
        }

        public Task<bool> CreateRequestDisputeUser(string DU1, string DU2, object requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.CreateRequestDisputeUser(DU1, DU2, requestId);
            });
        }

        public Task<bool> UpdateRequestDispute(string id, string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateRequestDispute(id, requestId);
            });
        }

        public Task<bool> UpdateRequestDecision(string id, string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateRequestDecision(id, requestId);
            });
        }

        public Task<bool> UpdateAllRequestDecisionToFalse(string requestId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateAllRequestDecisionToFalse(requestId, id);
            });
        }

        public Task<bool> CreateRequestDecisionUser(string DU1, string DU2, string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.CreateRequestDecisionUser(DU1, DU2, requestId);
            });
        }

        public Task<bool> UpdateRequestLastUpdateActionTime(string requestId, string id, string p, int stage)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.UpdateRequestLastUpdateActionTime(requestId, id, p, stage);
            });
        }

        public Task<bool> DeleteRequest(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.DeleteRequest(requestId);
            });
        }

        public Task<string> CreateSubscriptionRequest(SubscriptionRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.CreateSubscriptionRequest(request);
            });
        }
    }
}
