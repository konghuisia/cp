﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;

namespace DatabaseHelper.Store
{
    public class CompanyFinancialInformationStore
    {
        public Task<bool> CreateCompanyFinancialInformation(CompanyFinancialInformation cfi, string id, string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CompanyFinancialInformationControl.CreateCompanyFinancialInformation(cfi,id, ProfileId);
            });
        }
        public Task<List<CompanyFinancialInformation>> GetAllCompanyFinancialInformation()
        {
            return Task.Factory.StartNew(() =>
            {
                return CompanyFinancialInformationControl.GetAllCompanyFinancialInformation()?.ToList() ?? new List<CompanyFinancialInformation>();
            });
        }

        public Task<CompanyFinancialInformation> GetCompanyFinancialInformationByCompanyFinancialInformationId(string CompanyFinancialInformationId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CompanyFinancialInformationControl.GetCompanyFinancialInformationByCompanyFinancialInformationId(CompanyFinancialInformationId);
            });
        }
        public Task<bool> UpdateCompanyFinancialInformation(IList<CompanyFinancialInformation> cfi, string id, string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CompanyFinancialInformationControl.UpdateCompanyFinancialInformation(cfi, id, ProfileId);
            });
        }
        public Task<List<CompanyFinancialInformation>> GetCompanyFinancialInformationByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CompanyFinancialInformationControl.GetCompanyFinancialInformationByProfileId(ProfileId)?.ToList() ?? new List<CompanyFinancialInformation>();
            });
        }
    }
}
