﻿using DatabaseHelper.Control;
using ObjectClassesLibrary;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class SopShowInSelectStore
    {
        public Task<List<SopShowInSelect>> GetAllSopShowInSelect()
        {
            return Task.Factory.StartNew(() =>
            {
                return SopShowInSelectControl.GetAllSopShowInSelect()?.ToList() ?? new List<SopShowInSelect>();
            });
        }
       
        public Task<SopShowInSelect> DeleteSopShowInSelect(string SopShowInSelectId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopShowInSelectControl.DeleteSopShowInSelect(SopShowInSelectId, id);
            });
        }

        public Task<string> UpdateSopShowInSelect(SopShowInSelect showInSelect, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopShowInSelectControl.UpdateSopShowInSelect(showInSelect, id);
            });
        }

        public Task<string> CreateSopShowInSelect(SopShowInSelect showInSelect, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopShowInSelectControl.CreateSopShowInSelect(showInSelect, id);
            });
        }
        public Task<List<SopShowInSelect>> GetSopShowInSelectBySopId(string SopId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopShowInSelectControl.GetSopShowInSelectBySopId(SopId)?.ToList() ?? new List<SopShowInSelect>();
            });
        }
    }
}
