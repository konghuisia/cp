﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class StatusStore
    {
        public Task<List<Status>> StatusList()
        {
            return Task.Factory.StartNew(() =>
            {
                return StatusControl.StatusList()?.ToList() ?? new List<Status>();
            });
        }
    }
}
