﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class CommentRemarkStore
    {
        public Task<string> UpdateCommentRemark(CommentRemark c)
        {
            return Task.Factory.StartNew(() =>
            {
                return CommentRemarkControl.UpdateCommentRemark(c);
            });
        }

        public Task<string> CreateCommentRemark(CommentRemark c, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return CommentRemarkControl.CreateCommentRemark(c, id);
            });
        }
        public Task<List<CommentRemark>> GetAllCommentRemark()
        {
            return Task.Factory.StartNew(() =>
            {
                return CommentRemarkControl.GetAllCommentRemark()?.ToList() ?? new List<CommentRemark>();
            });
        }

        public Task<List<CommentRemark>> GetCommentRemarkByCommentRemarkId(string CommentRemarkId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CommentRemarkControl.GetCommentRemarkByCommentRemarkId(CommentRemarkId)?.ToList() ?? new List<CommentRemark>();
            });
        }

        public Task<List<CommentRemark>> GetCommentRemarkByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CommentRemarkControl.GetCommentRemarkByProfileId(ProfileId)?.ToList() ?? new List<CommentRemark>();
            });
        }

        public Task<string> UpdateCommentRemarkByProfileId(CommentRemark cr)
        {
            return Task.Factory.StartNew(() =>
            {
                return CommentRemarkControl.UpdateCommentRemarkByProfileId(cr);
            });
        }

        public Task<List<CommentRemark>> GetCommentRemarkByRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CommentRemarkControl.GetCommentRemarkByRequestId(requestId)?.ToList() ?? new List<CommentRemark>();
            });
        }
    }
}
