﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class SopStore
    {
        public Task<List<Sop>> SopList()
        {
            return Task.Factory.StartNew(() =>
            {
                return SopControl.SopList()?.ToList() ?? new List<Sop>();
            });
        }

        public Task<List<Sop>> SopList(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopControl.SopList(RequestId)?.ToList() ?? new List<Sop>();
            });
        }
    }
}
