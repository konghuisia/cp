﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class StatusOfCaseStore
    {
        public Task<string> UpdateStatusOfCase(StatusOfCase a)
        {
            return Task.Factory.StartNew(() =>
            {
                return StatusOfCaseControl.UpdateStatusOfCase(a);
            });
        }

        public Task<string> CreateStatusOfCase(StatusOfCase a)
        {
            return Task.Factory.StartNew(() =>
            {
                return StatusOfCaseControl.CreateStatusOfCase(a);
            });
        }
        public Task<List<StatusOfCase>> GetAllStatusOfCase()
        {
            return Task.Factory.StartNew(() =>
            {
                return StatusOfCaseControl.GetAllStatusOfCase()?.ToList() ?? new List<StatusOfCase>();
            });
        }

        public Task<StatusOfCase> GetStatusOfCaseByStatusOfCaseId(string StatusOfCaseId)
        {
            return Task.Factory.StartNew(() =>
            {
                return StatusOfCaseControl.GetStatusOfCaseByStatusOfCaseId(StatusOfCaseId);
            });
        }
    }
}
