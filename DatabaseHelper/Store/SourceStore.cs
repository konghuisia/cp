﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class SourceStore
    {
        public Task<string> UpdateSource(Source s)
        {
            return Task.Factory.StartNew(() =>
            {
                return SourceControl.UpdateSource(s);
            });
        }

        public Task<string> CreateSource(Source s)
        {
            return Task.Factory.StartNew(() =>
            {
                return SourceControl.CreateSource(s);
            });
        }
        public Task<List<Source>> GetAllSource()
        {
            return Task.Factory.StartNew(() =>
            {
                return SourceControl.GetAllSource()?.ToList() ?? new List<Source>();
            });
        }

        public Task<Source> GetSourceBySourceId(string SourceId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SourceControl.GetSourceBySourceId(SourceId);
            });
        }
    }
}
