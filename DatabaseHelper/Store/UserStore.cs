﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class UserStore
    {
        public Task<List<User>> GetAllUser()
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.GetAllUser()?.ToList() ?? new List<User>();
            });
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.FindByNameAsync(userName);
            });
        }

        public Task<string> CreateNewUser(User user, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.CreateNewUser(user, id);
            });
        }

        public Task<string> UpdateUserByUserId(User user, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UpdateUserByUserId(user, id);
            });
        }

        public Task<User> GetUserByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.GetUserByProfileId(ProfileId);
            });
        }

        public Task<bool> CheckUserName(string name)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.CheckUserName(name);
            });
        }

        public Task<User> GetUserByUserid(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.GetUserByUserid(userId);
            });
        }

        public Task<bool> ResetUserPassword(string userId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.ResetUserPassword(userId, id);
            });
        }

        public Task<bool> ChangePassword(User user, string pass)
        {
            try
            {
                return Task.Factory.StartNew(() =>
                {
                    return UserControl.ChangePassword(user, pass);
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<bool> DeleteUser(string userId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.DeleteUser(userId, id);
            });
        }
    }
}
