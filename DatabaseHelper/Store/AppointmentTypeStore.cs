﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class AppointmentTypeStore
    {
        public Task<List<AppointmentType>> GetAllAppointmentType()
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentTypeControl.GetAllAppointmentType()?.ToList() ?? new List<AppointmentType>();
            });
        }

        public Task<AppointmentType> CreateNewAppointmentType(AppointmentType AppointmentType)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentTypeControl.CreateNewAppointmentType(AppointmentType);
            });
        }
        public Task<AppointmentType> UpdateAppointmentType(AppointmentType AppointmentType)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentTypeControl.UpdateAppointmentType(AppointmentType);
            });
        }
        public Task<AppointmentType> DeleteAppointmentType(string AppointmentTypeId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentTypeControl.DeleteAppointmentType(AppointmentTypeId);
            });
        }
    }
}
