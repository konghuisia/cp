﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class DraftStore
    {
        public Task<string> CreateDraft(Draft d)
        {
            return Task.Factory.StartNew(() =>
            {
                return DraftControl.CreateDraft(d);
            });
        }
        public Task<List<Draft>> GetAllDraft()
        {
            return Task.Factory.StartNew(() =>
            {
                return DraftControl.GetAllDraft()?.ToList() ?? new List<Draft>();
            });
        }

        public Task<Draft> GetDraftByDraftId(string DraftId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DraftControl.GetDraftByDraftId(DraftId);
            });
        }

        public Task<Draft> DeleteDraft(string DraftId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DraftControl.DeleteDraft(DraftId);
            });
        }

        public Task<Draft> GetDraftByRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DraftControl.GetDraftByRequestId(requestId);
            });
        }

        public Task<string> UpdateDraft(Draft draft)
        {
            return Task.Factory.StartNew(() =>
            {
                return DraftControl.UpdateDraft(draft);
            });
        }
        public Task<List<Draft>> GetDraftByCreatedById(string CreatedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return DraftControl.GetDraftByCreatedById(CreatedBy)?.ToList() ?? new List<Draft>();
            });
        }
    }
}
