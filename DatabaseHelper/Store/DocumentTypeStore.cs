﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class DocumentTypeStore
    {
        public Task<List<DocumentType>> GetAllDocumentType()
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.GetAllDocumentType()?.ToList() ?? new List<DocumentType>();
            });
        }

        public Task<DocumentType> CreateNewDocType(DocumentType docType, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.CreateNewDocType(docType, id);
            });
        }
        public Task<DocumentType> UpdateDocType(DocumentType docType, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.UpdateDocType(docType, id);
            });
        }
        public Task<DocumentType> DeleteDocType(string docTypeId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.DeleteDocType(docTypeId, id);
            });
        }
    }
}
