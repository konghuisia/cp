﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class UserPositionStore
    {
        public Task<List<UserPosition>> GetAllUserPosition()
        {
            return Task.Factory.StartNew(() =>
            {
                return UserPositionControl.GetAllUserPosition()?.ToList() ?? new List<UserPosition>();
            });
        }

        public Task<UserPosition> CreateNewUserPosition(UserPosition UserPosition, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserPositionControl.CreateNewUserPosition(UserPosition, id);
            });
        }
        public Task<UserPosition> UpdateUserPosition(UserPosition UserPosition, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserPositionControl.UpdateUserPosition(UserPosition, id);
            });
        }
        public Task<UserPosition> DeleteUserPosition(string UserPositionId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserPositionControl.DeleteUserPosition(UserPositionId, id);
            });
        }
        public Task<UserPosition> GetUserPositionByUserPositionId(string UserPositionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserPositionControl.GetUserPositionByUserPositionId(UserPositionId);
            });
        }
    }
}
