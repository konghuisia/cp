﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class PersonStore
    {
        public Task<string> UpdatePerson(Person p)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonControl.UpdatePerson(p);
            });
        }

        public Task<string> CreatePerson(Person p)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonControl.CreatePerson(p);
            });
        }
        public Task<List<Person>> GetAllNotification()
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonControl.GetAllPerson()?.ToList() ?? new List<Person>();
            });
        }

        public Task<Person> GetPersonByPersonId(string PersonId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonControl.GetPersonByPersonId(PersonId);
            });
        }

        public Task<Person> GetPersonByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonControl.GetPersonByProfileId(ProfileId) ?? new Person();
            });
        }

        public Task<string> UpdatePersonByProfileId(Person p)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonControl.UpdatePersonByProfileId(p);
            });
        }

        public Task<string> UpdatePersonByPersonId(Person person)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonControl.UpdatePersonByPersonId(person);
            });
        }
    }
}
