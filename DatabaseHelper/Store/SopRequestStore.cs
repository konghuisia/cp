﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class SopRequestsStore
    {
        public Task<string> UpdateSopRequests(SopRequests sop, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopRequestsControl.UpdateSopRequests(sop, id);
            });
        }

        public Task<string> CreateSopRequests(SopRequests sop, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopRequestsControl.CreateSopRequests(sop, id);
            });
        }
        public Task<List<SopRequests>> GetAllSopRequests()
        {
            return Task.Factory.StartNew(() =>
            {
                return SopRequestsControl.SopRequestsList()?.ToList() ?? new List<SopRequests>();
            });
        }

        public Task<List<SopRequests>> GetSopRequestsByRequestsId(string RequestsId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopRequestsControl.GetSopRequestsByRequestId(RequestsId)?.ToList() ?? new List<SopRequests>();
            });
        }

        public Task<SopRequests> GetSopRequestsBySopRequestId(string sopId)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopRequestsControl.GetSopRequestsBySopRequestId(sopId);
            });
        }

        public Task<bool> UpdateSopRequestsStatus(string sopRequestId, int status)
        {
            return Task.Factory.StartNew(() =>
            {
                return SopRequestsControl.UpdateSopRequestsStatus(sopRequestId, status);
            });
        }
    }
}
