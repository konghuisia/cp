﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class AssetForInvestigationStore
    {
        public Task<string> UpdateAssetForInvestigation(AssetForInvestigation a, string id, string profileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AssetForInvestigationControl.UpdateAssetForInvestigation(a, id, profileId);
            });
        }

        public Task<string> CreateAssetForInvestigation(AssetForInvestigation a, string id, string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AssetForInvestigationControl.CreateAssetForInvestigation(a,id,ProfileId);
            });
        }
        public Task<List<AssetForInvestigation>> GetAllAssetForInvestigation()
        {
            return Task.Factory.StartNew(() =>
            {
                return AssetForInvestigationControl.GetAllAssetForInvestigation()?.ToList() ?? new List<AssetForInvestigation>();
            });
        }

        public Task<AssetForInvestigation> GetAssetForInvestigationByAssetForInvestigationId(string AssetForInvestigationId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AssetForInvestigationControl.GetAssetForInvestigationByAssetForInvestigationId(AssetForInvestigationId);
            });
        }
        public Task<AssetForInvestigation> GetAssetForInvestigationByProfileId(string ProfileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AssetForInvestigationControl.GetAssetForInvestigationByProfileId(ProfileId) ?? new AssetForInvestigation();
            });
        }
    }
}
