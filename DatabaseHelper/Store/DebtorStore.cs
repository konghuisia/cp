﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class DebtorStore
    {
        public Task<Debtor> CreateDebtor(Debtor d, string id, bool b, string profileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DebtorControl.CreateDebtor(d, id, b, profileId);
            });
        }
        public Task<List<Debtor>> GetAllDebtor()
        {
            return Task.Factory.StartNew(() =>
            {
                return DebtorControl.GetAllDebtor()?.ToList() ?? new List<Debtor>();
            });
        }

        public Task<Debtor> GetDebtorByDebtorId(string DebtorId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DebtorControl.GetDebtorByDebtorId(DebtorId);
            });
        }

        public Task<Debtor> DeleteDebtor(string DebtorId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DebtorControl.DeleteDebtor(DebtorId);
            });
        }

        public Task<List<Debtor>> GetDebtorByRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DebtorControl.GetDebtorByRequestId(requestId)?.ToList() ?? new List<Debtor>();
            });
        }
    }
}
