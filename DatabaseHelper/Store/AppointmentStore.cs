﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class AppointmentStore
    {
        public Task<string> UpdateAppointment(Appointment d)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.UpdateAppointment(d);
            });
        }

        public Task<string> CreateAppointment(Appointment d)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.CreateAppointment(d);
            });
        }
        public Task<List<Appointment>> GetAllAppointment()
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.GetAllAppointment()?.ToList() ?? new List<Appointment>();
            });
        }

        public Task<Appointment> GetAppointmentByAppointmentId(string AppointmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.GetAppointmentByAppointmentId(AppointmentId);
            });
        }
        public Task<List<Appointment>> GetAppointmentByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.GetAppointmentByRequestId(RequestId).ToList() ?? new List<Appointment>();
            });
        }

        public Task<Appointment> DeleteAppointment(string AppointmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.DeleteAppointment(AppointmentId);
            });
        }
        public Task<bool> DeleteAppointmentByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.DeleteAppointmentByRequestId(RequestId);
            });
        }

        public Task<bool> UpdateAppointmentStatus(string requestId, int stage)
        {
            return Task.Factory.StartNew(() =>
            {
                return AppointmentControl.UpdateAppointmentStatus(requestId, stage);
            });
        }
    }
}
