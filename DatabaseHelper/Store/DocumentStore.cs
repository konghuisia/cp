﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class DocumentStore
    {
        public Task<string> UpdateDocument(Document d)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.UpdateDocument(d);
            });
        }

        public Task<Document> CreateDocument(Document d)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.CreateDocument(d);
            });
        }
        public Task<List<Document>> GetAllDocument()
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.GetAllDocument()?.ToList() ?? new List<Document>();
            });
        }

        public Task<Document> GetDocumentByDocumentId(int DocumentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.GetDocumentByDocumentId(DocumentId);
            });
        }

        public Task<Document> DeleteDocument(string documentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.DeleteDocument(documentId);
            });
        }

        public Task<bool> UpdateDocumentsRequestIdByDocumentId(IList<Document> documents, string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.UpdateDocumentsRequestIdByDocumentId(documents, requestId);
            });
        }
        public Task<bool> UpdateDocumentsAppointmentIdByDocumentId(IList<Document> documents, string appointment)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.UpdateDocumentsAppointmentIdByDocumentId(documents, appointment);
            });
        }

        public Task<List<Document>> GetDocumentByRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.GetDocumentByRequestId(requestId)?.ToList() ?? new List<Document>();
            });
        }

        public Task<Document> CreateAppointmentDocument(Document d)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.CreateAppointmentDocument(d);
            });
        }

        public Task<Document> DeleteAppointmentDocument(string documentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.DeleteAppointmentDocument(documentId);
            });
        }

        public Task<List<Document>> GetDocumentsByAppointmentId(string appointmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.GetDocumentsByAppointmentId(appointmentId)?.ToList() ?? new List<Document>();
            });
        }

        public Task<List<Document>> GetDocumentBySubscriptionRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentControl.GetDocumentBySubscriptionRequestId(requestId)?.ToList() ?? new List<Document>();
            });
        }
    }
}
