﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;

namespace DatabaseHelper.Store
{
    public class PackageStore
    {
        public Task<List<Packages>> GetPackages()
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.GetPackages()?.ToList() ?? new List<Packages>();
            });
        }

        public Task<List<ConditionTypes>> GetConditionTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.GetConditionTypes()?.ToList() ?? new List<ConditionTypes>();
            });
        }

        public Task<Packages> GetPackagesByPackageId(string packageId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.GetPackagesByPackageId(packageId);
            });
        }

        public Task<PackageServices> GetPackageServiceByServiceId(string serviceId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.GetPackageServiceByServiceId(serviceId);
            });
        }

        public Task<List<PackageServiceConditions>> GetPackageServiceConditionsByServiceId(string serviceId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.GetPackageServiceConditionByServiceId(serviceId)?.ToList() ?? new List<PackageServiceConditions>();
            });
        }

        public Task<string> CreateNewPackage(Packages package, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.CreateNewPackage(package, id);
            });
        }

        public Task<string> CreateNewPackageService(PackageServices service, PackageServiceConditions condition, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.CreateNewPackageService(service, condition, id);
            });
        }

        public Task<string> CreateNewConditionType(string condName, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.CreateNewConditionType(condName, id);
            });
        }

        public Task<string> UpdatePackage(Packages package, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.UpdatePackage(package, id);
            });
        }

        //public Task<string> UpdatePackageService(PackageServices services, ConditionTypes condition, string id)
        //{
        //    return Task.Factory.StartNew(() =>
        //    {
        //        //return PackagesControl.UpdatePackage(package, id);
        //    });
        //}


        public Task<string> DeletePackage(string packageId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.DeletePackage(packageId, id);
            });
        }

        public Task<string> DeleteConditionType(string CondTypeId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.DeleteConditionType(CondTypeId, id);
            });
        }

        public Task<string> UpdateConditionType(ConditionTypes CondTypes, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.UpdateConditionType(CondTypes, id);
            });
        }

        public Task<List<PackageServices>> GetPackageServicesByPackageId(string packageId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.GetPackageServicesByPackageId(packageId)?.ToList() ?? new List<PackageServices>();
            });
        }

        public Task<List<PackageServices>> GetPackageServices()
        {
            return Task.Factory.StartNew(() =>
            {
                return PackagesControl.GetPackageServices()?.ToList() ?? new List<PackageServices>();
            });
        }
    }
}
