﻿using DatabaseHelper.Control;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Store
{
    public class TransactionStore
    {
        public Task<string> CreateTransaction(Transactions t, string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TransactionControl.CreateTransaction(t, userId);
            });
        }
        public Task<List<Transactions>> GetAllTransaction()
        {
            return Task.Factory.StartNew(() =>
            {
                return TransactionControl.GetAllTransaction()?.ToList() ?? new List<Transactions>();
            });
        }

        public Task<Transactions> GetTransactionByTransactionId(string TransactionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TransactionControl.GetTransactionByTransactionId(TransactionId);
            });
        }

        public Task<bool> DeleteTransaction(string TransactionId, string UserId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TransactionControl.DeleteTransaction(TransactionId, UserId);
            });
        }

        public Task<List<Transactions>> GetTransactionByRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TransactionControl.GetTransactionByRequestId(requestId)?.ToList() ?? new List<Transactions>();
            });
        }

        public Task<List<Transactions>> GetTransactionBySubscriptionId(string subscriptionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TransactionControl.GetTransactionBySubscriptionId(subscriptionId)?.ToList() ?? new List<Transactions>();
            });
        }

        public Task<List<Transactions>> GetTransactionByProfileId(string profileId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TransactionControl.GetTransactionByProfileId(profileId)?.ToList() ?? new List<Transactions>();
            });
        }
    }
}
