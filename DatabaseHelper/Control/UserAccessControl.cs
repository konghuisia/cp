﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class UserAccessControl
    {
        public static List<UserAccess> GetAllUserAccess()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<UserAccess>("cfg.Get_UserAccess", parameters);
        }
        //public static UserAccess DeleteUserAccess(string UserAccessId, string id)
        //{
        //    var parameters = new List<ParameterInfo>()
        //    {
        //        new ParameterInfo() { ParameterName = "UserAccessId", ParameterValue = UserAccessId },
        //        new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id }
        //    };
        //    return SqlHelper.GetRecord<UserAccess>("cfg.Delete_UserAccess", parameters);
        //}

        //public static UserAccess UpdateUserAccess(UserAccess UserAccess, string id)
        //{
        //    var parameters = new List<ParameterInfo>
        //    {
        //        new ParameterInfo() { ParameterName = "UserAccessId", ParameterValue = UserAccess.UserAccessId },
        //        new ParameterInfo() { ParameterName = "Name", ParameterValue = UserAccess.Name },
        //        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
        //    };
        //    return SqlHelper.GetRecord<UserAccess>("cfg.Update_UserAccess", parameters);
        //}

        //public static UserAccess CreateNewUserAccess(UserAccess UserAccess, string id)
        //{
        //    var parameters = new List<ParameterInfo>
        //    {
        //        new ParameterInfo() { ParameterName = "Name", ParameterValue = UserAccess.Name },
        //        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
        //    };
        //    return SqlHelper.GetRecord<UserAccess>("cfg.Create_UserAccess", parameters);
        //}
        public static UserAccess GetUserAccessByUserAccessId(string UserAccessId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "UserAccessId", ParameterValue = UserAccessId },
            };
            return SqlHelper.GetRecord<UserAccess>("cfg.Get_UserAccessByUserAccessId", parameters);
        }
        public static List<UserAccess> GetUserAccessByUserPositionId(string UserPositionId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "UserPositionId", ParameterValue = UserPositionId },
            };
            return SqlHelper.GetRecords<UserAccess>("cfg.Get_UserAccessByUserPositionId", parameters);
        }
    }
}
