﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class LeadControl
    {
        public static string UpdateLead(Lead l)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "LeadId", ParameterValue = l.LeadId },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = l.RequestId },
                new ParameterInfo() { ParameterName = "AssignTo", ParameterValue = l.AssignTo }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Lead", parameters);
        }

        public static string CreateLead(Lead l)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = l.RequestId },
                new ParameterInfo() { ParameterName = "AssignTo", ParameterValue = l.AssignTo },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = l.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Lead", parameters);
        }

        public static List<Lead> GetAllLead()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Lead>("dbo.Get_Lead", parameters);
        }

        public static bool UpdateRecycleCount(string LeadId, int v)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "LeadId", ParameterValue = LeadId },
                new ParameterInfo() { ParameterName = "RecycleCount", ParameterValue = v }
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_LeadRecycleCount", parameters);
        }

        public static Lead GetLeadByLeadId(string LeadId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "LeadId", ParameterValue = LeadId },
            };
            return SqlHelper.GetRecord<Lead>("dbo.Get_LeadByLeadId", parameters);
        }
    }
}
