﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class AssetForInvestigationControl
    {
        public static string UpdateAssetForInvestigation(AssetForInvestigation a, string id, string profileId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "AssetForInvestigationId", ParameterValue = a.AssetForInvestigationId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = a.ProfileId },
                new ParameterInfo() { ParameterName = "VehicleDetails1", ParameterValue = a.VehicleDetails1 },
                new ParameterInfo() { ParameterName = "VehicleDetails2", ParameterValue = a.VehicleDetails2 },
                new ParameterInfo() { ParameterName = "PropertySearchResult1", ParameterValue = a.PropertySearchResult1 },
                new ParameterInfo() { ParameterName = "PropertySearchResult2", ParameterValue = a.PropertySearchResult2 },
                new ParameterInfo() { ParameterName = "SiteInvestigation", ParameterValue = a.SiteInvestigation },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_AssetForInvestigation", parameters);
        }

        public static string CreateAssetForInvestigation(AssetForInvestigation a, string id, string ProfileId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                new ParameterInfo() { ParameterName = "VehicleDetails1", ParameterValue = a.VehicleDetails1 },
                new ParameterInfo() { ParameterName = "VehicleDetails2", ParameterValue = a.VehicleDetails2 },
                new ParameterInfo() { ParameterName = "PropertySearchResult1", ParameterValue = a.PropertySearchResult1 },
                new ParameterInfo() { ParameterName = "PropertySearchResult2", ParameterValue = a.PropertySearchResult2 },
                new ParameterInfo() { ParameterName = "SiteInvestigation", ParameterValue = a.SiteInvestigation },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_AssetForInvestigation", parameters);
        }

        public static AssetForInvestigation GetAssetForInvestigationByAssetForInvestigationId(string AssetForInvestigationId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "AssetForInvestigationId", ParameterValue = AssetForInvestigationId },
            };
            return SqlHelper.GetRecord<AssetForInvestigation>("dbo.Get_AssetForInvestigationByAssetForInvestigationId", parameters);
        }

        public static List<AssetForInvestigation> GetAllAssetForInvestigation()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<AssetForInvestigation>("dbo.Get_AssetForInvestigation", parameters);
        }
        public static AssetForInvestigation GetAssetForInvestigationByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecord<AssetForInvestigation>("dbo.Get_AssetForInvestigationByProfileId", parameters);
        }
    }
}
