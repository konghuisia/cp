﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class AddressControl
    {
        public static string UpdateAddress(Address a)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = a.ProfileId },
                    new ParameterInfo() { ParameterName = "AddressId", ParameterValue = a.AddressId },
                    new ParameterInfo() { ParameterName = "Line1", ParameterValue = a.Line1 },
                    new ParameterInfo() { ParameterName = "Line2", ParameterValue = a.Line2 },
                    new ParameterInfo() { ParameterName = "City", ParameterValue = a.City },
                    new ParameterInfo() { ParameterName = "State", ParameterValue = a.State },
                    new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = a.PostalCode },
                    new ParameterInfo() { ParameterName = "Country", ParameterValue = a.Country },
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Address", parameters);
        }

        public static string CreateAddress(Address a)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = a.ProfileId },
                    new ParameterInfo() { ParameterName = "Line1", ParameterValue = a.Line1 },
                    new ParameterInfo() { ParameterName = "Line2", ParameterValue = a.Line2 },
                    new ParameterInfo() { ParameterName = "City", ParameterValue = a.City },
                    new ParameterInfo() { ParameterName = "State", ParameterValue = a.State },
                    new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = a.PostalCode },
                    new ParameterInfo() { ParameterName = "Country", ParameterValue = a.Country },
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Address", parameters);
        }

        public static string UpdateAddressByAddressId(Address address)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "AddressId", ParameterValue = address.AddressId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = address.ProfileId },
                new ParameterInfo() { ParameterName = "Line1", ParameterValue = address.Line1 },
                new ParameterInfo() { ParameterName = "Line2", ParameterValue = address.Line2 },
                new ParameterInfo() { ParameterName = "City", ParameterValue = address.City },
                new ParameterInfo() { ParameterName = "State", ParameterValue = address.State },
                new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = address.PostalCode },
                new ParameterInfo() { ParameterName = "Country", ParameterValue = address.Country },
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Address_By_AddressId", parameters);
        }

        public static string UpdateAddressByProfileId(Address a)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = a.ProfileId },
                    new ParameterInfo() { ParameterName = "Line1", ParameterValue = a.Line1 },
                    new ParameterInfo() { ParameterName = "Line2", ParameterValue = a.Line2 },
                    new ParameterInfo() { ParameterName = "City", ParameterValue = a.City },
                    new ParameterInfo() { ParameterName = "State", ParameterValue = a.State },
                    new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = a.PostalCode },
                    new ParameterInfo() { ParameterName = "Country", ParameterValue = a.Country },
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Address_By_ProfileId", parameters);
        }

        public static Address GetAddressByProfileId(string profileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = profileId },
            };
            return SqlHelper.GetRecord<Address>("dbo.Get_AddressByProfileId", parameters);
        }

        public static List<Address> GetAllAddress()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Address>("dbo.Get_Address", parameters);
        }

        public static Address GetAddressByAddressId(string AddressId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "AddressId", ParameterValue = AddressId },
            };
            return SqlHelper.GetRecord<Address>("dbo.Get_AddressByAddressId", parameters);
        }
    }
}
