﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class NegotiatorProfileControl
    {
        public static string UpdateNegotiatorProfile(NegotiatorProfile p, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "NegotiatorProfileId", ParameterValue = p.NegotiatorProfileId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                    new ParameterInfo() { ParameterName = "Remark", ParameterValue = p.Remark },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                    new ParameterInfo() { ParameterName = "Active", ParameterValue = p.Active }
            };
            return SqlHelper.GetRecord<string>("n.Update_NegotiatorProfile", parameters);
        }

        public static string GetNegotiatorProfileByNegotiatorProfileName(NegotiatorProfile p)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name }
            };
            return SqlHelper.GetRecord<string>("dbo.Get_NegotiatorProfile_By_NegotiatorProfileName", parameters);
        }

        public static string DeleteNegotiatorProfile(string NegotiatorProfileId)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "NegotiatorProfileId", ParameterValue = NegotiatorProfileId }
            };
            return SqlHelper.GetRecord<string>("dbo.Delete_NegotiatorProfile", parameters);
        }

        public static string CreateNegotiatorProfile(NegotiatorProfile p, string id)
        {
            try
            {
                var parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                    new ParameterInfo() { ParameterName = "Remark", ParameterValue = p.Remark },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                    new ParameterInfo() { ParameterName = "Active", ParameterValue = p.Active }
                };
                return SqlHelper.GetRecord<string>("n.Create_NegotiatorProfile", parameters);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<NegotiatorProfile> GetAllNegotiatorProfile()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<NegotiatorProfile>("n.Get_NegotiatorProfile", parameters);
        }

        public static NegotiatorProfile GetNegotiatorProfileByNegotiatorProfileId(string NegotiatorProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "NegotiatorProfileId", ParameterValue = NegotiatorProfileId },
            };
            return SqlHelper.GetRecord<NegotiatorProfile>("n.Get_NegotiatorProfileByNegotiatorProfileId", parameters);
        }
    }
}
