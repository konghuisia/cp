﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class DebtorControl
    {
        public static string UpdateDebtor(Debtor d, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "DebtorId", ParameterValue = d.DebtorId },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = d.ProfileId },
                new ParameterInfo() { ParameterName = "ClaimAmount", ParameterValue = d.ClaimAmount },
                new ParameterInfo() { ParameterName = "StillInBusiness", ParameterValue = d.StillInBusiness },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = d.Remark },
                new ParameterInfo() { ParameterName = "InsolvencyStatus", ParameterValue = d.InsolvencyStatus },
                new ParameterInfo() { ParameterName = "MDIStatus", ParameterValue = d.MDIStatus },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Debtor", parameters);
        }

        public static Debtor CreateDebtor(Debtor d, string id, bool b, string profileId)
        {
            var p = profileId;
            if (!b)
            {
                var parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = d.CompanyName },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = d.ContactNo },
                    new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = d.BusinessRegNo },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
                };
                p = SqlHelper.GetRecord<string>("dbo.Create_DebtorProfile", parameters);
            }

            var parameters2 = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = p },
                new ParameterInfo() { ParameterName = "ClaimAmount", ParameterValue = d.ClaimAmount },
                new ParameterInfo() { ParameterName = "StillInBusiness", ParameterValue = d.StillInBusiness },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = d.Remark },
                new ParameterInfo() { ParameterName = "InsolvencyStatus", ParameterValue = d.InsolvencyStatus },
                new ParameterInfo() { ParameterName = "MDIStatus", ParameterValue = d.MDIStatus },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<Debtor>("dbo.Create_Debtor", parameters2);
        }

        public static Debtor DeleteDebtor(string DebtorId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DebtorId", ParameterValue = DebtorId },
            };
            return SqlHelper.GetRecord<Debtor>("dbo.Delete_DebtorByDebtorId", parameters);
        }

        public static List<Debtor> GetAllDebtor()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Debtor>("dbo.Get_Debtor", parameters);
        }

        public static Debtor GetDebtorByDebtorId(string DebtorId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DebtorId", ParameterValue = DebtorId },
            };
            return SqlHelper.GetRecord<Debtor>("dbo.Get_DebtorByDebtorId", parameters);
        }
        public static List<Debtor> GetDebtorByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecords<Debtor>("dbo.Get_DebtorByRequestId", parameters);
        }
    }
}
