﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClassesLibrary.Classes;
using DataAccessHelper.SqlConnectionHelper;

namespace DatabaseHelper.Control
{
    public class ProfileControl
    {
        public static string UpdateProfile(Profile p)
        {
            //int caseSwitch = p.BusinessType;
            //if (!string.IsNullOrEmpty(p.Name))
            //{
            //    p.Name = p.Name.Replace(" Sdn Bhd", "");
            //    p.Name = p.Name.Replace(" Berhad", "");
            //}

            //switch (caseSwitch)
            //{
            //    case 1:
            //        p.Name += " Sdn Bhd";
            //        break;
            //    case 2:
            //        p.Name += " Berhad";
            //        break;
            //    default:
            //        break;

            //}
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = p.ProfileId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                    new ParameterInfo() { ParameterName = "BusinessType", ParameterValue = p.BusinessType },
                    new ParameterInfo() { ParameterName = "ProfileType", ParameterValue = p.ProfileType },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                    new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = p.BusinessRegNo },
                    new ParameterInfo() { ParameterName = "DMS", ParameterValue = p.DMS },
                    new ParameterInfo() { ParameterName = "Source", ParameterValue = p.Source },
                    new ParameterInfo() { ParameterName = "NatureOfBusiness", ParameterValue = p.NatureOfBusiness },
                    new ParameterInfo() { ParameterName = "Website", ParameterValue = p.Website },
                    new ParameterInfo() { ParameterName = "Remark", ParameterValue = p.Remark },
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Profile", parameters);
        }

        public static string UpdateProfileSubscription(string profileId, int dms, string dmsStatus)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = profileId },
                new ParameterInfo() { ParameterName = "DMS", ParameterValue = dms },
                new ParameterInfo() { ParameterName = "DMSStatus", ParameterValue = dmsStatus }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_ProfileSubscription", parameters);
        }

        public static bool DeleteLawfirmProfile(string profileId)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = profileId }
            };
            return SqlHelper.GetRecord<bool>("lf.Delete_LawFirmProfile", parameters);
        }

        public static string GetProfileByProfileName(Profile p)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name }
            };
            return SqlHelper.GetRecord<string>("dbo.Get_Profile_By_ProfileName", parameters);
        }

        public static string DeleteProfile(string profileId)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = profileId }
            };
            return SqlHelper.GetRecord<string>("dbo.Delete_Profile", parameters);
        }

        public static string CreateProfile(Profile p, string id)
        {
            try
            {
                //int caseSwitch = p.BusinessType;

                //switch (caseSwitch)
                //{
                //    case 1:
                //        p.Name += " Sdn Bhd";
                //        break;
                //    case 2:
                //        p.Name += " Berhad";
                //        break;
                //    default:
                //        break;
                //}

                var parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                    new ParameterInfo() { ParameterName = "BusinessType", ParameterValue = p.BusinessType },
                    new ParameterInfo() { ParameterName = "ProfileType", ParameterValue = p.ProfileType },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                    new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = p.BusinessRegNo },
                    new ParameterInfo() { ParameterName = "DMS", ParameterValue = p.DMS },
                    new ParameterInfo() { ParameterName = "Source", ParameterValue = p.Source },
                    new ParameterInfo() { ParameterName = "NatureOfBusiness", ParameterValue = p.NatureOfBusiness },
                    new ParameterInfo() { ParameterName = "Website", ParameterValue = p.Website },
                    new ParameterInfo() { ParameterName = "Remark", ParameterValue = p.Remark },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
                };
                return SqlHelper.GetRecord<string>("dbo.Create_Profile", parameters);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<Profile> GetAllProfile()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Profile>("dbo.Get_Profile", parameters);
        }

        public static Profile GetProfileByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecord<Profile>("dbo.Get_ProfileByProfileId", parameters);
        }
    }
}
