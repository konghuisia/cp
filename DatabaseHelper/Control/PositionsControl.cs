﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class PositionsControl
    {
        public static List<Positions> GetAllPositions()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Positions>("cfg.Get_AllPositions", parameters);
        }
        //public static Positions DeletePositions(string PositionsId, string id)
        //{
        //    var parameters = new List<ParameterInfo>()
        //    {
        //        new ParameterInfo() { ParameterName = "PositionsId", ParameterValue = PositionsId },
        //        new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id }
        //    };
        //    return SqlHelper.GetRecord<Positions>("cfg.Delete_Positions", parameters);
        //}

        //public static Positions UpdatePositions(Positions Positions, string id)
        //{
        //    var parameters = new List<ParameterInfo>
        //    {
        //        new ParameterInfo() { ParameterName = "PositionsId", ParameterValue = Positions.PositionsId },
        //        new ParameterInfo() { ParameterName = "Name", ParameterValue = Positions.Name },
        //        new ParameterInfo() { ParameterName = "Description", ParameterValue = Positions.Description },
        //        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
        //    };
        //    return SqlHelper.GetRecord<Positions>("cfg.Update_Positions", parameters);
        //}

        //public static Positions CreateNewPositions(Positions Positions, string id)
        //{
        //    var parameters = new List<ParameterInfo>
        //    {
        //        new ParameterInfo() { ParameterName = "Name", ParameterValue = Positions.Name },
        //        new ParameterInfo() { ParameterName = "Description", ParameterValue = Positions.Description },
        //        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
        //    };
        //    return SqlHelper.GetRecord<Positions>("cfg.Create_Positions", parameters);
        //}
        //public static Positions GetPositionsByPositionsId(string PositionsId)
        //{
        //    var parameters = new List<ParameterInfo>()
        //    {
        //            new ParameterInfo() { ParameterName = "PositionsId", ParameterValue = PositionsId },
        //    };
        //    return SqlHelper.GetRecord<Positions>("cfg.Get_PositionsByPositionsId", parameters);
        //}
    }
}
