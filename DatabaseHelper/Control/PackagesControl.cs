﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClassesLibrary.Classes;
using DataAccessHelper.SqlConnectionHelper;

namespace DatabaseHelper.Control
{
    public class PackagesControl
    {
        public static List<Packages> GetPackages()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Packages>("sub.getPackages", parameters);
        }

        public static List<ConditionTypes> GetConditionTypes()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<ConditionTypes>("sub.getServicesConditions", parameters);
        }

        public static List<ConditionTypes> GetConditionTypes(string serviceId)
        {
            var parameters = new List<ParameterInfo>()
            {
                 new ParameterInfo() { ParameterName = "serviceId", ParameterValue = serviceId }
            };
            return SqlHelper.GetRecords<ConditionTypes>("sub.getServicesConditionsWithFilters", parameters);
        }

        public static Packages GetPackagesByPackageId(string packageId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "packageId", ParameterValue = packageId }
            };
            return SqlHelper.GetRecord<Packages>("sub.getPackagesByPackageId", parameters);
        }

        public static PackageServices GetPackageServiceByServiceId(string serviceId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "serviceId", ParameterValue = serviceId }
            };
            return SqlHelper.GetRecord<PackageServices>("sub.getPackageServicesByServiceId", parameters);
        }

        public static List<PackageServiceConditions> GetPackageServiceConditionByServiceId(string serviceId)
        {
            var parameters = new List<ParameterInfo>()
            {
                 new ParameterInfo() { ParameterName = "serviceId", ParameterValue = serviceId }
            };
            return SqlHelper.GetRecords<PackageServiceConditions>("sub.getPackageServicesConditionsByServiceId", parameters);
        }


        public static List<PackageServices> GetPackageServices()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<PackageServices>("sub.getPackageServices", parameters);
        }

        public static List<PackageServices> GetPackageServicesByPackageId(string packageId)
        {
            var parameters = new List<ParameterInfo>();
            {
                new ParameterInfo() { ParameterName = "packageId", ParameterValue = packageId };
            }
            return SqlHelper.GetRecords<PackageServices>("sub.getPackageServicesByPackageId", parameters);
        }

        public static string CreateNewPackage(Packages package, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "packageName", ParameterValue = package.PackageName },
                new ParameterInfo() { ParameterName = "packageDesc", ParameterValue = package.PackageDesc },
                new ParameterInfo() { ParameterName = "subfee", ParameterValue = package.SubscriptionFee_RM},
                new ParameterInfo() { ParameterName = "credit", ParameterValue = package.CreditGiven },
                //new ParameterInfo() { ParameterName = "multiPurchase", ParameterValue = package.multiplePurchase },
                new ParameterInfo() { ParameterName = "createBy", ParameterValue = id },
                new ParameterInfo() { ParameterName = "validity", ParameterValue = package.Validity_Months },
                new ParameterInfo() { ParameterName = "status", ParameterValue = package.status },
                new ParameterInfo() { ParameterName = "subServices", ParameterValue = package.SubServices }
            };
            return SqlHelper.GetRecord<string>("sub.addPackages", parameters);
        }


        public static string CreateNewPackageService(PackageServices service, PackageServiceConditions condition, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "condTypeId", ParameterValue = condition.CondTypeId },
                new ParameterInfo() { ParameterName = "priceRM", ParameterValue = condition.Price_RM},
                new ParameterInfo() { ParameterName = "priceSC", ParameterValue = condition.Price_SC},
                new ParameterInfo() { ParameterName = "condTypeId2", ParameterValue = condition.CondTypeId2 },
                new ParameterInfo() { ParameterName = "priceRM2", ParameterValue = condition.Price_RM2 },
                new ParameterInfo() { ParameterName = "priceSC2", ParameterValue = condition.Price_SC2 },
                new ParameterInfo() { ParameterName = "serviceName", ParameterValue = service.ServiceName },
                new ParameterInfo() { ParameterName = "status", ParameterValue = service.Status },
                new ParameterInfo() { ParameterName = "CreateBy", ParameterValue = id },
                new ParameterInfo() { ParameterName = "amtOfCondition", ParameterValue = service.AmtOfConditionLevel },
                new ParameterInfo() { ParameterName = "serviceDesc", ParameterValue = service.ServiceDesc }
            };
            return SqlHelper.GetRecord<string>("sub.addPackageServiceCondition", parameters);
        }



        public static string UpdatePackage(Packages package, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "packageId", ParameterValue = package.PackageId },
                new ParameterInfo() { ParameterName = "packageName", ParameterValue = package.PackageName },
                new ParameterInfo() { ParameterName = "packageDesc", ParameterValue = package.PackageDesc },
                new ParameterInfo() { ParameterName = "subfee", ParameterValue = package.SubscriptionFee_RM},
                new ParameterInfo() { ParameterName = "credit", ParameterValue = package.CreditGiven },
                //new ParameterInfo() { ParameterName = "multiPurchase", ParameterValue = package.multiplePurchase },
                new ParameterInfo() { ParameterName = "updateBy", ParameterValue = id },
                new ParameterInfo() { ParameterName = "validity", ParameterValue = package.Validity_Months },
                new ParameterInfo() { ParameterName = "status", ParameterValue = package.status },
                new ParameterInfo() { ParameterName = "subServices", ParameterValue = package.SubServices }
            };
            return SqlHelper.GetRecord<string>("sub.updatePackage", parameters);
        }

        public static string UpdatePackageServices(PackageServices services, ConditionTypes condition, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                //new ParameterInfo() { ParameterName = "packageId", ParameterValue = package.PackageId },
                //new ParameterInfo() { ParameterName = "packageName", ParameterValue = package.PackageName },
                //new ParameterInfo() { ParameterName = "packageDesc", ParameterValue = package.PackageDesc },
                //new ParameterInfo() { ParameterName = "subfee", ParameterValue = package.SubscriptionFee_RM},
                //new ParameterInfo() { ParameterName = "credit", ParameterValue = package.CreditGiven },
                ////new ParameterInfo() { ParameterName = "multiPurchase", ParameterValue = package.multiplePurchase },
                //new ParameterInfo() { ParameterName = "updateBy", ParameterValue = id },
                //new ParameterInfo() { ParameterName = "validity", ParameterValue = package.Validity_Months },
                //new ParameterInfo() { ParameterName = "status", ParameterValue = package.status },
                //new ParameterInfo() { ParameterName = "subServices", ParameterValue = package.SubServices }
            };
            return SqlHelper.GetRecord<string>("sub.updatePackage", parameters);
        }

        public static string DeletePackage(string packageId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "packageId", ParameterValue = packageId },
                new ParameterInfo() { ParameterName = "deleteBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<string>("sub.deletePackage", parameters);
        }

        public static string DeleteConditionType(string CondTypeId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "condTypeId", ParameterValue = CondTypeId },
                new ParameterInfo() { ParameterName = "actionBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<string>("sub.deleteConditionType", parameters);
        }

        public static string CreateNewConditionType(string name, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "conditionName", ParameterValue = name },
                new ParameterInfo() { ParameterName = "actionBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<string>("sub.addConditionType", parameters);
        }

        public static string UpdateConditionType(ConditionTypes cond, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "condTypeId", ParameterValue = cond.CondTypeId },
                new ParameterInfo() { ParameterName = "conditionName", ParameterValue = cond.CondName },
                new ParameterInfo() { ParameterName = "actionBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<string>("sub.updateServicesCondition", parameters);
        }

    }
}
