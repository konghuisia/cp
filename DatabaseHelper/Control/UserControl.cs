﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class UserControl
    {
        public static List<User> GetAllUser()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<User>("dbo.Get_User", parameters);
        }

        public static User FindByNameAsync(string userName)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = userName }
            };
            return SqlHelper.GetRecord<User>("dbo.Get_UserByName", parameters);
        }

        public static User GetUserByProfileId(string profileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = profileId }
            };
            return SqlHelper.GetRecord<User>("dbo.Get_UserByProfileId", parameters);
        }

        public static string UpdateUserByUserId(User user, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = user.UserId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo() { ParameterName = "Fullname", ParameterValue = user.Fullname },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo() { ParameterName = "Position", ParameterValue = user.Position },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = user.ProfileId },
                new ParameterInfo() { ParameterName = "Active", ParameterValue = user.Active },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = user.CreatedBy },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = user.Remark }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_UserByUserId", parameters);
        }

        public static bool ChangePassword(User user, string pass)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId }
            };
            var u = SqlHelper.GetRecord<User>("dbo.Get_UserByUserId", parameters);

            try
            {
                if (!HashSalt.VerifyPassword(user.Password, u.Password, u.Salt))
                {
                    throw new Exception("Current password incorrect.");
                }
                else
                {
                    HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, pass);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                        new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                        new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                        new ParameterInfo(){ ParameterName = "UpdatedBy", ParameterValue = user.CreatedBy }
                    };

                    return SqlHelper.ExecuteQuery("dbo.Update_UserPassword", parameters) > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }

        public static bool DeleteUser(string userId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id}
            };
            return SqlHelper.GetRecord<bool>("dbo.Delete_User", parameters);
        }

        public static bool ResetUserPassword(string userId, string id)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, "Corporate#2019");
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id},
                new ParameterInfo() { ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo() { ParameterName = "Salt", ParameterValue = hashSalt.Salt },
            };
            return SqlHelper.GetRecord<bool>("dbo.Reset_UserPassword", parameters);
        }

        public static User GetUserByUserid(string userId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            return SqlHelper.GetRecord<User>("dbo.Get_UserByUserId", parameters);
        }

        public static bool CheckUserName(string name)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = name }
            };
            return SqlHelper.GetRecord<bool>("dbo.Check_Username", parameters);
        }

        public static string CreateNewUser(User user, string id)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, "Corporate#2019");
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo() { ParameterName = "Fullname", ParameterValue = user.Fullname },
                new ParameterInfo() { ParameterName = "Position", ParameterValue = user.Position },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo() { ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo() { ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = user.ProfileId },
                new ParameterInfo() { ParameterName = "Active", ParameterValue = user.Active },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = user.CreatedBy },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = user.Remark }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_User", parameters);
        }
    }
}
