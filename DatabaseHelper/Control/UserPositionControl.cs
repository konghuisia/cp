﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class UserPositionControl
    {
        public static List<UserPosition> GetAllUserPosition()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<UserPosition>("cfg.Get_UserPosition", parameters);
        }
        public static UserPosition DeleteUserPosition(string UserPositionId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "UserPositionId", ParameterValue = UserPositionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<UserPosition>("cfg.Delete_UserPosition", parameters);
        }

        public static UserPosition UpdateUserPosition(UserPosition UserPosition, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserPositionId", ParameterValue = UserPosition.UserPositionId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = UserPosition.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = UserPosition.Description },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<UserPosition>("cfg.Update_UserPosition", parameters);
        }

        public static UserPosition CreateNewUserPosition(UserPosition UserPosition, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = UserPosition.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = UserPosition.Description },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<UserPosition>("cfg.Create_UserPosition", parameters);
        }
        public static UserPosition GetUserPositionByUserPositionId(string UserPositionId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "UserPositionId", ParameterValue = UserPositionId },
            };
            return SqlHelper.GetRecord<UserPosition>("cfg.Get_UserPositionByUserPositionId", parameters);
        }
    }
}
