﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class AppointmentControl
    {
        public static string UpdateAppointment(Appointment d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "AppointmentId", ParameterValue = d.AppointmentId },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = d.Status },
                new ParameterInfo() { ParameterName = "AppointmentType", ParameterValue = d.AppointmentType },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = d.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = d.Description },
                new ParameterInfo() { ParameterName = "Date", ParameterValue = d.Date },
                new ParameterInfo() { ParameterName = "EndDate", ParameterValue = d.EndDate },
                new ParameterInfo() { ParameterName = "IsAllDay", ParameterValue = d.IsAllDay },
                new ParameterInfo() { ParameterName = "Venue", ParameterValue = d.Venue },
                new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = d.DocumentId },
                new ParameterInfo() { ParameterName = "CommentRemarkId", ParameterValue = d.CommentRemarkId }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Appointment", parameters);
        }

        public static string CreateAppointment(Appointment d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = d.Status },
                new ParameterInfo() { ParameterName = "AppointmentType", ParameterValue = d.AppointmentType },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = d.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = d.Description },
                new ParameterInfo() { ParameterName = "Date", ParameterValue = d.Date },
                new ParameterInfo() { ParameterName = "EndDate", ParameterValue = d.EndDate },
                new ParameterInfo() { ParameterName = "IsAllDay", ParameterValue = d.IsAllDay },
                new ParameterInfo() { ParameterName = "Venue", ParameterValue = d.Venue },
                new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = d.DocumentId },
                new ParameterInfo() { ParameterName = "CommentRemarkId", ParameterValue = d.CommentRemarkId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = d.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Appointment", parameters);
        }

        public static bool DeleteAppointmentByRequestId(string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            return SqlHelper.GetRecord<bool>("dbo.Delete_AppointmentByRequestId", parameters);
        }

        public static bool UpdateAppointmentStatus(string requestId, int stage)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = stage }
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_AppointmentStatus", parameters);
        }

        public static Appointment DeleteAppointment(string AppointmentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "AppointmentId", ParameterValue = AppointmentId },
            };
            return SqlHelper.GetRecord<Appointment>("dbo.Delete_AppointmentByAppointmentId", parameters);
        }

        public static List<Appointment> GetAllAppointment()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Appointment>("dbo.Get_Appointment", parameters);
        }

        public static Appointment GetAppointmentByAppointmentId(string AppointmentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "AppointmentId", ParameterValue = AppointmentId },
            };
            return SqlHelper.GetRecord<Appointment>("dbo.Get_AppointmentByAppointmentId", parameters);
        }
        public static List<Appointment> GetAppointmentByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecords<Appointment>("dbo.Get_AppointmentByRequestId", parameters);
        }
    }
}
