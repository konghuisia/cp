﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class RequestControl
    {
        public static string UpdateRequest(Request r)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = r.RequestId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = r.ProfileId },
                new ParameterInfo() { ParameterName = "AddressId", ParameterValue = r.AddressId },
                new ParameterInfo() { ParameterName = "PersonId", ParameterValue = r.PersonId },
                new ParameterInfo() { ParameterName = "CommentRemarkId", ParameterValue = r.CommentRemarkId },
                new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = r.DocumentId },
                new ParameterInfo() { ParameterName = "LeadId", ParameterValue = r.LeadId },
                new ParameterInfo() { ParameterName = "Approval1User", ParameterValue = r.Approval1User },
                new ParameterInfo() { ParameterName = "AppointmentId", ParameterValue = r.AppointmentId },
                new ParameterInfo() { ParameterName = "Deleted", ParameterValue = r.Deleted },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = r.Status },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = r.Stage },
                new ParameterInfo() { ParameterName = "LawFirm", ParameterValue = r.LawFirm },
                new ParameterInfo() { ParameterName = "Negotiator", ParameterValue = r.NegotiatorId },
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Request", parameters);
        }

        public static bool CreateRequestApprovalUser(string AU1, string AU2, string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "Approval1User", ParameterValue = AU1 },
                new ParameterInfo() { ParameterName = "Approval2User", ParameterValue = AU2 },
            };
            return SqlHelper.GetRecord<bool>("dbo.Create_RequestApprovalUser", parameters);
        }

        public static bool UpdateAllRequestApprovalToFalse(string requestId, string id, string status)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = id },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = status }
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_AllRequestApprovalToFalse", parameters);
        }

        public static bool CreateRequestDisputeUser(object DU1, string DU2, object requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "Dispute1User", ParameterValue = DU1 },
                new ParameterInfo() { ParameterName = "Dispute2User", ParameterValue = DU2 },
            };
            return SqlHelper.GetRecord<bool>("dbo.Create_RequestDisputeUser", parameters);
        }

        public static bool UpdateAllRequestDecisionToFalse(string requestId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = id }
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_AllRequestDecisionToFalse", parameters);
        }

        public static bool UpdateRequestLastUpdateActionTime(string requestId, string id, string p, int stage)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = id },
                new ParameterInfo() { ParameterName = "PositionId", ParameterValue = p },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = stage },
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_RequestLastUpdateActionTime", parameters);
        }

        public static string CreateSubscriptionRequest(SubscriptionRequest request)
        {
            throw new NotImplementedException();
        }

        public static bool DeleteRequest(string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            return SqlHelper.GetRecord<bool>("dbo.Delete_Request", parameters);
        }

        public static bool CreateRequestDecisionUser(string DU1, string DU2, string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "Decision1User", ParameterValue = DU1 },
                new ParameterInfo() { ParameterName = "Decision2User", ParameterValue = DU2 },
            };
            return SqlHelper.GetRecord<bool>("dbo.Create_RequestDecisionUser", parameters);
        }

        public static bool UpdateRequestDecision(string id, string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "ApprovalId", ParameterValue = id },
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_RequestDecisionApproval", parameters);
        }

        public static bool UpdateRequestDispute(string id, string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "ApprovalId", ParameterValue = id },
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_RequestDisputeApproval", parameters);
        }

        public static bool UpdateRequestStage(string requestId, int stage, int previousStage)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = stage },
                new ParameterInfo() { ParameterName = "PreviousStage", ParameterValue = previousStage }
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_RequestStage", parameters);
        }

        public static bool UpdateRequestApproval(string id, string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "ApprovalId", ParameterValue = id },
            };
            return SqlHelper.GetRecord<bool>("dbo.Update_RequestApproval", parameters);
        }

        public static string CreateRequest(Request r)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = r.RequestId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = r.ProfileId },
                new ParameterInfo() { ParameterName = "AddressId", ParameterValue = r.AddressId },
                new ParameterInfo() { ParameterName = "PersonId", ParameterValue = r.PersonId },
                new ParameterInfo() { ParameterName = "CommentRemarkId", ParameterValue = r.CommentRemarkId },
                new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = r.DocumentId },
                new ParameterInfo() { ParameterName = "LeadId", ParameterValue = r.LeadId },
                new ParameterInfo() { ParameterName = "AppointmentId", ParameterValue = r.AppointmentId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = r.CreatedBy },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = r.Type },
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Request", parameters);
        }


        public static List<Request> GetAllRequest()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Request>("dbo.Get_Request", parameters);
        }

        public static Request GetRequestByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecord<Request>("dbo.Get_RequestByRequestId", parameters);
        }
    }
}
