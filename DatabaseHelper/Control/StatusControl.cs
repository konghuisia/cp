﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class StatusControl
    {
        public static IList<Status> StatusList()
        {
            return SqlHelper.GetRecords<Status>("cfg.Get_Status", new List<ParameterInfo>());
        }
    }
}
