﻿using DataAccessHelper.SqlConnectionHelper;
using System;
using System.Collections.Generic;
using ObjectClassesLibrary.Classes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class BlackListingRecordControl
    {
        public static bool CreateBlackListingRecord(BlackListingRecord b, string id, string ProfileId)
        {
            try
            {
                var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = b.DocumentId },
                        new ParameterInfo() { ParameterName = "Creditor1", ParameterValue = b.Creditor1 },
                        new ParameterInfo() { ParameterName = "StatementDate", ParameterValue = b.StatementDate?? null },
                        new ParameterInfo() { ParameterName = "ClaimAmount", ParameterValue = b.ClaimAmount },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                    };
                SqlHelper.ExecuteQuery("dbo.Create_BlackListingRecord", parameters);
                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
        public static bool UpdateBlackListingRecord(IList<BlackListingRecord> br, string id, string ProfileId)
        {
            try
            {
                foreach (var b in br)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "BlackListingRecordId", ParameterValue = b.BlackListingRecordId },
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = b.DocumentId },
                        new ParameterInfo() { ParameterName = "Creditor1", ParameterValue = b.Creditor1 },
                        new ParameterInfo() { ParameterName = "StatementDate", ParameterValue = b.StatementDate?? null },
                        new ParameterInfo() { ParameterName = "ClaimAmount", ParameterValue = b.ClaimAmount },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                    };
                    SqlHelper.ExecuteQuery("dbo.Create_BlackListingRecord", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }

        public static BlackListingRecord GetBlackListingRecordByBlackListingRecordId(string BlackListingRecordId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "BlackListingRecordId", ParameterValue = BlackListingRecordId },
            };
            return SqlHelper.GetRecord<BlackListingRecord>("dbo.Get_BlackListingRecordByBlacklistingRecordId", parameters);
        }

        public static List<BlackListingRecord> GetAllBlackListingRecord()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<BlackListingRecord>("dbo.Get_BlackListingRecord", parameters);
        }
        public static List<BlackListingRecord> GetBlackListingRecordByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecords<BlackListingRecord>("dbo.Get_BlackListingRecordByProfileId", parameters);
        }
    }
}
