﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class SopRequestsControl
    {
        public static IList<SopRequests> SopRequestsList()
        {
            return SqlHelper.GetRecords<SopRequests>("sop.Get_SopRequests", new List<ParameterInfo>());
        }

        public static string UpdateSopRequests(SopRequests sopRequests, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "SopRequestid", ParameterValue = sopRequests.SopRequestid },
                new ParameterInfo(){ ParameterName = "SopId", ParameterValue = sopRequests.SopId },
                new ParameterInfo(){ ParameterName = "RequestId", ParameterValue = sopRequests.RequestId },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id },
                new ParameterInfo(){ ParameterName = "Stage", ParameterValue = sopRequests.Stage },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = sopRequests.Status }
            };

            return SqlHelper.GetRecord<string>("sop.Update_SopRequests", parameters);
        }

        public static string CreateSopRequests(SopRequests sopRequests, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "SopRequestid", ParameterValue = sopRequests.SopRequestid },
                new ParameterInfo(){ ParameterName = "SopId", ParameterValue = sopRequests.SopId },
                new ParameterInfo(){ ParameterName = "RequestId", ParameterValue = sopRequests.RequestId },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id }
            };

            return SqlHelper.GetRecord<string>("sop.Create_SopRequests", parameters);
        }

        public static bool UpdateSopRequestsStatus(string sopRequestId, int status)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "SopRequestId", ParameterValue = sopRequestId },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = status },
            };

            return SqlHelper.GetRecord<bool>("sop.Update_SopRequestsStatus", parameters);
        }

        public static SopRequests GetSopRequestsBySopRequestId(string sopRequestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "SopRequestId", ParameterValue = sopRequestId },
            };

            return SqlHelper.GetRecord<SopRequests>("sop.Get_SopRequestsBySopRequestId", parameters);
        }

        public static List<SopRequests> GetSopRequestsByRequestId(string requestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "RequestId", ParameterValue = requestId },
            };

            return SqlHelper.GetRecords<SopRequests>("sop.Get_SopRequestsByRequestId", parameters);
        }
    }
}
