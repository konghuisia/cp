﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class ShowInSelectControl
    {
        public static List<ShowInSelect> GetAllShowInSelect()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<ShowInSelect>("dbo.Get_ShowInSelect", parameters);
        }
        public static ShowInSelect DeleteShowInSelect(string ShowInSelectId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "ShowInSelectId", ParameterValue = ShowInSelectId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<ShowInSelect>("dbo.Delete_ShowInSelect", parameters);
        }

        public static string UpdateShowInSelect(ShowInSelect showInSelect, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "ShowInSelectId", ParameterValue = showInSelect.ShowInSelectId },
                new ParameterInfo() { ParameterName = "ShowInSelectName", ParameterValue = showInSelect.ShowInSelectName },
                new ParameterInfo() { ParameterName = "DocumentTypeId", ParameterValue = showInSelect.DocumentTypeId },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = showInSelect.Stage },
                new ParameterInfo() { ParameterName = "Required", ParameterValue = showInSelect.Required },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id },
            };
            return SqlHelper.GetRecord<string>("dbo.Update_ShowInSelect", parameters);
        }

        public static string CreateShowInSelect(ShowInSelect showInSelect, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "ShowInSelectId", ParameterValue = showInSelect.ShowInSelectId },
                new ParameterInfo() { ParameterName = "ShowInSelectName", ParameterValue = showInSelect.ShowInSelectName },
                new ParameterInfo() { ParameterName = "DocumentTypeId", ParameterValue = showInSelect.DocumentTypeId },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = showInSelect.Stage },
                new ParameterInfo() { ParameterName = "Required", ParameterValue = showInSelect.Required },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
            };
            return SqlHelper.GetRecord<string>("dbo.Create_ShowInSelect", parameters);
        }

        //public static ShowInSelect UpdateShowInSelect(ShowInSelect ShowInSelect)
        //{
        //    var parameters = new List<ParameterInfo>
        //    {
        //        new ParameterInfo() { ParameterName = "ShowInSelectId", ParameterValue = ShowInSelect.ShowInSelectId },
        //        new ParameterInfo() { ParameterName = "Name", ParameterValue = ShowInSelect.Name },
        //        new ParameterInfo() { ParameterName = "Required", ParameterValue = ShowInSelect.Required }
        //    };
        //    return SqlHelper.GetRecord<ShowInSelect>("dbo.Update_ShowInSelect", parameters);
        //}

        //public static ShowInSelect CreateNewShowInSelect(ShowInSelect ShowInSelect)
        //{
        //    var parameters = new List<ParameterInfo>
        //    {
        //        new ParameterInfo() { ParameterName = "Name", ParameterValue = ShowInSelect.Name },
        //        new ParameterInfo() { ParameterName = "Required", ParameterValue = ShowInSelect.Required },
        //        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = ShowInSelect.CreatedBy }
        //    };
        //    return SqlHelper.GetRecord<ShowInSelect>("dbo.Create_ShowInSelect", parameters);
        //}
    }
}
