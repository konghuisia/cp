﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class AppointmentTypeControl
    {
        public static List<AppointmentType> GetAllAppointmentType()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<AppointmentType>("cfg.Get_AppointmentType", parameters);
        }
        public static AppointmentType DeleteAppointmentType(string AppointmentTypeId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "AppointmentTypeId", ParameterValue = AppointmentTypeId },
            };
            return SqlHelper.GetRecord<AppointmentType>("cfg.Delete_AppointmentType", parameters);
        }

        public static AppointmentType UpdateAppointmentType(AppointmentType AppointmentType)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "AppointmentTypeId", ParameterValue = AppointmentType.AppointmentTypeId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = AppointmentType.Name },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = AppointmentType.CreatedBy }
            };
            return SqlHelper.GetRecord<AppointmentType>("cfg.Update_AppointmentType", parameters);
        }

        public static AppointmentType CreateNewAppointmentType(AppointmentType AppointmentType)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = AppointmentType.Name },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = AppointmentType.CreatedBy }
            };
            return SqlHelper.GetRecord<AppointmentType>("cfg.Create_AppointmentType", parameters);
        }
    }
}
