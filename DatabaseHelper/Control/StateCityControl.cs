﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class StateCityControl
    {
        public static IList<City> CityList()
        {
            return SqlHelper.GetRecords<City>("cfg.Get_City", new List<ParameterInfo>());
        }
        public static IList<State> StateList()
        {
            return SqlHelper.GetRecords<State>("cfg.Get_State", new List<ParameterInfo>());
        }
    }
}
