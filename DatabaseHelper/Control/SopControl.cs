﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class SopControl
    {
        public static IList<Sop> SopList()
        {
            return SqlHelper.GetRecords<Sop>("sop.Get_Sop", new List<ParameterInfo>());
        }

        public static IList<Sop> SopList(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                 new ParameterInfo() { ParameterName = "requestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecords<Sop>("sop.Get_SopBasedOnRequestId", parameters);
        }
    }
}
