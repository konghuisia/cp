﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class SopShowInSelectControl
    {
        public static List<SopShowInSelect> GetAllSopShowInSelect()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<SopShowInSelect>("sop.Get_SopShowInSelect", parameters);
        }
        public static SopShowInSelect DeleteSopShowInSelect(string SopShowInSelectId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "SopShowInSelectId", ParameterValue = SopShowInSelectId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<SopShowInSelect>("sop.Delete_SopShowInSelect", parameters);
        }

        public static string UpdateSopShowInSelect(SopShowInSelect showInSelect, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "SopShowInSelectId", ParameterValue = showInSelect.ShowInSelectId },
                new ParameterInfo() { ParameterName = "SopId", ParameterValue = showInSelect.SopId },
                new ParameterInfo() { ParameterName = "SopShowInSelectName", ParameterValue = showInSelect.ShowInSelectName },
                new ParameterInfo() { ParameterName = "DocumentTypeId", ParameterValue = showInSelect.DocumentTypeId },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = showInSelect.Stage },
                new ParameterInfo() { ParameterName = "Required", ParameterValue = showInSelect.Required },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id },
            };
            return SqlHelper.GetRecord<string>("sop.Update_SopShowInSelect", parameters);
        }

        public static string CreateSopShowInSelect(SopShowInSelect showInSelect, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "SopShowInSelectId", ParameterValue = showInSelect.ShowInSelectId },
                new ParameterInfo() { ParameterName = "SopId", ParameterValue = showInSelect.SopId },
                new ParameterInfo() { ParameterName = "SopShowInSelectName", ParameterValue = showInSelect.ShowInSelectName },
                new ParameterInfo() { ParameterName = "DocumentTypeId", ParameterValue = showInSelect.DocumentTypeId },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = showInSelect.Stage },
                new ParameterInfo() { ParameterName = "Required", ParameterValue = showInSelect.Required },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
            };
            return SqlHelper.GetRecord<string>("sop.Create_SopShowInSelect", parameters);
        }
        
        public static List<SopShowInSelect> GetSopShowInSelectBySopId(string SopId)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "SopId", ParameterValue = SopId }
            };
            return SqlHelper.GetRecords<SopShowInSelect>("sop.Get_SopShowInSelectBySopId", parameters);
        }
    }
}
