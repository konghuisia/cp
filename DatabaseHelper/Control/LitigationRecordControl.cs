﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class LitigationRecordControl
    {
        public static bool CreateLitigationRecord(LitigationRecord l, string id, string ProfileId)
        {
            try
            {
                var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = l.DocumentId },
                        new ParameterInfo() { ParameterName = "Plaintiff", ParameterValue = l.Plaintiff },
                        new ParameterInfo() { ParameterName = "Defendant", ParameterValue = l.Defendant },
                        new ParameterInfo() { ParameterName = "ClaimAmount", ParameterValue = l.ClaimAmount },
                        new ParameterInfo() { ParameterName = "StatusOfCaseId", ParameterValue = l.StatusOfCaseId },
                        new ParameterInfo() { ParameterName = "HearingDate", ParameterValue = l.HearingDate },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
                    };
                SqlHelper.ExecuteQuery("dbo.Create_LitigationRecord", parameters);
                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
        public static bool UpdateLitigationRecord(IList<LitigationRecord> lr, string id, string ProfileId)
        {
            try
            {
                foreach (var l in lr)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "LitigationRecordId", ParameterValue = l.LitigationRecordId },
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = l.DocumentId },
                        new ParameterInfo() { ParameterName = "Plaintiff", ParameterValue = l.Plaintiff },
                        new ParameterInfo() { ParameterName = "Defendant", ParameterValue = l.Defendant },
                        new ParameterInfo() { ParameterName = "ClaimAmount", ParameterValue = l.ClaimAmount },
                        new ParameterInfo() { ParameterName = "StatusOfCase1", ParameterValue = l.StatusOfCaseId },
                        new ParameterInfo() { ParameterName = "HearingDate", ParameterValue = l.HearingDate },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
                    };
                    SqlHelper.ExecuteQuery("dbo.Create_LitigationRecord", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
        public static LitigationRecord GetLitigationRecordByLitigationRecordId(string LitigationRecordId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "LitigationRecordId", ParameterValue = LitigationRecordId },
            };
            return SqlHelper.GetRecord<LitigationRecord>("dbo.Get_LitigationRecordByLitigationRecordId", parameters);
        }

        public static List<LitigationRecord> GetAllLitigationRecord()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<LitigationRecord>("dbo.Get_LitigationRecord", parameters);
        }
        public static List<LitigationRecord> GetLitigationRecordByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecords<LitigationRecord>("dbo.Get_LitigationRecordByProfileId", parameters);
        }
    }
}
