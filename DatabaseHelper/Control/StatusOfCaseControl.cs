﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class StatusOfCaseControl
    {
        public static string UpdateStatusOfCase(StatusOfCase a)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = a.Name }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_StatusOfCase", parameters);
        }

        public static string CreateStatusOfCase(StatusOfCase a)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = a.Name },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = a.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_StatusOfCase", parameters);
        }

        public static StatusOfCase GetStatusOfCaseByStatusOfCaseId(string StatusOfCaseId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "StatusOfCaseId", ParameterValue = StatusOfCaseId },
            };
            return SqlHelper.GetRecord<StatusOfCase>("dbo.Get_StatusOfCaseByStatusOfCaseId", parameters);
        }

        public static List<StatusOfCase> GetAllStatusOfCase()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<StatusOfCase>("dbo.Get_StatusOfCase", parameters);
        }
    }
}
