﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClassesLibrary.Classes;
using DataAccessHelper.SqlConnectionHelper;

namespace DatabaseHelper.Control
{
    public class SubscriptionsControl
    {
        public static List<Subscriptions> GetSubscriptions()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Subscriptions>("sub.GetSubscriptions", parameters);
        }

        public static List<RedemptionHistory> GetRedemptionHistories(string profileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "profileId", ParameterValue = profileId },
            };
            return SqlHelper.GetRecords<RedemptionHistory>("sub.getRedemptionHistory", parameters);
        }

        public static Subscriptions GetSubscriptionByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecord<Subscriptions>("sub.GetSubscriptionsByProfileId", parameters);
        }

        public static SubscriptionRequest GetSubscriptionRequestByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecord<SubscriptionRequest>("sub.getSubscriptionRequestsByRequestId", parameters);
        }

        public static string CreateSubscription(Subscriptions s, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = s.RequestId },
                new ParameterInfo() { ParameterName = "Approver1_Id", ParameterValue = s.Approver1_Id },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                new ParameterInfo() { ParameterName = "packageId", ParameterValue =  s.PackageId},
                new ParameterInfo() { ParameterName = "profileId", ParameterValue = s.ProfileId },
            };
            return SqlHelper.GetRecord<string>("sub.CreateSubscription", parameters);
        }

        public static List<SubscriptionRequest> GetSubscriptionRequest(int status, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "status", ParameterValue = status },
                new ParameterInfo() { ParameterName = "approver", ParameterValue = id },
            };
            return SqlHelper.GetRecords<SubscriptionRequest>("sub.getSubscriptionRequests", parameters);
        }

        public static List<SubscriptionStatus> GetSubscriptionStatus()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<SubscriptionStatus>("sub.getSubscriptionStatus", parameters);
        }
    }
}
