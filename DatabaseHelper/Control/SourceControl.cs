﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class SourceControl
    {
        public static string UpdateSource(Source s)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = s.Name }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Source", parameters);
        }

        public static string CreateSource(Source s)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = s.Name },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = s.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Source", parameters);
        }

        public static Source GetSourceBySourceId(string SourceId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "SourceId", ParameterValue = SourceId },
            };
            return SqlHelper.GetRecord<Source>("dbo.Get_SourceBySourceId", parameters);
        }

        public static List<Source> GetAllSource()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Source>("dbo.Get_Source", parameters);
        }
    }
}
