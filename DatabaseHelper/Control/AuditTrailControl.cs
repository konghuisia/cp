﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class AuditTrailControl
    {
        public static string CreateAuditTrail(string RequestId, string UserId, string Type, string Action, string Comment, int stage)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = UserId },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = Type },
                new ParameterInfo() { ParameterName = "Action", ParameterValue = Action },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = Comment },
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = stage },
            };
            return SqlHelper.GetRecord<string>("dbo.Create_AuditTrail", parameters);
        }
        public static List<AuditTrail> GetAllAuditTrail()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<AuditTrail>("dbo.Get_AuditTrail", parameters);
        }
        public static List<AuditTrail> GetAuditTrailByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecords<AuditTrail>("dbo.Get_AuditTrailByRequestId", parameters);
        }
        public static string CreateProfileAuditTrail(string ProfileId, string UserId, string Type, string Action, string Comment)
        {
            try
            {
                var parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = UserId },
                    new ParameterInfo() { ParameterName = "Type", ParameterValue = Type },
                    new ParameterInfo() { ParameterName = "Action", ParameterValue = Action },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = Comment },
                };
                return SqlHelper.GetRecord<string>("dbo.Create_ProfileAuditTrail", parameters);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static List<ProfileAuditTrail> GetAllProfileAuditTrail()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<ProfileAuditTrail>("dbo.Get_ProfileAuditTrail", parameters);
        }
        public static List<ProfileAuditTrail> GetProfileAuditTrailByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecords<ProfileAuditTrail>("dbo.Get_ProfileAuditTrailByProfileId", parameters);
        }
        public static List<SubscriptionAuditTrail> GetSubscriptionAuditTrailsByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecords<SubscriptionAuditTrail>("dbo.Get_ProfileAuditTrailByProfileId", parameters);
        }
    }
}
