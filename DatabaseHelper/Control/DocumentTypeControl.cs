﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class DocumentTypeControl
    {
        public static List<DocumentType> GetAllDocumentType()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<DocumentType>("cfg.Get_DocumentType", parameters);
        }
        public static DocumentType DeleteDocType(string docTypeId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "DocumentTypeId", ParameterValue = docTypeId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id },
            };
            return SqlHelper.GetRecord<DocumentType>("cfg.Delete_DocumentType", parameters);
        }

        public static DocumentType UpdateDocType(DocumentType docType, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "DocumentTypeId", ParameterValue = docType.DocumentTypeId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = docType.Name },
                new ParameterInfo() { ParameterName = "Required", ParameterValue = docType.Required },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id },
            };
            return SqlHelper.GetRecord<DocumentType>("cfg.Update_DocumentType", parameters);
        }

        public static DocumentType CreateNewDocType(DocumentType docType, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = docType.Name },
                new ParameterInfo() { ParameterName = "Required", ParameterValue = docType.Required },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<DocumentType>("cfg.Create_DocumentType", parameters);
        }
    }
}
