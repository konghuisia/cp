﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class SopStageControl
    {
        public static IList<SopStage> SopStageList()
        {
            return SqlHelper.GetRecords<SopStage>("sop.Get_SopStage", new List<ParameterInfo>());
        }

        public static string UpdateSopStage(SopStage sopRequests, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "SopId", ParameterValue = sopRequests.SopId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = sopRequests.Name },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id }
            };

            return SqlHelper.GetRecord<string>("sop.Update_SopStage", parameters);
        }

        public static string CreateSopStage(SopStage sopRequests, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "SopId", ParameterValue = sopRequests.SopId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = sopRequests.Name },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id }
            };

            return SqlHelper.GetRecord<string>("sop.Create_SopStage", parameters);
        }

        public static List<SopStage> GetSopStageBySopId(string sopId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "SopId", ParameterValue = sopId },
            };

            return SqlHelper.GetRecords<SopStage>("sop.Get_SopStageBySopId", parameters);
        }
    }
}
