﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class LawFirmProfileControl
    {
        public static string UpdateLawFirmProfile(LawFirmProfile p, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "LawFirmProfileId", ParameterValue = p.LawFirmProfileId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                    new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = p.BusinessRegNo },
                    new ParameterInfo() { ParameterName = "Website", ParameterValue = p.Website },
                    new ParameterInfo() { ParameterName = "Remark", ParameterValue = p.Remark },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                    new ParameterInfo() { ParameterName = "Active", ParameterValue = p.Active }
            };
            return SqlHelper.GetRecord<string>("lf.Update_LawFirmProfile", parameters);
        }

        public static string GetLawFirmProfileByLawFirmProfileName(LawFirmProfile p)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name }
            };
            return SqlHelper.GetRecord<string>("dbo.Get_LawFirmProfile_By_LawFirmProfileName", parameters);
        }

        public static string DeleteLawFirmProfile(string LawFirmProfileId)
        {
            var parameters = new List<ParameterInfo>
            {
                    new ParameterInfo() { ParameterName = "LawFirmProfileId", ParameterValue = LawFirmProfileId }
            };
            return SqlHelper.GetRecord<string>("dbo.Delete_LawFirmProfile", parameters);
        }

        public static string CreateLawFirmProfile(LawFirmProfile p, string id)
        {
            try
            {
                var parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                    new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = p.BusinessRegNo },
                    new ParameterInfo() { ParameterName = "Website", ParameterValue = p.Website },
                    new ParameterInfo() { ParameterName = "Remark", ParameterValue = p.Remark },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                    new ParameterInfo() { ParameterName = "Active", ParameterValue = p.Active }
                };
                return SqlHelper.GetRecord<string>("lf.Create_LawFirmProfile", parameters);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<LawFirmProfile> GetAllLawFirmProfile()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<LawFirmProfile>("lf.Get_LawFirmProfile", parameters);
        }

        public static LawFirmProfile GetLawFirmProfileByLawFirmProfileId(string LawFirmProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "LawFirmProfileId", ParameterValue = LawFirmProfileId },
            };
            return SqlHelper.GetRecord<LawFirmProfile>("lf.Get_LawFirmProfileByLawFirmProfileId", parameters);
        }
    }
}
