﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class PersonControl
    {
        public static string UpdatePerson(Person p)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "PersonId", ParameterValue = p.PersonId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = p.ProfileId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Person", parameters);
        }

        public static string CreatePerson(Person p)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = p.ProfileId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Person", parameters);
        }

        public static string UpdatePersonByProfileId(Person p)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = p.ProfileId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Person_By_Profile_Id", parameters);
        }

        public static string UpdatePersonByPersonId(Person person)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "PersonId", ParameterValue = person.PersonId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = person.ProfileId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = person.Name },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = person.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = person.Email }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Person_By_Person_Id", parameters);
        }

        public static Person GetPersonByProfileId(string profileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = profileId },
            };
            return SqlHelper.GetRecord<Person>("dbo.Get_PersonByProfileId", parameters);
        }

        public static List<Person> GetAllPerson()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Person>("dbo.Get_Person", parameters);
        }

        public static Person GetPersonByPersonId(string PersonId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "PersonId", ParameterValue = PersonId },
            };
            return SqlHelper.GetRecord<Person>("dbo.Get_PersonByPersonId", parameters);
        }
    }
}
