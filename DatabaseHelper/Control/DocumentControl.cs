﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class DocumentControl
    {
        public static string UpdateDocument(Document d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = d.DocumentId },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = d.Path },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = d.Type },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = d.FileName },
                new ParameterInfo() { ParameterName = "Size", ParameterValue = d.Size },
                new ParameterInfo() { ParameterName = "CreatedDate", ParameterValue = d.CreatedDate }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Document", parameters);
        }

        public static Document CreateDocument(Document d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Path", ParameterValue = d.Path },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = d.Type },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = d.FileName },
                new ParameterInfo() { ParameterName = "Size", ParameterValue = d.Size },
                new ParameterInfo() { ParameterName = "CreatedDate", ParameterValue = d.CreatedDate }
            };
            return SqlHelper.GetRecord<Document>("dbo.Create_Document", parameters);
        }

        public static List<Document> GetDocumentByRequestId(string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
            };
            return SqlHelper.GetRecords<Document>("dbo.Get_DocumentByRequestId", parameters);
        }

        public static Document CreateAppointmentDocument(Document d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Path", ParameterValue = d.Path },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = d.Type },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = d.FileName },
                new ParameterInfo() { ParameterName = "Size", ParameterValue = d.Size },
                new ParameterInfo() { ParameterName = "CreatedDate", ParameterValue = d.CreatedDate }
            };
            return SqlHelper.GetRecord<Document>("dbo.Create_AppointmentDocument", parameters);
        }

        public static List<Document> GetDocumentsByAppointmentId(string appointmentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "AppointmentId", ParameterValue = appointmentId },
            };
            return SqlHelper.GetRecords<Document>("dbo.Get_DocumentByAppointmentId", parameters);
        }

        public static List<Document> GetDocumentBySubscriptionRequestId(string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
            };
            return SqlHelper.GetRecords<Document>("dbo.Get_DocumentBySubscriptionRequestId", parameters);
        }

        public static Document DeleteAppointmentDocument(string documentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = documentId },
            };
            return SqlHelper.GetRecord<Document>("dbo.Delete_AppointmentDocumentByDocumentId", parameters);
        }

        public static Document DeleteDocument(string documentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = documentId },
            };
            return SqlHelper.GetRecord<Document>("dbo.Delete_DocumentByDocumentId", parameters);
        }

        public static List<Document> GetAllDocument()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Document>("dbo.Get_Document", parameters);
        }

        public static Document GetDocumentByDocumentId(int DocumentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = DocumentId },
            };
            return SqlHelper.GetRecord<Document>("dbo.Get_DocumentByDocumentId", parameters);
        }
        public static bool UpdateDocumentsRequestIdByDocumentId(IList<Document> documents, string requestId)
        {
            try
            {
                foreach (var d in documents)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = d.DocumentId },
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                    };
                    SqlHelper.ExecuteQuery("dbo.Update_DocumentsRequestIdByDocumentId", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
        public static bool UpdateDocumentsAppointmentIdByDocumentId(IList<Document> documents, string appointmentId)
        {
            try
            {
                foreach (var d in documents)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = d.DocumentId },
                        new ParameterInfo() { ParameterName = "AppointmentId", ParameterValue = appointmentId },
                    };
                    SqlHelper.ExecuteQuery("dbo.Update_DocumentsAppointmentIdByDocumentId", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
    }
}
