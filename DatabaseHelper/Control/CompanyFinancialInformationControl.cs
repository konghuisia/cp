﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;

namespace DatabaseHelper.Control
{
    public class CompanyFinancialInformationControl
    {
        public static bool CreateCompanyFinancialInformation(CompanyFinancialInformation c, string id, string ProfileId)
        {
            try
            {
                var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = c.DocumentId },
                        new ParameterInfo() { ParameterName = "LastFinancialStatementSubmitted", ParameterValue = c.LastFinancialStatementSubmitted },
                        new ParameterInfo() { ParameterName = "LastFinancialStatementSubmittedDate", ParameterValue = c.LastFinancialStatementSubmittedDate?? null },
                        new ParameterInfo() { ParameterName = "FinancialYearEnd1", ParameterValue = c.FinancialYearEnd1?? null },
                        new ParameterInfo() { ParameterName = "AnnualTurnover1", ParameterValue = c.AnnualTurnover1 },
                        new ParameterInfo() { ParameterName = "NtaPositiveNegative1", ParameterValue = c.NtaPositiveNegative1 },
                        new ParameterInfo() { ParameterName = "ProfitAfterTax1", ParameterValue = c.ProfitAfterTax1 },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
                    };
                SqlHelper.ExecuteQuery("dbo.Create_CompanyFinancialInformation", parameters);
                return true;
            }
            catch(Exception e)
            {
                e.ToString();
                return false;
            }
        }
        public static bool UpdateCompanyFinancialInformation(IList<CompanyFinancialInformation> cfi, string id, string ProfileId)
        {
            try
            {
                foreach (var c in cfi)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "CompanyFinancialInformationId", ParameterValue = c.CompanyFinancialInformationId },
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = c.DocumentId },
                        new ParameterInfo() { ParameterName = "LastFinancialStatementSubmitted", ParameterValue = c.LastFinancialStatementSubmitted },
                        new ParameterInfo() { ParameterName = "LastFinancialStatementSubmittedDate", ParameterValue = c.LastFinancialStatementSubmittedDate?? null },
                        new ParameterInfo() { ParameterName = "FinancialYearEnd1", ParameterValue = c.FinancialYearEnd1?? null },
                        new ParameterInfo() { ParameterName = "AnnualTurnover1", ParameterValue = c.AnnualTurnover1 },
                        new ParameterInfo() { ParameterName = "NtaPositiveNegative1", ParameterValue = c.NtaPositiveNegative1 },
                        new ParameterInfo() { ParameterName = "ProfitAfterTax1", ParameterValue = c.ProfitAfterTax1 },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
                    };
                    SqlHelper.ExecuteQuery("dbo.Update_CompanyFinancialInformation", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
        public static CompanyFinancialInformation GetCompanyFinancialInformationByCompanyFinancialInformationId(string CompanyFinancialInformationId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "CompanyFinancialInformationId", ParameterValue = CompanyFinancialInformationId },
            };
            return SqlHelper.GetRecord<CompanyFinancialInformation>("dbo.Get_CompanyFinancialInformationByCompanyFinancialInformationId", parameters);
        }

        public static List<CompanyFinancialInformation> GetAllCompanyFinancialInformation()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<CompanyFinancialInformation>("dbo.Get_CompanyFinancialInformation", parameters);
        }
        public static List<CompanyFinancialInformation> GetCompanyFinancialInformationByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecords<CompanyFinancialInformation>("dbo.Get_CompanyFinancialInformationByProfileId", parameters);
        }
    }
}
