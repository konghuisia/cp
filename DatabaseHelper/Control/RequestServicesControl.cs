﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class RequestServicesControl
    {
        public static bool UpdateRequestServices(List<RequestServices> rs, string UserId, string RequestId, string ProfileId, string SubscriptionId)
        {
            try
            {
                foreach (var s in rs)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "SubscriptionId", ParameterValue = SubscriptionId },
                        new ParameterInfo() { ParameterName = "RequestServicesId", ParameterValue = s.RequestServicesId },
                        new ParameterInfo() { ParameterName = "SopId", ParameterValue = s.SopId },
                        new ParameterInfo() { ParameterName = "ServiceTypeId", ParameterValue = s.ServiceTypeId },
                        new ParameterInfo() { ParameterName = "Active", ParameterValue = s.Active },
                        new ParameterInfo() { ParameterName = "Type", ParameterValue = s.Type },
                        new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = UserId },
                    };
                    SqlHelper.ExecuteQuery("dbo.Update_RequestServices", parameters);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool CreateRequestServices(List<RequestServices> rs, string UserId, string RequestId, string ProfileId, string SubscriptionId)
        {
            try
            {
                foreach (var s in rs)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
                        new ParameterInfo() { ParameterName = "SubscriptionId", ParameterValue = SubscriptionId },
                        new ParameterInfo() { ParameterName = "RequestServicesId", ParameterValue = s.RequestServicesId },
                        new ParameterInfo() { ParameterName = "SopId", ParameterValue = s.SopId },
                        new ParameterInfo() { ParameterName = "ServiceTypeId", ParameterValue = s.ServiceTypeId },
                        new ParameterInfo() { ParameterName = "Active", ParameterValue = s.Active },
                        new ParameterInfo() { ParameterName = "Type", ParameterValue = s.Type },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = UserId },
                    };
                    SqlHelper.ExecuteQuery("dbo.Create_RequestServices", parameters);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static List<RequestServices> GetRequestServicesByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecords<RequestServices>("dbo.Get_RequestServicesByRequestId", parameters);
        }

        public static List<RequestServices> GetAllRequestServices()
        {
            return SqlHelper.GetRecords<RequestServices>("dbo.Get_RequestServices", new List<ParameterInfo>());
        }
    }
}
