﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class CommentRemarkControl
    {
        public static string UpdateCommentRemark(CommentRemark c)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "CommentRemarkId", ParameterValue = c.CommentRemarkId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = c.ProfileId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = c.Comment },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = c.Type},
                new ParameterInfo() { ParameterName = "Stage", ParameterValue = c.Stage}
            };
            return SqlHelper.GetRecord<string>("dbo.Update_CommentRemark", parameters);
        }

        public static string CreateCommentRemark(CommentRemark c, string id)
        {
            string caseSwitch = c.Comment;
            switch (caseSwitch) {
                case null:
                case "":
                    return null;
                default:
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = c.RequestId },
                        new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = c.ProfileId },
                        new ParameterInfo() { ParameterName = "Comment", ParameterValue = c.Comment },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id},
                        new ParameterInfo() { ParameterName = "Type", ParameterValue = c.Type},
                        new ParameterInfo() { ParameterName = "Stage", ParameterValue = c.Stage}
                    };
                    return SqlHelper.GetRecord<string>("dbo.Create_CommentRemark", parameters);
            }
        }

        public static List<CommentRemark> GetCommentRemarkByRequestId(string requestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
            };
            return SqlHelper.GetRecords<CommentRemark>("dbo.Get_CommentRemarkByRequestId", parameters);
        }

        public static List<CommentRemark> GetAllCommentRemark()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<CommentRemark>("dbo.Get_CommentRemark", parameters);
        }

        public static List<CommentRemark> GetCommentRemarkByProfileId(string profileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = profileId },
            };
            return SqlHelper.GetRecords<CommentRemark>("dbo.Get_CommentRemarkByProfileId", parameters);
        }

        internal static string UpdateCommentRemarkByProfileId(CommentRemark cr)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "CommentRemarkId", ParameterValue = cr.CommentRemarkId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = cr.ProfileId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = cr.Comment }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_CommentRemark_By_Profile_Id", parameters);
        }

        public static List<CommentRemark> GetCommentRemarkByCommentRemarkId(string CommentRemarkId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "CommentRemarkId", ParameterValue = CommentRemarkId },
            };
            return SqlHelper.GetRecords<CommentRemark>("dbo.Get_CommentRemarkByCommentRemarkId", parameters);
        }
    }
}
