﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class TransactionControl
    {
        public static string UpdateTransaction(Transactions t, string userId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "SubscriptionId", ParameterValue = t.SubscriptionId},
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = t.RequestId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = t.ProfileId },
                new ParameterInfo() { ParameterName = "TransactionId", ParameterValue = t.TransactionId},
                new ParameterInfo() { ParameterName = "TransactionType", ParameterValue = t.TransactionType },
                new ParameterInfo() { ParameterName = "TransactionDescription", ParameterValue = t.TransactionDescription },
                new ParameterInfo() { ParameterName = "SopId", ParameterValue = t.SopId },
                new ParameterInfo() { ParameterName = "ServiceTypeId", ParameterValue = t.ServiceTypeId },
                new ParameterInfo() { ParameterName = "Amount", ParameterValue = t.Amount },
                new ParameterInfo() { ParameterName = "TotalAmount", ParameterValue = t.TotalAmount },
                new ParameterInfo() { ParameterName = "WalletBalance", ParameterValue = t.WalletBalance },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = userId }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Transaction", parameters);
        }

        public static string CreateTransaction(Transactions t, string userId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "SubscriptionId", ParameterValue = t.SubscriptionId},
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = t.RequestId },
                new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = t.ProfileId },
                new ParameterInfo() { ParameterName = "TransactionId", ParameterValue = t.TransactionId},
                new ParameterInfo() { ParameterName = "TransactionType", ParameterValue = t.TransactionType },
                new ParameterInfo() { ParameterName = "TransactionDescription", ParameterValue = t.TransactionDescription },
                new ParameterInfo() { ParameterName = "SopId", ParameterValue = t.SopId },
                new ParameterInfo() { ParameterName = "ServiceTypeId", ParameterValue = t.ServiceTypeId },
                new ParameterInfo() { ParameterName = "Amount", ParameterValue = t.Amount },
                new ParameterInfo() { ParameterName = "TotalAmount", ParameterValue = t.TotalAmount },
                new ParameterInfo() { ParameterName = "WalletBalance", ParameterValue = t.WalletBalance },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Transaction", parameters);
        }

        public static bool DeleteTransaction(string TransactionId, string Userid)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "TransactionId", ParameterValue = TransactionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = Userid }
            };
            return SqlHelper.GetRecord<bool>("dbo.Delete_TransactionByTransactionId", parameters);
        }

        public static List<Transactions> GetAllTransaction()
        {
            return SqlHelper.GetRecords<Transactions>("dbo.Get_Transaction", new List<ParameterInfo>());
        }

        public static Transactions GetTransactionByTransactionId(string TransactionId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "TransactionId", ParameterValue = TransactionId },
            };
            return SqlHelper.GetRecord<Transactions>("dbo.Get_TransactionByTransactionId", parameters);
        }

        public static List<Transactions> GetTransactionByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecords<Transactions>("dbo.Get_TransactionByRequestId", parameters);
        }

        public static List<Transactions> GetTransactionBySubscriptionId(string SubscriptionId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "SubscriptionId", ParameterValue = SubscriptionId },
            };
            return SqlHelper.GetRecords<Transactions>("dbo.Get_TransactionBySubscriptionId", parameters);
        }

        public static List<Transactions> GetTransactionByProfileId(string ProfileId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "ProfileId", ParameterValue = ProfileId },
            };
            return SqlHelper.GetRecords<Transactions>("dbo.Get_TransactionByProfileId", parameters);
        }
    }
}
