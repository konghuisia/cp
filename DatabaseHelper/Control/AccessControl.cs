﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class AccessControl
    {
        public static IList<Access> AccessList()
        {
            return SqlHelper.GetRecords<Access>("cfg.Get_Access", new List<ParameterInfo>());
        }

        public static bool UpdateGroupAccess(List<UserAccess> access, string id, string UserPositionId)
        {
            try
            {
                foreach (var a in access)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "UserPositionId", ParameterValue = UserPositionId },
                        new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = a.AccessId },
                        new ParameterInfo(){ ParameterName = "IsCreate", ParameterValue = a.IsCreate },
                        new ParameterInfo(){ ParameterName = "IsRead", ParameterValue = a.IsRead },
                        new ParameterInfo(){ ParameterName = "IsUpdate", ParameterValue = a.IsUpdate },
                        new ParameterInfo(){ ParameterName = "IsDelete", ParameterValue = a.IsDelete },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id }
                    };

                    SqlHelper.ExecuteQuery("cfg.Update_UserAccess", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool CreateGroupAccess(List<UserAccess> access, string id, string userPositionId)
        {
            try
            {
                foreach (var a in access)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "UserPositionId", ParameterValue = userPositionId },
                        new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = a.AccessId },
                        new ParameterInfo(){ ParameterName = "IsCreate", ParameterValue = a.IsCreate },
                        new ParameterInfo(){ ParameterName = "IsRead", ParameterValue = a.IsRead },
                        new ParameterInfo(){ ParameterName = "IsUpdate", ParameterValue = a.IsUpdate },
                        new ParameterInfo(){ ParameterName = "IsDelete", ParameterValue = a.IsDelete },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id }
                    };

                    SqlHelper.ExecuteQuery("cfg.Create_UserAccess", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
