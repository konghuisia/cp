﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class DraftControl
    {
        public static string UpdateDraft(Draft d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "DraftId", ParameterValue = d.DraftId },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "Active", ParameterValue = d.Active },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = d.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("dbo.Update_Draft", parameters);
        }

        public static string CreateDraft(Draft d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "DraftId", ParameterValue = d.DraftId },
                new ParameterInfo() { ParameterName = "Active", ParameterValue = d.Active },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = d.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("dbo.Create_Draft", parameters);
        }

        public static Draft DeleteDraft(string DraftId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DraftId", ParameterValue = DraftId },
            };
            return SqlHelper.GetRecord<Draft>("dbo.Delete_DraftByDraftId", parameters);
        }

        public static List<Draft> GetAllDraft()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<Draft>("dbo.Get_Draft", parameters);
        }

        public static Draft GetDraftByDraftId(string DraftId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DraftId", ParameterValue = DraftId },
            };
            return SqlHelper.GetRecord<Draft>("dbo.Get_DraftByDraftId", parameters);
        }
        public static Draft GetDraftByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            return SqlHelper.GetRecord<Draft>("dbo.Get_DraftByRequestId", parameters);
        }
        public static List<Draft> GetDraftByCreatedById(string CreatedBy)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = CreatedBy },
            };
            return SqlHelper.GetRecords<Draft>("dbo.Get_DraftByCreatedById", parameters);
        }
    }
}
