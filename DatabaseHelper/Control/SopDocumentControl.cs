﻿using DataAccessHelper.SqlConnectionHelper;
using ObjectClassesLibrary.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHelper.Control
{
    public class SopDocumentControl
    {
        public static string UpdateSopDocument(SopDocuments d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "SopDocumentId", ParameterValue = d.DocumentId },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = d.Path },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "SopId", ParameterValue = d.SopId },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = d.Type },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = d.FileName },
                new ParameterInfo() { ParameterName = "Size", ParameterValue = d.Size },
                new ParameterInfo() { ParameterName = "CreatedDate", ParameterValue = d.CreatedDate }
            };
            return SqlHelper.GetRecord<string>("sop.Update_SopDocument", parameters);
        }

        public static SopDocuments CreateSopDocument(SopDocuments d)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = d.RequestId },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = d.Path },
                new ParameterInfo() { ParameterName = "SopId", ParameterValue = d.SopId },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = d.Type },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = d.FileName },
                new ParameterInfo() { ParameterName = "Size", ParameterValue = d.Size },
                new ParameterInfo() { ParameterName = "CreatedDate", ParameterValue = d.CreatedDate }
            };
            return SqlHelper.GetRecord<SopDocuments>("sop.Create_SopDocument", parameters);
        }

        public static List<SopDocuments> GetSopDocumentBySopId(string SopId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "SopId", ParameterValue = SopId }
            };
            return SqlHelper.GetRecords<SopDocuments>("sop.Get_SopDocumentBySopId", parameters);
        }

        public static SopDocuments DeleteSopDocument(string documentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "DocumentId", ParameterValue = documentId },
            };
            return SqlHelper.GetRecord<SopDocuments>("sop.Delete_SopDocumentByDocumentId", parameters);
        }

        public static List<SopDocuments> GetAllSopDocument()
        {
            var parameters = new List<ParameterInfo>();
            return SqlHelper.GetRecords<SopDocuments>("sop.Get_SopDocument", parameters);
        }

        public static SopDocuments GetSopDocumentBySopDocumentId(int SopDocumentId)
        {
            var parameters = new List<ParameterInfo>()
            {
                    new ParameterInfo() { ParameterName = "SopDocumentId", ParameterValue = SopDocumentId },
            };
            return SqlHelper.GetRecord<SopDocuments>("sop.Get_SopDocumentBySopDocumentId", parameters);
        }
        //public static bool UpdateSopDocumentsRequestIdBySopDocumentId(IList<SopDocument> documents, string requestId)
        //{
        //    try
        //    {
        //        foreach (var d in documents)
        //        {
        //            var parameters = new List<ParameterInfo>
        //            {
        //                new ParameterInfo() { ParameterName = "SopDocumentId", ParameterValue = d.SopDocumentId },
        //                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
        //            };
        //            SqlHelper.ExecuteQuery("sop.Update_SopDocumentsRequestIdBySopDocumentId", parameters);
        //        }
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        e.ToString();
        //        return false;
        //    }
        //}
    }
}
