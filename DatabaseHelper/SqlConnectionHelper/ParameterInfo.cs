﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SqlConnectionHelper
{
    public class ParameterInfo
    {
        public string ParameterName { get; set; }
        public object ParameterValue { get; set; }

    }
}
