﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityManagement.Entities
{
    class UserPosition
    {
        public string Id { get; set; }
        public string PositionId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
    }
}
