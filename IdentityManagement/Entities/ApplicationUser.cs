﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IdentityManagement.Entities
{
    public class ApplicationUser : UserInfo
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim(CustomClaimTypes.DisplayName, this.UserName));
            userIdentity.AddClaim(new Claim(CustomClaimTypes.Position, this.Position));
            userIdentity.AddClaim(new Claim(CustomClaimTypes.UserId, this.UserId));
            userIdentity.AddClaim(new Claim(CustomClaimTypes.Active, this.Active));
            if (!string.IsNullOrEmpty(this.ProfileId))
            {
                userIdentity.AddClaim(new Claim(CustomClaimTypes.ProfileId, this.ProfileId));
            }
            if (!string.IsNullOrEmpty(this.LastLoginDate))
            {
                userIdentity.AddClaim(new Claim(CustomClaimTypes.LastLoginDate, this.LastLoginDate));
            }
            if (!string.IsNullOrEmpty(this.Reporting))
            {
                userIdentity.AddClaim(new Claim(CustomClaimTypes.Reporting, this.Reporting));
            }
            return userIdentity;
        }
    }
}
