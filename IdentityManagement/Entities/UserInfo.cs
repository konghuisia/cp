﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityManagement.Entities
{
    public class UserInfo : IUser<string>
    {
        public UserInfo()
        {
            Groups = new List<UserGroupInfo>();
            Accesses = new List<UserAccessInfo>();
        }

        public string Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Position { get; set; }
        public string Grad { get; set; }
        public string Role { get; set; }
        public string Reporting { get; set; }
        public string LastLoginDate { get; set; }
        public string Active { get; set; }
        public string ProfileId { get; set; }
        public string SecurityStamp { get; set; }
        public List<UserGroupInfo> Groups { get; set; }
        public List<UserAccessInfo> Accesses { get; set; }
    }
}
