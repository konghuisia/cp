﻿using DataAccessHelper.SqlConnectionHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityManagement.DAL
{
    public static class UserPositionControl
    {
        public static IList<string> GetUserPosition(string userID)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            parameters.Add(new ParameterInfo() { ParameterName = "UserID", ParameterValue = userID });
            IList<string> positions = SqlHelper.GetRecords<string>("GetUserPosition", parameters);
            return positions;
        }
    }
}
