﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityManagement
{
    public class CustomClaimTypes
    {
        public const string DisplayName = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/displayName";
        public const string Position = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/position";
        //public const string Grad = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/grad";
        public const string UserId = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/userid";
        public const string Reporting = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/reporting";
        //public const string MasterUserId = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/masteruserid";
        public const string LastLoginDate = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/lastlogindate";
        public const string Active = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/actice";
        public const string ProfileId = "http://schemas.xmlsoap.org/ws/2014/03/mystuff/claims/profileid";
    }
}
